/*
 * MIT License
 *
 * Copyright (c) 2022 Steffen Schulze
 */

#include "cunit.h"

#include "cutil_nullptr.h"
#include "test_container.h"

// ----------------------------------------------------------------------------
static int compareItems(const void* l, const void* r)
{
    const int* li = (const int*)l;
    const int* ri = (const int*)r;
    return *li - *ri;
}

// ----------------------------------------------------------------------------
static int itemPredicate(const void* context, const void* item)
{
    const int* cint = (const int*)context;
    const int* iint = (const int*)item;
    return (int)(*cint - *iint);
}

// ----------------------------------------------------------------------------
struct test_data_s {
    unsigned idx;
    unsigned count;
    int values[7];
};

typedef struct test_data_s test_data_t;

// ----------------------------------------------------------------------------
static int containerVerifyItem(const void* item, void* context)
{
    test_data_t* expected = (test_data_t*)context;
    EXPECT_EQ(*(int*)item, expected->values[expected->idx]);
    expected->idx++;
    return 0;
}

// ----------------------------------------------------------------------------
void test_containerInsert(void* context)
{
    const container_test_fixture_t* test = (const container_test_fixture_t*)context;
    void* obj = test->fnCreate();
    ASSERT_TRUE(obj != nullptr);

    EXPECT_TRUE(test->fnIsEmpty(obj));

    const int valsInsert[7] = { 3, 4, 1, 2, 8, 5, 7 };
    test_data_t expectedValsInsert[7] = {
        { 0, 1, { 3 } },
        { 0, 2, { 3, 4 } },
        { 0, 3, { 1, 3, 4 } },
        { 0, 4, { 1, 2, 3, 4 } },
        { 0, 5, { 1, 2, 3, 4, 8 } },
        { 0, 6, { 1, 2, 3, 4, 5, 8 } },
        { 0, 7, { 1, 2, 3, 4, 5, 7, 8 } },
    };

    for (int i = 0; i < sizeof(valsInsert) / sizeof(int); ++i) {
        test->fnInsert(obj, compareItems, &valsInsert[i]);
        test->fnIterate(obj, containerVerifyItem, &expectedValsInsert[i]);
        EXPECT_EQ(test->fnGetLength(obj), expectedValsInsert[i].count);
    }

    EXPECT_FALSE(test->fnIsEmpty(obj));

    const int k0 = 5;
    const int* v0 = (const int*)test->fnFind(obj, itemPredicate, &k0);
    EXPECT_EQ(*v0, k0);

    const int valsRemove[7] = { 1, 8, 4, 2, 3, 5, 7 };
    test_data_t expectedValsRemove[7] = {
        { 0, 6, { 2, 3, 4, 5, 7, 8 } },
        { 0, 5, { 2, 3, 4, 5, 7 } },
        { 0, 4, { 2, 3, 5, 7 } },
        { 0, 3, { 3, 5, 7 } },
        { 0, 2, { 5, 7 } },
        { 0, 1, { 7 } },
    };

    for (int i = 0; i < sizeof(valsRemove) / sizeof(int); ++i) {
        const int* v = (const int*)test->fnRemove(obj, compareItems, &valsRemove[i]);
        EXPECT_EQ(*v, valsRemove[i]);

        test->fnIterate(obj, containerVerifyItem, &expectedValsRemove[i]);
        EXPECT_EQ(test->fnGetLength(obj), expectedValsRemove[i].count);
    }

    EXPECT_TRUE(test->fnIsEmpty(obj));
    test->fnDestroy(obj, nullptr);
}

// ----------------------------------------------------------------------------
void test_containerRemove(void* context)
{
    const container_test_fixture_t* test = (const container_test_fixture_t*)context;
    void* obj = test->fnCreate();
    ASSERT_TRUE(obj != nullptr);

    EXPECT_TRUE(test->fnIsEmpty(obj));

    const int valsInsert0[] = { 1, 2, 3, 4, 5, 6 };
    for (int i = 0; i < sizeof(valsInsert0) / sizeof(int); ++i) {
        test->fnInsert(obj, compareItems, &valsInsert0[i]);
    }

    EXPECT_FALSE(test->fnIsEmpty(obj));

    const int k = 2;
    test->fnRemove(obj, itemPredicate, &k);

    const int valsInsert1[] = { 10, 11, 12 };
    for (int i = 0; i < sizeof(valsInsert1) / sizeof(int); ++i) {
        test->fnInsert(obj, compareItems, &valsInsert1[i]);
    }

    const int valsRemove[] = { 1, 3, 4, 6, 10, 11, 12, 5 };

    for (int i = 0; i < sizeof(valsRemove) / sizeof(int); ++i) {
        const int* v = (const int*)test->fnRemove(obj, itemPredicate, &valsRemove[i]);
        EXPECT_EQ(*v, valsRemove[i]);
    }

    EXPECT_TRUE(test->fnIsEmpty(obj));
    test->fnDestroy(obj, nullptr);
}
