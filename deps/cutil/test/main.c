/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */

#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>

#include "cunit.h"
#include "cunit_fixture.h"
#include "cunit_testrun.h"

// ----------------------------------------------------------------------------
void test_containerInsert(void* context);
void test_containerRemove(void* context);

void* startupFixtureTree();
void teardownFixtureTree(void* context);

// ----------------------------------------------------------------------------
void* startupFixtureList();
void teardownFixtureList(void* context);

// ----------------------------------------------------------------------------
void* startupFixtureVecPtr();
void teardownFixtureVecPtr(void* context);
void test_vecptrAddItem(void* context);

// ----------------------------------------------------------------------------
void* startupFixtureBytes();
void teardownFixtureBytes(void* context);
void test_byteAddData(void* context);

// ----------------------------------------------------------------------------
void* startupFixtureByteWrite();
void teardownFixtureByteWrite(void* context);
void test_bytewriteInt(void* context);
void test_bytewriteVint(void* context);
void test_bytewriteBuffer(void* context);

// ----------------------------------------------------------------------------
void* startupFixtureByteRead();
void teardownFixtureByteRead(void* context);
void test_bytereadInt(void* context);
void test_bytereadVint(void* context);
void test_bytereadBuffer(void* context);

// ----------------------------------------------------------------------------
const cutilList* addTests()
{
    const cunitFixture* fixture = nullptr;
    cutilList* fixtures = createList(destroyFixtureItem);

    fixture = cunitAddFixture(STR("tree"), startupFixtureTree, teardownFixtureTree);
    cunitAddTest(fixture, STR("tree container"), test_containerInsert);
    cunitAddTest(fixture, STR("tree test"), test_containerRemove);
    listAddItem(fixtures, fixture);

    fixture = cunitAddFixture(STR("list"), startupFixtureList, teardownFixtureList);
    cunitAddTest(fixture, STR("list container"), test_containerInsert);
    cunitAddTest(fixture, STR("list test"), test_containerRemove);
    listAddItem(fixtures, fixture);

    fixture = cunitAddFixture(STR("vector"), startupFixtureVecPtr, teardownFixtureVecPtr);
    cunitAddTest(fixture, STR("addVectorItem"), test_vecptrAddItem);
    listAddItem(fixtures, fixture);

    fixture = cunitAddFixture(STR("bytes"), startupFixtureBytes, teardownFixtureBytes);
    cunitAddTest(fixture, STR("addData"), test_byteAddData);
    listAddItem(fixtures, fixture);

    fixture = cunitAddFixture(STR("bytewrite"), startupFixtureByteWrite, teardownFixtureByteWrite);
    cunitAddTest(fixture, STR("int"), test_bytewriteInt);
    cunitAddTest(fixture, STR("vint"), test_bytewriteVint);
    cunitAddTest(fixture, STR("buffer"), test_bytewriteBuffer);
    listAddItem(fixtures, fixture);

    fixture = cunitAddFixture(STR("byteread"), startupFixtureByteRead, teardownFixtureByteRead);
    cunitAddTest(fixture, STR("int"), test_bytereadInt);
    cunitAddTest(fixture, STR("vint"), test_bytereadVint);
    cunitAddTest(fixture, STR("buffer"), test_bytereadBuffer);
    listAddItem(fixtures, fixture);

    return fixtures;
}

// ----------------------------------------------------------------------------
int main(int argc, char* argv[])
{
    const cutilList* fixtures = addTests();

    int res = cunitRunTests(cunitPrintFailuresOnly, fixtures);
    printf("\nTests completed with return value %d.\n", res);

    destroyList(fixtures);
    return res;
}
