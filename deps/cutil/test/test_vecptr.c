/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */

#include "cunit.h"

#include "cutil_vecptr.h"
#include "cutil_nullptr.h"

// ----------------------------------------------------------------------------
struct testFixture_s {
    vecptr_t* vec;
};

typedef struct testFixture_s testFixture;

// ----------------------------------------------------------------------------
void* startupFixtureVecPtr()
{
    testFixture* context = malloc(sizeof(testFixture));
    ASSERT_TRUE(context != nullptr);

    context->vec = vecptrCreate(2, 4, nullptr);
    ASSERT_TRUE(context->vec != nullptr);

    return context;
}

// ----------------------------------------------------------------------------
void teardownFixtureVecPtr(void* context)
{
    testFixture* test = (testFixture*)context;
    vecptrDestroy(test->vec);
    free((void*)test);
}

// ----------------------------------------------------------------------------
struct VectorTest {
    unsigned idx;
    unsigned count;
    int values[7];
};

// ----------------------------------------------------------------------------
static int vecptrVerifyItem(const void* item, void* context)
{
    struct VectorTest* expected = (struct VectorTest*)context;
    EXPECT_EQ(*(int*)item, expected->values[expected->idx]);
    expected->idx++;
    return 0;
}

// ----------------------------------------------------------------------------
void test_vecptrAddItem(void* context)
{
    vecptr_t* vec = ((testFixture*)context)->vec;

    ASSERT_TRUE(vecptrIsEmpty(vec));

    int values[7] = { 3, 4, 1, 2, 8, 5, 7 };
    struct VectorTest expectedValues[7] = {
        { 0, 1, { 3 } },
        { 0, 2, { 3, 4 } },
        { 0, 3, { 3, 4, 1 } },
        { 0, 4, { 3, 4, 1, 2 } },
        { 0, 5, { 3, 4, 1, 2, 8 } },
        { 0, 6, { 3, 4, 1, 2, 8, 5 } },
        { 0, 7, { 3, 4, 1, 2, 8, 5, 7 } },
    };

    for (int i = 0; i < sizeof(values) / sizeof(int); ++i) {
        vecptrAddItem(vec, &values[i]);
        vecptrIterateItems(vec, vecptrVerifyItem, &expectedValues[i]);
    }

    ASSERT_FALSE(vecptrIsEmpty(vec));
}
