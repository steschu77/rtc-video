/*
 * MIT License
 *
 * Copyright (c) 2020 Steffen Schulze
 */

#include "cunit.h"

#include "cutil_bytewrite.h"
#include "cutil_nullptr.h"

// ----------------------------------------------------------------------------
static const uint8_t expected_stream0[16] = {
    0x09, 0x01, 0x42, 0x03, 0x84, 0x05, 0x06, 0x07,
    0xc8, 0x09, 0x05, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f,
};

#define DATA_SIZE (sizeof(expected_stream0))

// ----------------------------------------------------------------------------
struct testByteWrite_s {
    bytewrite_t* bytes;
    void* data;
};

typedef struct testByteWrite_s testByteWrite;

// ----------------------------------------------------------------------------
void* startupFixtureByteWrite()
{
    testByteWrite* context = malloc(sizeof(testByteWrite));
    ASSERT_TRUE(context != nullptr);

    context->data = malloc(DATA_SIZE);
    ASSERT_TRUE(context->data != nullptr);

    context->bytes = bytewriteCreate(context->data, DATA_SIZE);
    ASSERT_TRUE(context->bytes != nullptr);

    return context;
}

// ----------------------------------------------------------------------------
void teardownFixtureByteWrite(void* ptr)
{
    testByteWrite* context = (testByteWrite*)ptr;
    bytewriteDestroy(context->bytes);
    free(context->data);
    free((void*)context);
}

// ----------------------------------------------------------------------------
static void eval_bytewrite(bytewrite_t* s)
{
    size_t expected_size = sizeof(expected_stream0);
    size_t stream_len = bytewrite_length(s);
    ASSERT_EQ(stream_len, expected_size);
    EXPECT_FALSE(memcmp(bytewrite_data(s), expected_stream0, expected_size));
}

// ----------------------------------------------------------------------------
void test_bytewriteInt(void* context)
{
    testByteWrite* test = (testByteWrite*)context;
    bytewrite_t* s = test->bytes;
    EXPECT_FALSE(bytewrite_int8(s, 0x09));
    EXPECT_FALSE(bytewrite_int8(s, 0x01));
    EXPECT_FALSE(bytewrite_int16(s, 0x4203));
    EXPECT_FALSE(bytewrite_int32(s, 0x84050607));
    EXPECT_FALSE(bytewrite_int64(s, 0xc809050b0c0d0e0f));

    eval_bytewrite(test->bytes);
}

// ----------------------------------------------------------------------------
void test_bytewriteVint(void* context)
{
    testByteWrite* test = (testByteWrite*)context;
    bytewrite_t* s = test->bytes;
    EXPECT_FALSE(bytewrite_vint(s, 0x09));
    EXPECT_FALSE(bytewrite_vint(s, 0x01));
    EXPECT_FALSE(bytewrite_vint(s, 0x0203));
    EXPECT_FALSE(bytewrite_vint(s, 0x04050607));
    EXPECT_FALSE(bytewrite_vint(s, 0x0809050b0c0d0e0f));

    eval_bytewrite(s);
}

// ----------------------------------------------------------------------------
void test_bytewriteBuffer(void* context)
{
    testByteWrite* test = (testByteWrite*)context;
    bytewrite_t* s = test->bytes;
    EXPECT_FALSE(bytewrite_buffer(s, expected_stream0, 2));
    EXPECT_FALSE(bytewrite_buffer(s, expected_stream0 + 2, 14));

    eval_bytewrite(s);
}
