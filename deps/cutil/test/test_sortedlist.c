/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */

#include "cunit.h"

#include "cutil_sortedlist.h"
#include "cutil_nullptr.h"

#include "test_container.h"

// ----------------------------------------------------------------------------
void* startupFixtureList()
{
    container_test_fixture_t* context = malloc(sizeof(container_test_fixture_t));
    ASSERT_TRUE(context != nullptr);

    if (context != nullptr) {
        context->fnCreate = slistCreate;
        context->fnDestroy = slistDestroy;
        context->fnInsert = slistInsertItem;
        context->fnRemove = slistRemoveItem;
        context->fnFind = slistFindItem;
        context->fnIterate = slistIterate;
        context->fnIsEmpty = slistIsEmpty;
        context->fnGetLength = slistGetLength;
    }

    return context;
}

// ----------------------------------------------------------------------------
void teardownFixtureList(void* context)
{
    container_test_fixture_t* test = (container_test_fixture_t*)context;
    free((void*)test);
}
