/*
 * MIT License
 *
 * Copyright (c) 2020 Steffen Schulze
 */

#include "cunit.h"

#include "cutil_vecbyte.h"
#include "cutil_nullptr.h"

// ----------------------------------------------------------------------------
static const uint8_t expected_stream0[16] = {
    0x09, 0x01, 0x42, 0x03, 0x84, 0x05, 0x06, 0x07,
    0xc8, 0x09, 0x05, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f,
};

// ----------------------------------------------------------------------------
struct testFixture_s {
    bytes_t* bytes;
};

typedef struct testFixture_s testFixture;

// ----------------------------------------------------------------------------
void* startupFixtureBytes()
{
    testFixture* context = malloc(sizeof(testFixture));
    ASSERT_TRUE(context != nullptr);

    context->bytes = byteCreate(2, 4);
    ASSERT_TRUE(context->bytes != nullptr);

    return context;
}

// ----------------------------------------------------------------------------
void teardownFixtureBytes(void* context)
{
    testFixture* test = (testFixture*)context;
    byteDestroy(test->bytes);
    free((void*)test);
}

// ----------------------------------------------------------------------------
void test_byteAddData(void* context)
{
    bytes_t* obj = ((testFixture*)context)->bytes;

    ASSERT_TRUE(byteIsEmpty(obj));

    byteAddData(obj, expected_stream0,     3);
    byteAddData(obj, expected_stream0 + 3, 3);
    byteAddData(obj, expected_stream0 + 6, 10);

    ASSERT_FALSE(byteIsEmpty(obj));
    ASSERT_EQ(byteGetSize(obj), 16);

    const void* ptr = byteGetData(obj);
    EXPECT_EQ(memcmp(ptr, expected_stream0, 16), 0);
}
