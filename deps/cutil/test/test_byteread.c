/*
 * MIT License
 *
 * Copyright (c) 2020 Steffen Schulze
 */

#include "cunit.h"

#include "cutil_byteread.h"
#include "cutil_nullptr.h"

// ----------------------------------------------------------------------------
static const uint8_t expected_stream0[16] = {
    0x09, 0x01, 0x42, 0x03, 0x84, 0x05, 0x06, 0x07,
    0xc8, 0x09, 0x05, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f,
};

#define DATA_SIZE (sizeof(expected_stream0))

// ----------------------------------------------------------------------------
struct testByteRead_s {
    byteread_t* bytes;
};

typedef struct testByteRead_s testByteRead;

// ----------------------------------------------------------------------------
void* startupFixtureByteRead()
{
    testByteRead* test = malloc(sizeof(testByteRead));
    ASSERT_TRUE(test != nullptr);

    test->bytes = bytereadCreate(expected_stream0, DATA_SIZE);
    ASSERT_TRUE(test->bytes != nullptr);

    return test;
}

// ----------------------------------------------------------------------------
void teardownFixtureByteRead(void* context)
{
    testByteRead* test = (testByteRead*)context;
    bytereadDestroy(test->bytes);
    free((void*)test);
}

// ----------------------------------------------------------------------------
void test_bytereadInt(void* context)
{
    testByteRead* test = (testByteRead*)context;
    byteread_t* s = test->bytes;

    uint8_t val8 = 0;
    EXPECT_FALSE(byteread_int8(s, &val8) || val8 != 0x09);
    EXPECT_FALSE(byteread_int8(s, &val8) || val8 != 0x01);

    uint16_t val16 = 0;
    EXPECT_FALSE(byteread_int16(s, &val16) || val16 != 0x4203);

    uint32_t val32 = 0;
    EXPECT_FALSE(byteread_int32(s, &val32) || val32 != 0x84050607);

    uint64_t val64 = 0;
    EXPECT_FALSE(byteread_int64(s, &val64) || val64 != 0xc809050b0c0d0e0f);

    EXPECT_EQ(byteread_remain(s), 0);
}

// ----------------------------------------------------------------------------
void test_bytereadVint(void* context)
{
    testByteRead* test = (testByteRead*)context;
    byteread_t* s = test->bytes;

    uint64_t value64 = 0;
    EXPECT_FALSE(byteread_vint(s, &value64) != 0 || value64 != 0x09);
    EXPECT_FALSE(byteread_vint(s, &value64) != 0 || value64 != 0x01);
    EXPECT_FALSE(byteread_vint(s, &value64) != 0 || value64 != 0x0203);
    EXPECT_FALSE(byteread_vint(s, &value64) != 0 || value64 != 0x04050607);
    EXPECT_FALSE(byteread_vint(s, &value64) != 0 || value64 != 0x0809050b0c0d0e0f);
    EXPECT_EQ(byteread_remain(s), 0);
}

// ----------------------------------------------------------------------------
void test_bytereadBuffer(void* context)
{
    testByteRead* test = (testByteRead*)context;
    byteread_t* s = test->bytes;

    uint8_t buf[16];
    EXPECT_FALSE(byteread_buffer(s, buf, 2) != 0 || memcmp(expected_stream0, buf, 2) != 0);
    EXPECT_FALSE(byteread_buffer(s, buf, 14) != 0 || memcmp(expected_stream0 + 2, buf, 14) != 0);
    EXPECT_EQ(byteread_remain(s), 0);
}
