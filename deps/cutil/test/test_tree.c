/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */

#include "cunit.h"

#include "cutil_tree.h"
#include "cutil_nullptr.h"

#include "test_container.h"

// ----------------------------------------------------------------------------
void* startupFixtureTree()
{
    container_test_fixture_t* context = malloc(sizeof(container_test_fixture_t));
    ASSERT_TRUE(context != nullptr);

    if (context != nullptr) {
        context->fnCreate = treeCreate;
        context->fnDestroy = treeDestroy;
        context->fnInsert = treeInsertItem;
        context->fnRemove = treeRemoveItem;
        context->fnFind = treeFindItem;
        context->fnIterate = treeIterate;
        context->fnIsEmpty = treeIsEmpty;
        context->fnGetLength = treeGetLength;
    }

    return context;
}

// ----------------------------------------------------------------------------
void teardownFixtureTree(void* context)
{
    container_test_fixture_t* test = (container_test_fixture_t*)context;
    free((void*)test);
}
