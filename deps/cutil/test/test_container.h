/*
 * MIT License
 *
 * Copyright (c) 2022 Steffen Schulze
 */

#include "cunit.h"

#include "cutil_container.h"
#include "cutil_nullptr.h"

// ----------------------------------------------------------------------------
struct container_test_fixture_s {
    cutilFnContainerCreate fnCreate;
    cutilFnContainerDestroy fnDestroy;
    cutilFnInsertItem fnInsert;
    cutilFnRemoveItem fnRemove;
    cutilFnFindItem fnFind;
    cutilFnIterate fnIterate;
    cutilFnIsEmpty fnIsEmpty;
    cutilFnGetLength fnGetLength;
};

typedef struct container_test_fixture_s container_test_fixture_t;
