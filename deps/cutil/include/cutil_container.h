/*
 * MIT License
 *
 * Copyright (c) 2021 Steffen Schulze
 */

#pragma once

// ----------------------------------------------------------------------------
// Callback function types
typedef void (*cutilFnDestroy)(const void*);
typedef int (*cutilFnItem)(const void*, void*);
typedef int (*cutilFnCompare)(const void*, const void*);
typedef int (*cutilFnPredicate)(const void*, const void*);

// ----------------------------------------------------------------------------
// Container function types
typedef void* (*cutilFnContainerCreate)();
typedef void (*cutilFnContainerDestroy)(const void*, cutilFnDestroy);

typedef void (*cutilFnInsertItem)(void*, cutilFnCompare, const void*);
typedef void (*cutilFnRemoveAll)(void*, cutilFnDestroy);
typedef const void* (*cutilFnRemoveItem)(void*, cutilFnPredicate, const void*);
typedef void (*cutilFnMerge)(void*, void*, cutilFnCompare);
typedef void (*cutilFnFilter)(void*, cutilFnItem, cutilFnDestroy, void*);

typedef const void* (*cutilFnFindItem)(const void*, cutilFnPredicate, const void*);
typedef void (*cutilFnIterate)(const void*, cutilFnItem, void*);
typedef int (*cutilFnIsEmpty)(const void*);
typedef unsigned (*cutilFnGetLength)(const void*);

// ----------------------------------------------------------------------------
struct iterator_s;
typedef struct iterator_s iterator_t;

struct iterator_s {
    iterator_t* next;
    const void* item;
};

// ----------------------------------------------------------------------------
iterator_t* itCreate(const void* item);
void itDestroy(const iterator_t* it, cutilFnDestroy fnDestroy);
