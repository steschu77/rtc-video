/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */

#pragma once

#include "cutil_container.h"

// ----------------------------------------------------------------------------
struct tree_s;
typedef struct tree_s tree_t;

// ----------------------------------------------------------------------------
void* treeCreate();
void treeDestroy(const void*, cutilFnDestroy fnItem);

// ----------------------------------------------------------------------------
void treeInsertItem(void*, cutilFnCompare, const void*);
void treeRemoveAllItems(void*, cutilFnDestroy);
const void* treeRemoveItem(void*, cutilFnPredicate, const void*);

// ----------------------------------------------------------------------------
int treeIsEmpty(const void*);
unsigned treeGetLength(const void*);

void treeIterate(const void*, cutilFnItem fnItem, void* context);
const void* treeFindItem(const void* tree, cutilFnPredicate fnPredicate, const void* context);
const void* treePopItem(void* tree);
const void* treeFirst(const void* tree);
