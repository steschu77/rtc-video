/*
 * MIT License
 *
 * Copyright (c) 2021 Steffen Schulze
 */

#pragma once

#include <stdint.h>
#include "cutil_container.h"

// ----------------------------------------------------------------------------
typedef struct vecu64_s vecu64_t;

// ----------------------------------------------------------------------------
vecu64_t* vecu64Create(unsigned capacity, unsigned increase);
void vecu64Destroy(const vecu64_t*);

void vecu64RemoveAll(vecu64_t* vec);
void vecu64AddItem(vecu64_t*, uint64_t elem);
void vecu64Append(vecu64_t*, const vecu64_t* src);

unsigned vecu64ItemCount(const vecu64_t*);
int vecu64IsEmpty(const vecu64_t*);
uint64_t vecu64GetItem(const vecu64_t*, unsigned idx);
