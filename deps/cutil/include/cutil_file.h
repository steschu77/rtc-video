/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */

#pragma once

#include "cutil_nullptr.h"
#include "cutil_retcode.h"
#include "cutil_utf8str.h"

#define f_overwrite (1)

struct utilsFile_s;
typedef struct utilsFile_s utilsFile;

utilsFile* createFileRead(const utf8str_t* path);
utilsFile* createFileWrite(const utf8str_t* path, int overwrite);

utilsFile* assignFile(utilsFile* obj);
void freeFile(const utilsFile* file);

size_t readFile(utilsFile* file, void* data, size_t length);
size_t writeFile(utilsFile* file, const void* data, size_t length);
size_t seekFile(utilsFile* file, size_t length);

const utf8str_t* getFilePath(const utilsFile* file);
retcode getFileResult(const utilsFile* file);

uint64_t getFileSize(const utilsFile* file);

int fileCompare(utilsFile* f0, utilsFile* f1);