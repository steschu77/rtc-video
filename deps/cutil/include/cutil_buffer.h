/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */

#pragma once

#include <stddef.h>
#include <stdint.h>

/*
** Buffer implementation for shared ownership.
*/

// ----------------------------------------------------------------------------
typedef struct buffer_s buffer_t;

// ----------------------------------------------------------------------------
const buffer_t* allocBuffer(size_t size, void** ptr, size_t alignment);
const buffer_t* allocSubBuffer(const buffer_t* from, size_t offset, size_t size);
const buffer_t* assignBuffer(const buffer_t* obj);
void freeBuffer(const buffer_t* obj);

// ----------------------------------------------------------------------------
const uint8_t* getBufferData(const buffer_t* buf);
size_t getBufferSize(const buffer_t* buf);
