/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */

#pragma once

// ----------------------------------------------------------------------------
// clang-format off
#define ANSI_RESET            "\x1b[0m"
#define ANSI_BOLD             "\x1b[1m"
#define ANSI_UNDERLINE        "\x1b[4m"
#define ANSI_INVERT           "\x1b[7m"

#define ANSI_BLACK            "\x1b[30m"
#define ANSI_RED              "\x1b[31m"
#define ANSI_GREEN            "\x1b[32m"
#define ANSI_YELLOW           "\x1b[33m"
#define ANSI_BLUE             "\x1b[34m"
#define ANSI_MAGENTA          "\x1b[35m"
#define ANSI_CYAN             "\x1b[36m"
#define ANSI_WHITE            "\x1b[37m"

#define ANSI_BRIGHTRED        "\x1b[31;1m"
#define ANSI_BRIGHTGREEN      "\x1b[32;1m"
#define ANSI_BRIGHTYELLOW     "\x1b[33;1m"
#define ANSI_BRIGHTBLUE       "\x1b[34;1m"
#define ANSI_BRIGHTMAGENTA    "\x1b[35;1m"
#define ANSI_BRIGHTCYAN       "\x1b[36;1m"
// clang-format on
