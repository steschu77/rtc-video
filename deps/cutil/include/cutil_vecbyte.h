/*
 * MIT License
 *
 * Copyright (c) 2020 Steffen Schulze
 */

#pragma once

#include <stdint.h>
#include "cutil_container.h"
#include "cutil_buffer.h"

// ----------------------------------------------------------------------------
typedef struct bytes_s bytes_t;

// ----------------------------------------------------------------------------
bytes_t* byteCreate(size_t capacity, size_t increase);
void byteDestroy(bytes_t*);

void byteRemoveAll(bytes_t* obj);
void byteAddData(bytes_t*, const void* src, size_t count);
void byteReset(bytes_t* obj);

size_t byteGetSize(const bytes_t*);
int byteIsEmpty(const bytes_t*);
const void* byteGetData(const bytes_t*);
