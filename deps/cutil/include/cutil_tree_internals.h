/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */

#pragma once

#include <stdlib.h>

#include "cutil_nullptr.h"
#include "cutil_tree.h"

/* https://en.wikipedia.org/wiki/Splay_tree */

struct tree_node_s;
typedef struct tree_node_s cutilTreeNode;

#define c_right 0
#define c_left 1

// ----------------------------------------------------------------------------
cutilTreeNode* createNode(const void* item);
void destroyNode(const cutilTreeNode* node, cutilFnDestroy fnDestroy);
int verifyNode(const cutilTreeNode* node);
void rotate(cutilTreeNode* child);

// ----------------------------------------------------------------------------
struct tree_node_s {
    const void* item;

    cutilTreeNode* parent;
    cutilTreeNode* childs[2];
    int parent_idx;
};

// ----------------------------------------------------------------------------
struct tree_s {
    cutilFnCompare fnCompare;
    cutilFnDestroy fnDestroy;

    cutilTreeNode* root;
    unsigned numItems;
};

// ----------------------------------------------------------------------------
inline void setChild(cutilTreeNode* parent, cutilTreeNode* child, int idx)
{
    parent->childs[idx] = child;
    if (child != nullptr) {
        child->parent = parent;
        child->parent_idx = idx;
    }
}
