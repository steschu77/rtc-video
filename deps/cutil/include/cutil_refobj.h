/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */

#pragma once

#include "cutil_nullptr.h"
#include "cutil_refcount.h"

// ----------------------------------------------------------------------------
typedef void (*ptrFnAssign)(void* context);
typedef void (*ptrFnRelease)(void* context);

// ----------------------------------------------------------------------------
struct ref_s;
typedef struct ref_s ref_t;

struct ref_s {
    void* obj;
    void (*destroy)(const void*);
    volatile refcount_t count;
};

// ----------------------------------------------------------------------------
inline void ref_init(ref_t* ref, void* obj, void (*destroy)(const void*))
{
    ref->obj = obj;
    ref->destroy = destroy;
    ref->count = 0;
}

// ----------------------------------------------------------------------------
inline void ref_inc(const ref_t* ref)
{
    ref_t* ptr = (ref_t*)ref;
    incref(&ptr->count);
}

// ----------------------------------------------------------------------------
inline void ref_dec(const ref_t* ref)
{
    ref_t* ptr = (ref_t*)ref;
    if (decref(&ptr->count) == 0) {
        ptr->destroy(ptr->obj);
    }
}
