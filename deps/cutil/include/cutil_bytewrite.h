/*
 * MIT License
 *
 * Copyright (c) 2020 Steffen Schulze
 */

#pragma once

#include <stddef.h>
#include <stdint.h>

#include "cutil_buffer.h"

typedef struct bytewrite_s bytewrite_t;

// ----------------------------------------------------------------------------
bytewrite_t* bytewriteCreate(void* data, size_t size);
void bytewriteDestroy(const bytewrite_t* obj);

size_t bytewrite_length(const bytewrite_t* obj);
const void* bytewrite_data(const bytewrite_t* obj);

int bytewrite_int8(bytewrite_t* s, uint8_t value);
int bytewrite_int16(bytewrite_t* s, uint16_t value);
int bytewrite_int32(bytewrite_t* s, uint32_t value);
int bytewrite_int64(bytewrite_t* s, uint64_t value);
int bytewrite_vint(bytewrite_t* s, uint64_t value);
int bytewrite_fourcc(bytewrite_t* s, uint32_t value);
int bytewrite_buffer(bytewrite_t* s, const void* buffer, size_t length);
