/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */
#pragma once

#include <stdint.h>

#if defined(_MSC_VER)

#include <intrin.h>

// ----------------------------------------------------------------------------
inline uint16_t byteswap16(uint16_t x) { return _byteswap_ushort(x); }
inline uint32_t byteswap32(uint32_t x) { return _byteswap_ulong(x); }
inline uint64_t byteswap64(uint64_t x) { return _byteswap_uint64(x); }

#else // defined(_MSC_VER)

#include <byteswap.h>

// ----------------------------------------------------------------------------
inline uint16_t byteswap16(uint16_t x) { return bswap_16(x); }
inline uint32_t byteswap32(uint32_t x) { return bswap_32(x); }
inline uint64_t byteswap64(uint64_t x) { return bswap_64(x); }

#endif // defined(_MSC_VER)
