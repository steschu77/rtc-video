/*
 * MIT License
 *
 * Copyright (c) 2020 Steffen Schulze
 */

#pragma once

#include <stddef.h>
#include <stdint.h>

#include "cutil_buffer.h"

typedef struct byteread_s byteread_t;

// ----------------------------------------------------------------------------
byteread_t* bytereadCreate(const void* data, size_t size);
void bytereadDestroy(const byteread_t* obj);

size_t byteread_remain(const byteread_t* obj);
const void* byteread_data(const byteread_t* obj);
const void* byteread_ptr(const byteread_t* obj);

int byteread_int8(byteread_t* s, uint8_t* value);
int byteread_int16(byteread_t* s, uint16_t* value);
int byteread_int32(byteread_t* s, uint32_t* value);
int byteread_int64(byteread_t* s, uint64_t* value);
int byteread_vint(byteread_t* s, uint64_t* value);
int byteread_buffer(byteread_t* s, void* buffer, size_t length);

int byteread_skip(byteread_t* s, size_t length);
