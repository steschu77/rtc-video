/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */
#pragma once

#if defined(_MSC_VER)

#include <intrin.h>
typedef __int64 refcount_t;

// ----------------------------------------------------------------------------
inline refcount_t incref(const volatile refcount_t* ref)
{
    return _InterlockedIncrement64((volatile refcount_t*)ref);
}

// ----------------------------------------------------------------------------
inline refcount_t decref(const volatile refcount_t* ref)
{
    return _InterlockedDecrement64((volatile refcount_t*)ref);
}

#else // defined(_MSC_VER)

#include <stdint.h>
#include <stdatomic.h>

typedef atomic_uint_fast64_t refcount_t;

// ----------------------------------------------------------------------------
inline uint_fast64_t incref(const volatile refcount_t* ref)
{
    return atomic_fetch_add_explicit((volatile refcount_t*)ref, 1, memory_order_acquire) + 1;
}

// ----------------------------------------------------------------------------
inline uint_fast64_t decref(const volatile refcount_t* ref)
{
    return atomic_fetch_sub_explicit((volatile refcount_t*)ref, 1, memory_order_release) - 1;
}

#endif // defined(_MSC_VER)
