/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */

#pragma once

#include <ctype.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "cutil_nullptr.h"
#include "cutil_refcount.h"

/*
** Lightweight string representation for efficient use as parameter in
** interfaces and for efficient string storage by reference counting.
**
** String content can never be modified. If a string needs to be modified, a
** new one has to be created explicitly.
**
** Don't touch the reference count of const strings. Const strings are defined
** to have a reference count of 0, which is never incremented nor decremented.
** That implies, const strings will never be deleted.
**
** Empty strings are defined to be the utf8str_t* null pointer or to have
** utf8str_t::data == "".
*/

// ----------------------------------------------------------------------------
struct utf8str_s {
    refcount_t count;
    char data[];
};

typedef struct utf8str_s utf8str_t;

// ----------------------------------------------------------------------------
#define STR(x) (const utf8str_t*)("\0\0\0\0\0\0\0\0" x)

// ----------------------------------------------------------------------------
inline const char* utf8_cstr(const utf8str_t* str)
{
    return (str != nullptr) ? str->data : "";
}

// ----------------------------------------------------------------------------
inline size_t utf8_length(const utf8str_t* str)
{
    return (str != nullptr) ? strlen(str->data) : 0;
}

// ----------------------------------------------------------------------------
inline void utf8_incref(const utf8str_t* str)
{
    if (str == nullptr || str->count == 0) {
        return; // empty or constant string?
    }

    incref(&str->count);
}

// ----------------------------------------------------------------------------
inline void utf8_decref(const utf8str_t* str)
{
    if (str == nullptr || str->count == 0) {
        return; // empty or constant string?
    }

    if (decref(&str->count) == 0u) {
        free((void*)str);
    }
}

// ----------------------------------------------------------------------------
inline const utf8str_t* utf8_assign(const utf8str_t* str)
{
    utf8_incref(str);
    return str;
}

// ----------------------------------------------------------------------------
inline void utf8_free(const utf8str_t** str)
{
    utf8_decref(*str);
    *str = nullptr;
}

// ----------------------------------------------------------------------------
inline const utf8str_t* utf8_make(const char* c_str, size_t len)
{
    if (len == 0) {
        return nullptr;
    }

    size_t str_size = sizeof(utf8str_t) + sizeof(char) * (len + 1);
    utf8str_t* str = (utf8str_t*)malloc(str_size);

    if (str == nullptr) {
        return nullptr;
    }

    str->count = 1;

    memcpy(&str->data[0], c_str, sizeof(char) * len);
    str->data[len] = '\0';

    return str;
}

// ----------------------------------------------------------------------------
inline const utf8str_t* utf8_concat(const utf8str_t* str0, const utf8str_t* str1)
{
    size_t len0 = strlen(utf8_cstr(str0));
    size_t len1 = strlen(utf8_cstr(str1));
    size_t len = len0 + len1;
    if (len == 0 || len < len0 || len < len1) {
        return nullptr;
    }

    size_t str_size = sizeof(utf8str_t) + sizeof(char) * (len + 1);
    utf8str_t* str = (utf8str_t*)malloc(str_size);

    if (str == nullptr) {
        return nullptr;
    }

    str->count = 1;

    memcpy(&str->data[0], utf8_cstr(str0), sizeof(char) * len0);
    memcpy(&str->data[len0], utf8_cstr(str1), sizeof(char) * len1);
    str->data[len] = '\0';

    return str;
}

// ----------------------------------------------------------------------------
inline int utf8_ccmp(const utf8str_t* str, const char* cstr)
{
    return strcmp(utf8_cstr(str), cstr);
}

// ----------------------------------------------------------------------------
inline int utf8_cmp(const utf8str_t* pstr, const utf8str_t* qstr)
{
    return strcmp(utf8_cstr(pstr), utf8_cstr(qstr));
}

// ----------------------------------------------------------------------------
int utf8_to_int(const char* str);
unsigned utf8_to_unsigned(const char* str);

const utf8str_t* utf8_format(const char* format, ...);
