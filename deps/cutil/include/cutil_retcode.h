/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */

#pragma once

//! retcode = 0 means success, < 0 means error, > 0 means success but under
//! special conditions.
typedef int retcode;

// ----------------------------------------------------------------------------
// clang-format off
#define rcsetGenericSuccessCodes (0x00000000)
#define rcsetGenericErrorCodes  (-0x40000000)

// ----------------------------------------------------------------------------
enum GenericReturnCode
{
    rcSuccess                  = rcsetGenericSuccessCodes | 0x0000,
    rcFinished                 = rcsetGenericSuccessCodes | 0x0002,
    rcUnknownParam             = rcsetGenericSuccessCodes | 0x0003,
    rcEmpty                    = rcsetGenericSuccessCodes | 0x0004,

    rcFailed                   = rcsetGenericErrorCodes | 0x0000,
    rcNotInitialized           = rcsetGenericErrorCodes | 0x0001,
    rcAlreadyInitialized       = rcsetGenericErrorCodes | 0x0002,
    rcAlreadyExists            = rcsetGenericErrorCodes | 0x0003,
    rcParamOutOfRange          = rcsetGenericErrorCodes | 0x0004,
    rcInvalidParam             = rcsetGenericErrorCodes | 0x0005,
    rcInvalidPointer           = rcsetGenericErrorCodes | 0x0006,
    rcAborted                  = rcsetGenericErrorCodes | 0x0007,
    rcNotFound                 = rcsetGenericErrorCodes | 0x0008,
    rcNotAvailable             = rcsetGenericErrorCodes | 0x0009,
    rcNotHandled               = rcsetGenericErrorCodes | 0x000a,
    rcNoResponse               = rcsetGenericErrorCodes | 0x000b,
    rcInvalidVersion           = rcsetGenericErrorCodes | 0x000c,
    rcNotImplemented           = rcsetGenericErrorCodes | 0x000d,
    rcBufferOverflow           = rcsetGenericErrorCodes | 0x000e,
    rcBufferUnderrun           = rcsetGenericErrorCodes | 0x000f,
    rcDeadLock                 = rcsetGenericErrorCodes | 0x0010,
    rcAccessDenied             = rcsetGenericErrorCodes | 0x0011,
    rcTimeOut                  = rcsetGenericErrorCodes | 0x0012,
    rcInternal                 = rcsetGenericErrorCodes | 0x0013,
    rcLostSync                 = rcsetGenericErrorCodes | 0x0014,
    rcDiscontinuity            = rcsetGenericErrorCodes | 0x0015,
    rcInvalidState             = rcsetGenericErrorCodes | 0x0016,
    rcUnsupported              = rcsetGenericErrorCodes | 0x0017,
    rcPreconditionFailed       = rcsetGenericErrorCodes | 0x0018,
    rcDuplicate                = rcsetGenericErrorCodes | 0x0019,
    rcOutOfRange               = rcsetGenericErrorCodes | 0x001a,

    // file IO related errors
    rcInvalidFilename          = rcsetGenericErrorCodes | 0x0100,
    rcFileNotFound             = rcsetGenericErrorCodes | 0x0101,
    rcPathNotFound             = rcsetGenericErrorCodes | 0x0102,
    rcFileNotOpen              = rcsetGenericErrorCodes | 0x0103,
    rcCreatingFile             = rcsetGenericErrorCodes | 0x0104,
    rcInvalidFile              = rcsetGenericErrorCodes | 0x0105,
    rcInvalidFormat            = rcsetGenericErrorCodes | 0x0106,
    rcWritingFile              = rcsetGenericErrorCodes | 0x0107,
    rcReadingFile              = rcsetGenericErrorCodes | 0x0108,
    rcEndOfFile                = rcsetGenericErrorCodes | 0x0109,
    rcTooManyOpenFiles         = rcsetGenericErrorCodes | 0x010a,

    // memory related error codes
    rcNotEnoughMemory          = rcsetGenericErrorCodes | 0x0200,
    rcInvalidBufferSize        = rcsetGenericErrorCodes | 0x0201,
};
// clang-format on

// ----------------------------------------------------------------------------
inline int failed(retcode rc)
{
    return (rc < 0);
}

// ----------------------------------------------------------------------------
inline int success(retcode rc)
{
    return (rc == 0);
}
