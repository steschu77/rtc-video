/*
 * MIT License
 *
 * Copyright (c) 2021 Steffen Schulze
 */

#pragma once

#include "cutil_container.h"

// ----------------------------------------------------------------------------
struct sortedlist_s;
typedef struct sortedlist_s sortedlist_t;

// ----------------------------------------------------------------------------
void* slistCreate();
void slistDestroy(const void*, cutilFnDestroy);

// ----------------------------------------------------------------------------
void slistInsertItem(void*, cutilFnCompare, const void*);
void slistRemoveAll(void*, cutilFnDestroy);
const void* slistRemoveItem(void*, cutilFnPredicate, const void*);
void slistMerge(void*, void*, cutilFnCompare);
void slistFilter(void*, cutilFnItem, cutilFnDestroy, void*);

const void* slistFindItem(const void*, cutilFnPredicate, const void*);
void slistIterate(const void*, cutilFnItem, void*);
int slistIsEmpty(const void*);
unsigned slistGetLength(const void*);

const void* slistPopItem(void*);
const void* slistFirst(const void*);
