/*
 * MIT License
 *
 * Copyright (c) 2021 Steffen Schulze
 */

#pragma once

#include <stddef.h>
#include <stdint.h>

typedef struct bitread_s bitread_t;

// ----------------------------------------------------------------------------
bitread_t* bitreadCreate(const void* data, size_t size);
void bitreadDestroy(const bitread_t* obj);

size_t bitreadBitCount(const bitread_t* obj);
size_t bitreadFlushBits(bitread_t* obj, unsigned bits);
size_t bitreadAlign(bitread_t* obj);

uint32_t bitreadGetBits(bitread_t* obj, unsigned bits);
uint8_t bitreadGetBit(bitread_t* obj);
uint16_t bitreadGetBits2(bitread_t* obj);
uint16_t bitreadGetBits3(bitread_t* obj);
uint16_t bitreadGetBits4(bitread_t* obj);
uint16_t bitreadGetBits5(bitread_t* obj);
uint16_t bitreadGetBits6(bitread_t* obj);
uint16_t bitreadGetBits7(bitread_t* obj);
uint16_t bitreadGetBits8(bitread_t* obj);
uint16_t bitreadGetBits9(bitread_t* obj);
uint16_t bitreadGetBits10(bitread_t* obj);
uint16_t bitreadGetBits11(bitread_t* obj);
uint16_t bitreadGetBits12(bitread_t* obj);
uint16_t bitreadGetBits13(bitread_t* obj);
uint16_t bitreadGetBits14(bitread_t* obj);
uint16_t bitreadGetBits15(bitread_t* obj);
uint32_t bitreadGetBits16(bitread_t* obj);
uint32_t bitreadGetBits17(bitread_t* obj);
uint32_t bitreadGetBits18(bitread_t* obj);
uint32_t bitreadGetBits19(bitread_t* obj);
uint32_t bitreadGetBits20(bitread_t* obj);
uint32_t bitreadGetBits21(bitread_t* obj);
uint32_t bitreadGetBits22(bitread_t* obj);
uint32_t bitreadGetBits23(bitread_t* obj);
uint32_t bitreadGetBits24(bitread_t* obj);
uint32_t bitreadGetBits32(bitread_t* obj);
