/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */
#pragma once

#include <stdint.h>

// ----------------------------------------------------------------------------
inline unsigned unsigned_max(unsigned x0, unsigned x1)
{
    return x0 > x1 ? x0 : x1;
}

// ----------------------------------------------------------------------------
inline unsigned unsigned_min(unsigned x0, unsigned x1)
{
    return x0 < x1 ? x0 : x1;
}

// ----------------------------------------------------------------------------
inline unsigned unsigned_clamp(unsigned x, unsigned x0, unsigned x1)
{
    return x < x0 ? x0 : (x > x1 ? x1 : x);
}

// ----------------------------------------------------------------------------
inline uint64_t uint64_max(uint64_t x0, uint64_t x1)
{
    return x0 > x1 ? x0 : x1;
}

// ----------------------------------------------------------------------------
inline uint64_t uint64_min(uint64_t x0, uint64_t x1)
{
    return x0 < x1 ? x0 : x1;
}

// ----------------------------------------------------------------------------
inline uint64_t uint64_clamp(uint64_t x, uint64_t x0, uint64_t x1)
{
    return x < x0 ? x0 : (x > x1 ? x1 : x);
}

// ----------------------------------------------------------------------------
inline size_t size_t_max(size_t x0, size_t x1)
{
    return x0 > x1 ? x0 : x1;
}

// ----------------------------------------------------------------------------
inline size_t size_t_min(size_t x0, size_t x1)
{
    return x0 < x1 ? x0 : x1;
}

// ----------------------------------------------------------------------------
inline size_t size_t_clamp(size_t x, size_t x0, size_t x1)
{
    return x < x0 ? x0 : (x > x1 ? x1 : x);
}

// ----------------------------------------------------------------------------
inline double double_max(double x0, double x1)
{
    return x0 > x1 ? x0 : x1;
}

// ----------------------------------------------------------------------------
inline double double_min(double x0, double x1)
{
    return x0 < x1 ? x0 : x1;
}

// ----------------------------------------------------------------------------
inline double double_clamp(double x, double x0, double x1)
{
    return x < x0 ? x0 : (x > x1 ? x1 : x);
}

// ----------------------------------------------------------------------------
// multiple = 0 is forbidden as there is no number != 0 which is a multiple of 0.
inline unsigned unsigned_roundUpMultiple(unsigned x, unsigned multiple)
{
    x += multiple - 1;
    return x - x % multiple;
}

// ----------------------------------------------------------------------------
// multiple = 0 is forbidden as there is no number != 0 which is a multiple of 0.
inline size_t size_t_roundUpMultiple(size_t x, size_t multiple)
{
    x += multiple - 1;
    return x - x % multiple;
}
