/*
 * MIT License
 *
 * Copyright (c) 2022 Steffen Schulze
 */

#pragma once

struct rnd_s;
typedef struct rnd_s rnd_t;

rnd_t* rndCreate(unsigned short seed);
void rndDestroy(const rnd_t*);

unsigned short rndGenerate(rnd_t*);
