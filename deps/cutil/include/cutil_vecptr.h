/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */

#pragma once

#include <stdint.h>
#include "cutil_container.h"

// ----------------------------------------------------------------------------
typedef struct vecptr_s vecptr_t;

// ----------------------------------------------------------------------------
vecptr_t* vecptrCreate(unsigned capacity, unsigned increase, cutilFnDestroy fnDestroy);
void vecptrDestroy(vecptr_t*);

void vecptrRemoveAll(vecptr_t* vec);

void vecptrAddItem(vecptr_t*, void* elem);
void vecptrAppend(vecptr_t*, const vecptr_t* src);

unsigned vecptrItemCount(const vecptr_t*);
int vecptrIsEmpty(const vecptr_t*);
void* vecptrGetItem(const vecptr_t*, unsigned idx);

void vecptrIterateItems(const vecptr_t* vec, cutilFnItem fnItem, void* context);
