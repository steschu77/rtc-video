/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */
#pragma once

#include "cutil_utf8str.h"

struct cutilPath_s;
typedef struct cutilPath_s cutilPath;

const cutilPath* createPath(const utf8str_t* strPath);
void destroyPath(const cutilPath* path);

const utf8str_t* getPathName(const cutilPath* path);
const utf8str_t* getDirectory(const cutilPath* path);
const utf8str_t* getFileName(const cutilPath* path);
const utf8str_t* getExtension(const cutilPath* path);
