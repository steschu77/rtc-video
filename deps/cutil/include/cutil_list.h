/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */

#pragma once

#include "cutil_container.h"

struct cutilList_s;
typedef struct cutilList_s cutilList;

cutilList* createList(cutilFnDestroy fnDestroy);
void destroyList(const cutilList*);

int listIsEmpty(const cutilList* list);
void listAddItem(cutilList*, const void* item);
void listIterate(const cutilList* list, cutilFnItem fnItem, void* context);
