/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */

#include <malloc.h>

#include "cutil_refobj.h"

// ----------------------------------------------------------------------------
extern void ref_init(ref_t* ref, void* obj, void (*destroy)(const void*));
extern void ref_inc(const ref_t* ref);
extern void ref_dec(const ref_t* ref);
extern void ref_copy(ref_t** r, ref_t* q);
