/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */

#include "cutil_byteswap.h"

// ----------------------------------------------------------------------------
extern inline uint16_t byteswap16(uint16_t x);
extern inline uint32_t byteswap32(uint32_t x);
extern inline uint64_t byteswap64(uint64_t x);
