/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */

#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "cutil_math.h"
#include "cutil_nullptr.h"
#include "cutil_vecbyte.h"

// ----------------------------------------------------------------------------
void byteRealloc(bytes_t* ids, size_t new_capacity);

// ----------------------------------------------------------------------------
struct bytes_s {
    void* data;
    size_t length;
    size_t capacity;
    size_t increase;
};

// ----------------------------------------------------------------------------
bytes_t* byteCreate(size_t capacity, size_t increase)
{
    bytes_t* obj = (bytes_t*)malloc(sizeof(bytes_t));
    if (obj == nullptr) {
        return nullptr;
    }

    obj->data = nullptr;
    obj->length = 0;
    obj->capacity = 0;
    obj->increase = size_t_max(increase, 1u);
    byteRealloc(obj, capacity);

    return obj;
}

// ----------------------------------------------------------------------------
void byteDestroy(bytes_t* obj)
{
    if (obj != nullptr) {
        byteRemoveAll(obj);
        free((void*)obj);
    }
}

// ----------------------------------------------------------------------------
void byteRemoveAll(bytes_t* obj)
{
    free((void*)obj->data);
    obj->data = nullptr;

    obj->length = 0;
    obj->capacity = 0;
}

// ----------------------------------------------------------------------------
void byteRealloc(bytes_t* obj, size_t new_capacity)
{
    size_t capacity = size_t_roundUpMultiple(new_capacity, obj->increase);
    if (capacity <= obj->capacity) {
        return;
    }

    void* data = (void**)realloc(obj->data, capacity);
    if (data == nullptr) {
        return;
    }

    obj->data = data;
    obj->capacity = capacity;
}

// ----------------------------------------------------------------------------
void byteAddData(bytes_t* obj, const void* src, size_t count)
{
    size_t idx = obj->length;
    size_t length = obj->length + count;
    if (length > obj->capacity) {
        byteRealloc(obj, length);
    }

    if (length <= obj->capacity) {
        memcpy((uint8_t*)obj->data + idx, src, count);
        obj->length = length;
    }
}

// ----------------------------------------------------------------------------
void byteReset(bytes_t* obj)
{
    obj->length = 0;
}

// ----------------------------------------------------------------------------
size_t byteGetSize(const bytes_t* obj)
{
    return obj->length;
}

// ----------------------------------------------------------------------------
int byteIsEmpty(const bytes_t* obj)
{
    return obj->length == 0;
}

// ----------------------------------------------------------------------------
const void* byteGetData(const bytes_t* obj)
{
    return obj->data;
}
