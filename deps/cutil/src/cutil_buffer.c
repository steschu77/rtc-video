/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */

#include <stdlib.h>

#include "cutil_buffer.h"
#include "cutil_nullptr.h"
#include "cutil_refobj.h"

// ----------------------------------------------------------------------------
static void* alignPointer(void* unaligned, size_t alignment);

// ----------------------------------------------------------------------------
struct buffer_blob_s {
    ref_t ref;

    const void* data;
    size_t size;

    void* alloc;
};

typedef struct buffer_blob_s buffer_blob_t;

// ----------------------------------------------------------------------------
static void destroyBufferBlob(const void* obj)
{
    const buffer_blob_t* buf = (const buffer_blob_t*)obj;
    free((void*)buf->alloc);
    free((void*)buf);
}

// ----------------------------------------------------------------------------
const buffer_blob_t* assignBufferBlob(const buffer_blob_t* obj)
{
    ref_inc(&obj->ref);
    return obj;
}

// ----------------------------------------------------------------------------
void freeBufferBlob(const buffer_blob_t* obj)
{
    ref_dec(&obj->ref);
}

// ----------------------------------------------------------------------------
const buffer_blob_t* createBufferBlob(size_t size, void** ptr, size_t alignment)
{
    buffer_blob_t* buf = (buffer_blob_t*)malloc(sizeof(buffer_blob_t));
    void* alloc = malloc(size + alignment);

    if (alloc == nullptr || buf == nullptr) {
        free(buf);
        free(alloc);
        return nullptr;
    }

    void* data = alignPointer(alloc, alignment);

    ref_init(&buf->ref, buf, destroyBufferBlob);
    buf->alloc = alloc;
    buf->data = data;
    buf->size = size;

    *ptr = data;
    return assignBufferBlob(buf);
}

// ----------------------------------------------------------------------------
struct buffer_s {
    ref_t ref;

    size_t offset;
    size_t size;

    const buffer_blob_t* blob;
};

// ----------------------------------------------------------------------------
static void destroyBuffer(const void* obj)
{
    const buffer_t* buf = (const buffer_t*)obj;
    freeBufferBlob(buf->blob);
    free((void*)buf);
}

// ----------------------------------------------------------------------------
static const buffer_t* createBuffer(const buffer_blob_t* blob, size_t offset, size_t size)
{
    if (offset + size > blob->size) {
        return nullptr;
    }

    buffer_t* buf = (buffer_t*)malloc(sizeof(buffer_t));
    if (buf == nullptr) {
        return nullptr;
    }

    ref_init(&buf->ref, buf, destroyBuffer);
    buf->offset = offset;
    buf->size = size;
    buf->blob = assignBufferBlob(blob);

    return assignBuffer(buf);
}

// ----------------------------------------------------------------------------
const buffer_t* assignBuffer(const buffer_t* obj)
{
    if (obj != nullptr) {
        ref_inc(&obj->ref);
    }
    return obj;
}

// ----------------------------------------------------------------------------
void freeBuffer(const buffer_t* obj)
{
    if (obj != nullptr) {
        ref_dec(&obj->ref);
    }
}

// ----------------------------------------------------------------------------
void* alignPointer(void* unaligned, size_t alignment)
{
    uintptr_t offset = alignment - 1;
    uintptr_t mask = ~(alignment - 1);
    return (void*)(((uintptr_t)unaligned + offset) & mask);
}

// ----------------------------------------------------------------------------
const buffer_t* allocBuffer(size_t size, void** ptr, size_t alignment)
{
    const buffer_blob_t* blob = createBufferBlob(size, ptr, alignment);
    if (blob == nullptr) {
        return nullptr;
    }

    const buffer_t* buf = createBuffer(blob, 0, size);
    freeBufferBlob(blob);

    return buf;
}

// ----------------------------------------------------------------------------
const buffer_t* allocSubBuffer(const buffer_t* from, size_t offset, size_t size)
{
    return createBuffer(from->blob, from->offset + offset, size);
}

// ----------------------------------------------------------------------------
const uint8_t* getBufferData(const buffer_t* buf)
{
    return (const uint8_t*)buf->blob->data + buf->offset;
}

// ----------------------------------------------------------------------------
size_t getBufferSize(const buffer_t* buf)
{
    return buf == nullptr ? 0 : buf->size;
}
