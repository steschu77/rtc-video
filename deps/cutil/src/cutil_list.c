/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */

#include <stdlib.h>

#include "cutil_list.h"
#include "cutil_nullptr.h"

// ----------------------------------------------------------------------------
struct cutilList_s {
    cutilFnDestroy fnDestroy;
    iterator_t* first;
    iterator_t* last;
    unsigned numItems;
};

// ----------------------------------------------------------------------------
cutilList* createList(cutilFnDestroy fnDestroy)
{
    cutilList* list = (cutilList*)malloc(sizeof(cutilList));
    if (list == nullptr) {
        return nullptr;
    }

    list->fnDestroy = fnDestroy;
    list->first = nullptr;
    list->last = nullptr;
    list->numItems = 0;
    return list;
}

// ----------------------------------------------------------------------------
void destroyList(const cutilList* list)
{
    if (list == nullptr) {
        return;
    }

    const iterator_t* first = list->first;

    while (first != nullptr) {
        const iterator_t* it = first;
        first = it->next;
        itDestroy(it, list->fnDestroy);
    }

    free((void*)list);
}

// ----------------------------------------------------------------------------
int listIsEmpty(const cutilList* list)
{
    return list->first == nullptr;
}

// ----------------------------------------------------------------------------
void listAddItem(cutilList* list, const void* item)
{
    iterator_t* it = itCreate(item);
    if (it == nullptr) {
        return;
    }

    if (list->last != nullptr) {
        list->last->next = it;
    } else {
        list->first = it;
    }

    list->last = it;
    list->numItems++;
}

// ----------------------------------------------------------------------------
void listIterate(const cutilList* list, cutilFnItem fnItem, void* ptr)
{
    if (list == nullptr || fnItem == nullptr) {
        return;
    }

    for (const iterator_t* it = list->first; it != nullptr; it = it->next) {
        if (fnItem(it->item, ptr) != 0) {
            break;
        }
    }
}
