/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */

#include <stdlib.h>
#include <string.h>

#include "cutil_bytestream.h"
#include "cutil_bytewrite.h"
#include "cutil_byteswap.h"
#include "cutil_nullptr.h"

// ----------------------------------------------------------------------------
struct bytewrite_s {
    bytestream_t stream;
    uint8_t* data;
};

// ----------------------------------------------------------------------------
bytewrite_t* bytewriteCreate(void* data, size_t size)
{
    bytewrite_t* obj = (bytewrite_t*)malloc(sizeof(bytewrite_t));
    if (obj == nullptr) {
        return nullptr;
    }

    obj->stream.size = size;
    obj->stream.ptr = 0;
    obj->data = (uint8_t*)data;

    return obj;
}

// ----------------------------------------------------------------------------
void bytewriteDestroy(const bytewrite_t* obj)
{
    free((void*)obj);
}

// ----------------------------------------------------------------------------
size_t bytewrite_length(const bytewrite_t* obj)
{
    return obj->stream.ptr;
}

// ----------------------------------------------------------------------------
const void* bytewrite_data(const bytewrite_t* obj)
{
    return obj->data;
}

// ----------------------------------------------------------------------------
int bytewrite_int8(bytewrite_t* obj, uint8_t value)
{
    if (bytes_check(&obj->stream, 1)) {
        return -1;
    }

    obj->data[obj->stream.ptr++] = value;
    return 0;
}

// ----------------------------------------------------------------------------
int bytewrite_int16(bytewrite_t* obj, uint16_t value)
{
    if (bytes_check(&obj->stream, 2)) {
        return -1;
    }

    obj->data[obj->stream.ptr++] = (uint8_t)(value >> 8);
    obj->data[obj->stream.ptr++] = (uint8_t)(value);
    return 0;
}

// ----------------------------------------------------------------------------
int bytewrite_int32(bytewrite_t* obj, uint32_t value)
{
    if (bytes_check(&obj->stream, 4)) {
        return -1;
    }

    obj->data[obj->stream.ptr++] = (uint8_t)(value >> 24);
    obj->data[obj->stream.ptr++] = (uint8_t)(value >> 16);
    obj->data[obj->stream.ptr++] = (uint8_t)(value >> 8);
    obj->data[obj->stream.ptr++] = (uint8_t)(value);
    return 0;
}

// ----------------------------------------------------------------------------
int bytewrite_int64(bytewrite_t* obj, uint64_t value)
{
    if (bytes_check(&obj->stream, 8)) {
        return -1;
    }

    for (size_t i = 0; i < 8; i++) {
        obj->data[obj->stream.ptr++] = (uint8_t)(value >> (64 - 8 - 8 * i));
    }
    return 0;
}

// ----------------------------------------------------------------------------
int bytewrite_vint(bytewrite_t* obj, uint64_t value)
{
    if (value < 16384) {
        if (value < 64) {
            return bytewrite_int8(obj, (uint8_t)value);
        } else {
            return bytewrite_int16(obj, (uint16_t)value | 0x4000);
        }
    } else {
        if (value < 1073741824) {
            return bytewrite_int32(obj, (uint32_t)value | 0x80000000);
        } else {
            return bytewrite_int64(obj, (uint64_t)value | 0xc000000000000000);
        }
    }
}

// ----------------------------------------------------------------------------
int bytewrite_fourcc(bytewrite_t* obj, uint32_t value)
{
    if (bytes_check(&obj->stream, 4)) {
        return -1;
    }

    obj->data[obj->stream.ptr++] = (uint8_t)(value);
    obj->data[obj->stream.ptr++] = (uint8_t)(value >> 8);
    obj->data[obj->stream.ptr++] = (uint8_t)(value >> 16);
    obj->data[obj->stream.ptr++] = (uint8_t)(value >> 24);
    return 0;
}

// ----------------------------------------------------------------------------
int bytewrite_buffer(bytewrite_t* obj, const void* buffer, size_t length)
{
    if (bytes_check(&obj->stream, length)) {
        return -1;
    }

    memcpy(obj->data + obj->stream.ptr, buffer, length);
    obj->stream.ptr += length;
    return 0;
}
