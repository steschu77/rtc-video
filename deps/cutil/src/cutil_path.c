/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */

#include "cutil_path.h"

// ----------------------------------------------------------------------------
struct cutilPath_s {
    const utf8str_t* path;
    const utf8str_t* dir;
    const utf8str_t* file;
    const utf8str_t* ext;
};

// ----------------------------------------------------------------------------
const cutilPath* createPath(const utf8str_t* path)
{
    static const char* strEmpty = "";
    const char* strPath = utf8_cstr(path);
    const char* strLastSlash = strrchr(strPath, '/');
    const char* strLastDot = strrchr(strPath, '.');

    const char* strDir = strPath;
    const char* strName = (strLastSlash == nullptr) ? strPath : strLastSlash + 1;
    const char* strExt = (strLastDot == nullptr || strLastDot < strLastSlash) ? strEmpty : strLastDot;

    size_t lenExt = strlen(strExt);
    size_t lenName = strExt - strName;
    size_t lenDir = strName - strPath;

    cutilPath* obj = (cutilPath*)malloc(sizeof(cutilPath));
    if (obj == nullptr) {
        return nullptr;
    }

    obj->path = utf8_assign(path);
    obj->dir = utf8_make(strDir, lenDir);
    obj->file = utf8_make(strName, lenName);
    obj->ext = utf8_make(strExt, lenExt);

    return obj;
}

// ----------------------------------------------------------------------------
void destroyPath(const cutilPath* path)
{
    utf8_decref(path->path);
    utf8_decref(path->dir);
    utf8_decref(path->file);
    utf8_decref(path->ext);
    free((void*)path);
}

// ----------------------------------------------------------------------------
const utf8str_t* getPathName(const cutilPath* path)
{
    return path->path;
}

// ----------------------------------------------------------------------------
const utf8str_t* getDirectory(const cutilPath* path)
{
    return path->dir;
}

// ----------------------------------------------------------------------------
const utf8str_t* getFileName(const cutilPath* path)
{
    return path->file;
}

// ----------------------------------------------------------------------------
const utf8str_t* getExtension(const cutilPath* path)
{
    return path->ext;
}
