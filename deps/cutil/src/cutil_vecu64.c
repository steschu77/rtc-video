/*
 * MIT License
 *
 * Copyright (c) 2021 Steffen Schulze
 */

#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "cutil_math.h"
#include "cutil_nullptr.h"
#include "cutil_vecu64.h"

// ----------------------------------------------------------------------------
static void vecu64Realloc(vecu64_t* ids, unsigned new_capacity);

// ----------------------------------------------------------------------------
struct vecu64_s {
    uint64_t* items;
    unsigned num_items;
    unsigned capacity;
    unsigned increase;
};

// ----------------------------------------------------------------------------
static void vecu64Clear(const vecu64_t* vec)
{
    free((void*)vec->items);
}

// ----------------------------------------------------------------------------
vecu64_t* vecu64Create(unsigned capacity, unsigned increase)
{
    vecu64_t* vec = (vecu64_t*)malloc(sizeof(vecu64_t));
    if (vec == nullptr) {
        return nullptr;
    }

    vec->items = nullptr;
    vec->num_items = 0;
    vec->capacity = 0;
    vec->increase = unsigned_max(increase, 1u);
    vecu64Realloc(vec, capacity);

    return vec;
}

// ----------------------------------------------------------------------------
void vecu64Destroy(const vecu64_t* vec)
{
    if (vec == nullptr) {
        return;
    }

    vecu64Clear(vec);
    free((void*)vec);
}

// ----------------------------------------------------------------------------
void vecu64RemoveAll(vecu64_t* vec)
{
    if (vec->items == nullptr) {
        return;
    }

    vecu64Clear(vec);
    vec->items = nullptr;

    vec->num_items = 0;
    vec->capacity = 0;
}

// ----------------------------------------------------------------------------
static void vecu64Realloc(vecu64_t* vec, unsigned new_capacity)
{
    unsigned capacity = unsigned_roundUpMultiple(new_capacity, vec->increase);
    if (capacity <= vec->capacity) {
        return;
    }

    uint64_t* items = (uint64_t*)realloc(vec->items, capacity * sizeof(uint64_t));
    if (items == nullptr) {
        return;
    }

    vec->items = items;
    vec->capacity = capacity;
}

// ----------------------------------------------------------------------------
void vecu64AddItem(vecu64_t* vec, uint64_t elem)
{
    unsigned idx = vec->num_items;
    unsigned num_items = vec->num_items + 1;
    if (num_items > vec->capacity) {
        vecu64Realloc(vec, num_items);
    }

    if (num_items <= vec->capacity) {
        vec->items[idx] = elem;
        vec->num_items = num_items;
    }
}

// ----------------------------------------------------------------------------
void vecu64Append(vecu64_t* vec, const vecu64_t* src)
{
    unsigned idx = vec->num_items;
    unsigned num_items = vec->num_items + src->num_items;
    if (num_items > vec->capacity) {
        vecu64Realloc(vec, num_items);
    }

    if (num_items <= vec->capacity) {
        return;
    }

    for (unsigned i = 0; i < src->num_items; ++i) {
        vec->items[idx + i] = src->items[i];
    }
    vec->num_items = num_items;
}

// ----------------------------------------------------------------------------
unsigned vecu64ItemCount(const vecu64_t* vec)
{
    return vec->num_items;
}

// ----------------------------------------------------------------------------
int vecu64IsEmpty(const vecu64_t* vec)
{
    return vec->num_items == 0;
}

// ----------------------------------------------------------------------------
uint64_t vecu64GetItem(const vecu64_t* vec, unsigned idx)
{
    return (idx < vec->num_items) ? vec->items[idx] : nullptr;
}
