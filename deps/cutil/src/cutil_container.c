/*
 * MIT License
 *
 * Copyright (c) 2021 Steffen Schulze
 */

#include <stdlib.h>

#include "cutil_nullptr.h"
#include "cutil_container.h"

// ----------------------------------------------------------------------------
struct iterator_s;
typedef struct iterator_s iterator_t;

// ----------------------------------------------------------------------------
iterator_t* itCreate(const void* item)
{
    iterator_t* it = (iterator_t*)malloc(sizeof(iterator_t));
    if (it == nullptr) {
        return nullptr;
    }

    it->next = nullptr;
    it->item = item;
    return it;
}

// ----------------------------------------------------------------------------
void itDestroy(const iterator_t* it, cutilFnDestroy fnDestroy)
{
    if (fnDestroy != nullptr) {
        fnDestroy(it->item);
    }
    free((void*)it);
}
