/*
 * MIT License
 *
 * Copyright (c) 2022 Steffen Schulze
 */

#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "cutil_nullptr.h"
#include "cutil_random.h"

// ----------------------------------------------------------------------------
struct rnd_s {
    unsigned lin;
};

// ----------------------------------------------------------------------------
rnd_t* rndCreate(unsigned short seed)
{
    rnd_t* obj = (rnd_t*)malloc(sizeof(rnd_t));
    if (obj == nullptr) {
        return nullptr;
    }

    obj->lin = seed;

    return obj;
}

// ----------------------------------------------------------------------------
void rndDestroy(const rnd_t* obj)
{
    free((void*)obj);
}

// ----------------------------------------------------------------------------
unsigned short rndGenerate(rnd_t* obj)
{
    obj->lin = 0xabe1 * obj->lin + 43;
    return obj->lin & 0xffff;
}
