/*
 * MIT License
 *
 * Copyright (c) 2020 Steffen Schulze
 */

#pragma once

// ----------------------------------------------------------------------------
struct bytestream_s {
    size_t size;
    size_t ptr;
};

typedef struct bytestream_s bytestream_t;

// ----------------------------------------------------------------------------
inline int bytes_check(bytestream_t* s, size_t count)
{
    const size_t max_bytes = s->size - s->ptr;
    if (max_bytes < count) {
        s->ptr = s->size;
        return -1;
    }

    return 0;
}
