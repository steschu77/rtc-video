/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */

#include "cutil_refcount.h"

#if defined(_MSC_VER)
extern inline refcount_t incref(const volatile refcount_t* ref);
extern inline refcount_t decref(const volatile refcount_t* ref);
#else
extern inline uint_fast64_t incref(const volatile refcount_t* ref);
extern inline uint_fast64_t decref(const volatile refcount_t* ref);
#endif
