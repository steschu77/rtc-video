/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */

// do not include rare used Win32 Headers, but include WinXP stuff
#define WIN32_LEAN_AND_MEAN
#define _WIN32_WINNT 0x0501

#include <windows.h>

#include "cutil_file.h"
#include "cutil_refobj.h"

// ----------------------------------------------------------------------------
struct utilsFile_s {
    ref_t ref;
    const utf8str_t* path;
    void* handle;
    retcode rv;
};

// ----------------------------------------------------------------------------
static retcode mapWin32ErrorCode(DWORD error);

// ----------------------------------------------------------------------------
void destroyFile(const void* obj)
{
    utilsFile* file = (utilsFile*)obj;
    if (file == nullptr) {
        return;
    }

    utf8_free(&file->path);

    if (file->handle != INVALID_HANDLE_VALUE) {
        CloseHandle(file->handle);
    }
    free((void*)file);
}

// ----------------------------------------------------------------------------
static int isValidHandle(HANDLE h)
{
    return h != 0 && h != INVALID_HANDLE_VALUE;
}

// ----------------------------------------------------------------------------
static utilsFile* createFile(const utf8str_t* path,
    DWORD access, DWORD share, DWORD creation, DWORD attribs)
{
    const char* strPath = utf8_cstr(path);

    utilsFile* file = (utilsFile*)malloc(sizeof(utilsFile));
    if (file == nullptr) {
        return nullptr;
    }

    ref_init(&file->ref, file, destroyFile);
    file->path = utf8_assign(path);
    file->handle = CreateFileA(strPath, access, share,
        NULL, creation, attribs, NULL);
    file->rv = isValidHandle(file->handle) ? rcSuccess : mapWin32ErrorCode(GetLastError());
    
    return assignFile(file);
}

// ----------------------------------------------------------------------------
utilsFile* createFileRead(const utf8str_t* path)
{
    return createFile(path, GENERIC_READ, FILE_SHARE_READ,
        OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL);
}

// ----------------------------------------------------------------------------
utilsFile* createFileWrite(const utf8str_t* path, int overwrite)
{
    return createFile(path, GENERIC_WRITE, FILE_SHARE_WRITE,
        overwrite ? CREATE_ALWAYS : CREATE_NEW, FILE_ATTRIBUTE_NORMAL);
}

// ----------------------------------------------------------------------------
utilsFile* assignFile(utilsFile* obj)
{
    ref_inc(&obj->ref);
    return obj;
}

// ----------------------------------------------------------------------------
void freeFile(const utilsFile* obj)
{
    if (obj != nullptr) {
        ref_dec(&obj->ref);
    }
}

// ----------------------------------------------------------------------------
size_t readFile(utilsFile* file, void* data, size_t length)
{
    if (file->rv != rcSuccess) {
        return 0;
    }

    DWORD dwLength = (DWORD)length;
    if (dwLength != length) {
        file->rv = rcInvalidParam;
        return 0;
    }

    DWORD dwBytes = 0;
    if (!ReadFile(file->handle, data, dwLength, &dwBytes, nullptr)) {
        file->rv = rcReadingFile;
        return 0;
    }

    return dwBytes;
}

// ----------------------------------------------------------------------------
size_t writeFile(utilsFile* file, const void* data, size_t length)
{
    if (file->rv != rcSuccess) {
        return 0;
    }

    DWORD dwLength = (DWORD)length;
    if (dwLength != length) {
        file->rv = rcInvalidParam;
        return 0;
    }

    DWORD dwBytes = 0;
    if (!WriteFile(file->handle, data, dwLength, &dwBytes, nullptr)) {
        file->rv = rcWritingFile;
        return 0;
    }

    return dwBytes;
}

// ----------------------------------------------------------------------------
size_t seekFile(utilsFile* file, size_t length)
{
    if (file->rv != rcSuccess) {
        return 0;
    }

    LARGE_INTEGER distance, newPos;
    distance.QuadPart = (LONGLONG)length;
    SetFilePointerEx(file->handle, distance, &newPos, FILE_BEGIN);

    return (size_t)newPos.QuadPart;
}

// ----------------------------------------------------------------------------
const utf8str_t* getFilePath(const utilsFile* file)
{
    return file->path;
}

// ----------------------------------------------------------------------------
retcode getFileResult(const utilsFile* file)
{
    return file->rv;
}

// ----------------------------------------------------------------------------
uint64_t getFileSize(const utilsFile* file)
{
    LARGE_INTEGER size;
    if (!GetFileSizeEx(file->handle, &size)) {
        return 0;
    }

    return size.QuadPart;
}

// ----------------------------------------------------------------------------
retcode mapWin32ErrorCode(DWORD error)
{
    switch (error) {
    case NO_ERROR:
        return rcSuccess;
    case ERROR_FILE_NOT_FOUND:
        return rcFileNotFound;
    case ERROR_PATH_NOT_FOUND:
        return rcPathNotFound;
    case ERROR_TOO_MANY_OPEN_FILES:
        return rcTooManyOpenFiles;
    case ERROR_ACCESS_DENIED:
        return rcAccessDenied;
    case ERROR_ALREADY_EXISTS:
        return rcAlreadyExists;
    default:
        return rcFailed;
    }
}
