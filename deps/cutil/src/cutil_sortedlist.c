/*
 * MIT License
 *
 * Copyright (c) 2021 Steffen Schulze
 */

#include <stdlib.h>

#include "cutil_sortedlist.h"
#include "cutil_nullptr.h"

// ----------------------------------------------------------------------------
struct sortedlist_s {
    iterator_t* first;
    unsigned numItems;
};

// ----------------------------------------------------------------------------
static void slistClear(const sortedlist_t*, cutilFnDestroy);
static iterator_t*const* slistFindIt(const sortedlist_t*, cutilFnPredicate, const void*);

// ----------------------------------------------------------------------------
void* slistCreate()
{
    sortedlist_t* obj = (sortedlist_t*)malloc(sizeof(sortedlist_t));
    if (obj == nullptr) {
        return nullptr;
    }

    obj->first = nullptr;
    obj->numItems = 0;
    return obj;
}

// ----------------------------------------------------------------------------
void slistDestroy(const void* ptr, cutilFnDestroy fnDestroy)
{
    const sortedlist_t* obj = (const sortedlist_t*)ptr;
    if (obj == nullptr) {
        return;
    }

    slistClear(obj, fnDestroy);
    free((void*)obj);
}

// ----------------------------------------------------------------------------
void slistInsertItem(void* ptr, cutilFnCompare fnCompare, const void* item)
{
    sortedlist_t* obj = (sortedlist_t*)ptr;

    iterator_t* it = itCreate(item);
    if (it == nullptr) {
        return;
    }

    iterator_t** last = &obj->first;
    while (*last != nullptr) {
        if (fnCompare(item, (*last)->item) < 0) {
            break;
        }
        last = &(*last)->next;
    }

    it->next = *last;
    *last = it;

    obj->numItems++;
}

// ----------------------------------------------------------------------------
void slistRemoveAll(void* ptr, cutilFnDestroy fnDestroy)
{
    sortedlist_t* obj = (sortedlist_t*)ptr;

    slistClear(obj, fnDestroy);
    obj->first = nullptr;
    obj->numItems = 0;
}

// ----------------------------------------------------------------------------
const void* slistRemoveItem(void* ptr, cutilFnPredicate fnPredicate, const void* context)
{
    sortedlist_t* obj = (sortedlist_t*)ptr;

    iterator_t** last = (iterator_t**)slistFindIt(obj, fnPredicate, context);
    if (last == nullptr) {
        return nullptr;
    }

    const iterator_t* node = *last;
    const void* item = node->item;
    (*last) = (*last)->next;
    itDestroy(node, nullptr);
    obj->numItems--;

    return item;
}

// ----------------------------------------------------------------------------
const void* slistFindItem(const void* ptr, cutilFnPredicate fnPredicate, const void* context)
{
    const sortedlist_t* obj = (const sortedlist_t*)ptr;

    iterator_t*const* last = slistFindIt(obj, fnPredicate, context);
    if (last == nullptr) {
        return nullptr;
    }

    return (*last)->item;
}

// ----------------------------------------------------------------------------
void slistMerge(void* pdst, void* psrc, cutilFnCompare fnCompare)
{
    sortedlist_t* dst = (sortedlist_t*)pdst;
    sortedlist_t* src = (sortedlist_t*)psrc;

    iterator_t* it0 = dst->first;
    iterator_t* it1 = src->first;

    iterator_t* first = nullptr;
    iterator_t** it = &first;

    while (it0 != nullptr && it1 != nullptr) {

        int x = fnCompare(it0->item, it1->item);

        if (x < 0) {
            *it = it0;
            it0 = it0->next;
        } else {
            *it = it1;
            it1 = it1->next;
        }

        it = &(*it)->next;
    }

    if (it0 != nullptr) {
        *it = it0;
    }

    if (it1 != nullptr) {
        *it = it1;
    }

    dst->first = first;
    dst->numItems += src->numItems;

    src->first = nullptr;
    src->numItems = 0;
}

// ----------------------------------------------------------------------------
void slistFilter(void* ptr, cutilFnItem fnItem, cutilFnDestroy fnDestroy, void* context)
{
    sortedlist_t* obj = (sortedlist_t*)ptr;

    iterator_t** prev = &obj->first;
    while (*prev != nullptr) {

        iterator_t* it = *prev;

        if (fnItem(it->item, context) != 0) {
            *prev = it->next;
            itDestroy(it, fnDestroy);
            obj->numItems--;
        } else {
            prev = &(*prev)->next;
        }
    }
}

// ----------------------------------------------------------------------------
void slistIterate(const void* ptr, cutilFnItem fnItem, void* context)
{
    const sortedlist_t* obj = (const sortedlist_t*)ptr;

    if (obj == nullptr) {
        return;
    }

    for (const iterator_t* it = obj->first; it != nullptr; it = it->next) {
        if (fnItem(it->item, context) != 0) {
            break;
        }
    }
}

// ----------------------------------------------------------------------------
int slistIsEmpty(const void* ptr)
{
    const sortedlist_t* obj = (const sortedlist_t*)ptr;
    return obj->first == nullptr;
}

// ----------------------------------------------------------------------------
unsigned slistGetLength(const void* ptr)
{
    const sortedlist_t* obj = (const sortedlist_t*)ptr;
    return obj->numItems;
}

// ----------------------------------------------------------------------------
const void* slistPopItem(void* ptr)
{
    sortedlist_t* obj = (sortedlist_t*)ptr;
    const void* item = nullptr;

    if (obj->first != nullptr) {
        item = obj->first->item;
        obj->first = obj->first->next;
    }

    return item;
}

// ----------------------------------------------------------------------------
const void* slistFirst(const void* ptr)
{
    const sortedlist_t* obj = (const sortedlist_t*)ptr;
    return obj->first != nullptr ? obj->first->item : nullptr;
}

// ----------------------------------------------------------------------------
static void slistClear(const sortedlist_t* obj, cutilFnDestroy fnDestroy)
{
    const iterator_t* next = obj->first;

    while (next != nullptr) {
        const iterator_t* it = next;
        next = it->next;
        itDestroy(it, fnDestroy);
    }
}

// ----------------------------------------------------------------------------
static iterator_t*const* slistFindIt(const sortedlist_t* obj, cutilFnPredicate fnPredicate, const void* context)
{
    iterator_t*const* last = &obj->first;
    while (*last != nullptr) {
        int r = fnPredicate(context, (*last)->item);
        if (r == 0) {
            return last;
        } else if (r < 0) {
            return nullptr;
        }
        last = &(*last)->next;
    }

    return nullptr;
}
