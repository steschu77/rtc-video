/*
 * MIT License
 *
 * Copyright (c) 2021 Steffen Schulze
 */

#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "cutil_bitreader.h"
#include "cutil_bytestream.h"
#include "cutil_byteswap.h"
#include "cutil_nullptr.h"

// ----------------------------------------------------------------------------
struct bitread_s
{
    bytestream_t stream;
    const uint8_t* data;
};

// ----------------------------------------------------------------------------
bitread_t* bitreadCreate(const void* data, size_t size)
{
    bitread_t* obj = (bitread_t*)malloc(sizeof(bitread_t));
    if (obj == nullptr) {
        return nullptr;
    }

    obj->stream.size = size;
    obj->stream.ptr = 0;
    obj->data = (const uint8_t*)data;

    return obj;
}

// ----------------------------------------------------------------------------
void bitreadDestroy(const bitread_t* obj)
{
    if (obj != nullptr) {
        free((void*)obj);
    }
}

// ----------------------------------------------------------------------------
size_t bitreadBitCount(const bitread_t* obj)
{
    return obj->stream.ptr;
}

// ----------------------------------------------------------------------------
size_t bitreadFlushBits(bitread_t* obj, unsigned bits)
{
    obj->stream.ptr += bits;
    return obj->stream.ptr;
}

// ----------------------------------------------------------------------------
size_t bitreadAlign(bitread_t* obj)
{
    obj->stream.ptr += 7 - ((obj->stream.ptr + 7) & 7);
    return obj->stream.ptr;
}

// ----------------------------------------------------------------------------
uint32_t bitreadGetBits(bitread_t* obj, unsigned bits)
{
    assert(bits <= 25);

    const uint8_t* ptr = obj->data + (obj->stream.ptr >> 3);
    const size_t shift = 32 - (obj->stream.ptr & 7) - bits;
    const uint32_t mask = (1 << bits) - 1;
    obj->stream.ptr += bits;
    return (byteswap32(((const uint32_t*)ptr)[0]) >> shift) & mask;
}

// ----------------------------------------------------------------------------
uint8_t bitreadGetBit(bitread_t* obj)
{
    const size_t ptr = obj->stream.ptr >> 3;
    const size_t shift = (obj->stream.ptr & 7) ^ 7;
    obj->stream.ptr++;
    return (obj->data[ptr] >> shift) & 1;
}

// ----------------------------------------------------------------------------
uint16_t bitreadGetBits2(bitread_t* obj)
{
    const uint8_t *ptr = obj->data + (obj->stream.ptr >> 3);
    size_t shift = 14 - (obj->stream.ptr & 7);
    obj->stream.ptr += 2;
    return (byteswap16(((const uint16_t*)ptr)[0]) >> shift) & 3;
}

// ----------------------------------------------------------------------------
uint16_t bitreadGetBits3(bitread_t* obj)
{
    const uint8_t *ptr = obj->data + (obj->stream.ptr >> 3);
    size_t shift = 13 - (obj->stream.ptr & 7);
    obj->stream.ptr += 3;
    return (byteswap16(((const uint16_t*)ptr)[0]) >> shift) & 7;
}

// ----------------------------------------------------------------------------
uint16_t bitreadGetBits4(bitread_t* obj)
{
    const uint8_t *ptr = obj->data + (obj->stream.ptr >> 3);
    size_t shift = 12 - (obj->stream.ptr & 7);
    obj->stream.ptr += 4;
    return (byteswap16(((const uint16_t*)ptr)[0]) >> shift) & 15;
}

// ----------------------------------------------------------------------------
uint16_t bitreadGetBits5(bitread_t* obj)
{
    const uint8_t* ptr = obj->data + (obj->stream.ptr >> 3);
    size_t shift = 11 - (obj->stream.ptr & 7);
    obj->stream.ptr += 5;
    return (byteswap16(((const uint16_t*)ptr)[0]) >> shift) & 31;
}

// ----------------------------------------------------------------------------
uint16_t bitreadGetBits6(bitread_t* obj)
{
    const uint8_t* ptr = obj->data + (obj->stream.ptr >> 3);
    size_t shift = 10 - (obj->stream.ptr & 7);
    obj->stream.ptr += 6;
    return (byteswap16(((const uint16_t*)ptr)[0]) >> shift) & 63;
}

// ----------------------------------------------------------------------------
uint16_t bitreadGetBits7(bitread_t* obj)
{
    const uint8_t* ptr = obj->data + (obj->stream.ptr >> 3);
    size_t shift = 9 - (obj->stream.ptr & 7);
    obj->stream.ptr += 7;
    return (byteswap16(((const uint16_t*)ptr)[0]) >> shift) & 127;
}

// ----------------------------------------------------------------------------
uint16_t bitreadGetBits8(bitread_t* obj)
{
    const uint8_t* ptr = obj->data + (obj->stream.ptr >> 3);
    size_t shift = 8 - (obj->stream.ptr & 7);
    obj->stream.ptr += 8;
    return (byteswap16(((const uint16_t*)ptr)[0]) >> shift) & 255;
}

// ----------------------------------------------------------------------------
uint16_t bitreadGetBits9(bitread_t* obj);
// ----------------------------------------------------------------------------
uint32_t bitreadGetbits10(bitread_t* obj);
// ----------------------------------------------------------------------------
uint32_t bitreadGetbits11(bitread_t* obj);
// ----------------------------------------------------------------------------
uint32_t bitreadGetbits12(bitread_t* obj);
// ----------------------------------------------------------------------------
uint32_t bitreadGetbits13(bitread_t* obj);
// ----------------------------------------------------------------------------
uint32_t bitreadGetbits14(bitread_t* obj);
// ----------------------------------------------------------------------------
uint32_t bitreadGetbits15(bitread_t* obj);
// ----------------------------------------------------------------------------
uint32_t bitreadGetbits16(bitread_t* obj);
// ----------------------------------------------------------------------------
uint32_t bitreadGetbits17(bitread_t* obj);
// ----------------------------------------------------------------------------
uint32_t bitreadGetbits18(bitread_t* obj);
// ----------------------------------------------------------------------------
uint32_t bitreadGetbits19(bitread_t* obj);
// ----------------------------------------------------------------------------
uint32_t bitreadGetbits20(bitread_t* obj);
// ----------------------------------------------------------------------------
uint32_t bitreadGetbits21(bitread_t* obj);
// ----------------------------------------------------------------------------
uint32_t bitreadGetbits22(bitread_t* obj);
// ----------------------------------------------------------------------------
uint32_t bitreadGetbits23(bitread_t* obj);
// ----------------------------------------------------------------------------
uint32_t bitreadGetbits24(bitread_t* obj);
// ----------------------------------------------------------------------------
uint32_t bitreadGetbits32(bitread_t* obj);
