/*
 * MIT License
 *
 * Copyright (c) 2020 Steffen Schulze
 */

#include <stdlib.h>
#include <string.h>

#include "cutil_bytestream.h"
#include "cutil_byteread.h"
#include "cutil_byteswap.h"
#include "cutil_nullptr.h"

// ----------------------------------------------------------------------------
struct byteread_s {
    bytestream_t stream;
    const uint8_t* data;
};

// ----------------------------------------------------------------------------
byteread_t* bytereadCreate(const void* data, size_t size)
{
    byteread_t* obj = (byteread_t*)malloc(sizeof(byteread_t));
    if (obj == nullptr) {
        return nullptr;
    }

    obj->stream.size = size;
    obj->stream.ptr = 0;
    obj->data = (const uint8_t*)data;

    return obj;
}

// ----------------------------------------------------------------------------
void bytereadDestroy(const byteread_t* obj)
{
    if (obj != nullptr) {
        free((void*)obj);
    }
}

// ----------------------------------------------------------------------------
size_t byteread_remain(const byteread_t* obj)
{
    return obj->stream.size - obj->stream.ptr;
}

// ----------------------------------------------------------------------------
const void* byteread_data(const byteread_t* obj)
{
    return obj->data;
}

// ----------------------------------------------------------------------------
const void* byteread_ptr(const byteread_t* obj)
{
    return obj->data + obj->stream.ptr;
}

// ----------------------------------------------------------------------------
int byteread_int8(byteread_t* obj, uint8_t* value)
{
    if (bytes_check(&obj->stream, 1)) {
        return -1;
    }

    *value = obj->data[obj->stream.ptr++];
    return 0;
}

// ----------------------------------------------------------------------------
int byteread_int16(byteread_t* obj, uint16_t* value)
{
    if (bytes_check(&obj->stream, 2)) {
        return -1;
    }

    const uint8_t* ptr = obj->data + obj->stream.ptr;
    *value = (ptr[0] << 8) | ptr[1];
    obj->stream.ptr += 2;
    return 0;
}

// ----------------------------------------------------------------------------
int byteread_int32(byteread_t* obj, uint32_t* value)
{
    if (bytes_check(&obj->stream, 4)) {
        return -1;
    }

    const uint8_t* ptr = obj->data + obj->stream.ptr;
    *value = (ptr[0] << 24) | (ptr[1] << 16) | (ptr[2] << 8) | ptr[3];
    obj->stream.ptr += 4;
    return 0;
}

// ----------------------------------------------------------------------------
int byteread_int64(byteread_t* obj, uint64_t* value)
{
    if (bytes_check(&obj->stream, 8)) {
        return -1;
    }

    uint64_t v = 0;
    for (size_t i = 0; i < 8; i++) {
        v <<= 8;
        v += obj->data[obj->stream.ptr++];
    }
    *value = v;
    return 0;
}

// ----------------------------------------------------------------------------
int byteread_vint(byteread_t* obj, uint64_t* value)
{
    if (bytes_check(&obj->stream, 1)) {
        return -1;
    }

    const size_t length = 1ull << ((obj->data[obj->stream.ptr] >> 6) & 3);
    if (bytes_check(&obj->stream, length)) {
        return -1;
    }

    uint64_t v = obj->data[obj->stream.ptr++] & 0x3f;
    for (size_t i = 1; i < length; i++) {
        v <<= 8;
        v += obj->data[obj->stream.ptr++];
    }

    *value = v;
    return 0;
}

// ----------------------------------------------------------------------------
int byteread_buffer(byteread_t* obj, void* buffer, size_t length)
{
    if (bytes_check(&obj->stream, length)) {
        return -1;
    }

    memcpy(buffer, obj->data + obj->stream.ptr, length);
    obj->stream.ptr += length;
    return 0;
}

// ----------------------------------------------------------------------------
int byteread_skip(byteread_t* obj, size_t length)
{
    if (bytes_check(&obj->stream, length)) {
        return -1;
    }

    obj->stream.ptr += length;
    return 0;
}
