/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */

#include "cutil_math.h"

// ----------------------------------------------------------------------------
extern inline unsigned unsigned_max(unsigned x0, unsigned x1);
extern inline unsigned unsigned_min(unsigned x0, unsigned x1);
extern inline unsigned unsigned_clamp(unsigned x, unsigned x0, unsigned x1);
extern inline unsigned unsigned_roundUpMultiple(unsigned x, unsigned multiple);
extern inline uint64_t uint64_max(uint64_t x0, uint64_t x1);
extern inline uint64_t uint64_min(uint64_t x0, uint64_t x1);
extern inline uint64_t uint64_clamp(uint64_t x, uint64_t x0, uint64_t x1);
extern inline size_t size_t_max(size_t x0, size_t x1);
extern inline size_t size_t_min(size_t x0, size_t x1);
extern inline size_t size_t_clamp(size_t x, size_t x0, size_t x1);
extern inline size_t size_t_roundUpMultiple(size_t x, size_t multiple);
extern inline double double_max(double x0, double x1);
extern inline double double_min(double x0, double x1);
extern inline double double_clamp(double x, double x0, double x1);
