/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */

#include "cutil_bitreader.c"
#include "cutil_buffer.c"
#include "cutil_byteread.c"
#include "cutil_bytestream.c"
#include "cutil_byteswap.c"
#include "cutil_bytewrite.c"
#include "cutil_container.c"
#include "cutil_file.c"
#include "cutil_list.c"
#include "cutil_math.c"
#include "cutil_path.c"
#include "cutil_random.c"
#include "cutil_refcount.c"
#include "cutil_refobj.c"
#include "cutil_sortedlist.c"
#include "cutil_tree.c"
#include "cutil_utf8str.c"
#include "cutil_vecbyte.c"
#include "cutil_vecptr.c"
#include "cutil_vecu64.c"

#if defined(_MSC_VER)
#include "cutil_file_win32.c"
#include "cutil_getopt.c"
#else
#include "cutil_file_posix.c"
#endif
