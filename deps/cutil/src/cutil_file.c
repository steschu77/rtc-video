/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */

#include "cutil_file.h"
// ----------------------------------------------------------------------------
#define BUFSIZE 4096
int fileCompare(utilsFile* f0, utilsFile* f1)
{
    if (getFileSize(f0) != getFileSize(f1)) {
        return -1;
    }

    size_t len0 = 0, len1 = 0;
    do {
        uint8_t buf0[BUFSIZE];
        uint8_t buf1[BUFSIZE];

        len0 = readFile(f0, buf0, BUFSIZE);
        len1 = readFile(f1, buf1, BUFSIZE);

        if (len0 != len1 || memcmp(buf0, buf1, len0) != 0) {
            return -1;
        }
    } while (len0 == BUFSIZE);

    return 0;
}
