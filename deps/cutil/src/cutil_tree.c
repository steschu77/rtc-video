#include <assert.h>
#include <stdlib.h>

#include "cutil_nullptr.h"
#include "cutil_tree.h"
#include "cutil_tree_internals.h"

/* https://en.wikipedia.org/wiki/Splay_tree */
extern inline void setChild(cutilTreeNode* parent, cutilTreeNode* child, int idx);

// ----------------------------------------------------------------------------
static cutilTreeNode* treeCreateNode(const void* item)
{
    cutilTreeNode* node = (cutilTreeNode*)malloc(sizeof(cutilTreeNode));
    if (node == nullptr) {
        return nullptr;
    }

    node->item = item;

    node->parent = nullptr;
    node->childs[c_left] = nullptr;
    node->childs[c_right] = nullptr;
    node->parent_idx = 0;

    return node;
}

// ----------------------------------------------------------------------------
static void treeRotate(cutilTreeNode* child)
{
    cutilTreeNode* parent = child->parent;
    cutilTreeNode* grand = parent->parent;

    int c_idx = child->parent_idx;

    child->parent = nullptr;
    if (grand != nullptr) {
        setChild(grand, child, parent->parent_idx);
    }

    setChild(parent, child->childs[1 - c_idx], c_idx);
    setChild(child, parent, 1 - c_idx);
}

// ----------------------------------------------------------------------------
static void treeSplay(tree_t* tree, cutilTreeNode* x)
{
    while (1) {
        cutilTreeNode* p = x->parent;
        if (p == nullptr) {
            tree->root = x;
            return;
        }
        if (p->parent == nullptr) {
            treeRotate(x);
        } else if (x->parent_idx == p->parent_idx) {
            treeRotate(p);
            treeRotate(x);
        } else {
            treeRotate(x);
            treeRotate(x);
        }
    }
}

// ----------------------------------------------------------------------------
static cutilTreeNode* treeOutermost(cutilTreeNode* node, int idx)
{
    if (node != nullptr) {
        while (node->childs[idx] != nullptr) {
            node = node->childs[idx];
        }
    }
    return node;
}

// ----------------------------------------------------------------------------
static const cutilTreeNode* treeNextNode(const cutilTreeNode* node)
{
    if (node->childs[c_right] != nullptr) {
        return treeOutermost(node->childs[c_right], c_left);
    }

    while (node->parent != nullptr && node->parent_idx == c_right) {
        node = node->parent;
    }

    return node->parent;
}

// ----------------------------------------------------------------------------
static cutilTreeNode* treeFindNode(tree_t* tree, cutilFnPredicate fnPredicate, const void* context)
{
    cutilTreeNode* curr = tree->root;
    while (curr != nullptr) {
        int relation = fnPredicate(context, curr->item);
        if (relation == 0) {
            return curr;
        } else {
            curr = curr->childs[relation < 0];
        }
    }

    return nullptr;
}

// ----------------------------------------------------------------------------
static void treeInsertNode(tree_t* tree, cutilFnCompare fnCompare, cutilTreeNode* node)
{
    if (tree->root == nullptr) {
        tree->root = node;
    } else {
        cutilTreeNode* curr = tree->root;
        cutilTreeNode* parent = nullptr;
        int idx = 0;

        while (curr != nullptr) {
            parent = curr;

            idx = fnCompare(node->item, curr->item) < 0;
            curr = curr->childs[idx];
        }

        setChild(parent, node, idx);
    }

    treeSplay(tree, node);
    tree->numItems++;
}

// ----------------------------------------------------------------------------
static void treeDeleteNode(tree_t* tree, cutilTreeNode* node)
{
    if (node == nullptr) {
        return;
    }

    treeSplay(tree, node);

    cutilTreeNode* left = node->childs[c_left];
    cutilTreeNode* right = node->childs[c_right];

    if (left == nullptr && right == nullptr) {
        tree->root = nullptr;
    } else if (left == nullptr) {
        tree->root = right;
        tree->root->parent = nullptr;
    } else if (right == nullptr) {
        tree->root = left;
        tree->root->parent = nullptr;
    } else {
        cutilTreeNode* x = treeOutermost(right, c_left);
        if (x->parent != node) {
            setChild(x->parent, x->childs[c_right], c_left);
            setChild(x, node->childs[c_right], c_right);
        }

        tree->root = x;
        x->parent = nullptr;

        setChild(x, node->childs[c_left], c_left);
    }

    free(node);
    tree->numItems--;
}

// ----------------------------------------------------------------------------
void* treeCreate()
{
    tree_t* tree = (tree_t*)malloc(sizeof(tree_t));
    if (tree == nullptr) {
        return nullptr;
    }

    tree->root = nullptr;
    tree->numItems = 0;
    return tree;
}

// ----------------------------------------------------------------------------
void treeDestroy(const void* ptr, cutilFnDestroy fnItem)
{
    const tree_t* obj = (const tree_t*)ptr;

    treeRemoveAllItems((tree_t*)obj, fnItem);
    free((void*)obj);
}

// ----------------------------------------------------------------------------
void treeInsertItem(void* ptr, cutilFnCompare fnCompare, const void* item)
{
    tree_t* obj = (tree_t*)ptr;

    cutilTreeNode* node = treeCreateNode(item);
    if (node == nullptr) {
        return;
    }

    treeInsertNode(obj, fnCompare, node);
}

// ----------------------------------------------------------------------------
const void* treeRemoveItem(void* ptr, cutilFnPredicate fnPredicate, const void* context)
{
    tree_t* obj = (tree_t*)ptr;
    const void* item = nullptr;

    cutilTreeNode* node = treeFindNode(obj, fnPredicate, context);
    if (node != nullptr) {
        item = node->item;
        treeDeleteNode(obj, node);
    }

    return item;
}

// ----------------------------------------------------------------------------
void treeRemoveAllItems(void* ptr, cutilFnDestroy fnItem)
{
    tree_t* obj = (tree_t*)ptr;

    while (obj->root != nullptr) {
        cutilTreeNode* node = obj->root;
        if (fnItem != nullptr) {
            fnItem(node->item);
        }
        treeDeleteNode(obj, node);
    }
}

// ----------------------------------------------------------------------------
int treeIsEmpty(const void* ptr)
{
    const tree_t* obj = (const tree_t*)ptr;
    return obj->root == nullptr;
}

// ----------------------------------------------------------------------------
unsigned treeGetLength(const void* ptr)
{
    const tree_t* obj = (const tree_t*)ptr;
    return obj->numItems;
}

// ----------------------------------------------------------------------------
const void* treeFindItem(const void* ptr, cutilFnPredicate fnPredicate, const void* context)
{
    const tree_t* obj = (const tree_t*)ptr;

    cutilTreeNode* node = treeFindNode((tree_t*)obj, fnPredicate, context);
    return (node != nullptr) ? node->item : nullptr;
}

// ----------------------------------------------------------------------------
void treeIterate(const void* ptr, cutilFnItem fnItem, void* context)
{
    const tree_t* obj = (const tree_t*)ptr;

    const cutilTreeNode* next = treeOutermost(obj->root, c_left);
    while (next != nullptr) {
        if (fnItem(next->item, context) != 0) {
            break;
        }
        next = treeNextNode(next);
    }
}

// ----------------------------------------------------------------------------
const void* treePopItem(void* ptr)
{
    tree_t* obj = (tree_t*)ptr;

    cutilTreeNode* node = treeOutermost(obj->root, c_left);
    if (node == nullptr) {
        return nullptr;
    }

    const void* item = node->item;
    treeDeleteNode(obj, node);

    return item;
}

// ----------------------------------------------------------------------------
const void* treeFirst(const void* ptr)
{
    tree_t* obj = (tree_t*)ptr;

    cutilTreeNode* node = treeOutermost(obj->root, c_left);
    if (node == nullptr) {
        return nullptr;
    }

    return node->item;
}
