/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */

#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "cutil_math.h"
#include "cutil_nullptr.h"
#include "cutil_vecptr.h"

// ----------------------------------------------------------------------------
void vecRealloc(vecptr_t* ids, unsigned new_capacity);

// ----------------------------------------------------------------------------
struct vecptr_s {
    void** items;
    unsigned num_items;
    unsigned capacity;
    unsigned increase;
    cutilFnDestroy fnDestroy;
};

// ----------------------------------------------------------------------------
vecptr_t* vecptrCreate(unsigned capacity, unsigned increase, cutilFnDestroy fnDestroy)
{
    vecptr_t* vec = (vecptr_t*)malloc(sizeof(vecptr_t));
    if (vec == nullptr) {
        return nullptr;
    }

    vec->items = nullptr;
    vec->num_items = 0;
    vec->capacity = 0;
    vec->increase = unsigned_max(increase, 1u);
    vec->fnDestroy = fnDestroy;
    vecRealloc(vec, capacity);

    return vec;
}

// ----------------------------------------------------------------------------
void vecptrDestroy(vecptr_t* vec)
{
    if (vec == nullptr) {
        return;
    }

    vecptrRemoveAll(vec);

    free((void*)vec);
}

// ----------------------------------------------------------------------------
void vecptrRemoveAll(vecptr_t* vec)
{
    if (vec->items == nullptr) {
        return;
    }

    if (vec->fnDestroy != nullptr) {
        for (unsigned i = 0u; i < vec->num_items; ++i) {
            vec->fnDestroy(vec->items[i]);
        }
    }

    free((void*)vec->items);
    vec->items = nullptr;

    vec->num_items = 0;
    vec->capacity = 0;
}

// ----------------------------------------------------------------------------
void vecRealloc(vecptr_t* vec, unsigned new_capacity)
{
    unsigned capacity = unsigned_roundUpMultiple(new_capacity, vec->increase);
    if (capacity <= vec->capacity) {
        return;
    }

    void** items = (void**)realloc(vec->items, capacity * sizeof(void*));
    if (items == nullptr) {
        return;
    }

    vec->items = items;
    vec->capacity = capacity;
}

// ----------------------------------------------------------------------------
void vecptrAddItem(vecptr_t* vec, void* elem)
{
    unsigned idx = vec->num_items;
    unsigned num_items = vec->num_items + 1;
    if (num_items > vec->capacity) {
        vecRealloc(vec, num_items);
    }

    if (num_items <= vec->capacity) {
        vec->items[idx] = elem;
        vec->num_items = num_items;
    }
}

// ----------------------------------------------------------------------------
void vecptrAppend(vecptr_t* vec, const vecptr_t* src)
{
    unsigned idx = vec->num_items;
    unsigned num_items = vec->num_items + src->num_items;
    if (num_items > vec->capacity) {
        vecRealloc(vec, num_items);
    }

    if (num_items <= vec->capacity) {
        return;
    }

    for (unsigned i = 0; i < src->num_items; ++i) {
        vec->items[idx + i] = src->items[i];
    }
    vec->num_items = num_items;
}

// ----------------------------------------------------------------------------
unsigned vecptrItemCount(const vecptr_t* vec)
{
    return vec->num_items;
}

// ----------------------------------------------------------------------------
int vecptrIsEmpty(const vecptr_t* vec)
{
    return vec->num_items == 0;
}

// ----------------------------------------------------------------------------
void* vecptrGetItem(const vecptr_t* vec, unsigned idx)
{
    return (idx < vec->num_items) ? vec->items[idx] : nullptr;
}

// ----------------------------------------------------------------------------
void vecptrIterateItems(const vecptr_t* vec, cutilFnItem fnItem, void* context)
{
    for (unsigned i = 0u; i < vec->num_items; ++i) {
        if (fnItem(vec->items[i], context) != 0) {
            break;
        }
    }
}
