/*
 * MIT License
 *
 * Copyright (c) 2020 Steffen Schulze
 */

#include "cutil_bytestream.h"

// ----------------------------------------------------------------------------
extern inline int bytes_check(bytestream_t* s, size_t count);
