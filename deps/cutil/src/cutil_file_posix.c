/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */

#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <wordexp.h>

#include "cutil_file.h"
#include "cutil_refobj.h"

#define PATH_MAX 4096

// ----------------------------------------------------------------------------
struct utilsFile_s {
    ref_t ref;
    const utf8str_t* path;
    int handle;
    retcode rv;
};

// ----------------------------------------------------------------------------
static retcode mapPosixErrorCode();

// ----------------------------------------------------------------------------
void destroyFile(const void* obj)
{
    utilsFile* file = (utilsFile*)obj;
    if (file == nullptr) {
        return;
    }

    utf8_free(&file->path);

    if (file->handle != -1) {
        close(file->handle);
    }
    free((void*)file);
}

// ----------------------------------------------------------------------------
utilsFile* createFile(const utf8str_t* path, unsigned mode, mode_t permissions)
{
    utilsFile* file = (utilsFile*)malloc(sizeof(utilsFile));
    if (file == nullptr) {
        return nullptr;
    }

    char resolved_name[PATH_MAX + 1];
    char* absolute;

    wordexp_t p;
    if (wordexp(utf8_cstr(path), &p, 0) == 0) {
        absolute = realpath(p.we_wordv[0], resolved_name);
        wordfree(&p);
    } else {
        absolute = realpath(utf8_cstr(path), resolved_name);
    }

    if (absolute || errno == 0x2) {
        file->handle = open(resolved_name, mode, permissions);
    }

    ref_init(&file->ref, file, destroyFile);
    file->path = utf8_assign(path);
    file->rv = mapPosixErrorCode();
    
    return assignFile(file);
}

// ----------------------------------------------------------------------------
utilsFile* createFileRead(const utf8str_t* path)
{
    return createFile(path, O_RDONLY, 0);
}

// ----------------------------------------------------------------------------
utilsFile* createFileWrite(const utf8str_t* path, int overwrite)
{
    return createFile(path, O_WRONLY | O_NONBLOCK | O_CREAT | (overwrite ? O_TRUNC : O_EXCL), S_IRUSR | S_IWUSR);
}

// ----------------------------------------------------------------------------
utilsFile* assignFile(utilsFile* obj)
{
    ref_inc(&obj->ref);
    return obj;
}

// ----------------------------------------------------------------------------
void freeFile(const utilsFile* obj)
{
    if (obj != nullptr) {
        ref_dec(&obj->ref);
    }
}

// ----------------------------------------------------------------------------
size_t readFile(utilsFile* file, void* data, size_t length)
{
    return read(file->handle, data, length);
}

// ----------------------------------------------------------------------------
size_t writeFile(utilsFile* file, const void* data, size_t length)
{
    return write(file->handle, data, length);
}

// ----------------------------------------------------------------------------
size_t seekFile(utilsFile* file, size_t length)
{
    return (size_t)lseek(file->handle, length, SEEK_SET);
}

// ----------------------------------------------------------------------------
uint64_t getFileSize(const utilsFile* file)
{
  struct stat fileStat;
  if (fstat(file->handle, &fileStat) != 0) {
    return 0;
  }

  return fileStat.st_size;
}

// ----------------------------------------------------------------------------
retcode mapPosixErrorCode()
{
    switch (errno) {
    case 0:
        return rcSuccess;
    case ENOENT:
        return rcFileNotFound;
    case EPERM:
    case EACCES:
        return rcAccessDenied;
    case EMFILE:
        return rcTooManyOpenFiles;
    default:
        return rcFailed;
    }
}
