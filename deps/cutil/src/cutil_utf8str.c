/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */

#include <ctype.h>
#include <stdarg.h>
#include <stdio.h>

#include "cutil_utf8str.h"

extern inline void utf8_incref(const utf8str_t* str);
extern inline void utf8_decref(const utf8str_t* str);

extern inline const char* utf8_cstr(const utf8str_t* str);
extern inline size_t utf8_length(const utf8str_t* str);
extern inline const utf8str_t* utf8_assign(const utf8str_t* str);
extern inline void utf8_free(const utf8str_t** str);
extern inline const utf8str_t* utf8_make(const char* c_str, size_t len);
extern inline const utf8str_t* utf8_concat(const utf8str_t* str0, const utf8str_t* str1);
extern inline int utf8_ccmp(const utf8str_t* str, const char* cstr);
extern inline int utf8_cmp(const utf8str_t* pstr, const utf8str_t* qstr);

// ----------------------------------------------------------------------------
int utf8_to_int(const char* str)
{
    unsigned val = 0;
    unsigned sign = 0;

    if (*str == '-') {
        sign = ~0u;
        str++;
    }

    while (isdigit((unsigned char)(*str))) {
        val = 10 * val + (*str++ - '0');
    }

    return (int)((val ^ sign) - sign); // Apply the sign
}

// ----------------------------------------------------------------------------
unsigned utf8_to_unsigned(const char* str)
{
    unsigned val = 0;
    while (isdigit((unsigned char)(*str))) {
        val = 10 * val + (*str++ - '0');
    }

    return val;
}

// ----------------------------------------------------------------------------
const utf8str_t* utf8_format(const char* format, ...)
{
    va_list args0, args1;
    va_start(args0, format);
    va_copy(args1, args0);

    size_t len = vsnprintf(nullptr, 0, format, args0);
    va_end(args0);

    size_t str_size = sizeof(utf8str_t) + sizeof(char) * (len + 1);
    utf8str_t* str = (utf8str_t*)malloc(str_size);

    if (str != nullptr) {
        str->count = 1;
        vsnprintf(str->data, len + 1, format, args1);
    }

    va_end(args1);
    return str;
}
