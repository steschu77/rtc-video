/*
 *  Copyright (c) 2013 The WebM project authors. All Rights Reserved.
 *
 *  Use of this source code is governed by a BSD-style license
 *  that can be found in the LICENSE file in the root of the source
 *  tree. An additional intellectual property rights grant can be found
 *  in the file PATENTS.  All contributing project authors may
 *  be found in the AUTHORS file in the root of the source tree.
 */

#include <math.h>

#include "vpx_ports/mem.h"
#include "vpx_ports/system_state.h"

#include "vp9/encoder/vp9_aq_variance.h"

#include "vp9/common/vp9_seg_common.h"

#include "vp9/encoder/vp9_rd.h"
#include "vp9/encoder/vp9_encodeframe.h"

#define ENERGY_MIN (-4)
#define ENERGY_MAX (1)

DECLARE_ALIGNED(16, static const uint8_t, vp9_64_zeros[64]) = { 0 };

/* TODO(agrange, paulwilkins): The block_variance calls the unoptimized versions
 * of variance() and highbd_8_variance(). It should not.
 */
static void aq_variance(const uint8_t *a, int a_stride, const uint8_t *b,
                        int b_stride, int w, int h, unsigned int *sse,
                        int *sum) {
  int i, j;

  *sum = 0;
  *sse = 0;

  for (i = 0; i < h; i++) {
    for (j = 0; j < w; j++) {
      const int diff = a[j] - b[j];
      *sum += diff;
      *sse += diff * diff;
    }

    a += a_stride;
    b += b_stride;
  }
}

static unsigned int block_variance(VP9_COMP *cpi, MACROBLOCK *x,
                                   BLOCK_SIZE bs) {
  MACROBLOCKD *xd = &x->e_mbd;
  unsigned int var, sse;
  int right_overflow =
      (xd->mb_to_right_edge < 0) ? ((-xd->mb_to_right_edge) >> 3) : 0;
  int bottom_overflow =
      (xd->mb_to_bottom_edge < 0) ? ((-xd->mb_to_bottom_edge) >> 3) : 0;

  if (right_overflow || bottom_overflow) {
    const int bw = 8 * num_8x8_blocks_wide_lookup[bs] - right_overflow;
    const int bh = 8 * num_8x8_blocks_high_lookup[bs] - bottom_overflow;
    int avg;
    aq_variance(x->plane[0].src.buf, x->plane[0].src.stride, vp9_64_zeros, 0,
                bw, bh, &sse, &avg);
    var = sse - (unsigned int)(((int64_t)avg * avg) / (bw * bh));
    return (unsigned int)(((uint64_t)256 * var) / (bw * bh));
  } else {
    var = cpi->fn_ptr[bs].vf(x->plane[0].src.buf, x->plane[0].src.stride,
                             vp9_64_zeros, 0, &sse);
    return (unsigned int)(((uint64_t)256 * var) >> num_pels_log2_lookup[bs]);
  }
}

double vp9_log_block_var(VP9_COMP *cpi, MACROBLOCK *x, BLOCK_SIZE bs) {
  unsigned int var = block_variance(cpi, x, bs);
  vpx_clear_system_state();
  return log(var + 1.0);
}

#define DEFAULT_E_MIDPOINT 10.0
static int vp9_block_energy(VP9_COMP* cpi, MACROBLOCK* x, BLOCK_SIZE bs)
{
    double energy;
    double energy_midpoint;
    vpx_clear_system_state();
    energy_midpoint = DEFAULT_E_MIDPOINT;
    energy = vp9_log_block_var(cpi, x, bs) - energy_midpoint;
    return clamp((int)round(energy), ENERGY_MIN, ENERGY_MAX);
}

// Get the range of sub block energy values;
void vp9_get_sub_block_energy(VP9_COMP *cpi, MACROBLOCK *mb, int mi_row,
                              int mi_col, BLOCK_SIZE bsize, int *min_e,
                              int *max_e) {
  VP9_COMMON *const cm = &cpi->common;
  const int bw = num_8x8_blocks_wide_lookup[bsize];
  const int bh = num_8x8_blocks_high_lookup[bsize];
  const int xmis = VPXMIN(cm->mi_cols - mi_col, bw);
  const int ymis = VPXMIN(cm->mi_rows - mi_row, bh);
  int x, y;

  if (xmis < bw || ymis < bh) {
    vp9_setup_src_planes(mb, cpi->Source, mi_row, mi_col);
    *min_e = vp9_block_energy(cpi, mb, bsize);
    *max_e = *min_e;
  } else {
    int energy;
    *min_e = ENERGY_MAX;
    *max_e = ENERGY_MIN;

    for (y = 0; y < ymis; ++y) {
      for (x = 0; x < xmis; ++x) {
        vp9_setup_src_planes(mb, cpi->Source, mi_row + y, mi_col + x);
        energy = vp9_block_energy(cpi, mb, BLOCK_8X8);
        *min_e = VPXMIN(*min_e, energy);
        *max_e = VPXMAX(*max_e, energy);
      }
    }
  }

  // Re-instate source pointers back to what they should have been on entry.
  vp9_setup_src_planes(mb, cpi->Source, mi_row, mi_col);
}
