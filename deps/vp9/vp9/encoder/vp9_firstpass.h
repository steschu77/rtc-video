/*
 *  Copyright (c) 2010 The WebM project authors. All Rights Reserved.
 *
 *  Use of this source code is governed by a BSD-style license
 *  that can be found in the LICENSE file in the root of the source
 *  tree. An additional intellectual property rights grant can be found
 *  in the file PATENTS.  All contributing project authors may
 *  be found in the AUTHORS file in the root of the source tree.
 */

#ifndef VPX_VP9_ENCODER_VP9_FIRSTPASS_H_
#define VPX_VP9_ENCODER_VP9_FIRSTPASS_H_

#include <assert.h>

#ifdef __cplusplus
extern "C" {
#endif

#define MAX_LAG_BUFFERS 25
#define MAX_ARF_LAYERS 6

typedef enum {
  KF_UPDATE = 0,
  LF_UPDATE = 1,
  GF_UPDATE = 2,
  ARF_UPDATE = 3,
  OVERLAY_UPDATE = 4,
  MID_OVERLAY_UPDATE = 5,
  USE_BUF_FRAME = 6,  // Use show existing frame, no ref buffer update
  FRAME_UPDATE_TYPES = 7
} FRAME_UPDATE_TYPE;

typedef struct {
  unsigned char index;

  int frame_start;
  int frame_end;
  // TODO(jingning): The array size of arf_stack could be reduced.
  int arf_index_stack[MAX_LAG_BUFFERS * 2];
  int top_arf_idx;
  int stack_size;
  int gf_group_size;
  int max_layer_depth;
  int allowed_max_layer_depth;
  int group_noise_energy;
} GF_GROUP;

typedef struct {
  double mb_smooth_pct;

  int kf_zeromotion_pct;
  int last_kfgroup_zeromotion_pct;
  int active_worst_quality;
  int extend_minq;
  int extend_maxq;
  int extend_minq_fast;
  int last_qindex_of_arf_layer[MAX_ARF_LAYERS];

  GF_GROUP gf_group;
} TWO_PASS;

#ifdef __cplusplus
}  // extern "C"
#endif

#endif  // VPX_VP9_ENCODER_VP9_FIRSTPASS_H_
