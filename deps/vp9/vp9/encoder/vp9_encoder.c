/*
 * Copyright (c) 2010 The WebM project authors. All Rights Reserved.
 *
 *  Use of this source code is governed by a BSD-style license
 *  that can be found in the LICENSE file in the root of the source
 *  tree. An additional intellectual property rights grant can be found
 *  in the file PATENTS.  All contributing project authors may
 *  be found in the AUTHORS file in the root of the source tree.
 */

#include <limits.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include "./vp9_rtcd.h"
#include "./vpx_config.h"
#include "./vpx_dsp_rtcd.h"
#include "./vpx_scale_rtcd.h"
#include "vpx_dsp/psnr.h"
#include "vpx_dsp/vpx_dsp_common.h"
#include "vpx_dsp/vpx_filter.h"
#include "vpx_ports/mem.h"
#include "vpx_ports/system_state.h"

#include "vp9/common/vp9_alloccommon.h"
#include "vp9/common/vp9_filter.h"
#include "vp9/common/vp9_idct.h"
#include "vp9/common/vp9_reconinter.h"
#include "vp9/common/vp9_reconintra.h"
#include "vp9/common/vp9_tile_common.h"
#include "vp9/common/vp9_scan.h"

#if !CONFIG_REALTIME_ONLY
#include "vp9/encoder/vp9_aq_variance.h"
#endif
#include "vp9/encoder/vp9_bitstream.h"
#include "vp9/encoder/vp9_context_tree.h"
#include "vp9/encoder/vp9_encodeframe.h"
#include "vp9/encoder/vp9_encodemb.h"
#include "vp9/encoder/vp9_encodemv.h"
#include "vp9/encoder/vp9_encoder.h"
#include "vp9/encoder/vp9_extend.h"
#include "vp9/encoder/vp9_firstpass.h"
#include "vp9/encoder/vp9_picklpf.h"
#include "vp9/encoder/vp9_rd.h"
#include "vp9/encoder/vp9_resize.h"
#include "vp9/encoder/vp9_speed_features.h"

// Whether to use high precision mv for altref computation.
#define ALTREF_HIGH_PRECISION_MV 1

// Q threshold for high precision mv. Choose a very high value for now so that
// HIGH_PRECISION is always chosen.
#define HIGH_PRECISION_MV_QTHRESH 200

#define FRAME_SIZE_FACTOR 128  // empirical params for context model threshold
#define FRAME_RATE_FACTOR 8

void vp9_set_high_precision_mv(VP9_COMP *cpi, int allow_high_precision_mv) {
  MACROBLOCK *const mb = &cpi->td.mb;
  cpi->common.allow_high_precision_mv = allow_high_precision_mv;
  if (cpi->common.allow_high_precision_mv) {
    mb->mvcost = mb->nmvcost_hp;
    mb->mvsadcost = mb->nmvsadcost_hp;
  } else {
    mb->mvcost = mb->nmvcost;
    mb->mvsadcost = mb->nmvsadcost;
  }
}

static void setup_frame(VP9_COMP *cpi) {
  VP9_COMMON *const cm = &cpi->common;
  // Set up entropy context depending on frame type. The decoder mandates
  // the use of the default context, index 0, for keyframes and inter
  // frames where the error_resilient_mode or intra_only flag is set. For
  // other inter-frames the encoder currently uses only two contexts;
  // context 1 for ALTREF frames and context 0 for the others.
  if (frame_is_intra_only(cm) || cm->error_resilient_mode) {
    vp9_setup_past_independence(cm);
  } else {
    cm->frame_context_idx = cpi->refresh_alt_ref_frame;
  }

  if (cm->frame_type == KEY_FRAME) {
    cpi->refresh_golden_frame = 1;
    cpi->refresh_alt_ref_frame = 1;
    vp9_zero(cpi->interp_filter_selected);
  } else {
    *cm->fc = cm->frame_contexts[cm->frame_context_idx];
    vp9_zero(cpi->interp_filter_selected[0]);
  }
}

static void vp9_enc_setup_mi(VP9_COMMON *cm) {
  int i;
  cm->mi = cm->mip + cm->mi_stride + 1;
  memset(cm->mip, 0, cm->mi_stride * (cm->mi_rows + 1) * sizeof(*cm->mip));
  cm->prev_mi = cm->prev_mip + cm->mi_stride + 1;
  // Clear top border row
  memset(cm->prev_mip, 0, sizeof(*cm->prev_mip) * cm->mi_stride);
  // Clear left border column
  for (i = 1; i < cm->mi_rows + 1; ++i)
    memset(&cm->prev_mip[i * cm->mi_stride], 0, sizeof(*cm->prev_mip));

  cm->mi_grid_visible = cm->mi_grid_base + cm->mi_stride + 1;
  cm->prev_mi_grid_visible = cm->prev_mi_grid_base + cm->mi_stride + 1;

  memset(cm->mi_grid_base, 0,
         cm->mi_stride * (cm->mi_rows + 1) * sizeof(*cm->mi_grid_base));
}

static int vp9_enc_alloc_mi(VP9_COMMON *cm, int mi_size) {
  cm->mip = vpx_calloc(mi_size, sizeof(*cm->mip));
  if (!cm->mip) return 1;
  cm->prev_mip = vpx_calloc(mi_size, sizeof(*cm->prev_mip));
  if (!cm->prev_mip) return 1;
  cm->mi_alloc_size = mi_size;

  cm->mi_grid_base = (MODE_INFO **)vpx_calloc(mi_size, sizeof(MODE_INFO *));
  if (!cm->mi_grid_base) return 1;
  cm->prev_mi_grid_base =
      (MODE_INFO **)vpx_calloc(mi_size, sizeof(MODE_INFO *));
  if (!cm->prev_mi_grid_base) return 1;

  return 0;
}

static void vp9_enc_free_mi(VP9_COMMON *cm) {
  vpx_free(cm->mip);
  cm->mip = NULL;
  vpx_free(cm->prev_mip);
  cm->prev_mip = NULL;
  vpx_free(cm->mi_grid_base);
  cm->mi_grid_base = NULL;
  vpx_free(cm->prev_mi_grid_base);
  cm->prev_mi_grid_base = NULL;
  cm->mi_alloc_size = 0;
}

static void vp9_swap_mi_and_prev_mi(VP9_COMMON *cm) {
  // Current mip will be the prev_mip for the next frame.
  MODE_INFO **temp_base = cm->prev_mi_grid_base;
  MODE_INFO *temp = cm->prev_mip;

  // Skip update prev_mi frame in show_existing_frame mode.
  if (cm->show_existing_frame) return;

  cm->prev_mip = cm->mip;
  cm->mip = temp;

  // Update the upper left visible macroblock ptrs.
  cm->mi = cm->mip + cm->mi_stride + 1;
  cm->prev_mi = cm->prev_mip + cm->mi_stride + 1;

  cm->prev_mi_grid_base = cm->mi_grid_base;
  cm->mi_grid_base = temp_base;
  cm->mi_grid_visible = cm->mi_grid_base + cm->mi_stride + 1;
  cm->prev_mi_grid_visible = cm->prev_mi_grid_base + cm->mi_stride + 1;
}

void vp9_initialize_enc(void) {
  static volatile int init_done = 0;

  if (!init_done) {
    vp9_rtcd();
    vpx_dsp_rtcd();
    vpx_scale_rtcd();
    vp9_init_intra_predictors();
    vp9_init_me_luts();
    vp9_entropy_mv_init();
    init_done = 1;
  }
}

static void dealloc_compressor_data(VP9_COMP *cpi) {
  VP9_COMMON *const cm = &cpi->common;

  vpx_free(cpi->mbmi_ext_base);
  cpi->mbmi_ext_base = NULL;

  vpx_free(cpi->tile_data);
  cpi->tile_data = NULL;

  vpx_free(cpi->nmvcosts[0]);
  vpx_free(cpi->nmvcosts[1]);
  cpi->nmvcosts[0] = NULL;
  cpi->nmvcosts[1] = NULL;

  vpx_free(cpi->nmvcosts_hp[0]);
  vpx_free(cpi->nmvcosts_hp[1]);
  cpi->nmvcosts_hp[0] = NULL;
  cpi->nmvcosts_hp[1] = NULL;

  vpx_free(cpi->nmvsadcosts[0]);
  vpx_free(cpi->nmvsadcosts[1]);
  cpi->nmvsadcosts[0] = NULL;
  cpi->nmvsadcosts[1] = NULL;

  vpx_free(cpi->nmvsadcosts_hp[0]);
  vpx_free(cpi->nmvsadcosts_hp[1]);
  cpi->nmvsadcosts_hp[0] = NULL;
  cpi->nmvsadcosts_hp[1] = NULL;

  vpx_free(cpi->consec_zero_mv);
  cpi->consec_zero_mv = NULL;

  vp9_free_ref_frame_buffers(cm->buffer_pool);
  vp9_free_context_buffers(cm);

  vpx_free_frame_buffer(&cpi->source);
  vpx_free_frame_buffer(&cpi->scaled_source);

  vpx_free(cpi->tile_tok[0][0]);
  cpi->tile_tok[0][0] = 0;

  vpx_free(cpi->tplist[0][0]);
  cpi->tplist[0][0] = NULL;

  vp9_free_pc_tree(&cpi->td);
}

static void save_coding_context(VP9_COMP *cpi) {
  CODING_CONTEXT *const cc = &cpi->coding_context;
  VP9_COMMON *cm = &cpi->common;

  // Stores a snapshot of key state variables which can subsequently be
  // restored with a call to vp9_restore_coding_context. These functions are
  // intended for use in a re-code loop in vp9_compress_frame where the
  // quantizer value is adjusted between loop iterations.
  vp9_copy(cc->nmvjointcost, cpi->td.mb.nmvjointcost);

  memcpy(cc->nmvcosts[0], cpi->nmvcosts[0],
         MV_VALS * sizeof(*cpi->nmvcosts[0]));
  memcpy(cc->nmvcosts[1], cpi->nmvcosts[1],
         MV_VALS * sizeof(*cpi->nmvcosts[1]));
  memcpy(cc->nmvcosts_hp[0], cpi->nmvcosts_hp[0],
         MV_VALS * sizeof(*cpi->nmvcosts_hp[0]));
  memcpy(cc->nmvcosts_hp[1], cpi->nmvcosts_hp[1],
         MV_VALS * sizeof(*cpi->nmvcosts_hp[1]));

  vp9_copy(cc->last_ref_lf_deltas, cm->lf.last_ref_deltas);
  vp9_copy(cc->last_mode_lf_deltas, cm->lf.last_mode_deltas);

  cc->fc = *cm->fc;
}

static void restore_coding_context(VP9_COMP *cpi) {
  CODING_CONTEXT *const cc = &cpi->coding_context;
  VP9_COMMON *cm = &cpi->common;

  // Restore key state variables to the snapshot state stored in the
  // previous call to vp9_save_coding_context.
  vp9_copy(cpi->td.mb.nmvjointcost, cc->nmvjointcost);

  memcpy(cpi->nmvcosts[0], cc->nmvcosts[0], MV_VALS * sizeof(*cc->nmvcosts[0]));
  memcpy(cpi->nmvcosts[1], cc->nmvcosts[1], MV_VALS * sizeof(*cc->nmvcosts[1]));
  memcpy(cpi->nmvcosts_hp[0], cc->nmvcosts_hp[0],
         MV_VALS * sizeof(*cc->nmvcosts_hp[0]));
  memcpy(cpi->nmvcosts_hp[1], cc->nmvcosts_hp[1],
         MV_VALS * sizeof(*cc->nmvcosts_hp[1]));

  vp9_copy(cm->lf.last_ref_deltas, cc->last_ref_lf_deltas);
  vp9_copy(cm->lf.last_mode_deltas, cc->last_mode_lf_deltas);

  *cm->fc = cc->fc;
}

static void alloc_util_frame_buffers(VP9_COMP *cpi) {
  VP9_COMMON *const cm = &cpi->common;
  if (vpx_realloc_frame_buffer(&cpi->scaled_source, cm->width, cm->height,
                               cm->subsampling_x, cm->subsampling_y,
                               VP9_ENC_BORDER_IN_PIXELS, cm->byte_alignment,
                               NULL, NULL, NULL))
    vpx_internal_error(&cm->error, VPX_CODEC_MEM_ERROR,
                       "Failed to allocate scaled source buffer");
}

static int alloc_context_buffers_ext(VP9_COMP *cpi) {
  VP9_COMMON *cm = &cpi->common;
  int mi_size = cm->mi_cols * cm->mi_rows;

  cpi->mbmi_ext_base = vpx_calloc(mi_size, sizeof(*cpi->mbmi_ext_base));
  if (!cpi->mbmi_ext_base) return 1;

  return 0;
}

static void alloc_compressor_data(VP9_COMP *cpi) {
  VP9_COMMON *cm = &cpi->common;
  int sb_rows;

  vp9_alloc_context_buffers(cm, cm->width, cm->height);

  alloc_context_buffers_ext(cpi);

  vpx_free(cpi->tile_tok[0][0]);

  {
    unsigned int tokens = get_token_alloc(cm->mb_rows, cm->mb_cols);
    CHECK_MEM_ERROR(cm, cpi->tile_tok[0][0],
                    vpx_calloc(tokens, sizeof(*cpi->tile_tok[0][0])));
  }

  sb_rows = mi_cols_aligned_to_sb(cm->mi_rows) >> MI_BLOCK_SIZE_LOG2;
  vpx_free(cpi->tplist[0][0]);
  CHECK_MEM_ERROR(
      cm, cpi->tplist[0][0],
      vpx_calloc(sb_rows * 4 * (1 << 6), sizeof(*cpi->tplist[0][0])));

  vp9_setup_pc_tree(&cpi->common, &cpi->td);
}

static void set_tile_limits(VP9_COMP *cpi) {
  VP9_COMMON *const cm = &cpi->common;

  cm->log2_tile_cols = 6;
  cm->log2_tile_rows = 0;

  int min_log2_tile_cols, max_log2_tile_cols;
  vp9_get_tile_n_bits(cm->mi_cols, &min_log2_tile_cols, &max_log2_tile_cols);
  cm->log2_tile_cols = clamp(cm->log2_tile_cols, min_log2_tile_cols, max_log2_tile_cols);
}

static void update_frame_size(VP9_COMP *cpi) {
  VP9_COMMON *const cm = &cpi->common;
  MACROBLOCKD *const xd = &cpi->td.mb.e_mbd;

  vp9_set_mb_mi(cm, cm->width, cm->height);
  vp9_init_context_buffers(cm);
  vp9_init_macroblockd(cm, xd, NULL);
  cpi->td.mb.mbmi_ext_base = cpi->mbmi_ext_base;
  memset(cpi->mbmi_ext_base, 0,
         cm->mi_rows * cm->mi_cols * sizeof(*cpi->mbmi_ext_base));

  set_tile_limits(cpi);
}

static void init_buffer_indices(VP9_COMP *cpi) {
  for (int ref_frame = 0; ref_frame < REF_FRAMES; ++ref_frame)
    cpi->ref_fb_idx[ref_frame] = ref_frame;

  cpi->lst_fb_idx = cpi->ref_fb_idx[LAST_FRAME - 1];
  cpi->gld_fb_idx = cpi->ref_fb_idx[GOLDEN_FRAME - 1];
  cpi->alt_fb_idx = cpi->ref_fb_idx[ALTREF_FRAME - 1];
}

static void init_config(struct VP9_COMP *cpi, const VP9EncoderConfig *oxcf) {
  VP9_COMMON *const cm = &cpi->common;

  cpi->oxcf = *oxcf;
  cm->profile = PROFILE_0;
  cm->color_space = VPX_CS_UNKNOWN;
  cm->color_range = 0;

  cm->width = oxcf->width;
  cm->height = oxcf->height;
  alloc_compressor_data(cpi);

  // Single thread case: use counts in common.
  cpi->td.counts = &cm->counts;

  // change includes all joint functionality
  vp9_change_config(cpi, oxcf);

  cpi->ref_frame_flags = 0;

  init_buffer_indices(cpi);
}

static void vp9_reset_segment_features(struct segmentation *seg) {
  // Set up default state for MB feature flags
  seg->enabled = 0;
  seg->update_map = 0;
  seg->update_data = 0;
  memset(seg->tree_probs, 255, sizeof(seg->tree_probs));
  vp9_clearall_segfeatures(seg);
}

void vp9_change_config(struct VP9_COMP *cpi, const VP9EncoderConfig *oxcf) {
  VP9_COMMON *const cm = &cpi->common;
  int last_w = cpi->oxcf.width;
  int last_h = cpi->oxcf.height;

  vp9_init_quantizer(cpi);

  cpi->oxcf = *oxcf;

  cpi->last_q = 128;

  cpi->refresh_golden_frame = 0;
  cpi->refresh_last_frame = 1;
  cm->refresh_frame_context = 1;
  cm->reset_frame_context = 0;

  vp9_reset_segment_features(&cm->seg);
  vp9_set_high_precision_mv(cpi, 0);

  cm->interp_filter = SWITCHABLE;

  cm->render_width = cpi->oxcf.width;
  cm->render_height = cpi->oxcf.height;

  if (last_w != cpi->oxcf.width || last_h != cpi->oxcf.height) {
    cm->width = cpi->oxcf.width;
    cm->height = cpi->oxcf.height;
    cpi->external_resize = 1;
  }

  if (cpi->initial_width) {
    int new_mi_size = 0;
    vp9_set_mb_mi(cm, cm->width, cm->height);
    new_mi_size = cm->mi_stride * calc_mi_size(cm->mi_rows);
    if (cm->mi_alloc_size < new_mi_size) {
      vp9_free_context_buffers(cm);
      alloc_compressor_data(cpi);
      cpi->initial_width = cpi->initial_height = 0;
      cpi->external_resize = 0;
    } else if (cm->mi_alloc_size == new_mi_size &&
               (cpi->oxcf.width > last_w || cpi->oxcf.height > last_h)) {
      vp9_alloc_loop_filter(cm);
    }
  }

  if (cm->current_video_frame == 0 || last_w != cpi->oxcf.width ||
      last_h != cpi->oxcf.height)
    update_frame_size(cpi);

  if (last_w != cpi->oxcf.width || last_h != cpi->oxcf.height) {
    memset(cpi->consec_zero_mv, 0,
           cm->mi_rows * cm->mi_cols * sizeof(*cpi->consec_zero_mv));
  }

  set_tile_limits(cpi);
}

#ifndef M_LOG2_E
#define M_LOG2_E 0.693147180559945309417
#endif
#define log2f(x) (log(x) / (float)M_LOG2_E)

/***********************************************************************
 * Read before modifying 'cal_nmvjointsadcost' or 'cal_nmvsadcosts'    *
 ***********************************************************************
 * The following 2 functions ('cal_nmvjointsadcost' and                *
 * 'cal_nmvsadcosts') are used to calculate cost lookup tables         *
 * used by 'vp9_diamond_search_sad'. The C implementation of the       *
 * function is generic, but the AVX intrinsics optimised version       *
 * relies on the following properties of the computed tables:          *
 * For cal_nmvjointsadcost:                                            *
 *   - mvjointsadcost[1] == mvjointsadcost[2] == mvjointsadcost[3]     *
 * For cal_nmvsadcosts:                                                *
 *   - For all i: mvsadcost[0][i] == mvsadcost[1][i]                   *
 *         (Equal costs for both components)                           *
 *   - For all i: mvsadcost[0][i] == mvsadcost[0][-i]                  *
 *         (Cost function is even)                                     *
 * If these do not hold, then the AVX optimised version of the         *
 * 'vp9_diamond_search_sad' function cannot be used as it is, in which *
 * case you can revert to using the C function instead.                *
 ***********************************************************************/

static void cal_nmvjointsadcost(int *mvjointsadcost) {
  /*********************************************************************
   * Warning: Read the comments above before modifying this function   *
   *********************************************************************/
  mvjointsadcost[0] = 600;
  mvjointsadcost[1] = 300;
  mvjointsadcost[2] = 300;
  mvjointsadcost[3] = 300;
}

static void cal_nmvsadcosts(int *mvsadcost[2]) {
  /*********************************************************************
   * Warning: Read the comments above before modifying this function   *
   *********************************************************************/
  int i = 1;

  mvsadcost[0][0] = 0;
  mvsadcost[1][0] = 0;

  do {
    double z = 256 * (2 * (log2f(8 * i) + .6));
    mvsadcost[0][i] = (int)z;
    mvsadcost[1][i] = (int)z;
    mvsadcost[0][-i] = (int)z;
    mvsadcost[1][-i] = (int)z;
  } while (++i <= MV_MAX);
}

static void cal_nmvsadcosts_hp(int *mvsadcost[2]) {
  int i = 1;

  mvsadcost[0][0] = 0;
  mvsadcost[1][0] = 0;

  do {
    double z = 256 * (2 * (log2f(8 * i) + .6));
    mvsadcost[0][i] = (int)z;
    mvsadcost[1][i] = (int)z;
    mvsadcost[0][-i] = (int)z;
    mvsadcost[1][-i] = (int)z;
  } while (++i <= MV_MAX);
}

static void init_ref_frame_bufs(VP9_COMMON *cm) {
  int i;
  BufferPool *const pool = cm->buffer_pool;
  cm->new_fb_idx = INVALID_IDX;
  for (i = 0; i < REF_FRAMES; ++i) {
    cm->ref_frame_map[i] = INVALID_IDX;
  }
  for (i = 0; i < FRAME_BUFFERS; ++i) {
    pool->frame_bufs[i].ref_count = 0;
  }
}

static void update_initial_width(VP9_COMP *cpi, int subsampling_x, int subsampling_y) {
  VP9_COMMON *const cm = &cpi->common;

  if (!cpi->initial_width ||
      cm->subsampling_x != subsampling_x ||
      cm->subsampling_y != subsampling_y) {
    cm->subsampling_x = subsampling_x;
    cm->subsampling_y = subsampling_y;

    cpi->initial_width = cm->width;
    cpi->initial_height = cm->height;
    cpi->initial_mbs = cm->MBs;
  }
}

static FRAME_INFO vp9_get_frame_info(const VP9EncoderConfig* oxcf)
{
    FRAME_INFO frame_info;
    int dummy;
    frame_info.frame_width = oxcf->width;
    frame_info.frame_height = oxcf->height;
    frame_info.render_frame_width = oxcf->width;
    frame_info.render_frame_height = oxcf->height;
    vp9_set_mi_size(&frame_info.mi_rows, &frame_info.mi_cols, &dummy,
        frame_info.frame_width, frame_info.frame_height);
    vp9_set_mb_size(&frame_info.mb_rows, &frame_info.mb_cols, &frame_info.num_mbs,
        frame_info.mi_rows, frame_info.mi_cols);
    // TODO(angiebird): Figure out how to get subsampling_x/y here
    return frame_info;
}

VP9_COMP *vp9_create_compressor(const VP9EncoderConfig *oxcf,
                                BufferPool *const pool) {
  VP9_COMP *volatile const cpi = vpx_memalign(32, sizeof(VP9_COMP));
  VP9_COMMON *volatile const cm = cpi != NULL ? &cpi->common : NULL;

  if (!cm) return NULL;

  vp9_zero(*cpi);

  if (setjmp(cm->error.jmp)) {
    cm->error.setjmp = 0;
    vp9_remove_compressor(cpi);
    return 0;
  }

  cm->error.setjmp = 1;
  cm->alloc_mi = vp9_enc_alloc_mi;
  cm->free_mi = vp9_enc_free_mi;
  cm->setup_mi = vp9_enc_setup_mi;

  CHECK_MEM_ERROR(cm, cm->fc, (FRAME_CONTEXT *)vpx_calloc(1, sizeof(*cm->fc)));
  CHECK_MEM_ERROR(
      cm, cm->frame_contexts,
      (FRAME_CONTEXT *)vpx_calloc(FRAME_CONTEXTS, sizeof(*cm->frame_contexts)));

  cpi->external_resize = 0;
  cpi->common.buffer_pool = pool;
  init_ref_frame_bufs(cm);

  init_config(cpi, oxcf);
  cpi->frame_info = vp9_get_frame_info(oxcf);

  cm->current_video_frame = 0;
  cpi->tile_data = NULL;

  CHECK_MEM_ERROR(
      cm, cpi->consec_zero_mv,
      vpx_calloc(cm->mi_rows * cm->mi_cols, sizeof(*cpi->consec_zero_mv)));

  CHECK_MEM_ERROR(cm, cpi->nmvcosts[0],
                  vpx_calloc(MV_VALS, sizeof(*cpi->nmvcosts[0])));
  CHECK_MEM_ERROR(cm, cpi->nmvcosts[1],
                  vpx_calloc(MV_VALS, sizeof(*cpi->nmvcosts[1])));
  CHECK_MEM_ERROR(cm, cpi->nmvcosts_hp[0],
                  vpx_calloc(MV_VALS, sizeof(*cpi->nmvcosts_hp[0])));
  CHECK_MEM_ERROR(cm, cpi->nmvcosts_hp[1],
                  vpx_calloc(MV_VALS, sizeof(*cpi->nmvcosts_hp[1])));
  CHECK_MEM_ERROR(cm, cpi->nmvsadcosts[0],
                  vpx_calloc(MV_VALS, sizeof(*cpi->nmvsadcosts[0])));
  CHECK_MEM_ERROR(cm, cpi->nmvsadcosts[1],
                  vpx_calloc(MV_VALS, sizeof(*cpi->nmvsadcosts[1])));
  CHECK_MEM_ERROR(cm, cpi->nmvsadcosts_hp[0],
                  vpx_calloc(MV_VALS, sizeof(*cpi->nmvsadcosts_hp[0])));
  CHECK_MEM_ERROR(cm, cpi->nmvsadcosts_hp[1],
                  vpx_calloc(MV_VALS, sizeof(*cpi->nmvsadcosts_hp[1])));

  cpi->refresh_alt_ref_frame = 0;

  /*********************************************************************
   * Warning: Read the comments around 'cal_nmvjointsadcost' and       *
   * 'cal_nmvsadcosts' before modifying how these tables are computed. *
   *********************************************************************/
  cal_nmvjointsadcost(cpi->td.mb.nmvjointsadcost);
  cpi->td.mb.nmvcost[0] = &cpi->nmvcosts[0][MV_MAX];
  cpi->td.mb.nmvcost[1] = &cpi->nmvcosts[1][MV_MAX];
  cpi->td.mb.nmvsadcost[0] = &cpi->nmvsadcosts[0][MV_MAX];
  cpi->td.mb.nmvsadcost[1] = &cpi->nmvsadcosts[1][MV_MAX];
  cal_nmvsadcosts(cpi->td.mb.nmvsadcost);

  cpi->td.mb.nmvcost_hp[0] = &cpi->nmvcosts_hp[0][MV_MAX];
  cpi->td.mb.nmvcost_hp[1] = &cpi->nmvcosts_hp[1][MV_MAX];
  cpi->td.mb.nmvsadcost_hp[0] = &cpi->nmvsadcosts_hp[0][MV_MAX];
  cpi->td.mb.nmvsadcost_hp[1] = &cpi->nmvsadcosts_hp[1][MV_MAX];
  cal_nmvsadcosts_hp(cpi->td.mb.nmvsadcost_hp);

  vp9_set_speed_features_framesize_independent(cpi);

#define BFP(BT, SDF, SDAF, VF, SVF, SVAF, SDX4DF, SDX8F) \
  cpi->fn_ptr[BT].sdf = SDF;                             \
  cpi->fn_ptr[BT].sdaf = SDAF;                           \
  cpi->fn_ptr[BT].vf = VF;                               \
  cpi->fn_ptr[BT].svf = SVF;                             \
  cpi->fn_ptr[BT].svaf = SVAF;                           \
  cpi->fn_ptr[BT].sdx4df = SDX4DF;                       \
  cpi->fn_ptr[BT].sdx8f = SDX8F;

  // TODO(angiebird): make sdx8f available for every block size
  BFP(BLOCK_32X16, vpx_sad32x16, vpx_sad32x16_avg, vpx_variance32x16,
      vpx_sub_pixel_variance32x16, vpx_sub_pixel_avg_variance32x16,
      vpx_sad32x16x4d, NULL)

  BFP(BLOCK_16X32, vpx_sad16x32, vpx_sad16x32_avg, vpx_variance16x32,
      vpx_sub_pixel_variance16x32, vpx_sub_pixel_avg_variance16x32,
      vpx_sad16x32x4d, NULL)

  BFP(BLOCK_64X32, vpx_sad64x32, vpx_sad64x32_avg, vpx_variance64x32,
      vpx_sub_pixel_variance64x32, vpx_sub_pixel_avg_variance64x32,
      vpx_sad64x32x4d, NULL)

  BFP(BLOCK_32X64, vpx_sad32x64, vpx_sad32x64_avg, vpx_variance32x64,
      vpx_sub_pixel_variance32x64, vpx_sub_pixel_avg_variance32x64,
      vpx_sad32x64x4d, NULL)

  BFP(BLOCK_32X32, vpx_sad32x32, vpx_sad32x32_avg, vpx_variance32x32,
      vpx_sub_pixel_variance32x32, vpx_sub_pixel_avg_variance32x32,
      vpx_sad32x32x4d, vpx_sad32x32x8)

  BFP(BLOCK_64X64, vpx_sad64x64, vpx_sad64x64_avg, vpx_variance64x64,
      vpx_sub_pixel_variance64x64, vpx_sub_pixel_avg_variance64x64,
      vpx_sad64x64x4d, NULL)

  BFP(BLOCK_16X16, vpx_sad16x16, vpx_sad16x16_avg, vpx_variance16x16,
      vpx_sub_pixel_variance16x16, vpx_sub_pixel_avg_variance16x16,
      vpx_sad16x16x4d, vpx_sad16x16x8)

  BFP(BLOCK_16X8, vpx_sad16x8, vpx_sad16x8_avg, vpx_variance16x8,
      vpx_sub_pixel_variance16x8, vpx_sub_pixel_avg_variance16x8,
      vpx_sad16x8x4d, vpx_sad16x8x8)

  BFP(BLOCK_8X16, vpx_sad8x16, vpx_sad8x16_avg, vpx_variance8x16,
      vpx_sub_pixel_variance8x16, vpx_sub_pixel_avg_variance8x16,
      vpx_sad8x16x4d, vpx_sad8x16x8)

  BFP(BLOCK_8X8, vpx_sad8x8, vpx_sad8x8_avg, vpx_variance8x8,
      vpx_sub_pixel_variance8x8, vpx_sub_pixel_avg_variance8x8, vpx_sad8x8x4d,
      vpx_sad8x8x8)

  BFP(BLOCK_8X4, vpx_sad8x4, vpx_sad8x4_avg, vpx_variance8x4,
      vpx_sub_pixel_variance8x4, vpx_sub_pixel_avg_variance8x4, vpx_sad8x4x4d,
      NULL)

  BFP(BLOCK_4X8, vpx_sad4x8, vpx_sad4x8_avg, vpx_variance4x8,
      vpx_sub_pixel_variance4x8, vpx_sub_pixel_avg_variance4x8, vpx_sad4x8x4d,
      NULL)

  BFP(BLOCK_4X4, vpx_sad4x4, vpx_sad4x4_avg, vpx_variance4x4,
      vpx_sub_pixel_variance4x4, vpx_sub_pixel_avg_variance4x4, vpx_sad4x4x4d,
      vpx_sad4x4x8)

  /* vp9_init_quantizer() is first called here. Add check in
   * vp9_frame_init_quantizer() so that vp9_init_quantizer is only
   * called later when needed. This will avoid unnecessary calls of
   * vp9_init_quantizer() for every frame.
   */
  vp9_init_quantizer(cpi);

  vp9_loop_filter_init(cm);

  // Set up the unit scaling factor used during motion search.
  vp9_setup_scale_factors_for_frame(&cpi->me_sf, cm->width, cm->height,
                                    cm->width, cm->height);
  cpi->td.mb.me_sf = &cpi->me_sf;

  cm->error.setjmp = 0;

  return cpi;
}

void vp9_remove_compressor(VP9_COMP *cpi) {
  VP9_COMMON *cm;

  if (!cpi) return;

  cm = &cpi->common;

  dealloc_compressor_data(cpi);

  vp9_remove_common(cm);
  vp9_free_ref_frame_buffers(cm->buffer_pool);
  vpx_free(cpi);
}

void vp9_get_psnr(const VP9_COMP *cpi, PSNR_STATS *psnr) {
  vpx_calc_psnr(cpi->Source, cpi->common.frame_to_show, psnr);
}

static void scale_and_extend_frame_nonnormative(const YV12_BUFFER_CONFIG *src,
                                                YV12_BUFFER_CONFIG *dst) {
  // TODO(dkovalev): replace YV12_BUFFER_CONFIG with vpx_image_t
  int i;
  const uint8_t *const srcs[3] = { src->y_buffer, src->u_buffer,
                                   src->v_buffer };
  const int src_strides[3] = { src->y_stride, src->uv_stride, src->uv_stride };
  const int src_widths[3] = { src->y_crop_width, src->uv_crop_width,
                              src->uv_crop_width };
  const int src_heights[3] = { src->y_crop_height, src->uv_crop_height,
                               src->uv_crop_height };
  uint8_t *const dsts[3] = { dst->y_buffer, dst->u_buffer, dst->v_buffer };
  const int dst_strides[3] = { dst->y_stride, dst->uv_stride, dst->uv_stride };
  const int dst_widths[3] = { dst->y_crop_width, dst->uv_crop_width,
                              dst->uv_crop_width };
  const int dst_heights[3] = { dst->y_crop_height, dst->uv_crop_height,
                               dst->uv_crop_height };

  for (i = 0; i < MAX_MB_PLANE; ++i) {
    vp9_resize_plane(srcs[i], src_heights[i], src_widths[i], src_strides[i],
                     dsts[i], dst_heights[i], dst_widths[i], dst_strides[i]);
  }
  vpx_extend_frame_borders(dst);
}

static void update_ref_frames(VP9_COMP *cpi) {
  VP9_COMMON *const cm = &cpi->common;
  BufferPool *const pool = cm->buffer_pool;

  // At this point the new frame has been encoded.
  // If any buffer copy / swapping is signaled it should be done here.
  if (cm->frame_type == KEY_FRAME) {
    ref_cnt_fb(pool->frame_bufs, &cm->ref_frame_map[cpi->gld_fb_idx],
               cm->new_fb_idx);
    ref_cnt_fb(pool->frame_bufs, &cm->ref_frame_map[cpi->alt_fb_idx],
               cm->new_fb_idx);
  } else { /* For non key/golden frames */
    if (cpi->refresh_alt_ref_frame) {
      ref_cnt_fb(pool->frame_bufs, &cm->ref_frame_map[cpi->alt_fb_idx], cm->new_fb_idx);
      memcpy(cpi->interp_filter_selected[ALTREF_FRAME],
             cpi->interp_filter_selected[0],
             sizeof(cpi->interp_filter_selected[0]));
    }

    if (cpi->refresh_golden_frame) {
      ref_cnt_fb(pool->frame_bufs, &cm->ref_frame_map[cpi->gld_fb_idx],
                 cm->new_fb_idx);
      memcpy(cpi->interp_filter_selected[GOLDEN_FRAME],
             cpi->interp_filter_selected[0],
             sizeof(cpi->interp_filter_selected[0]));
    }
  }

  if (cpi->refresh_last_frame) {
    ref_cnt_fb(pool->frame_bufs, &cm->ref_frame_map[cpi->lst_fb_idx],
               cm->new_fb_idx);
    memcpy(cpi->interp_filter_selected[LAST_FRAME],
           cpi->interp_filter_selected[0],
           sizeof(cpi->interp_filter_selected[0]));
  }
}

void vp9_update_reference_frames(VP9_COMP *cpi) {
  update_ref_frames(cpi);
}

static void loopfilter_frame(VP9_COMP *cpi, VP9_COMMON *cm) {
  MACROBLOCKD *xd = &cpi->td.mb.e_mbd;
  struct loopfilter *lf = &cm->lf;
  int is_reference_frame =
      (cm->frame_type == KEY_FRAME || cpi->refresh_last_frame ||
       cpi->refresh_golden_frame || cpi->refresh_alt_ref_frame);

  // Skip loop filter in show_existing_frame mode.
  if (cm->show_existing_frame) {
    lf->filter_level = 0;
    return;
  }

  if (xd->lossless) {
    lf->filter_level = 0;
    lf->last_filt_level = 0;
  } else {
    vpx_clear_system_state();

    if (cpi->common.frame_type == KEY_FRAME) {
      lf->last_filt_level = 0;
    }
    vp9_pick_filter_level(cpi->Source, cpi);
    lf->last_filt_level = lf->filter_level;
  }

  if (lf->filter_level > 0 && is_reference_frame) {
    vp9_build_mask_frame(cm, lf->filter_level, 0);

    vp9_loop_filter_frame(cm->frame_to_show, cm, xd, lf->filter_level, 0, 0);
  }

  vpx_extend_frame_inner_borders(cm->frame_to_show);
}

static INLINE void alloc_frame_mvs(VP9_COMMON *const cm, int buffer_idx) {
  RefCntBuffer *const new_fb_ptr = &cm->buffer_pool->frame_bufs[buffer_idx];
  if (new_fb_ptr->mvs == NULL || new_fb_ptr->mi_rows < cm->mi_rows ||
      new_fb_ptr->mi_cols < cm->mi_cols) {
    vpx_free(new_fb_ptr->mvs);
    CHECK_MEM_ERROR(cm, new_fb_ptr->mvs,
                    (MV_REF *)vpx_calloc(cm->mi_rows * cm->mi_cols,
                                         sizeof(*new_fb_ptr->mvs)));
    new_fb_ptr->mi_rows = cm->mi_rows;
    new_fb_ptr->mi_cols = cm->mi_cols;
  }
}

void vp9_scale_references(VP9_COMP *cpi) {
  VP9_COMMON *cm = &cpi->common;
  MV_REFERENCE_FRAME ref_frame;
  const VP9_REFFRAME ref_mask[3] = { VP9_LAST_FLAG, VP9_GOLD_FLAG,
                                     VP9_ALT_FLAG };

  for (ref_frame = LAST_FRAME; ref_frame <= ALTREF_FRAME; ++ref_frame) {
    // Need to convert from VP9_REFFRAME to index into ref_mask (subtract 1).
    if (cpi->ref_frame_flags & ref_mask[ref_frame - 1]) {
      BufferPool *const pool = cm->buffer_pool;
      const YV12_BUFFER_CONFIG *const ref =
          get_ref_frame_buffer(cpi, ref_frame);

      if (ref == NULL) {
        cpi->scaled_ref_idx[ref_frame - 1] = INVALID_IDX;
        continue;
      }

      if (ref->y_crop_width != cm->width || ref->y_crop_height != cm->height) {
        RefCntBuffer *new_fb_ptr = NULL;
        int force_scaling = 0;
        int new_fb = cpi->scaled_ref_idx[ref_frame - 1];
        if (new_fb == INVALID_IDX) {
          new_fb = get_free_fb(cm);
          force_scaling = 1;
        }
        if (new_fb == INVALID_IDX) return;
        new_fb_ptr = &pool->frame_bufs[new_fb];
        if (force_scaling || new_fb_ptr->buf.y_crop_width != cm->width ||
            new_fb_ptr->buf.y_crop_height != cm->height) {
          if (vpx_realloc_frame_buffer(&new_fb_ptr->buf, cm->width, cm->height,
                                       cm->subsampling_x, cm->subsampling_y,
                                       VP9_ENC_BORDER_IN_PIXELS,
                                       cm->byte_alignment, NULL, NULL, NULL))
            vpx_internal_error(&cm->error, VPX_CODEC_MEM_ERROR,
                               "Failed to allocate frame buffer");
          vp9_scale_and_extend_frame(ref, &new_fb_ptr->buf, EIGHTTAP, 0);
          cpi->scaled_ref_idx[ref_frame - 1] = new_fb;
          alloc_frame_mvs(cm, new_fb);
        }
      } else {
        RefCntBuffer *buf = NULL;
        // Check for release of scaled reference.
        int buf_idx = cpi->scaled_ref_idx[ref_frame - 1];
        if (buf_idx != INVALID_IDX) {
          buf = &pool->frame_bufs[buf_idx];
          --buf->ref_count;
          cpi->scaled_ref_idx[ref_frame - 1] = INVALID_IDX;
        }
        buf_idx = get_ref_frame_buf_idx(cpi, ref_frame);
        buf = &pool->frame_bufs[buf_idx];
        buf->buf.y_crop_width = ref->y_crop_width;
        buf->buf.y_crop_height = ref->y_crop_height;
        cpi->scaled_ref_idx[ref_frame - 1] = buf_idx;
        ++buf->ref_count;
      }
    }
  }
}

static void release_scaled_references(VP9_COMP *cpi) {
  VP9_COMMON *cm = &cpi->common;
  // Only release scaled references under certain conditions:
  // if reference will be updated, or if scaled reference has same resolution.
  int refresh[3];
  refresh[0] = (cpi->refresh_last_frame) ? 1 : 0;
  refresh[1] = (cpi->refresh_golden_frame) ? 1 : 0;
  refresh[2] = (cpi->refresh_alt_ref_frame) ? 1 : 0;
  for (int i = LAST_FRAME; i <= ALTREF_FRAME; ++i) {
    const int idx = cpi->scaled_ref_idx[i - 1];
    if (idx != INVALID_IDX) {
      RefCntBuffer *const buf = &cm->buffer_pool->frame_bufs[idx];
      const YV12_BUFFER_CONFIG *const ref = get_ref_frame_buffer(cpi, i);
      if (refresh[i - 1] || (buf->buf.y_crop_width == ref->y_crop_width &&
                             buf->buf.y_crop_height == ref->y_crop_height)) {
        --buf->ref_count;
        cpi->scaled_ref_idx[i - 1] = INVALID_IDX;
      }
    }
  }
}

static void full_to_model_count(unsigned int *model_count,
                                unsigned int *full_count) {
  model_count[ZERO_TOKEN] = full_count[ZERO_TOKEN];
  model_count[ONE_TOKEN] = full_count[ONE_TOKEN];
  model_count[TWO_TOKEN] = full_count[TWO_TOKEN];
  for (int n = THREE_TOKEN; n < EOB_TOKEN; ++n)
    model_count[TWO_TOKEN] += full_count[n];
  model_count[EOB_MODEL_TOKEN] = full_count[EOB_TOKEN];
}

static void full_to_model_counts(vp9_coeff_count_model *model_count,
                                 vp9_coeff_count *full_count) {
  int i, j, k, l;

  for (i = 0; i < PLANE_TYPES; ++i)
    for (j = 0; j < REF_TYPES; ++j)
      for (k = 0; k < COEF_BANDS; ++k)
        for (l = 0; l < BAND_COEFF_CONTEXTS(k); ++l)
          full_to_model_count(model_count[i][j][k][l], full_count[i][j][k][l]);
}

static void set_size_independent_vars(VP9_COMP *cpi) {
  vp9_set_speed_features_framesize_independent(cpi);
  vp9_set_rd_speed_thresholds(cpi);
  vp9_set_rd_speed_thresholds_sub8x8(cpi);
  cpi->common.interp_filter = SWITCHABLE;
}

static void set_frame_size(VP9_COMP *cpi) {
  int ref_frame;
  VP9_COMMON *const cm = &cpi->common;
  MACROBLOCKD *const xd = &cpi->td.mb.e_mbd;

  alloc_frame_mvs(cm, cm->new_fb_idx);

  // Reset the frame pointers to the current frame size.
  if (vpx_realloc_frame_buffer(get_frame_new_buffer(cm), cm->width, cm->height,
                               cm->subsampling_x, cm->subsampling_y,
                               VP9_ENC_BORDER_IN_PIXELS, cm->byte_alignment,
                               NULL, NULL, NULL))
    vpx_internal_error(&cm->error, VPX_CODEC_MEM_ERROR,
                       "Failed to allocate frame buffer");

  alloc_util_frame_buffers(cpi);

  for (ref_frame = LAST_FRAME; ref_frame <= ALTREF_FRAME; ++ref_frame) {
    RefBuffer *const ref_buf = &cm->frame_refs[ref_frame - 1];
    const int buf_idx = get_ref_frame_buf_idx(cpi, ref_frame);

    ref_buf->idx = buf_idx;

    if (buf_idx != INVALID_IDX) {
      YV12_BUFFER_CONFIG *const buf = &cm->buffer_pool->frame_bufs[buf_idx].buf;
      ref_buf->buf = buf;
      vp9_setup_scale_factors_for_frame(&ref_buf->sf, buf->y_crop_width,
                                        buf->y_crop_height, cm->width,
                                        cm->height);
      if (vp9_is_scaled(&ref_buf->sf)) vpx_extend_frame_borders(buf);
    } else {
      ref_buf->buf = NULL;
    }
  }

  set_ref_ptrs(cm, xd, LAST_FRAME, LAST_FRAME);
}

static int encode_basic(VP9_COMP* cpi, int q, size_t* size, uint8_t* dest, int i)
{
  VP9_COMMON* const cm = &cpi->common;

  vpx_clear_system_state();

  set_frame_size(cpi);

  cpi->Source = vp9_scale_if_required(cm, cpi->un_scaled_source, &cpi->scaled_source, 1, EIGHTTAP, 0);

  if (frame_is_intra_only(cm) == 0) {
    vp9_set_high_precision_mv(cpi, q < HIGH_PRECISION_MV_QTHRESH);
    if (i > 0) {
      release_scaled_references(cpi);
    }
    vp9_scale_references(cpi);
  }

  vp9_set_quantizer(cm, q);

  if (i == 0) {
    setup_frame(cpi);
  }

  vp9_encode_frame(cpi);

  vpx_clear_system_state();
  
  save_coding_context(cpi);
  vp9_pack_bitstream(cpi, dest, size);
  restore_coding_context(cpi);

  return 0;
}

static int encode_with_recode_loop(VP9_COMP* cpi, size_t* enc_size, uint8_t* dest, int enc_flags, size_t target_size)
{
  vpx_clear_system_state();
 
  set_size_independent_vars(cpi);

  if ((enc_flags & VP8_EFLAG_NO_QPOPT) != 0) {
    encode_basic(cpi, cpi->last_q, enc_size, dest, 0);
    vpx_clear_system_state();
    return 1;
  }

  int min_q = 32;
  int max_q = 255-32;

  size_t max_size = 0;
  encode_basic(cpi, min_q, &max_size, dest, 0);
  if (target_size >= max_size) {
    *enc_size = max_size;
    vpx_clear_system_state();
    cpi->last_q = min_q;
    return 1;
  }

  size_t min_size = 0;
  encode_basic(cpi, max_q, &min_size, dest, 1);

  if (target_size < min_size) {
    vpx_clear_system_state();
    cpi->last_q = max_q;
    return 0;
  }

  while (max_q - min_q >= 32) {

    int q = (min_q + max_q + 1) / 2;
    size_t size = 0;
    encode_basic(cpi, q, &size, dest, 1);

    if (size < target_size) {
      min_size = size;
      max_q = q;
    } else {
      max_size = size;
      min_q = q;
    }
  }

  size_t size_diff = max_size - min_size;
  int q_diff = max_q - min_q;
  int q = max_q - (target_size - min_size) * q_diff / size_diff;

  encode_basic(cpi, q, enc_size, dest, 1);

  vpx_clear_system_state();
  cpi->last_q = q;

  return 1;
}

static int get_ref_frame_flags(const VP9_COMP *cpi) {
  const int *const map = cpi->common.ref_frame_map;
  const int gold_is_last = map[cpi->gld_fb_idx] == map[cpi->lst_fb_idx];
  const int alt_is_last = map[cpi->alt_fb_idx] == map[cpi->lst_fb_idx];
  const int gold_is_alt = map[cpi->gld_fb_idx] == map[cpi->alt_fb_idx];
  int flags = VP9_ALT_FLAG | VP9_GOLD_FLAG | VP9_LAST_FLAG;

  if (gold_is_last) flags &= ~VP9_GOLD_FLAG;

  if (alt_is_last) flags &= ~VP9_ALT_FLAG;

  if (gold_is_alt) flags &= ~VP9_ALT_FLAG;

  return flags;
}

YV12_BUFFER_CONFIG *vp9_scale_if_required(
    VP9_COMMON *cm, YV12_BUFFER_CONFIG *unscaled, YV12_BUFFER_CONFIG *scaled,
    int use_normative_scaler, INTERP_FILTER filter_type, int phase_scaler) {
  if (cm->mi_cols * MI_SIZE != unscaled->y_width ||
      cm->mi_rows * MI_SIZE != unscaled->y_height) {
    if (use_normative_scaler && unscaled->y_width <= (scaled->y_width << 1) &&
        unscaled->y_height <= (scaled->y_height << 1))
      vp9_scale_and_extend_frame(unscaled, scaled, filter_type, phase_scaler);
    else
      scale_and_extend_frame_nonnormative(unscaled, scaled);
    return scaled;
  } else {
    return unscaled;
  }
}

static void set_ref_sign_bias(VP9_COMP *cpi) {
  VP9_COMMON *const cm = &cpi->common;
  RefCntBuffer *const ref_buffer = get_ref_cnt_buffer(cm, cm->new_fb_idx);
  const int cur_frame_index = ref_buffer->frame_index;
  MV_REFERENCE_FRAME ref_frame;

  for (ref_frame = LAST_FRAME; ref_frame < MAX_REF_FRAMES; ++ref_frame) {
    const int buf_idx = get_ref_frame_buf_idx(cpi, ref_frame);
    const RefCntBuffer *const ref_cnt_buf =
        get_ref_cnt_buffer(&cpi->common, buf_idx);
    if (ref_cnt_buf) {
      cm->ref_frame_sign_bias[ref_frame] =
          cur_frame_index < ref_cnt_buf->frame_index;
    }
  }
}

static void set_frame_index(VP9_COMP *cpi, VP9_COMMON *cm) {
  RefCntBuffer *const ref_buffer = get_ref_cnt_buffer(cm, cm->new_fb_idx);

  if (ref_buffer) {
    ref_buffer->frame_index = cm->current_video_frame;
  }
}

static void encode_frame_to_data_rate(VP9_COMP *cpi, size_t *size, uint8_t *dest,
    unsigned int* frame_flags, int enc_flags, size_t targetSize)
{
  VP9_COMMON *const cm = &cpi->common;
  TX_SIZE t;

  vpx_clear_system_state();

  if (cm->show_existing_frame == 0) {
    // Update frame index
    set_frame_index(cpi, cm);

    // Set the arf sign bias for this frame.
    set_ref_sign_bias(cpi);
  }

  // Set default state for segment based loop filter update flags.
  cm->lf.mode_ref_delta_update = 0;

  // Set various flags etc to special state if it is a key frame.
  if (frame_is_intra_only(cm)) {
    // Reset the loop filter deltas and segmentation map.
    vp9_reset_segment_features(&cm->seg);

    cm->error_resilient_mode = 1;
    cm->frame_parallel_decoding_mode = 1;

    // By default, encoder assumes decoder can use prev_mi.
    if (cm->error_resilient_mode) {
      cm->reset_frame_context = 0;
      cm->refresh_frame_context = 0;
    } else if (cm->intra_only) {
      // Only reset the current context.
      cm->reset_frame_context = 2;
    }
  }

  vpx_clear_system_state();

  if (!encode_with_recode_loop(cpi, size, dest, enc_flags, targetSize)) {
    return;
  }

  // TODO(jingning): When using show existing frame mode, we assume that the
  // current ARF will be directly used as the final reconstructed frame. This is
  // an encoder control scheme. One could in principle explore other
  // possibilities to arrange the reference frame buffer and their coding order.
  if (cm->show_existing_frame) {
    ref_cnt_fb(cm->buffer_pool->frame_bufs, &cm->new_fb_idx,
               cm->ref_frame_map[cpi->alt_fb_idx]);
  }

  // If the encoder forced a KEY_FRAME decision
  if (cm->frame_type == KEY_FRAME) cpi->refresh_last_frame = 1;

  cm->frame_to_show = get_frame_new_buffer(cm);
  cm->frame_to_show->color_space = cm->color_space;
  cm->frame_to_show->color_range = cm->color_range;
  cm->frame_to_show->render_width = cm->render_width;
  cm->frame_to_show->render_height = cm->render_height;

  // Pick the loop filter level for the frame.
  loopfilter_frame(cpi, cm);

  // build the bitstream
  vp9_pack_bitstream(cpi, dest, size);

  if (frame_is_intra_only(cm) == 0) {
    release_scaled_references(cpi);
  }
  vp9_update_reference_frames(cpi);

  if (!cm->show_existing_frame) {
    for (t = TX_4X4; t <= TX_32X32; ++t) {
      full_to_model_counts(cpi->td.counts->coef[t],
                           cpi->td.rd_counts.coef_counts[t]);
    }

    if (!cm->error_resilient_mode && !cm->frame_parallel_decoding_mode) {
      if (!frame_is_intra_only(cm)) {
        vp9_adapt_mode_probs(cm);
        vp9_adapt_mv_probs(cm, cm->allow_high_precision_mv);
      }
      vp9_adapt_coef_probs(cm);
    }
  }

  if (cpi->refresh_golden_frame == 1)
    cpi->frame_flags |= FRAMEFLAGS_GOLDEN;
  else
    cpi->frame_flags &= ~FRAMEFLAGS_GOLDEN;

  if (cpi->refresh_alt_ref_frame == 1)
    cpi->frame_flags |= FRAMEFLAGS_ALTREF;
  else
    cpi->frame_flags &= ~FRAMEFLAGS_ALTREF;

  cm->last_frame_type = cm->frame_type;

  *size = VPXMAX(1, *size);

  if (cm->frame_type == KEY_FRAME) {
    // Tell the caller that the frame was coded as a key frame
    *frame_flags = cpi->frame_flags | FRAMEFLAGS_KEY;
  } else {
    *frame_flags = cpi->frame_flags & ~FRAMEFLAGS_KEY;
  }

  // Clear the one shot update flags for segmentation map and mode/ref loop
  // filter deltas.
  cm->seg.update_map = 0;
  cm->seg.update_data = 0;
  cm->lf.mode_ref_delta_update = 0;

  // keep track of the last coded dimensions
  cm->last_width = cm->width;
  cm->last_height = cm->height;

  // reset to normal state now that we are done.
  if (!cm->show_existing_frame) {
    cm->last_show_frame = cm->show_frame;
    cm->prev_frame = cm->cur_frame;
  }

  if (cm->show_frame) {
    vp9_swap_mi_and_prev_mi(cm);
    // Don't increment frame counters if this was an altref buffer
    // update not a real frame
    ++cm->current_video_frame;
  }
}

static int frame_is_reference(const VP9_COMP *cpi) {
  const VP9_COMMON *cm = &cpi->common;

  return cm->frame_type == KEY_FRAME || cpi->refresh_last_frame ||
         cpi->refresh_golden_frame || cpi->refresh_alt_ref_frame ||
         cm->refresh_frame_context || cm->lf.mode_ref_delta_update ||
         cm->seg.update_map || cm->seg.update_data;
}

int vp9_get_compressed_data(VP9_COMP* cpi, YV12_BUFFER_CONFIG* sd, int enc_flags, size_t targetSize, 
  unsigned int* frame_flags, size_t* size, uint8_t* dest, PSNR_STATS* psnr)
{
  VP9_COMMON *const cm = &cpi->common;
  BufferPool *const pool = cm->buffer_pool;

  update_initial_width(cpi, sd->subsampling_x, sd->subsampling_y);

  int new_dimensions = sd->y_crop_width != cpi->source.y_crop_width ||
                       sd->y_crop_height != cpi->source.y_crop_height ||
                       sd->uv_crop_width != cpi->source.uv_crop_width ||
                       sd->uv_crop_height != cpi->source.uv_crop_height;

  if (new_dimensions) {
    YV12_BUFFER_CONFIG new_img;
    memset(&new_img, 0, sizeof(new_img));
    if (vpx_alloc_frame_buffer(&new_img,
      sd->y_crop_width, sd->y_crop_height,
      sd->subsampling_x, sd->subsampling_y,
      VP9_ENC_BORDER_IN_PIXELS, 0)) {
      return 1;
    }
    vpx_free_frame_buffer(&cpi->source);
    cpi->source = new_img;
  }

  vp9_copy_and_extend_frame(sd, &cpi->source);

  vp9_set_high_precision_mv(cpi, ALTREF_HIGH_PRECISION_MV);

  cm->show_frame = 1;
  cm->intra_only = 0;

  cpi->un_scaled_source = cpi->Source = &cpi->source;

  *frame_flags = (enc_flags & VPX_EFLAG_FORCE_KF) ? FRAMEFLAGS_KEY : 0;

  // Clear down mmx registers
  vpx_clear_system_state();

  // Find a free buffer for the new frame, releasing the reference previously
  // held.
  if (cm->new_fb_idx != INVALID_IDX) {
    --pool->frame_bufs[cm->new_fb_idx].ref_count;
  }
  cm->new_fb_idx = get_free_fb(cm);

  if (cm->new_fb_idx == INVALID_IDX) return -1;

  cm->cur_frame = &pool->frame_bufs[cm->new_fb_idx];

  // Start with a 0 size frame.
  *size = 0;

  cpi->frame_flags = *frame_flags;

  if (frame_is_intra_only(cm) == 1) {
    for (int i = 0; i < REFS_PER_FRAME; ++i) cpi->scaled_ref_idx[i] = INVALID_IDX;
  }

  if ((cm->current_video_frame == 0) || (cpi->frame_flags & FRAMEFLAGS_KEY)) {
    cm->frame_type = KEY_FRAME;
  } else {
    cm->frame_type = INTER_FRAME;
  }

  cpi->td.mb.fp_src_pred = 0;

  encode_frame_to_data_rate(cpi, size, dest, frame_flags, enc_flags, targetSize);
  vpx_calc_psnr(cpi->Source, cpi->common.frame_to_show, psnr);

  if (cm->show_frame) cm->cur_show_frame_fb_idx = cm->new_fb_idx;

  if (cm->refresh_frame_context)
    cm->frame_contexts[cm->frame_context_idx] = *cm->fc;

  // No frame encoded, or frame was dropped, release scaled references.
  if ((*size == 0) && (frame_is_intra_only(cm) == 0)) {
    release_scaled_references(cpi);
  }

  if (*size > 0) {
    cpi->droppable = !frame_is_reference(cpi);
  }

  vpx_clear_system_state();
  return 0;
}

void vp9_apply_encoding_flags(VP9_COMP* cpi, int lst_fb_idx, int gld_fb_idx, int alt_fb_idx, vpx_enc_frame_flags_t flags)
{
  cpi->lst_fb_idx = lst_fb_idx;
  cpi->gld_fb_idx = gld_fb_idx;
  cpi->alt_fb_idx = alt_fb_idx;

  int ref = get_ref_frame_flags(cpi);
  if (flags & VP8_EFLAG_NO_REF_LAST) ref &= ~VP9_LAST_FLAG;
  if (flags & VP8_EFLAG_NO_REF_GF) ref &= ~VP9_GOLD_FLAG;
  if (flags & VP8_EFLAG_NO_REF_ARF) ref &= ~VP9_ALT_FLAG;
  cpi->ref_frame_flags = ref;

  cpi->refresh_last_frame = (flags & VP8_EFLAG_NO_UPD_LAST) == 0;
  cpi->refresh_golden_frame = (flags & VP8_EFLAG_NO_UPD_GF) == 0;
  cpi->refresh_alt_ref_frame = (flags & VP8_EFLAG_NO_UPD_ARF) == 0;

  cpi->common.reset_frame_context = 0;
  cpi->common.refresh_frame_context = (flags & VP8_EFLAG_NO_UPD_ENTROPY) == 0;
}
