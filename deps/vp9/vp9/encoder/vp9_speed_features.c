/*
 *  Copyright (c) 2010 The WebM project authors. All Rights Reserved.
 *
 *  Use of this source code is governed by a BSD-style license
 *  that can be found in the LICENSE file in the root of the source
 *  tree. An additional intellectual property rights grant can be found
 *  in the file PATENTS.  All contributing project authors may
 *  be found in the AUTHORS file in the root of the source tree.
 */

#include <limits.h>

#include "vp9/encoder/vp9_encoder.h"
#include "vp9/encoder/vp9_speed_features.h"
#include "vp9/encoder/vp9_rdopt.h"
#include "vpx_dsp/vpx_dsp_common.h"

void vp9_set_speed_features_framesize_independent(VP9_COMP *cpi) {
  SPEED_FEATURES *const sf = &cpi->sf;
  MACROBLOCK *const x = &cpi->td.mb;
  VP9_COMMON* const cm = &cpi->common;
  const int is_keyframe = cm->frame_type == KEY_FRAME;
  int i;

  for (i = 0; i < TX_SIZES; i++) {
    sf->intra_y_mode_mask[i] = INTRA_ALL;
    sf->intra_uv_mode_mask[i] = INTRA_ALL;
  }

  sf->intra_y_mode_mask[TX_16X16] = INTRA_DC_H_V;
  sf->intra_y_mode_mask[TX_32X32] = INTRA_DC_H_V;
  sf->intra_uv_mode_mask[TX_32X32] = INTRA_DC_H_V;
  sf->intra_uv_mode_mask[TX_16X16] = INTRA_DC_H_V;

  for (int i = 0; i < TX_SIZES; i++) {
      sf->intra_y_mode_mask[i] = INTRA_DC_H_V;
      sf->intra_uv_mode_mask[i] = INTRA_DC;
  }
  sf->intra_y_mode_mask[TX_32X32] = INTRA_DC;

  for (i = 0; i < BLOCK_SIZES; ++i)
      sf->inter_mode_mask[i] = INTER_ALL;

  sf->inter_mode_mask[BLOCK_32X32] = INTER_NEAREST_NEW_ZERO;
  sf->inter_mode_mask[BLOCK_32X64] = INTER_NEAREST_NEW_ZERO;
  sf->inter_mode_mask[BLOCK_64X32] = INTER_NEAREST_NEW_ZERO;
  sf->inter_mode_mask[BLOCK_64X64] = INTER_NEAREST_NEW_ZERO;

  // Reference masking only enabled for 1 spatial layer, and if none of the
  // references have been scaled. The latter condition needs to be checked
  // for external dynamic resize.
  sf->reference_masking = 1;
  if (sf->reference_masking == 1 && cpi->external_resize == 1) {
      MV_REFERENCE_FRAME ref_frame;
      static const int flag_list[4] = { 0, VP9_LAST_FLAG, VP9_GOLD_FLAG,
          VP9_ALT_FLAG };
      for (ref_frame = LAST_FRAME; ref_frame <= ALTREF_FRAME; ++ref_frame) {
          const YV12_BUFFER_CONFIG* yv12 = get_ref_frame_buffer(cpi, ref_frame);
          if (yv12 != NULL && (cpi->ref_frame_flags & flag_list[ref_frame])) {
              const struct scale_factors* const scale_fac = &cm->frame_refs[ref_frame - 1].sf;
              if (vp9_is_scaled(scale_fac))
                  sf->reference_masking = 0;
          }
      }
  }

  sf->tx_size_search_method = is_keyframe ? USE_LARGESTALL : USE_TX_8X8;

  x->min_partition_size = BLOCK_4X4;
  x->max_partition_size = BLOCK_64X64;
}
