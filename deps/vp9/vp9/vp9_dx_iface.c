/*
 *  Copyright (c) 2010 The WebM project authors. All Rights Reserved.
 *
 *  Use of this source code is governed by a BSD-style license
 *  that can be found in the LICENSE file in the root of the source
 *  tree. An additional intellectual property rights grant can be found
 *  in the file PATENTS.  All contributing project authors may
 *  be found in the AUTHORS file in the root of the source tree.
 */

#include <stdlib.h>
#include <string.h>

#include "./vpx_config.h"
#include "./vpx_version.h"

#include "vpx/vp8dx.h"
#include "vpx_dsp/bitreader_buffer.h"
#include "vpx_dsp/vpx_dsp_common.h"

#include "vp9/common/vp9_alloccommon.h"
#include "vp9/common/vp9_frame_buffers.h"

#include "vp9/decoder/vp9_decodeframe.h"

#include "vp9/vp9_dx_iface.h"
#include "vp9/vp9_iface_common.h"

vpx_codec_err_t vp9_codec_dec_init(
    vpx_codec_ctx_t* ctx,
    const vpx_codec_dec_cfg_t* cfg)
{
  // This function only allocates space for the vpx_codec_alg_priv_t
  // structure. More memory may be required at the time the stream
  // information becomes known.

  memset(ctx, 0, sizeof(*ctx));
  ctx->priv = NULL;
  ctx->config.dec = cfg;

  if (!ctx->priv) {
    vpx_codec_alg_priv_t *const priv =
        (vpx_codec_alg_priv_t *)vpx_calloc(1, sizeof(*priv));
    if (priv == NULL) return VPX_CODEC_MEM_ERROR;

    ctx->priv = (vpx_codec_priv_t *)priv;
    priv->si.sz = sizeof(priv->si);
    priv->flushed = 0;
    if (ctx->config.dec) {
      priv->cfg = *ctx->config.dec;
      ctx->config.dec = &priv->cfg;
    }
  }

  return VPX_CODEC_OK;
}

vpx_codec_err_t vp9_codec_dec_destroy(vpx_codec_ctx_t* ctx)
{
  vpx_codec_alg_priv_t* priv = (vpx_codec_alg_priv_t*)ctx->priv;
  if (priv->pbi != NULL) {
    vp9_decoder_remove(priv->pbi);
  }

  if (priv->buffer_pool) {
    vp9_free_ref_frame_buffers(priv->buffer_pool);
    vp9_free_internal_frame_buffers(&priv->buffer_pool->int_frame_buffers);
  }

  vpx_free(priv->buffer_pool);
  vpx_free(priv);

  ctx->priv = NULL;
  return VPX_CODEC_OK;
}

static int parse_bitdepth_colorspace_sampling(BITSTREAM_PROFILE profile,
                                              struct vpx_read_bit_buffer *rb) {
  vpx_color_space_t color_space;
  if (profile >= PROFILE_2) rb->bit_offset += 1;  // Bit-depth 10 or 12.
  color_space = (vpx_color_space_t)vpx_rb_read_literal(rb, 3);
  if (color_space != VPX_CS_SRGB) {
    rb->bit_offset += 1;  // [16,235] (including xvycc) vs [0,255] range.
    if (profile == PROFILE_1 || profile == PROFILE_3) {
      rb->bit_offset += 2;  // subsampling x/y.
      rb->bit_offset += 1;  // unused.
    }
  } else {
    if (profile == PROFILE_1 || profile == PROFILE_3) {
      rb->bit_offset += 1;  // unused
    } else {
      // RGB is only available in version 1.
      return 0;
    }
  }
  return 1;
}

static vpx_codec_err_t decoder_peek_si_internal(
    const uint8_t *data, unsigned int data_sz, vpx_codec_stream_info_t *si,
    int *is_intra_only, vpx_decrypt_cb decrypt_cb, void *decrypt_state) {
  int intra_only_flag = 0;
  uint8_t clear_buffer[11];

  if (data + data_sz <= data) return VPX_CODEC_INVALID_PARAM;

  si->is_kf = 0;
  si->w = si->h = 0;

  if (decrypt_cb) {
    data_sz = VPXMIN(sizeof(clear_buffer), data_sz);
    decrypt_cb(decrypt_state, data, clear_buffer, data_sz);
    data = clear_buffer;
  }

  // A maximum of 6 bits are needed to read the frame marker, profile and
  // show_existing_frame.
  if (data_sz < 1) return VPX_CODEC_UNSUP_BITSTREAM;

  {
    int show_frame;
    int error_resilient;
    struct vpx_read_bit_buffer rb = { data, data + data_sz, 0, NULL, NULL };
    const int frame_marker = vpx_rb_read_literal(&rb, 2);
    const BITSTREAM_PROFILE profile = vp9_read_profile(&rb);

    if (frame_marker != VP9_FRAME_MARKER) return VPX_CODEC_UNSUP_BITSTREAM;

    if (profile >= MAX_PROFILES) return VPX_CODEC_UNSUP_BITSTREAM;

    if (vpx_rb_read_bit(&rb)) {  // show an existing frame
      // If profile is > 2 and show_existing_frame is true, then at least 1 more
      // byte (6+3=9 bits) is needed.
      if (profile > 2 && data_sz < 2) return VPX_CODEC_UNSUP_BITSTREAM;
      vpx_rb_read_literal(&rb, 3);  // Frame buffer to show.
      return VPX_CODEC_OK;
    }

    // For the rest of the function, a maximum of 9 more bytes are needed
    // (computed by taking the maximum possible bits needed in each case). Note
    // that this has to be updated if we read any more bits in this function.
    if (data_sz < 10) return VPX_CODEC_UNSUP_BITSTREAM;

    si->is_kf = !vpx_rb_read_bit(&rb);
    show_frame = vpx_rb_read_bit(&rb);
    error_resilient = vpx_rb_read_bit(&rb);

    if (si->is_kf) {
      if (!vp9_read_sync_code(&rb)) return VPX_CODEC_UNSUP_BITSTREAM;

      if (!parse_bitdepth_colorspace_sampling(profile, &rb))
        return VPX_CODEC_UNSUP_BITSTREAM;
      vp9_read_frame_size(&rb, (int *)&si->w, (int *)&si->h);
    } else {
      intra_only_flag = show_frame ? 0 : vpx_rb_read_bit(&rb);

      rb.bit_offset += error_resilient ? 0 : 2;  // reset_frame_context

      if (intra_only_flag) {
        if (!vp9_read_sync_code(&rb)) return VPX_CODEC_UNSUP_BITSTREAM;
        if (profile > PROFILE_0) {
          if (!parse_bitdepth_colorspace_sampling(profile, &rb))
            return VPX_CODEC_UNSUP_BITSTREAM;
          // The colorspace info may cause vp9_read_frame_size() to need 11
          // bytes.
          if (data_sz < 11) return VPX_CODEC_UNSUP_BITSTREAM;
        }
        rb.bit_offset += REF_FRAMES;  // refresh_frame_flags
        vp9_read_frame_size(&rb, (int *)&si->w, (int *)&si->h);
      }
    }
  }
  if (is_intra_only != NULL) *is_intra_only = intra_only_flag;
  return VPX_CODEC_OK;
}

vpx_codec_err_t vp9_codec_peek_stream_info(
    const uint8_t* data,
    unsigned int data_sz,
    vpx_codec_stream_info_t* si) {
  return decoder_peek_si_internal(data, data_sz, si, NULL, NULL, NULL);
}

vpx_codec_err_t vpx_codec_get_stream_info(vpx_codec_ctx_t * ctx,
    vpx_codec_stream_info_t * si) {

  vpx_codec_alg_priv_t* priv = (vpx_codec_alg_priv_t*)ctx->priv;

  const size_t sz = (si->sz >= sizeof(vp9_stream_info_t))
                        ? sizeof(vp9_stream_info_t)
                        : sizeof(vpx_codec_stream_info_t);
  memcpy(si, &priv->si, sz);
  si->sz = (unsigned int)sz;

  return VPX_CODEC_OK;
}

static void init_buffer_callbacks(vpx_codec_alg_priv_t *ctx) {
  VP9_COMMON *const cm = &ctx->pbi->common;
  BufferPool *const pool = cm->buffer_pool;

  cm->new_fb_idx = INVALID_IDX;
  cm->byte_alignment = ctx->byte_alignment;
  cm->skip_loop_filter = ctx->skip_loop_filter;

  pool->get_fb_cb = vp9_get_frame_buffer;
  pool->release_fb_cb = vp9_release_frame_buffer;

  if (vp9_alloc_internal_frame_buffers(&pool->int_frame_buffers))
    vpx_internal_error(&cm->error, VPX_CODEC_MEM_ERROR,
                       "Failed to initialize internal frame buffers");

  pool->cb_priv = &pool->int_frame_buffers;
}

static vpx_codec_err_t init_decoder(vpx_codec_alg_priv_t *ctx) {
  ctx->last_show_frame = -1;
  ctx->need_resync = 1;
  ctx->flushed = 0;

  ctx->buffer_pool = (BufferPool *)vpx_calloc(1, sizeof(BufferPool));
  if (ctx->buffer_pool == NULL) return VPX_CODEC_MEM_ERROR;

  ctx->pbi = vp9_decoder_create(ctx->buffer_pool);
  if (ctx->pbi == NULL) {
    return VPX_CODEC_MEM_ERROR;
  }
  ctx->pbi->inv_tile_order = ctx->invert_tile_order;

  init_buffer_callbacks(ctx);

  return VPX_CODEC_OK;
}

static INLINE void check_resync(vpx_codec_alg_priv_t *const ctx,
                                const VP9Decoder *const pbi) {
  // Clear resync flag if the decoder got a key frame or intra only frame.
  if (ctx->need_resync == 1 && pbi->need_resync == 0 &&
      (pbi->common.intra_only || pbi->common.frame_type == KEY_FRAME))
    ctx->need_resync = 0;
}

static vpx_codec_err_t decode_one(vpx_codec_alg_priv_t *ctx,
                                  const uint8_t **data, unsigned int data_sz,
                                  void *user_priv) {
  // Determine the stream parameters. Note that we rely on peek_si to
  // validate that we have a buffer that does not wrap around the top
  // of the heap.
  if (!ctx->si.h) {
    int is_intra_only = 0;
    const vpx_codec_err_t res =
        decoder_peek_si_internal(*data, data_sz, &ctx->si, &is_intra_only,
                                 ctx->decrypt_cb, ctx->decrypt_state);
    if (res != VPX_CODEC_OK) return res;

    if (!ctx->si.is_kf && !is_intra_only) return VPX_CODEC_ERROR;
  }

  ctx->user_priv = user_priv;

  // Set these even if already initialized.  The caller may have changed the
  // decrypt config between frames.
  ctx->pbi->decrypt_cb = ctx->decrypt_cb;
  ctx->pbi->decrypt_state = ctx->decrypt_state;

  if (vp9_receive_compressed_data(ctx->pbi, data_sz, data)) {
    ctx->pbi->cur_buf->buf.corrupted = 1;
    ctx->pbi->need_resync = 1;
    ctx->need_resync = 1;
    return ctx->pbi->common.error.error_code;
  }

  check_resync(ctx, ctx->pbi);

  return VPX_CODEC_OK;
}

vpx_codec_err_t vp9_codec_decode(vpx_codec_ctx_t* ctx,
                                 const uint8_t *data, unsigned int data_sz,
                                 void *user_priv) {
  vpx_codec_alg_priv_t* priv = (vpx_codec_alg_priv_t*)ctx->priv;

  const uint8_t* data_start = data;
  const uint8_t *const data_end = data + data_sz;
  vpx_codec_err_t res;
  uint32_t frame_sizes[8];
  int frame_count;

  if (data == NULL && data_sz == 0) {
    priv->flushed = 1;
    return VPX_CODEC_OK;
  }

  // Reset flushed when receiving a valid frame.
  priv->flushed = 0;

  // Initialize the decoder on the first frame.
  if (priv->pbi == NULL) {
    const vpx_codec_err_t res = init_decoder(priv);
    if (res != VPX_CODEC_OK) return res;
  }

  res = vp9_parse_superframe_index(data, data_sz, frame_sizes, &frame_count,
      priv->decrypt_cb, priv->decrypt_state);
  if (res != VPX_CODEC_OK) return res;

  if (priv->svc_decoding && priv->svc_spatial_layer < frame_count - 1)
    frame_count = priv->svc_spatial_layer + 1;

  // Decode in serial mode.
  if (frame_count > 0) {

    for (int i = 0; i < frame_count; ++i) {
      const uint8_t *data_start_copy = data_start;
      const uint32_t frame_size = frame_sizes[i];
      vpx_codec_err_t res;
      if (data_start < data || frame_size > (uint32_t)(data_end - data_start)) {
        return VPX_CODEC_CORRUPT_FRAME;
      }

      res = decode_one(priv, &data_start_copy, frame_size, user_priv);
      if (res != VPX_CODEC_OK) return res;

      data_start += frame_size;
    }
  } else {
    while (data_start < data_end) {
      const uint32_t frame_size = (uint32_t)(data_end - data_start);
      const vpx_codec_err_t res = decode_one(priv, &data_start, frame_size, user_priv);
      if (res != VPX_CODEC_OK) return res;

      // Account for suboptimal termination by the encoder.
      while (data_start < data_end) {
        const uint8_t marker = read_marker(priv->decrypt_cb, priv->decrypt_state, data_start);
        if (marker) break;
        ++data_start;
      }
    }
  }

  return res;
}

vpx_image_t* vp9_codec_get_frame(vpx_codec_ctx_t* ctx) {
  vpx_codec_alg_priv_t* priv = (vpx_codec_alg_priv_t*)ctx->priv;
  vpx_image_t *img = NULL;

  if (priv->pbi != NULL) {
    YV12_BUFFER_CONFIG sd;
    if (vp9_get_raw_frame(priv->pbi, &sd) == 0) {
      VP9_COMMON *const cm = &priv->pbi->common;
      RefCntBuffer *const frame_bufs = cm->buffer_pool->frame_bufs;
      priv->last_show_frame = priv->pbi->common.new_fb_idx;
      if (priv->need_resync)
          return NULL;
      yuvconfig2image(&priv->img, &sd, priv->user_priv);
      priv->img.fb_priv = frame_bufs[cm->new_fb_idx].raw_frame_buffer.priv;
      img = &priv->img;
      return img;
    }
  }
  return NULL;
}
