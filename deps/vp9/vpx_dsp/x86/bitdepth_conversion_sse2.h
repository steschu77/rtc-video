/*
 *  Copyright (c) 2017 The WebM project authors. All Rights Reserved.
 *
 *  Use of this source code is governed by a BSD-style license
 *  that can be found in the LICENSE file in the root of the source
 *  tree. An additional intellectual property rights grant can be found
 *  in the file PATENTS.  All contributing project authors may
 *  be found in the AUTHORS file in the root of the source tree.
 */
#ifndef VPX_VPX_DSP_X86_BITDEPTH_CONVERSION_SSE2_H_
#define VPX_VPX_DSP_X86_BITDEPTH_CONVERSION_SSE2_H_

#include <xmmintrin.h>

#include "./vpx_config.h"
#include "vpx/vpx_integer.h"
#include "vpx_dsp/vpx_dsp_common.h"

// Load 8 16 bit values. If the source is 32 bits then pack down with
// saturation.
static INLINE __m128i load_tran_low(const tran_low_t *a) {
  return _mm_load_si128((const __m128i *)a);
}

// Store 8 16 bit values. If the destination is 32 bits then sign extend the
// values by multiplying by 1.
static INLINE void store_tran_low(__m128i a, tran_low_t *b) {
  _mm_store_si128((__m128i *)(b), a);
}

// Zero fill 8 positions in the output buffer.
static INLINE void store_zero_tran_low(tran_low_t *a) {
  const __m128i zero = _mm_setzero_si128();
  _mm_store_si128((__m128i *)(a), zero);
}
#endif  // VPX_VPX_DSP_X86_BITDEPTH_CONVERSION_SSE2_H_
