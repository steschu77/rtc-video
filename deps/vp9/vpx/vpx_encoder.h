/*
 *  Copyright (c) 2010 The WebM project authors. All Rights Reserved.
 *
 *  Use of this source code is governed by a BSD-style license
 *  that can be found in the LICENSE file in the root of the source
 *  tree. An additional intellectual property rights grant can be found
 *  in the file PATENTS.  All contributing project authors may
 *  be found in the AUTHORS file in the root of the source tree.
 */
#ifndef VPX_VPX_VPX_ENCODER_H_
#define VPX_VPX_VPX_ENCODER_H_

/*!\defgroup encoder Encoder Algorithm Interface
 * \ingroup codec
 * This abstraction allows applications using this encoder to easily support
 * multiple video formats with minimal code duplication. This section describes
 * the interface common to all encoders.
 * @{
 */

/*!\file
 * \brief Describes the encoder algorithm interface to applications.
 *
 * This file describes the interface between an application and a
 * video encoder algorithm.
 *
 */
#ifdef __cplusplus
extern "C" {
#endif

#include "./vpx_codec.h"

/*! Temporal Scalability: Maximum length of the sequence defining frame
 * layer membership
 */
#define VPX_TS_MAX_PERIODICITY 16

/*! Temporal Scalability: Maximum number of coding layers */
#define VPX_TS_MAX_LAYERS 5

/*! Temporal+Spatial Scalability: Maximum number of coding layers */
#define VPX_MAX_LAYERS 12  // 3 temporal + 4 spatial layers are allowed.

/*! Spatial Scalability: Maximum number of coding layers */
#define VPX_SS_MAX_LAYERS 5

/*! Spatial Scalability: Default number of coding layers */
#define VPX_SS_DEFAULT_LAYERS 1

/*!\brief Compressed Frame Flags
 *
 * This type represents a bitfield containing information about a compressed
 * frame that may be useful to an application. The most significant 16 bits
 * can be used by an algorithm to provide additional detail, for example to
 * support frame types that are codec specific (MPEG-1 D-frames for example)
 */
typedef uint32_t vpx_codec_frame_flags_t;
#define VPX_FRAME_IS_KEY 0x1 /**< frame is the start of a GOP */
/*!\brief frame can be dropped without affecting the stream (no future frame
 * depends on this one) */
#define VPX_FRAME_IS_DROPPABLE 0x2
/*!\brief frame should be decoded but will not be shown */
#define VPX_FRAME_IS_INVISIBLE 0x4
/*!\brief this is a fragment of the encoded frame */
#define VPX_FRAME_IS_FRAGMENT 0x8

/*!\brief Encoder output packet
 *
 * This structure contains the different kinds of output data the encoder
 * may produce while compressing a frame.
 */
typedef struct vpx_codec_cx_pkt {
  union {
    struct {
      void *buf; /**< compressed data buffer */
      size_t sz; /**< length of compressed data */
      /*!\brief time stamp to show frame (in timebase units) */
      vpx_codec_pts_t pts;
      vpx_codec_frame_flags_t flags; /**< flags for this frame */
    } frame;                            /**< data for compressed frame packet */
    vpx_fixed_buf_t raw;       /**< data for arbitrary packets */
  } data;                                               /**< packet data */
} vpx_codec_cx_pkt_t; /**< alias for struct vpx_codec_cx_pkt */

/*!\brief Encoder return output buffer callback
 *
 * This callback function, when registered, returns with packets when each
 * spatial layer is encoded.
 */
typedef void (*vpx_codec_enc_output_cx_pkt_cb_fn_t)(vpx_codec_cx_pkt_t *pkt,
                                                    void *user_data);

/*!\brief Rational Number
 *
 * This structure holds a fractional value.
 */
typedef struct vpx_rational {
  int num;        /**< fraction numerator */
  int den;        /**< fraction denominator */
} vpx_rational_t; /**< alias for struct vpx_rational */

/*!\brief Encoded Frame Flags
 *
 * This type indicates a bitfield to be passed to vpx_codec_encode(), defining
 * per-frame boolean values. By convention, bits common to all codecs will be
 * named VPX_EFLAG_*, and bits specific to an algorithm will be named
 * /algo/_eflag_*. The lower order 16 bits are reserved for common use.
 */
typedef long vpx_enc_frame_flags_t;
#define VPX_EFLAG_FORCE_KF (1 << 0) /**< Force this frame to be a keyframe */

/*!\brief Encoder configuration structure
 *
 * This structure contains the encoder settings that have common representations
 * across all codecs. This doesn't imply that all codecs support all features,
 * however.
 */
typedef struct vpx_codec_enc_cfg {
  /*
   * generic settings (g)
   */

  /*!\brief Width of the frame
   *
   * This value identifies the presentation resolution of the frame,
   * in pixels. Note that the frames passed as input to the encoder must
   * have this resolution. Frames will be presented by the decoder in this
   * resolution, independent of any spatial resampling the encoder may do.
   */
  unsigned int g_w;

  /*!\brief Height of the frame
   *
   * This value identifies the presentation resolution of the frame,
   * in pixels. Note that the frames passed as input to the encoder must
   * have this resolution. Frames will be presented by the decoder in this
   * resolution, independent of any spatial resampling the encoder may do.
   */
  unsigned int g_h;

  /*!\brief Stream timebase units
   *
   * Indicates the smallest interval of time, in seconds, used by the stream.
   * For fixed frame rate material, or variable frame rate material where
   * frames are timed at a multiple of a given clock (ex: video capture),
   * the \ref RECOMMENDED method is to set the timebase to the reciprocal
   * of the frame rate (ex: 1001/30000 for 29.970 Hz NTSC). This allows the
   * pts to correspond to the frame number, which can be handy. For
   * re-encoding video from containers with absolute time timestamps, the
   * \ref RECOMMENDED method is to set the timebase to that of the parent
   * container or multimedia framework (ex: 1/1000 for ms, as in FLV).
   */
  struct vpx_rational g_timebase;

} vpx_codec_enc_cfg_t; /**< alias for struct vpx_codec_enc_cfg */

/*!\brief Get a default configuration
 *
 * Initializes a encoder configuration structure with default values. Supports
 * the notion of "usages" so that an algorithm may offer different default
 * settings depending on the user's intended goal. This function \ref SHOULD
 * be called by all applications to initialize the configuration structure
 * before specializing the configuration with application specific values.
 *
 * \param[in]    iface     Pointer to the algorithm interface to use.
 * \param[out]   cfg       Configuration buffer to populate.
 * \param[in]    usage     Must be set to 0.
 *
 * \retval #VPX_CODEC_OK
 *     The configuration was populated.
 * \retval #VPX_CODEC_INCAPABLE
 *     Interface is not an encoder interface.
 * \retval #VPX_CODEC_INVALID_PARAM
 *     A parameter was NULL, or the usage value was not recognized.
 */
vpx_codec_err_t vpx_codec_enc_config_default(vpx_codec_enc_cfg_t *cfg);

/*!@} - end defgroup encoder*/
#ifdef __cplusplus
}
#endif
#endif  // VPX_VPX_VPX_ENCODER_H_
