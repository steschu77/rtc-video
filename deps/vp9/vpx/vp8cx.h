/*
 *  Copyright (c) 2010 The WebM project authors. All Rights Reserved.
 *
 *  Use of this source code is governed by a BSD-style license
 *  that can be found in the LICENSE file in the root of the source
 *  tree. An additional intellectual property rights grant can be found
 *  in the file PATENTS.  All contributing project authors may
 *  be found in the AUTHORS file in the root of the source tree.
 */
#ifndef VPX_VPX_VP8CX_H_
#define VPX_VPX_VP8CX_H_

/*!\defgroup vp8_encoder WebM VP8/VP9 Encoder
 * \ingroup vp8
 *
 * @{
 */
#include "./vp8.h"
#include "./vpx_encoder.h"

/*!\file
 * \brief Provides definitions for using VP8 or VP9 encoder algorithm within the
 *        vpx Codec Interface.
 */

#ifdef __cplusplus
extern "C" {
#endif

/*!\name Algorithm interface for VP8
 *
 * This interface provides the capability to encode raw VP8 streams.
 * @{
 */
extern vpx_codec_iface_t vpx_codec_vp8_cx_algo;
extern vpx_codec_iface_t *vpx_codec_vp8_cx(void);
/*!@} - end algorithm interface member group*/

/*!\name Algorithm interface for VP9
 *
 * This interface provides the capability to encode raw VP9 streams.
 * @{
 */
extern vpx_codec_iface_t vpx_codec_vp9_cx_algo;
extern vpx_codec_iface_t *vpx_codec_vp9_cx(void);
/*!@} - end algorithm interface member group*/

/*
 * Algorithm Flags
 */

/*!\brief Don't reference the last frame
 *
 * When this flag is set, the encoder will not use the last frame as a
 * predictor. When not set, the encoder will choose whether to use the
 * last frame or not automatically.
 */
#define VP8_EFLAG_NO_REF_LAST (1 << 16)

/*!\brief Don't reference the golden frame
 *
 * When this flag is set, the encoder will not use the golden frame as a
 * predictor. When not set, the encoder will choose whether to use the
 * golden frame or not automatically.
 */
#define VP8_EFLAG_NO_REF_GF (1 << 17)

/*!\brief Don't reference the alternate reference frame
 *
 * When this flag is set, the encoder will not use the alt ref frame as a
 * predictor. When not set, the encoder will choose whether to use the
 * alt ref frame or not automatically.
 */
#define VP8_EFLAG_NO_REF_ARF (1 << 21)

/*!\brief Don't update the last frame
 *
 * When this flag is set, the encoder will not update the last frame with
 * the contents of the current frame.
 */
#define VP8_EFLAG_NO_UPD_LAST (1 << 18)

/*!\brief Don't update the golden frame
 *
 * When this flag is set, the encoder will not update the golden frame with
 * the contents of the current frame.
 */
#define VP8_EFLAG_NO_UPD_GF (1 << 22)

/*!\brief Don't update the alternate reference frame
 *
 * When this flag is set, the encoder will not update the alt ref frame with
 * the contents of the current frame.
 */
#define VP8_EFLAG_NO_UPD_ARF (1 << 23)

/*!\brief Force golden frame update
 *
 * When this flag is set, the encoder copy the contents of the current frame
 * to the golden frame buffer.
 */
#define VP8_EFLAG_FORCE_GF (1 << 19)

/*!\brief Force alternate reference frame update
 *
 * When this flag is set, the encoder copy the contents of the current frame
 * to the alternate reference frame buffer.
 */
#define VP8_EFLAG_FORCE_ARF (1 << 24)

/*!\brief Disable entropy update
 *
 * When this flag is set, the encoder will not update its internal entropy
 * model based on the entropy of this frame.
 */
#define VP8_EFLAG_NO_UPD_ENTROPY (1 << 20)

#define VP8_EFLAG_NO_QPOPT (1 << 28)

/*!\brief VPx encoder control functions
 *
 * This set of macros define the control functions available for VPx
 * encoder interface.
 *
 * \sa #vpx_codec_control
 */
enum vp8e_enc_control_id {

  VP9_FIRST_CONTROL_ID = 11,

  /*!\brief Codec control function to turn on/off SVC in encoder.
   * \note Return value is VPX_CODEC_INVALID_PARAM if the encoder does not
   *       support SVC in its current encoding mode
   *  0: off, 1: on
   *
   * Supported in codecs: VP9
   */
  VP9E_SET_SVC,

  /*!\brief Codec control function to set parameters for SVC.
   * \note Parameters contain min_q, max_q, scaling factor for each of the
   *       SVC layers.
   *
   * Supported in codecs: VP9
   */
  VP9E_SET_SVC_PARAMETERS,

  /*!\brief Codec control function to set svc layer for spatial and temporal.
   * \note Valid ranges: 0..#vpx_codec_enc_cfg::ss_number_layers for spatial
   *                     layer and 0..#vpx_codec_enc_cfg::ts_number_layers for
   *                     temporal layer.
   *
   * Supported in codecs: VP9
   */
  VP9E_SET_SVC_LAYER_ID,

  /*!\brief Codec control function to get svc layer ID.
   * \note The layer ID returned is for the data packet from the registered
   *       callback function.
   *
   * Supported in codecs: VP9
   */
  VP9E_GET_SVC_LAYER_ID,

  /*!\brief Codec control function to register callback to get per layer packet.
   * \note Parameter for this control function is a structure with a callback
   *       function and a pointer to private data used by the callback.
   *
   * Supported in codecs: VP9
   */
  VP9E_REGISTER_CX_CALLBACK,

  /*!\brief Codec control function to set the frame flags and buffer indices
   * for spatial layers. The frame flags and buffer indices are set using the
   * struct #vpx_svc_ref_frame_config defined below.
   *
   * Supported in codecs: VP9
   */
  VP9E_SET_SVC_REF_FRAME_CONFIG,

  /*!\brief Codec control function to enable postencode frame drop.
   *
   * This will allow encoder to drop frame after it's encoded.
   *
   * 0: Off (default), 1: Enabled
   *
   * Supported in codecs: VP9
   */
  VP9E_SET_POSTENCODE_DROP,
};

/*!\brief vpx 1-D scaling mode
 *
 * This set of constants define 1-D vpx scaling modes
 */
typedef enum vpx_scaling_mode_1d {
  VP8E_NORMAL = 0,
  VP8E_FOURFIVE = 1,
  VP8E_THREEFIVE = 2,
  VP8E_ONETWO = 3
} VPX_SCALING_MODE;

/*!\brief  vp9 svc layer parameters
 *
 * This defines the spatial and temporal layer id numbers for svc encoding.
 * This is used with the #VP9E_SET_SVC_LAYER_ID control to set the spatial and
 * temporal layer id for the current frame.
 *
 */
typedef struct vpx_svc_layer_id {
  int spatial_layer_id; /**< First spatial layer to start encoding. */
  // TODO(jianj): Deprecated, to be removed.
  int temporal_layer_id; /**< Temporal layer id number. */
  int temporal_layer_id_per_spatial[VPX_SS_MAX_LAYERS]; /**< Temp layer id. */
} vpx_svc_layer_id_t;

/*!\brief vp9 svc frame flag parameters.
 *
 * This defines the frame flags and buffer indices for each spatial layer for
 * svc encoding.
 * This is used with the #VP9E_SET_SVC_REF_FRAME_CONFIG control to set frame
 * flags and buffer indices for each spatial layer for the current (super)frame.
 *
 */
typedef struct vpx_svc_ref_frame_config {
  int lst_fb_idx[VPX_SS_MAX_LAYERS];         /**< Last buffer index. */
  int gld_fb_idx[VPX_SS_MAX_LAYERS];         /**< Golden buffer index. */
  int alt_fb_idx[VPX_SS_MAX_LAYERS];         /**< Altref buffer index. */
  int update_buffer_slot[VPX_SS_MAX_LAYERS]; /**< Update reference frames. */
  int reference_last[VPX_SS_MAX_LAYERS];    /**< Last as reference. */
  int reference_golden[VPX_SS_MAX_LAYERS];  /**< Golden as reference. */
  int reference_alt_ref[VPX_SS_MAX_LAYERS]; /**< Altref as reference. */
} vpx_svc_ref_frame_config_t;

/*!\brief VP9 svc frame dropping mode.
 *
 * This defines the frame drop mode for SVC.
 *
 */
typedef enum {
  CONSTRAINED_LAYER_DROP,
  /**< Upper layers are constrained to drop if current layer drops. */
  LAYER_DROP,           /**< Any spatial layer can drop. */
  FULL_SUPERFRAME_DROP, /**< Only full superframe can drop. */
  CONSTRAINED_FROM_ABOVE_DROP,
  /**< Lower layers are constrained to drop if current layer drops. */
} SVC_LAYER_DROP_MODE;

/*!\cond */

/*!\endcond */
/*! @} - end defgroup vp8_encoder */
#ifdef __cplusplus
}  // extern "C"
#endif

#endif  // VPX_VPX_VP8CX_H_
