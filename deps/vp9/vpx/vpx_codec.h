/*
 *  Copyright (c) 2010 The WebM project authors. All Rights Reserved.
 *
 *  Use of this source code is governed by a BSD-style license
 *  that can be found in the LICENSE file in the root of the source
 *  tree. An additional intellectual property rights grant can be found
 *  in the file PATENTS.  All contributing project authors may
 *  be found in the AUTHORS file in the root of the source tree.
 */

/*!\defgroup codec Common Algorithm Interface
 * This abstraction allows applications to easily support multiple video
 * formats with minimal code duplication. This section describes the interface
 * common to all codecs (both encoders and decoders).
 * @{
 */

/*!\file
 * \brief Describes the codec algorithm interface to applications.
 *
 * This file describes the interface between an application and a
 * video codec algorithm.
 *
 * An application instantiates a specific codec instance by using
 * vpx_codec_init() and a pointer to the algorithm's interface structure:
 *     <pre>
 *     my_app.c:
 *       extern vpx_codec_iface_t my_codec;
 *       {
 *           vpx_codec_ctx_t algo;
 *           res = vpx_codec_init(&algo, &my_codec);
 *       }
 *     </pre>
 *
 * Once initialized, the instance is manged using other functions from
 * the vpx_codec_* family.
 */
#ifndef VPX_VPX_VPX_CODEC_H_
#define VPX_VPX_VPX_CODEC_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "./vpx_image.h"
#include "./vpx_integer.h"

/*!\brief Decorator indicating a function is deprecated */
#ifndef VPX_DEPRECATED
#if defined(__GNUC__) && __GNUC__
#define VPX_DEPRECATED __attribute__((deprecated))
#elif defined(_MSC_VER)
#define VPX_DEPRECATED
#else
#define VPX_DEPRECATED
#endif
#endif /* VPX_DEPRECATED */

#ifndef VPX_DECLSPEC_DEPRECATED
#if defined(__GNUC__) && __GNUC__
#define VPX_DECLSPEC_DEPRECATED /**< \copydoc #VPX_DEPRECATED */
#elif defined(_MSC_VER)
/*!\brief \copydoc #VPX_DEPRECATED */
#define VPX_DECLSPEC_DEPRECATED __declspec(deprecated)
#else
#define VPX_DECLSPEC_DEPRECATED /**< \copydoc #VPX_DEPRECATED */
#endif
#endif /* VPX_DECLSPEC_DEPRECATED */

/*!\brief Decorator indicating a function is potentially unused */
#ifndef VPX_UNUSED
#if defined(__GNUC__) || defined(__clang__)
#define VPX_UNUSED __attribute__((unused))
#else
#define VPX_UNUSED
#endif
#endif /* VPX_UNUSED */

/*!\brief Current ABI version number
 *
 * \internal
 * If this file is altered in any way that changes the ABI, this value
 * must be bumped.  Examples include, but are not limited to, changing
 * types, removing or reassigning enums, adding/removing/rearranging
 * fields to structures
 */
#define VPX_CODEC_ABI_VERSION (4 + VPX_IMAGE_ABI_VERSION) /**<\hideinitializer*/

/*!\brief Algorithm return codes */
typedef enum {
  /*!\brief Operation completed without error */
  VPX_CODEC_OK,

  /*!\brief Unspecified error */
  VPX_CODEC_ERROR,

  /*!\brief Memory operation failed */
  VPX_CODEC_MEM_ERROR,

  /*!\brief ABI version mismatch */
  VPX_CODEC_ABI_MISMATCH,

  /*!\brief Algorithm does not have required capability */
  VPX_CODEC_INCAPABLE,

  /*!\brief The given bitstream is not supported.
   *
   * The bitstream was unable to be parsed at the highest level. The decoder
   * is unable to proceed. This error \ref SHOULD be treated as fatal to the
   * stream. */
  VPX_CODEC_UNSUP_BITSTREAM,

  /*!\brief Encoded bitstream uses an unsupported feature
   *
   * The decoder does not implement a feature required by the encoder. This
   * return code should only be used for features that prevent future
   * pictures from being properly decoded. This error \ref MAY be treated as
   * fatal to the stream or \ref MAY be treated as fatal to the current GOP.
   */
  VPX_CODEC_UNSUP_FEATURE,

  /*!\brief The coded data for this stream is corrupt or incomplete
   *
   * There was a problem decoding the current frame.  This return code
   * should only be used for failures that prevent future pictures from
   * being properly decoded. This error \ref MAY be treated as fatal to the
   * stream or \ref MAY be treated as fatal to the current GOP. If decoding
   * is continued for the current GOP, artifacts may be present.
   */
  VPX_CODEC_CORRUPT_FRAME,

  /*!\brief An application-supplied parameter is not valid.
   *
   */
  VPX_CODEC_INVALID_PARAM,

  /*!\brief An iterator reached the end of list.
   *
   */
  VPX_CODEC_LIST_END

} vpx_codec_err_t;

/*!\brief Codec interface structure.
 *
 * Contains function pointers and other data private to the codec
 * implementation. This structure is opaque to the application.
 */
typedef const struct vpx_codec_iface vpx_codec_iface_t;

/*!\brief Codec private data structure.
 *
 * Contains data private to the codec implementation. This structure is opaque
 * to the application.
 */
typedef struct vpx_codec_priv vpx_codec_priv_t;

/*!\brief Codec context structure
 *
 * All codecs \ref MUST support this context structure fully. In general,
 * this data should be considered private to the codec algorithm, and
 * not be manipulated or examined by the calling application. Applications
 * may reference the 'name' member to get a printable description of the
 * algorithm.
 */
typedef struct vpx_codec_ctx {
  union {
    /**< Decoder Configuration Pointer */
    const struct vpx_codec_dec_cfg *dec;
    /**< Encoder Configuration Pointer */
    const struct vpx_codec_enc_cfg *enc;
    const void *raw;
  } config;               /**< Configuration pointer aliasing union */
  vpx_codec_priv_t *priv; /**< Algorithm private storage */
} vpx_codec_ctx_t;

/*!\brief Bit depth for codec
 * *
 * This enumeration determines the bit depth of the codec.
 */
typedef enum vpx_bit_depth {
  VPX_BITS_8 = 8,   /**<  8 bits */
  VPX_BITS_10 = 10, /**< 10 bits */
  VPX_BITS_12 = 12, /**< 12 bits */
} vpx_bit_depth_t;

/*!\brief Stream properties
 *
 * This structure is used to query or set properties of the decoded
 * stream. Algorithms may extend this structure with data specific
 * to their bitstream by setting the sz member appropriately.
 */
typedef struct vpx_codec_stream_info {
    unsigned int sz; /**< Size of this structure */
    unsigned int w; /**< Width (or 0 for unknown/default) */
    unsigned int h; /**< Height (or 0 for unknown/default) */
    unsigned int is_kf; /**< Current frame is a keyframe */
} vpx_codec_stream_info_t;

/* REQUIRED FUNCTIONS
 *
 * The following functions are required to be implemented for all decoders.
 * They represent the base case functionality expected of all decoders.
 */

/*!\brief Initialization Configurations
 *
 * This structure is used to pass init time configuration options to the
 * decoder.
 */
typedef struct vpx_codec_dec_cfg {
    unsigned int w; /**< Width */
    unsigned int h; /**< Height */
} vpx_codec_dec_cfg_t; /**< alias for struct vpx_codec_dec_cfg */

/*!\brief Generic fixed size buffer structure
 *
 * This structure is able to hold a reference to any fixed size buffer.
 */
typedef struct vpx_fixed_buf {
    void* buf; /**< Pointer to the data */
    size_t sz; /**< Length of the buffer, in chars */
} vpx_fixed_buf_t; /**< alias for struct vpx_fixed_buf */

/*!\brief Time Stamp Type
 *
 * An integer, which when multiplied by the stream's time base, provides
 * the absolute time of a sample.
 */
typedef int64_t vpx_codec_pts_t;

/*!@} - end defgroup codec*/
#ifdef __cplusplus
}
#endif
#endif  // VPX_VPX_VPX_CODEC_H_
