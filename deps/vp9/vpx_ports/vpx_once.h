/*
 *  Copyright (c) 2015 The WebM project authors. All Rights Reserved.
 *
 *  Use of this source code is governed by a BSD-style license
 *  that can be found in the LICENSE file in the root of the source
 *  tree. An additional intellectual property rights grant can be found
 *  in the file PATENTS.  All contributing project authors may
 *  be found in the AUTHORS file in the root of the source tree.
 */

#ifndef VPX_VPX_PORTS_VPX_ONCE_H_
#define VPX_VPX_PORTS_VPX_ONCE_H_

#include "vpx_config.h"

/* Implement a function wrapper to guarantee initialization
 * thread-safety for library singletons.
 *
 * NOTE: These functions use static locks, and can only be
 * used with one common argument per compilation unit. So
 *
 * file1.c:
 *   vpx_once(foo);
 *   ...
 *   vpx_once(foo);
 *
 *   file2.c:
 *     vpx_once(bar);
 *
 * will ensure foo() and bar() are each called only once, but in
 *
 * file1.c:
 *   vpx_once(foo);
 *   vpx_once(bar):
 *
 * bar() will never be called because the lock is used up
 * by the call to foo().
 */

/* No-op version that performs no synchronization. *_rtcd() is idempotent,
 * so as long as your platform provides atomic loads/stores of pointers
 * no synchronization is strictly necessary.
 */

static void once(void (*func)(void)) {
  static int done;

  if (!done) {
    func();
    done = 1;
  }
}

#endif  // VPX_VPX_PORTS_VPX_ONCE_H_
