/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */

#pragma once

#include <stdio.h>

#include "cutil_utf8str.h"

#include "cunit_fixture.h"
#include "cunit_test.h"

#include "cutil_list.h"

// ----------------------------------------------------------------------------
struct cunitFailure_s;
typedef struct cunitFailure_s cunitFailure;

// ----------------------------------------------------------------------------
const cunitFailure* cunitCreateFailure(
    const cunitFixture* fixture, const cunitTest* test,
    const utf8str_t* file, unsigned line, const utf8str_t* condition);

void cunitDestroyFailure(cunitFailure*);
void destroyFailureItem(const void* item);

void printFailure(const cunitFailure*, FILE* output);
void printFailures(const cutilList*, FILE* output);
