/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */

#pragma once

#include "cutil_list.h"
#include "cutil_utf8str.h"

#include "cunit_test.h"

typedef void* (*cunitFnSetup)();
typedef void (*cunitFnTeardown)(void* context);

// ----------------------------------------------------------------------------
struct cunitFixture_s;
typedef struct cunitFixture_s cunitFixture;

// ----------------------------------------------------------------------------
const cunitFixture* cunitAddFixture(const utf8str_t* name,
    cunitFnSetup fnSetup, cunitFnTeardown fnTeardown);
void destroyFixture(const cunitFixture*);
void destroyFixtureItem(const void*);

const utf8str_t* getFixtureName(const cunitFixture*);
const cutilList* getFixtureTests(const cunitFixture*);

void* startupFixture(const cunitFixture*);
void teardownFixture(const cunitFixture*, void* context);

void cunitAddTest(const cunitFixture*, const utf8str_t* name, cunitFnTest fnTest);

#define CUNIT_ADD_TEST(fixture, test) (cunitAddTest(#test, (cunitFnTest)test, fixture))
