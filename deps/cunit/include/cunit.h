/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */

#pragma once

#include <math.h>
#include <string.h>

#include "cutil_list.h"
#include "cutil_utf8str.h"

#define EXPECT_TRUE(value) \
    cunitAssert((value), STR("EXPECT_TRUE(" #value ")"), STR(__FILE__), __LINE__, 0)

#define ASSERT_TRUE(value) \
    cunitAssert((value), STR("ASSERT_TRUE(" #value ")"), STR(__FILE__), __LINE__, 1)

#define EXPECT_FALSE(value) \
    cunitAssert(!(value), STR("EXPECT_FALSE(" #value ")"), STR(__FILE__), __LINE__, 0)

#define ASSERT_FALSE(value) \
    cunitAssert(!(value), STR("ASSERT_FALSE(" #value ")"), STR(__FILE__), __LINE__, 1)

#define EXPECT_EQ(actual, expected) \
    cunitAssert(((actual) == (expected)), STR(#actual " == " #expected), STR(__FILE__), __LINE__, 0)

#define ASSERT_EQ(actual, expected) \
    cunitAssert(((actual) == (expected)), STR(#actual " == " #expected), STR(__FILE__), __LINE__, 1)

#define EXPECT_NE(actual, expected) \
    cunitAssert(((actual) != (expected)), STR(#actual " != " #expected), STR(__FILE__), __LINE__, 0)

#define ASSERT_NE(actual, expected) \
    cunitAssert(((actual) != (expected)), STR(#actual " != " #expected), STR(__FILE__), __LINE__, 1)

#define EXPECT_LT(actual, expected) \
    cunitAssert(((actual) < (expected)), STR(#actual " < " #expected), STR(__FILE__), __LINE__, 0)

#define ASSERT_LT(actual, expected) \
    cunitAssert(((actual) < (expected)), STR(#actual " < " #expected), STR(__FILE__), __LINE__, 1)

#define EXPECT_LE(actual, expected) \
    cunitAssert(((actual) <= (expected)), STR(#actual " <= " #expected), STR(__FILE__), __LINE__, 0)

#define ASSERT_LE(actual, expected) \
    cunitAssert(((actual) <= (expected)), STR(#actual " <= " #expected), STR(__FILE__), __LINE__, 1)

#define EXPECT_GT(actual, expected) \
    cunitAssert(((actual) > (expected)), STR(#actual " > " #expected), STR(__FILE__), __LINE__, 0)

#define ASSERT_GT(actual, expected) \
    cunitAssert(((actual) > (expected)), STR(#actual " > " #expected), STR(__FILE__), __LINE__, 1)

#define EXPECT_GE(actual, expected) \
    cunitAssert(((actual) >= (expected)), STR(#actual " >= " #expected), STR(__FILE__), __LINE__, 0)

#define ASSERT_GE(actual, expected) \
    cunitAssert(((actual) >= (expected)), STR(#actual " >= " #expected), STR(__FILE__), __LINE__, 1)

typedef enum {
    cunitPrintNothing,
    cunitPrintFailuresOnly,
    cunitPrintEverything,
} cunitVerboseMode;

int cunitRunTests(cunitVerboseMode mode, const cutilList* fixtures);

int cunitAssert(int value, const utf8str_t* condition,
    const utf8str_t* file, unsigned line, int fatal);
