/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */

#pragma once

#include <stdio.h>
#include <time.h>

#include "cutil_list.h"
#include "cutil_utf8str.h"

#include "cunit.h"
#include "cunit_failure.h"
#include "cunit_fixture.h"
#include "cunit_test.h"

typedef void (*cunitFnFixtureStart)(const cunitFixture* fixture, void* context);
typedef void (*cunitFnFixtureComplete)(const cunitFixture* fixture, void* context);
typedef void (*cunitFnTestStart)(const cunitFixture* fixture, const cunitTest* test, void* context);
typedef void (*cunitFnTestComplete)(const cunitFixture* fixture, const cunitTest* test, const cutilList* failures, void* context);

struct cunitTestRun_s;
typedef struct cunitTestRun_s cunitTestRun;

const cunitTestRun* runAllTests(
    const cutilList* fixtures,
    cunitFnFixtureStart fnFixtureStart,
    cunitFnFixtureComplete fnFixtureComplete,
    cunitFnTestStart fnTestStart,
    cunitFnTestComplete fnTestComplete,
    void* context);

void destroyTestRun(const cunitTestRun* run);

const utf8str_t* getTestResultDesc(const cunitTestRun* testrun);
int getTestResult(const cunitTestRun* testrun);
