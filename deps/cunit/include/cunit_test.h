/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */

#pragma once

#include "cutil_utf8str.h"

// ----------------------------------------------------------------------------
struct cunitTest_s;
typedef struct cunitTest_s cunitTest;

typedef void (*cunitFnTest)(void* context);

cunitTest* createTest(const utf8str_t* name, cunitFnTest fnTest);
void destroyTest(const cunitTest*);
void destroyTestItem(const void*);

const utf8str_t* getTestName(const cunitTest*);
void runTest(const cunitTest*, void* fixture);
