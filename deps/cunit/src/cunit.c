/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "cutil_color.h"

#include "cunit.h"
#include "cunit_fixture.h"
#include "cunit_testrun.h"

#include "cunit_failure.c"
#include "cunit_fixture.c"
#include "cunit_test.c"
#include "cunit_testrun.c"

// ----------------------------------------------------------------------------
struct cunitBasicTest_s {
    cunitVerboseMode mode;
    FILE* output;
};

typedef struct cunitBasicTest_s cunitBasicTest;

// ----------------------------------------------------------------------------
static void onFixtureStart(const cunitFixture* fixture, void* ptr)
{
    cunitBasicTest* env = (cunitBasicTest*)ptr;

    if (env->mode == cunitPrintEverything) {
        fprintf(env->output, "\n\nFixture: %s", utf8_cstr(getFixtureName(fixture)));
    }
}

// ----------------------------------------------------------------------------
static void onFixtureComplete(const cunitFixture* fixture, void* ptr)
{
}

// ----------------------------------------------------------------------------
static void onTestStart(const cunitFixture* fixture, const cunitTest* test, void* ptr)
{
    cunitBasicTest* env = (cunitBasicTest*)ptr;

    if (env->mode == cunitPrintEverything) {
        fprintf(env->output, "\n   Test: %s ...", utf8_cstr(getTestName(test)));
    }
}

// ----------------------------------------------------------------------------
static void onTestComplete(
    const cunitFixture* fixture,
    const cunitTest* test, const cutilList* failures, void* ptr)
{
    cunitBasicTest* env = (cunitBasicTest*)ptr;

    switch (env->mode) {
    case cunitPrintEverything:
        fprintf(env->output, "%s", (failures == nullptr) ? ANSI_BRIGHTGREEN "passed" ANSI_RESET : ANSI_BRIGHTRED "FAILED" ANSI_RESET);
        if (failures != nullptr) {
            printFailures(failures, env->output);
        }
        break;
    case cunitPrintFailuresOnly:
        if (failures != nullptr) {
            fprintf(env->output, "\nTest " ANSI_YELLOW "%s.%s" ANSI_RESET " had failures:",
                utf8_cstr(getFixtureName(fixture)),
                utf8_cstr(getTestName(test)));
            printFailures(failures, env->output);
        }
        break;
    case cunitPrintNothing:
        break;
    }
}

// ----------------------------------------------------------------------------
int cunitRunTests(cunitVerboseMode mode, const cutilList* fixtures)
{
    cunitBasicTest test;
    test.mode = mode;
    test.output = stdout;

    setvbuf(test.output, NULL, _IONBF, 0);

    if (test.mode != cunitPrintNothing) {
        fprintf(test.output, ANSI_BRIGHTMAGENTA "cunit" ANSI_RESET " - Unit testing for C\n");
    }

    const cunitTestRun* run = runAllTests(fixtures,
        onFixtureStart, onFixtureComplete, onTestStart, onTestComplete, &test);

    if (test.mode != cunitPrintNothing) {
        const utf8str_t* result = getTestResultDesc(run);
        fprintf(test.output, "\n\n%s", utf8_cstr(result));
        utf8_free(&result);
    }

    int result = getTestResult(run);
    destroyTestRun(run);

    return result;
}
