/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */

#define _CRT_SECURE_NO_WARNINGS

#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "cutil_nullptr.h"

#include "cunit_test.h"

// ----------------------------------------------------------------------------
struct cunitTest_s {
    const utf8str_t* name;
    cunitFnTest fnTest;
};

// ----------------------------------------------------------------------------
cunitTest* createTest(const utf8str_t* name, cunitFnTest fnTest)
{
    if (fnTest == nullptr) {
        return nullptr;
    }

    cunitTest* test = (cunitTest*)malloc(sizeof(cunitTest));
    if (test == nullptr) {
        return nullptr;
    }

    test->name = utf8_assign(name);
    test->fnTest = fnTest;
    return test;
}

// ----------------------------------------------------------------------------
void destroyTest(const cunitTest* test)
{
    utf8_decref(test->name);
    free((void*)test);
}

// ----------------------------------------------------------------------------
void destroyTestItem(const void* item)
{
    destroyTest((const cunitTest*)item);
}

// ----------------------------------------------------------------------------
const utf8str_t* getTestName(const cunitTest* test)
{
    return test->name;
}

// ----------------------------------------------------------------------------
void runTest(const cunitTest* test, void* fixture)
{
    (*test->fnTest)(fixture);
}
