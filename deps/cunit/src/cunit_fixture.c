/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */

#define _CRT_SECURE_NO_WARNINGS

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "cunit.h"
#include "cunit_fixture.h"
#include "cunit_testrun.h"

#include "cutil_list.h"

// ----------------------------------------------------------------------------
struct cunitFixture_s {
    const utf8str_t* name;
    cunitFnSetup fnSetup;
    cunitFnTeardown fnTeardown;
    cutilList* tests;
};

// ----------------------------------------------------------------------------
const cunitFixture* cunitAddFixture(const utf8str_t* name,
    cunitFnSetup fnSetup, cunitFnTeardown fnTeardown)
{
    cutilList* tests = createList(destroyTestItem);
    if (tests == nullptr) {
        return nullptr;
    }

    cunitFixture* fixture = (cunitFixture*)malloc(sizeof(cunitFixture));
    if (fixture == nullptr) {
        return nullptr;
    }

    fixture->name = utf8_assign(name);
    fixture->fnSetup = fnSetup;
    fixture->fnTeardown = fnTeardown;
    fixture->tests = tests;

    return fixture;
}

// ----------------------------------------------------------------------------
void destroyFixture(const cunitFixture* fixture)
{
    if (fixture == nullptr) {
        return;
    }

    destroyList(fixture->tests);
    free((void*)fixture);
}

// ----------------------------------------------------------------------------
void destroyFixtureItem(const void* item)
{
    destroyFixture((const cunitFixture*)item);
}

// ----------------------------------------------------------------------------
const utf8str_t* getFixtureName(const cunitFixture* fixture)
{
    return fixture->name;
}
// ----------------------------------------------------------------------------
const cutilList* getFixtureTests(const cunitFixture* fixture)
{
    return fixture->tests;
}

// ----------------------------------------------------------------------------
void* startupFixture(const cunitFixture* fixture)
{
    if (fixture->fnSetup != nullptr) {
        return (*fixture->fnSetup)();
    } else {
        return nullptr;
    }
}

// ----------------------------------------------------------------------------
void teardownFixture(const cunitFixture* fixture, void* context)
{
    if (fixture->fnTeardown != nullptr) {
        (*fixture->fnTeardown)(context);
    }
}

// ----------------------------------------------------------------------------
void cunitAddTest(const cunitFixture* fixture, const utf8str_t* name, cunitFnTest fnTest)
{
    cunitTest* test = createTest(name, fnTest);
    listAddItem(fixture->tests, test);
}
