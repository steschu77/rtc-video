/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */

#define _CRT_SECURE_NO_WARNINGS

#include <assert.h>
#include <setjmp.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "cutil_color.h"

#include "cunit.h"
#include "cunit_failure.h"
#include "cunit_fixture.h"
#include "cunit_testrun.h"

// ----------------------------------------------------------------------------
struct cunitFailure_s {
    const cunitFixture* fixture;
    const cunitTest* test;
    const utf8str_t* file;
    unsigned line;
    const utf8str_t* condition;
};

// ----------------------------------------------------------------------------
const cunitFailure* cunitCreateFailure(
    const cunitFixture* fixture, const cunitTest* test,
    const utf8str_t* file, unsigned line, const utf8str_t* condition)
{
    cunitFailure* failure = (cunitFailure*)malloc(sizeof(cunitFailure));
    if (NULL == failure) {
        return nullptr;
    }

    failure->fixture = fixture;
    failure->test = test;
    failure->file = utf8_assign(file);
    failure->line = line;
    failure->condition = utf8_assign(condition);

    return failure;
}

// ----------------------------------------------------------------------------
void cunitDestroyFailure(cunitFailure* failure)
{
    if (failure != nullptr) {
        utf8_free(&failure->file);
        utf8_free(&failure->condition);
        free(failure);
    }
}

// ----------------------------------------------------------------------------
void destroyFailureItem(const void* item)
{
    cunitDestroyFailure((cunitFailure*)item);
}

// ----------------------------------------------------------------------------
void printFailure(const cunitFailure* failure, FILE* output)
{
    fprintf(output, "\n   %s:%u - %s",
        utf8_cstr(failure->file),
        failure->line,
        utf8_cstr(failure->condition));
}

// ----------------------------------------------------------------------------
int printFailureItem(const void* item, void* ptr)
{
    printFailure((const cunitFailure*)item, (FILE*)ptr);
    return 0;
}

// ----------------------------------------------------------------------------
void printFailures(const cutilList* failures, FILE* output)
{
    listIterate(failures, printFailureItem, output);
}
