/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */

#define _CRT_SECURE_NO_WARNINGS

#include <assert.h>
#include <setjmp.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "cutil_color.h"
#include "cutil_nullptr.h"
#include "cutil_utf8str.h"

#include "cunit.h"
#include "cunit_failure.h"
#include "cunit_fixture.h"
#include "cunit_test.h"
#include "cunit_testrun.h"

// ----------------------------------------------------------------------------
void addTestFailure(cunitTestRun* testrun, const cunitFailure* failure);

// ----------------------------------------------------------------------------
struct cunitTestRun_s {

    unsigned numFixturesRun;
    unsigned numFixturesFailed;
    unsigned numTestsRun;
    unsigned numTestsFailed;
    unsigned numAsserts;
    unsigned numAssertsFailed;
    double elapsedTime;

    cutilList* allFailures;
    jmp_buf* jmpFatal;

    cutilList* currentFailures;
    const cunitFixture* currentFixture;
    const cunitTest* currentTest;

    cunitFnFixtureStart fnFixtureStart;
    cunitFnFixtureComplete fnFixtureComplete;
    cunitFnTestStart fnTestStart;
    cunitFnTestComplete fnTestComplete;
    void* context;
};

// ----------------------------------------------------------------------------
static cunitTestRun* testrun = nullptr;

// ----------------------------------------------------------------------------
int cunitAssert(int value, const utf8str_t* condition,
    const utf8str_t* file, unsigned line, int fatal)
{
    ++testrun->numAsserts;

    if (!value) {
        ++testrun->numAssertsFailed;

        const cunitFailure* failure = cunitCreateFailure(
            testrun->currentFixture, testrun->currentTest,
            file, line, condition);

        addTestFailure(testrun, failure);

        if (fatal) {
            longjmp(*(testrun->jmpFatal), 1);
        }
    }

    return value;
}

// ----------------------------------------------------------------------------
void runSingleTest(const cunitTest* test, cunitTestRun* run)
{
    const cunitFixture* fixture = run->currentFixture;

    run->currentTest = test;

    if (run->fnTestStart != nullptr) {
        (*run->fnTestStart)(fixture, test, run->context);
    }

    cutilList* failures = createList(destroyFailureItem);
    if (failures == nullptr) {
        return;
    }

    run->currentFailures = failures;

    jmp_buf buf;
    jmp_buf* jmpFatal = run->jmpFatal;
    run->jmpFatal = &buf;

    if (0 == setjmp(buf)) {

        void* context = startupFixture(fixture);
        runTest(test, context);
        teardownFixture(fixture, context);
    }

    run->jmpFatal = jmpFatal;

    ++run->numTestsRun;

    if (listIsEmpty(run->currentFailures)) {
        destroyList(failures);
        failures = nullptr;
    } else {
        ++run->numTestsFailed;
        listAddItem(run->allFailures, failures);
    }

    run->currentFailures = nullptr;

    if (run->fnTestComplete != nullptr) {
        (*run->fnTestComplete)(fixture, test, failures, run->context);
    }

    run->currentTest = nullptr;
}

// ----------------------------------------------------------------------------
int runSingleTestItem(const void* item, void* ptr)
{
    runSingleTest((const cunitTest*)item, (cunitTestRun*)ptr);
    return 0;
}

// ----------------------------------------------------------------------------
void runSingleFixture(const cunitFixture* fixture, cunitTestRun* run)
{
    run->currentFixture = fixture;

    if (run->fnFixtureStart != nullptr) {
        (*run->fnFixtureStart)(fixture, run->context);
    }

    jmp_buf buf;
    run->jmpFatal = &buf;
    if (0 == setjmp(buf)) {

        const cutilList* tests = getFixtureTests(fixture);
        listIterate(tests, runSingleTestItem, run);
        run->numFixturesRun++;
    }

    if (run->fnFixtureComplete != nullptr) {
        (*run->fnFixtureComplete)(fixture, run->context);
    }

    run->currentFixture = nullptr;
}

// ----------------------------------------------------------------------------
int runSingleFixtureItem(const void* item, void* ptr)
{
    runSingleFixture((const cunitFixture*)item, (cunitTestRun*)ptr);
    return 0;
}

// ----------------------------------------------------------------------------
void destroyListItem(const void* item)
{
    destroyList((const cutilList*)item);
}

// ----------------------------------------------------------------------------
const cunitTestRun* runAllTests(
    const cutilList* fixtures,
    cunitFnFixtureStart fnFixtureStart,
    cunitFnFixtureComplete fnFixtureComplete,
    cunitFnTestStart fnTestStart,
    cunitFnTestComplete fnTestComplete,
    void* context)
{
    cunitTestRun* run = (cunitTestRun*)malloc(sizeof(cunitTestRun));
    if (run == nullptr) {
        return nullptr;
    }

    run->numFixturesRun = 0;
    run->numFixturesFailed = 0;
    run->numTestsRun = 0;
    run->numTestsFailed = 0;
    run->numAsserts = 0;
    run->numAssertsFailed = 0;
    run->allFailures = createList(destroyListItem);

    run->fnFixtureStart = fnFixtureStart;
    run->fnFixtureComplete = fnFixtureComplete;
    run->fnTestStart = fnTestStart;
    run->fnTestComplete = fnTestComplete;
    run->context = context;

    testrun = run; // provide context to cunitAssert()

    clock_t f_start_time = clock();
    listIterate(fixtures, runSingleFixtureItem, run);
    run->elapsedTime = ((double)clock() - (double)f_start_time) / (double)CLOCKS_PER_SEC;

    testrun = nullptr;
    return run;
}

// ----------------------------------------------------------------------------
void destroyTestRun(const cunitTestRun* run)
{
    if (run == nullptr) {
        return;
    }

    destroyList(run->allFailures);
    free((void*)run);
}

// ----------------------------------------------------------------------------
const utf8str_t* getTestResultDesc(const cunitTestRun* testrun)
{
    const int width[5] = { 4, 8, 8, 8, 8 };
    char str[500];

    snprintf(str, 500,
        ANSI_BOLD "Run Summary:\n" ANSI_RESET
                        "%*s%*s%*s%*s%*s\n"
                        "%*s%*s%*u%*u%*u\n"
                        "%*s%*s%*u%*u%*u\n"
                        "%*s%*s%*u%*u%*u\n\n" ANSI_BOLD "Elapsed time:\n" ANSI_RESET
                        "%*s%.3f seconds\n",
        width[0], " ",
        width[1], " ",
        width[2], "Total",
        width[3], "Passed",
        width[4], "Failed",
        width[0], " ",
        width[1], "fixtures",
        width[2], testrun->numFixturesRun,
        width[3], testrun->numFixturesRun - testrun->numFixturesFailed,
        width[4], testrun->numFixturesFailed,
        width[0], " ",
        width[1], "tests",
        width[2], testrun->numTestsRun,
        width[3], testrun->numTestsRun - testrun->numTestsFailed,
        width[4], testrun->numTestsFailed,
        width[0], " ",
        width[1], "asserts",
        width[2], testrun->numAsserts,
        width[3], testrun->numAsserts - testrun->numAssertsFailed,
        width[4], testrun->numAssertsFailed,
        width[0], " ",
        testrun->elapsedTime);
    return utf8_make(str, strlen(str));
}

// ----------------------------------------------------------------------------
int getTestResult(const cunitTestRun* testrun)
{
    return ! listIsEmpty(testrun->allFailures);
}

// ----------------------------------------------------------------------------
void addTestFailure(cunitTestRun* testrun, const cunitFailure* failure)
{
    if (testrun->currentFailures == nullptr) {
        testrun->currentFailures = createList(destroyFailureItem);
    }
    if (testrun->currentFailures != nullptr) {
        listAddItem(testrun->currentFailures, failure);
    }
}
