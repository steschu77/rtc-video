/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */

#include <malloc.h>

#include "cutil_nullptr.h"
#include "cutil_refobj.h"

#include "task_impl.h"

// ----------------------------------------------------------------------------
int taskCompareTime(const void* x0, const void* x1)
{
    const task_t* t0 = (const task_t*)x0;
    const task_t* t1 = (const task_t*)x1;

    return (int)(t0->tRun - t1->tRun);
}

// ----------------------------------------------------------------------------
task_t* taskInit(
    task_t* obj,
    media_clock_t tRun,
    void* context,
    ptrFnAssign fnAssign,
    ptrFnRelease fnRelease,
    taskFnRun fnRun)
{
    obj->tRun = tRun;
    obj->context = context;
    obj->fnAssign = fnAssign;
    obj->fnRelease = fnRelease;
    obj->fnRun = fnRun;

    return obj;
}

// ----------------------------------------------------------------------------
static void objDestroy(const void* ptr)
{
    free((void*)ptr);
}

// ----------------------------------------------------------------------------
static void task1pAssign(void* ptr)
{
    const task1p_t* obj = (const task1p_t*)ptr;
    ref_inc(&obj->ref);
}

// ----------------------------------------------------------------------------
static void task1pRelease(void* ptr)
{
    const task1p_t* obj = (const task1p_t*)ptr;
    ref_dec(&obj->ref);
}

// ----------------------------------------------------------------------------
static void task1pRun(void* ptr, media_clock_t t)
{
    const task1p_t* obj = (const task1p_t*)ptr;
    obj->fnRun(t, obj->p0);
}

// ----------------------------------------------------------------------------
task_t* task1pCreate(media_clock_t t, FnRunTask1p fnRun, void* p0)
{
    task1p_t* obj = (task1p_t*)malloc(sizeof(task1p_t));
    if (obj == nullptr) {
        return nullptr;
    }

    ref_init(&obj->ref, obj, objDestroy);
    taskInit(
        &obj->task,
      t,
        obj,
      task1pAssign,
      task1pRelease,
      task1pRun);

    obj->fnRun = fnRun;
    obj->p0 = p0;

    return taskAssign(&obj->task);
}

// ----------------------------------------------------------------------------
static void task2pAssign(void* ptr)
{
    const task2p_t* obj = (const task2p_t*)ptr;
    ref_inc(&obj->ref);
}

// ----------------------------------------------------------------------------
static void task2pRelease(void* ptr)
{
    const task2p_t* obj = (const task2p_t*)ptr;
    ref_dec(&obj->ref);
}

// ----------------------------------------------------------------------------
static void task2pRun(void* ptr, media_clock_t t)
{
    const task2p_t* obj = (const task2p_t*)ptr;
    obj->fnRun(t, obj->p0, obj->p1);
}

// ----------------------------------------------------------------------------
task_t* task2pCreate(media_clock_t t, FnRunTask2p fnRun, void* p0, void* p1)
{
    task2p_t* obj = (task2p_t*)malloc(sizeof(task2p_t));
    if (obj == nullptr) {
        return nullptr;
    }

    ref_init(&obj->ref, obj, objDestroy);
    taskInit(
        &obj->task,
        t,
        obj,
        task2pAssign,
        task2pRelease,
        task2pRun);

    obj->fnRun = fnRun;
    obj->p0 = p0;
    obj->p1 = p1;

    return taskAssign(&obj->task);
}

// Task Interface
// ----------------------------------------------------------------------------
task_t* taskAssign(task_t* obj)
{
    obj->fnAssign(obj->context);
    return obj;
}

// ----------------------------------------------------------------------------
task_t* taskRelease(const task_t* obj)
{
    obj->fnRelease(obj->context);
    return nullptr;
}

// ----------------------------------------------------------------------------
void taskRun(const task_t* obj)
{
    obj->fnRun(obj->context, obj->tRun);
}

// ----------------------------------------------------------------------------
media_clock_t taskTime(const task_t* obj)
{
    return obj->tRun;
}
