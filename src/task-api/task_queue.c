/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */

#include <malloc.h>
#include <assert.h>

#include "cutil_nullptr.h"
#include "cutil_refobj.h"
#include "cutil_tree.h"

#include "task-api/task_queue.h"

// ----------------------------------------------------------------------------
struct task_queue_s {
    ref_t ref;

    tree_t* tasks;
    media_clock_t t;
};


// ----------------------------------------------------------------------------
void taskQueueDestroy(const void* obj)
{
    const task_queue_t* tasks = (const task_queue_t*)obj;
    if (tasks != nullptr) {
        assert(treeIsEmpty(tasks->tasks));
        treeDestroy(tasks->tasks, nullptr);
        free((void*)tasks);
    }
}

// ----------------------------------------------------------------------------
task_queue_t* taskQueueCreate()
{
    task_queue_t* tasks = (task_queue_t*)malloc(sizeof(task_queue_t));
    if (tasks == nullptr) {
        return nullptr;
    }

    ref_init(&tasks->ref, tasks, taskQueueDestroy);

    tasks->tasks = treeCreate();
    tasks->t = 0;

    return tasks;
}

// ----------------------------------------------------------------------------
task_queue_t* taskqueueAssign(task_queue_t* obj)
{
    ref_inc(&obj->ref);
    return obj;
}

// ----------------------------------------------------------------------------
task_queue_t* taskqueueRelease(const task_queue_t* obj)
{
    ref_dec(&obj->ref);
    return nullptr;
}

// ----------------------------------------------------------------------------
int taskqueueProcessNextEvent(task_queue_t* tasks)
{
    const task_t* task = (const task_t*)treePopItem(tasks->tasks);
    if (task == nullptr) {
        return 0;
    }

    tasks->t = taskTime(task);
    taskRun(task);

    taskRelease(task);
    return 1;
}

// ----------------------------------------------------------------------------
void taskqueueScheduleEvent(task_queue_t* obj, task_t* task)
{
    treeInsertItem(obj->tasks, taskCompareTime, taskAssign(task));
}

// ----------------------------------------------------------------------------
media_clock_t taskqueueTime(const task_queue_t* obj)
{
    return obj->t;
}
