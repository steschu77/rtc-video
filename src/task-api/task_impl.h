/*
 * MIT License
 *
 * Copyright (c) 2020 Steffen Schulze
 */

#pragma once

#include "cutil_refobj.h"

#include "task-api/task.h"

// ----------------------------------------------------------------------------
struct task_s {
    media_clock_t tRun;
    void* context;

    ptrFnAssign fnAssign;
    ptrFnRelease fnRelease;
    taskFnRun fnRun;
};

// ----------------------------------------------------------------------------
typedef struct task1p_s {
    ref_t ref;
    task_t task;

    FnRunTask1p fnRun;
    void* p0;
} task1p_t;

// ----------------------------------------------------------------------------
typedef struct task2p_s {
    ref_t ref;
    task_t task;

    FnRunTask2p fnRun;
    void* p0;
    void* p1;
} task2p_t;
