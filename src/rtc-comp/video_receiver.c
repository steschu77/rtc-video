/*
 * MIT License
 *
 * Copyright (c) 2020 Steffen Schulze
 */
#define _CRT_SECURE_NO_WARNINGS

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <assert.h>

#include "cutil_nullptr.h"
#include "cutil_math.h"
#include "cutil_refobj.h"
#include "cutil_tree.h"
#include "cutil_sortedlist.h"
#include "cutil_vecbyte.h"
#include "cutil_byteread.h"

#include "rtc-comp/video_receiver.h"

#include "../task-api/task_impl.h"

#include "rtc-proto/packet.h"
#include "rtc-proto/caps_frame.h"
#include "rtc-proto/video_frame.h"
#include "rtc-proto/endpoint.h"

#include "../rtc-api/packet_sink_impl.h"
#include "../rtc-api/video_source_impl.h"

// ----------------------------------------------------------------------------
struct video_receiver_s {
    ref_t ref;
    packet_sink_t packetRecvObj;
    video_source_t videoSourceObj;

    packet_sink_t* packetSend;
    packet_source_t* packetSource;
    video_sink_t* videoSink;
    task_queue_t* taskQueue;

    tree_t* pendingFrames;
    uint64_t nextSeqNo;
    uint64_t rxSeqNo;

    bytes_t* pendingPicture;
    uint64_t lastPicFrameSeqNo;
    pic_id_t picid;

    const caps_frame_t* caps_requested;
    unsigned caps_seqid;

    media_clock_t tNow;
    media_clock_t tLastSend;

    uint64_t packetSeqNo;

    sortedlist_t* inflightPackets;
    uint64_t lastAckedTxSeqNo;
    endpoint_t* ep;
};

// ----------------------------------------------------------------------------
static void objAssign(void* ptr)
{
    const video_receiver_t* obj = (const video_receiver_t*)ptr;
    ref_inc(&obj->ref);
}

// ----------------------------------------------------------------------------
static void objRelease(void* ptr)
{
    const video_receiver_t* obj = (const video_receiver_t*)ptr;
    ref_dec(&obj->ref);
}

// ----------------------------------------------------------------------------
static void releaseFrame(const void* ptr)
{
    const frame_t* obj = (const frame_t*)ptr;
    frameRelease(obj);
}

// ----------------------------------------------------------------------------
static void releasePacket(const void* ptr)
{
    const packet_t* obj = (const packet_t*)ptr;
    packetRelease(obj);
}

// ----------------------------------------------------------------------------
static void objDestroy(const void* ptr)
{
    const video_receiver_t* obj = (const video_receiver_t*)ptr;
    if (obj != nullptr) {
        assert(obj->videoSink == nullptr);
        assert(obj->packetSend == nullptr);
        pcksrcRelease(obj->packetSource);
        taskqueueRelease(obj->taskQueue);
        
        treeDestroy(obj->pendingFrames, releaseFrame);
        byteDestroy(obj->pendingPicture);

        capsframeRelease(obj->caps_requested);

        slistDestroy(obj->inflightPackets, releasePacket);
        epDestroy(obj->ep);

        free((void*)obj);
    }
}

// ----------------------------------------------------------------------------
void consumeVideoFrame(video_receiver_t* obj, const video_frame_t* frame)
{
    const uint64_t picFlags = videoframeGetFlags(frame);
    const ref_type_t type = videoframeGetRefType(frame);

    const media_clock_t picTS = videoframeGetPresentationTime(frame);

    if (type != ref_endframe) {

        const buffer_t* data = videoframeGetData(frame);
        const void* ptr = getBufferData(data);
        const size_t size = getBufferSize(data);

        const uint64_t picid = videoframeGetPicId(frame);
        const uint64_t psnr = videoframeGetPSNR(frame);

        if ((picFlags & (2 << 6)) != 0) {
            assert(byteGetSize(obj->pendingPicture) == 0 || type == ref_keyframe);
            byteReset(obj->pendingPicture);
        }

        byteAddData(obj->pendingPicture, ptr, size);
        obj->lastPicFrameSeqNo = videoframeGetSeqNo(frame);

        if ((picFlags & (1 << 6)) != 0) {

            const size_t picDataSize = byteGetSize(obj->pendingPicture);
            const void* picDataData = byteGetData(obj->pendingPicture);

            void* bufPtr = nullptr;
            const buffer_t* picData = allocBuffer(picDataSize, &bufPtr, 16);

            if (picData != nullptr) {
                memcpy(bufPtr, picDataData, picDataSize);

                const video_pic_t* pic = createVideoPicture(picid, picTS, picData, type, psnr);

                videosinkDeliverPic(obj->videoSink, obj->tNow, pic);

                freeBuffer(picData);
                videopicRelease(pic);
            }

            byteReset(obj->pendingPicture);
        }
    } else {
        videosinkDeliverPic(obj->videoSink, picTS, nullptr);
    }
}

// ----------------------------------------------------------------------------
enum kf_search_state_e {
    state_kf_start,
    state_kf_end,
    state_kf_found,
};

typedef enum kf_search_state_e kf_search_state_t;

// ----------------------------------------------------------------------------
struct kf_search_context_s {
    uint64_t seqno;
    uint64_t seqnoKeyframeStart;
    kf_search_state_t state;
};

typedef struct kf_search_context_s kf_search_context_t;

// ----------------------------------------------------------------------------
int iteratePendingFramesKeyframe(const void* ptr, void* context)
{
    kf_search_context_t* state = (kf_search_context_t*)context;

    const video_frame_t* frame = (const video_frame_t*)ptr;
    const uint64_t picFlags = videoframeGetFlags(frame);
    const uint64_t seqno = videoframeGetSeqNo(frame);
    const ref_type_t type = videoframeGetRefType(frame);

    if (type != ref_keyframe && type != ref_syncframe && type != ref_endframe) {
        state->state = state_kf_start;
        return 0;
    }

    if (state->seqno != seqno) {
        state->state = state_kf_start;
    }

    if (picFlags & (2 << 6)) {
        state->state = state_kf_end;
        state->seqnoKeyframeStart = seqno;
        state->seqno = seqno + 1;
    }

    if (picFlags & (1 << 6)) {
        state->state = state_kf_found;
        return 1;
    }

    return 0;
}

// ----------------------------------------------------------------------------
void dropPendingFramesUntil(tree_t* pendingFrames, uint64_t seqno)
{
    const video_frame_t* frame = (const video_frame_t*)treeFirst(pendingFrames);
    while (frame != nullptr && videoframeGetSeqNo(frame) != seqno) {
        treePopItem(pendingFrames);
        videoframeRelease(frame);
        frame = (const video_frame_t*)treeFirst(pendingFrames);
    }
}

// ----------------------------------------------------------------------------
int objVideoFrame(void* ptr, const packet_t* packet, const video_frame_t* videoFrame)
{
    video_receiver_t* obj = (video_receiver_t*)ptr;

    const uint64_t videoSeqNo = videoframeGetSeqNo(videoFrame);
    if (videoSeqNo < obj->lastPicFrameSeqNo) {
        return 0;
    }

    tree_t* pendingFrames = obj->pendingFrames;
    treeInsertItem(pendingFrames, videoFrameCompare, videoframeAssign((video_frame_t*)videoFrame));

    kf_search_context_t kfsearch;
    do {

        const video_frame_t* nextFrame = (const video_frame_t*)treeFirst(pendingFrames);

        while (nextFrame != nullptr) {

            uint64_t nextSeqNo = videoframeGetSeqNo(nextFrame);
            if (nextSeqNo != obj->nextSeqNo) {
                break;
            }

            consumeVideoFrame(obj, nextFrame);

            treePopItem(obj->pendingFrames);
            videoframeRelease(nextFrame);

            obj->nextSeqNo = nextSeqNo + 1;

            nextFrame = (const video_frame_t*)treeFirst(pendingFrames);
        }

        // search for a complete key-frame in the remaining pending frames
        kfsearch.state = state_kf_start;
        treeIterate(pendingFrames, iteratePendingFramesKeyframe, &kfsearch);

        if (kfsearch.state == state_kf_found) {
            dropPendingFramesUntil(pendingFrames, kfsearch.seqnoKeyframeStart);
            obj->nextSeqNo = kfsearch.seqnoKeyframeStart;
        }

        // continue as long as there are complete key-frames available
    } while (kfsearch.state == state_kf_found);

    return 0;
}

// ----------------------------------------------------------------------------
int objAckFrame(void* ptr, const packet_t* packet, const ack_frame_t* ackFrame)
{
    video_receiver_t* obj = (video_receiver_t*)ptr;
    const uint64_t seqno = packetGetSeqNo(packet);
    epPacketReceived(obj->ep, seqno, ackFrame);
    return 0;
}

// ----------------------------------------------------------------------------
static frame_enum_t cbsDeliverPacket = {
    objAckFrame,
    nullptr,
    objVideoFrame
};

static void sendPacket(video_receiver_t* obj);

// ----------------------------------------------------------------------------
static void objResendCaps(media_clock_t t, void* p0)
{
    video_receiver_t* obj = (video_receiver_t*)p0;
    obj->tNow = t;
    if (obj->caps_requested != nullptr && t - obj->tLastSend >= make_msec(50)) {
        sendPacket(obj);
    }
}

// ----------------------------------------------------------------------------
static void sendPacket(video_receiver_t* obj)
{
    if (obj->packetSend == nullptr) {
        // already disconnected
        return;
    }

    const uint64_t packetSeqNo = obj->packetSeqNo++;
    packet_t* packet = packetCreate(dir_recv_send, packetSeqNo);

    packetAddTime(packet, packet_send, obj->tNow);

    const ack_frame_t* ackFrame = epPreparePacket(obj->ep, packetSeqNo);
    packetAddAckFrame(packet, ackFrame);

    if (obj->caps_requested != nullptr) {
        packetAddCapsFrame(packet, obj->caps_requested);

        task_t* task = task1pCreate(obj->tNow + make_msec(50), objResendCaps, obj);
        taskqueueScheduleEvent(obj->taskQueue, task);
        taskRelease(task);
    }

    pcksinkDeliverPacket(obj->packetSend, obj->tNow, packet);

    slistInsertItem(obj->inflightPackets, packetCompare, packetAssign(packet));

    packetRelease(packet);

    obj->tLastSend = obj->tNow;
}

// ----------------------------------------------------------------------------
static void objDeliverPacket(void* ptr, media_clock_t t, const packet_t* packet)
{
    video_receiver_t* obj = (video_receiver_t*)ptr;
    obj->tNow = t;

    packetEnumFrames(packet, obj, &cbsDeliverPacket);

    sendPacket(obj);
}

// Video Source interface
// ----------------------------------------------------------------------------
void objConnect(void* ptr, video_sink_t* sink)
{
    video_receiver_t* obj = (video_receiver_t*)ptr;
    videosinkAssign(sink, &obj->videoSink);

    pcksrcConnect(obj->packetSource, sink != nullptr ? &obj->packetRecvObj : nullptr, &obj->packetSend);
}

// ----------------------------------------------------------------------------
void objChangeCaps(void* ptr, const video_caps_t* caps)
{
    video_receiver_t* obj = (video_receiver_t*)ptr;
    obj->caps_requested = capsframeRelease(obj->caps_requested);
    obj->caps_requested = capsframeCreate(obj->caps_seqid++, caps);
    sendPacket(obj);
}

// ----------------------------------------------------------------------------
void objRequestSyncPoint(void* ptr)
{
    assert(0); // needs new signaling messages
}

// ----------------------------------------------------------------------------
void objNotifyPictureLost(void* ptr, const media_id_t id)
{
    assert(0); // needs new signaling messages
}

// ----------------------------------------------------------------------------
void objNotifyPictureProcessed(void* ptr, const media_id_t id)
{
    assert(0); // needs new signaling messages
}

// __ Packet Frame Enum _______________________________________________________
// ----------------------------------------------------------------------------
static int objTxCapsFrame(void* ptr, const packet_t* packet, const caps_frame_t* capsFrame)
{
    video_receiver_t* obj = (video_receiver_t*)ptr;
    if (obj->caps_requested != nullptr &&
        capsframeGetSeqid(obj->caps_requested) == capsframeGetSeqid(capsFrame)) {
        obj->caps_requested = capsframeRelease(obj->caps_requested);
    }

    return 0;
}

// ----------------------------------------------------------------------------
static frame_enum_t cbsAckPacket = {
    nullptr,
    objTxCapsFrame,
    nullptr
};

// ----------------------------------------------------------------------------
// Called by endpoint.c when a previously sent packet is acknowledged by the receiver.
static void objAckPacket(void* ptr, uint64_t seqno)
{
    video_receiver_t* obj = (video_receiver_t*)ptr;

    const packet_t* packet = (const packet_t*)slistRemoveItem(obj->inflightPackets, packetPredicate, &seqno);
    if (packet != nullptr) {
        packetEnumFrames(packet, obj, &cbsAckPacket);
        packetRelease(packet);
    }

    obj->lastAckedTxSeqNo = uint64_max(obj->lastAckedTxSeqNo, seqno);
}

// ----------------------------------------------------------------------------
video_receiver_t* videorecvCreate(task_queue_t* taskQueue, packet_source_t* packetSource)
{
    video_receiver_t* obj = (video_receiver_t*)malloc(sizeof(video_receiver_t));
    if (obj == nullptr) {
        return nullptr;
    }

    ref_init(&obj->ref, obj, objDestroy);

    pcksinkInit(
        &obj->packetRecvObj,
        obj,
        objAssign,
        objRelease,
        objDeliverPacket);

    videosrcInit(
        &obj->videoSourceObj,
        obj,
        objAssign,
        objRelease,
        objConnect,
        objChangeCaps,
        objRequestSyncPoint,
        objNotifyPictureLost,
        objNotifyPictureProcessed);

    obj->packetSend = nullptr;
    obj->packetSource = pcksrcCopy(packetSource);
    obj->videoSink = nullptr;
    obj->taskQueue = taskqueueAssign(taskQueue);

    obj->pendingFrames = treeCreate();
    obj->nextSeqNo = 0;
    obj->rxSeqNo = 0;

    obj->caps_requested = nullptr;
    obj->caps_seqid = 0;

    obj->tNow = 0;
    obj->tLastSend = 0;

    obj->pendingPicture = byteCreate(0x10000, 0x10000);
    obj->lastPicFrameSeqNo = 0;
    obj->picid = 0;

    obj->packetSeqNo = 0;

    obj->inflightPackets = slistCreate();
    obj->lastAckedTxSeqNo = 0;
    obj->ep = epCreate(objAckPacket, obj);

    return obj;
}

// ----------------------------------------------------------------------------
video_source_t* videorecvQueryVideoSource(video_receiver_t* obj)
{
    return videosrcCopy(&obj->videoSourceObj);
}
