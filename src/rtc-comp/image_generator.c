/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */
#define _CRT_SECURE_NO_WARNINGS

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <assert.h>

#include "cutil_nullptr.h"
#include "cutil_refobj.h"

#include "rtc-comp/image_gen.h"

#include "../rtc-api/image_source_impl.h"

// ----------------------------------------------------------------------------
typedef const image_t* (*FnGenerate)(unsigned, const image_size_t*, fourcc_t);

// ----------------------------------------------------------------------------
const image_t* gfxCreateGradientImage(unsigned idx, const image_size_t* size, fourcc_t fmt);

// ----------------------------------------------------------------------------
struct image_gen_s {
    ref_t ref;
    image_source_t imageSourceObj;

    image_sink_t* imageSink;
    task_queue_t* taskQueue;

    FnGenerate fnGenerate;

    image_caps_t caps;
    media_clock_t t0;
    media_clock_t dt;

    unsigned numPics;
    unsigned maxPics;
};

// ----------------------------------------------------------------------------
static void objAssign(void* ptr)
{
    const image_gen_t* obj = (const image_gen_t*)ptr;
    ref_inc(&obj->ref);
}

// ----------------------------------------------------------------------------
static void objRelease(void* ptr)
{
    const image_gen_t* obj = (const image_gen_t*)ptr;
    ref_dec(&obj->ref);
}

// ----------------------------------------------------------------------------
static void objDestroy(const void* ptr)
{
    const image_gen_t* obj = (const image_gen_t*)ptr;
    if (obj != nullptr) {
        assert(obj->imageSink == nullptr);
        taskqueueRelease(obj->taskQueue);
        free((void*)obj);
    }
}

// ----------------------------------------------------------------------------
static void imggenGenerate(media_clock_t t, void* ptr)
{
    image_gen_t* obj = (image_gen_t*)ptr;

    obj->numPics++;
    if (obj->numPics <= obj->maxPics) {
        const image_size_t size = { 128, 96 };
        const image_t* img = gfxCreateGradientImage(obj->numPics, &size, fccYUV12);

        imgsinkDeliverPicture(obj->imageSink, t, img);
        gfxFreeImage(img);

        task_t* task = task1pCreate(t + obj->dt, imggenGenerate, obj);
        taskqueueScheduleEvent(obj->taskQueue, task);
        taskRelease(task);
    } else {
        imgsinkDeliverPicture(obj->imageSink, t, nullptr);
    }
}

// ----------------------------------------------------------------------------
void imggenConnect(void* ptr, image_sink_t* sink)
{
    image_gen_t* obj = (image_gen_t*)ptr;
    imgsinkAssign(sink, &obj->imageSink);
}

// ----------------------------------------------------------------------------
void imggenChangeCaps(void* ptr, const image_caps_t* caps)
{
    image_gen_t* obj = (image_gen_t*)ptr;

    if (obj->caps.mute == muted && caps->mute != muted) {
        obj->t0 = taskqueueTime(obj->taskQueue);

        task_t* task = task1pCreate(obj->t0, imggenGenerate, obj);
        taskqueueScheduleEvent(obj->taskQueue, task);
        taskRelease(task);
    }

    obj->caps = *caps;
}

// ----------------------------------------------------------------------------
image_gen_t* imggenCreate(task_queue_t* taskQueue, media_clock_t freq, unsigned maxPics)
{
    image_gen_t* obj = (image_gen_t*)malloc(sizeof(image_gen_t));
    if (obj == nullptr) {
        return nullptr;
    }

    ref_init(&obj->ref, obj, objDestroy);

    imgsrcInit(
        &obj->imageSourceObj,
        obj,
        objAssign,
        objRelease,
        imggenConnect,
        imggenChangeCaps);

    obj->imageSink = nullptr;
    obj->taskQueue = taskqueueAssign(taskQueue);
    obj->caps.mute = muted;
    obj->t0 = 0;
    obj->dt = freq;
    obj->numPics = 0;
    obj->maxPics = maxPics;

    return obj;
}

// ----------------------------------------------------------------------------
image_source_t* imggenQueryImageSource(image_gen_t* obj)
{
    return imgsrcCopy(&obj->imageSourceObj);
}
