/*
 * MIT License
 *
 * Copyright (c) 2021 Steffen Schulze
 */
#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#include "cutil_nullptr.h"
#include "cutil_refobj.h"

#include "rtc-api/image_source.h"

#include "../rtc-api/image_sink_impl.h"
#include "../rtc-api/media_control_impl.h"

#include "rtc-comp/media_ctrl.h"

// ----------------------------------------------------------------------------
struct img_mediactrl_s {
    ref_t ref;
    image_sink_t imageSinkObj;
    media_control_t mediaControlObj;
    image_source_t* imageSource;
};

// ----------------------------------------------------------------------------
static void objAssign(void* ptr)
{
    const img_mediactrl_t* obj = (const img_mediactrl_t*)ptr;
    ref_inc(&obj->ref);
}

// ----------------------------------------------------------------------------
static void objRelease(void* ptr)
{
    const img_mediactrl_t* obj = (const img_mediactrl_t*)ptr;
    ref_dec(&obj->ref);
}

// ----------------------------------------------------------------------------
static void objDestroy(const void* ptr)
{
    const img_mediactrl_t* obj = (const img_mediactrl_t*)ptr;
    if (obj != nullptr) {
        imgsrcRelease(obj->imageSource);
        free((void*)obj);
    }
}

// Image Sink
// ----------------------------------------------------------------------------
static void imgctrlDeliverPicture(void* ptr, media_clock_t t, const image_t* img)
{
    const img_mediactrl_t* obj = (const img_mediactrl_t*)ptr;
    if (img == nullptr) {
        imgsrcConnect(obj->imageSource, nullptr);
    }
}

// Media Control
// ----------------------------------------------------------------------------
void imgctrlStart(void* ptr, const image_caps_t* caps)
{
    img_mediactrl_t* obj = (img_mediactrl_t*)ptr;
    imgsrcConnect(obj->imageSource, &obj->imageSinkObj);
    imgsrcChangeCaps(obj->imageSource, caps);
}

// ----------------------------------------------------------------------------
img_mediactrl_t* imgctrlCreate(image_source_t* imageSource)
{
    img_mediactrl_t* obj = (img_mediactrl_t*)malloc(sizeof(img_mediactrl_t));
    if (obj == nullptr) {
        return nullptr;
    }

    ref_init(&obj->ref, obj, objDestroy);
    imgsinkInit(
        &obj->imageSinkObj,
        obj,
        objAssign,
        objRelease,
        imgctrlDeliverPicture);

    mediactrlInit(
        &obj->mediaControlObj,
        obj,
        objAssign,
        objRelease,
        imgctrlStart);

    obj->imageSource = imgsrcCopy(imageSource);
    return obj;
}

// ----------------------------------------------------------------------------
img_mediactrl_t* imgctrlAssign(img_mediactrl_t* obj)
{
    ref_inc(&obj->ref);
    return obj;
}

// ----------------------------------------------------------------------------
img_mediactrl_t* imgctrlRelease(const img_mediactrl_t* obj)
{
    ref_dec(&obj->ref);
    return nullptr;
}

// ----------------------------------------------------------------------------
media_control_t* imgctrlQueryMediaControl(img_mediactrl_t* obj)
{
    return mediactrlCopy(&obj->mediaControlObj);
}
