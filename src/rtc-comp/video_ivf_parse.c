/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */
#define _CRT_SECURE_NO_WARNINGS

#include <assert.h>

#include "cutil_buffer.h"
#include "cutil_file.h"
#include "cutil_path.h"
#include "cutil_refobj.h"
#include "cutil_bytewrite.h"

#include "../rtc-api/video_sink_impl.h"
#include "../rtc-api/video_source_impl.h"
#include "rtc-comp/video_ivffile.h"

// ----------------------------------------------------------------------------
static void ivfparseConnect(void* ptr, video_sink_t* sink);
static void ivfparseChangeCaps(void* ptr, const video_caps_t* caps);
static void ivfparseRequestSyncPoint(void* ptr);
static void ivfparseNotifyPictureLost(void* ptr, const media_id_t id);
static void ivfparseNotifyPictureProcessed(void* ptr, const media_id_t id);

// ----------------------------------------------------------------------------
static void ivfparseParse(media_clock_t t, void* ptr);

// ----------------------------------------------------------------------------
static int readIVFFileHeader(utilsFile* file, frame_rate_t* rate, image_size_t* size);
static int readIVFPicture(utilsFile* file, const buffer_t** buf, uint64_t* pts);

// ----------------------------------------------------------------------------
static uint8_t mem_read_le8(const uint8_t* ptr);
static uint16_t mem_read_le16(const uint8_t* ptr);
static uint32_t mem_read_le32(const uint8_t* ptr);
static uint64_t mem_read_le64(const uint8_t* ptr);

// ----------------------------------------------------------------------------
struct vidivf_parser_s {
    ref_t ref;
    video_source_t videoSourceObj;

    video_sink_t* videoSink;
    task_queue_t* taskQueue;

    utilsFile* file;

    image_size_t size;
    frame_rate_t rate;
    fourcc_t fmt;

    media_clock_t t0;
};

// ----------------------------------------------------------------------------
static void objAssign(void* ptr)
{
    const vidivf_parser_t* obj = (const vidivf_parser_t*)ptr;
    ref_inc(&obj->ref);
}

// ----------------------------------------------------------------------------
static void objRelease(void* ptr)
{
    const vidivf_parser_t* obj = (const vidivf_parser_t*)ptr;
    ref_dec(&obj->ref);
}

// ----------------------------------------------------------------------------
static void objDestroy(const void* ptr)
{
    const vidivf_parser_t* obj = (const vidivf_parser_t*)ptr;
    if (obj != nullptr) {
        assert(obj->videoSink == nullptr);
        taskqueueRelease(obj->taskQueue);
        freeFile(obj->file);
        free((void*)obj);
    }
}

// ----------------------------------------------------------------------------
vidivf_parser_t* ivfparseCreate(task_queue_t* tasks, utilsFile* file)
{
    vidivf_parser_t* obj = (vidivf_parser_t*)malloc(sizeof(vidivf_parser_t));
    if (obj == nullptr) {
        return nullptr;
    }

    ref_init(&obj->ref, obj, objDestroy);

    videosrcInit(
        &obj->videoSourceObj,
        obj,
        objAssign,
        objRelease,
        ivfparseConnect,
        ivfparseChangeCaps,
        ivfparseRequestSyncPoint,
        ivfparseNotifyPictureLost,
        ivfparseNotifyPictureProcessed);

    obj->videoSink = nullptr;
    obj->taskQueue = taskqueueAssign(tasks);
    obj->file = assignFile(file);
    obj->t0 = 0;

    return obj;
}

// ----------------------------------------------------------------------------
const vidivf_parser_t* ivfparseAssign(const vidivf_parser_t* obj)
{
    if (obj != nullptr) {
        ref_inc(&obj->ref);
    }
    return obj;
}

// ----------------------------------------------------------------------------
void ivfparseRelease(const vidivf_parser_t* obj)
{
    if (obj != nullptr) {
        ref_dec(&obj->ref);
    }
}

// ----------------------------------------------------------------------------
video_source_t* ivfparseQueryVideoSource(vidivf_parser_t* obj)
{
    return videosrcCopy(&obj->videoSourceObj);
}

// Video Source interface
// ----------------------------------------------------------------------------
void ivfparseConnect(void* ptr, video_sink_t* sink)
{
    vidivf_parser_t* obj = (vidivf_parser_t*)ptr;

    videosinkAssign(sink, &obj->videoSink);

    if (sink != nullptr) {
        readIVFFileHeader(obj->file, &obj->rate, &obj->size);

        obj->t0 = taskqueueTime(obj->taskQueue);

        task_t* task = task1pCreate(obj->t0, ivfparseParse, obj);
        taskqueueScheduleEvent(obj->taskQueue, task);
        taskRelease(task);
    } else {
    }
}

// ----------------------------------------------------------------------------
void ivfparseChangeCaps(void* ptr, const video_caps_t* caps)
{
    // can't do anything about it. Bitrate is fixed in the IVF file.
}

// ----------------------------------------------------------------------------
void ivfparseRequestSyncPoint(void* ptr)
{
    // can't do anything about it. Bitrate is fixed in the IVF file.
}

// ----------------------------------------------------------------------------
void ivfparseNotifyPictureLost(void* ptr, const media_id_t id)
{
    // can't do anything about it. Bitrate is fixed in the IVF file.
}

// ----------------------------------------------------------------------------
void ivfparseNotifyPictureProcessed(void* ptr, const media_id_t id)
{
    // can't do anything about it. Bitrate is fixed in the IVF file.
}

// ----------------------------------------------------------------------------
static void ivfparseParse(media_clock_t t, void* ptr)
{
    vidivf_parser_t* obj = (vidivf_parser_t*)ptr;

    const buffer_t* buf = nullptr;
    uint64_t pts = 0;
    int res = readIVFPicture(obj->file, &buf, &pts);

    if (res == 0) {
        const video_pic_t* pic = createVideoPicture(0, pts, buf, ref_refframe, 0);
        videosinkDeliverPic(obj->videoSink, pts, pic);
        videopicRelease(pic);
        freeBuffer(buf);

        task_t* task = task1pCreate(t + obj->rate.num, ivfparseParse, obj);
        taskqueueScheduleEvent(obj->taskQueue, task);
        taskRelease(task);
    } else {
        videosinkDeliverPic(obj->videoSink, pts, nullptr);
    }
}

// ----------------------------------------------------------------------------
int readIVFFileHeader(utilsFile* file, frame_rate_t* rate, image_size_t* size)
{
    uint8_t header[32];
    if (readFile(file, header, sizeof(header)) != sizeof(header)) {
        return -1;
    }

    if (mem_read_le32(header + 0) != make_fourcc('D', 'K', 'I', 'F') ||
        mem_read_le16(header + 4) != 0 ||
        mem_read_le16(header + 6) != sizeof(header) ||
        mem_read_le32(header + 8) != make_fourcc('V', 'P', '9', '0')) {
        return -1;
    }

    size->cx = mem_read_le16(header + 12);
    size->cy = mem_read_le16(header + 14);
    rate->den = mem_read_le32(header + 16);
    rate->num = mem_read_le32(header + 20);

    return 0;
}

// ----------------------------------------------------------------------------
int readIVFPicture(utilsFile* file, const buffer_t** buf, uint64_t* pts)
{
    uint8_t header[12];
    if (readFile(file, header, sizeof(header)) != sizeof(header)) {
        return -1;
    }

    size_t size = (size_t)mem_read_le32(header + 0);
    *pts = mem_read_le64(header + 4);

    void* data = nullptr;
    *buf = allocBuffer(size, &data, 16u);

    if (readFile(file, data, size) != size) {
        return -1;
    }

    return 0;
}

// ----------------------------------------------------------------------------
uint8_t mem_read_le8(const uint8_t* ptr)
{
    return ptr[0];
}

// ----------------------------------------------------------------------------
uint16_t mem_read_le16(const uint8_t* ptr)
{
    return (mem_read_le8(ptr + 0)) | (mem_read_le8(ptr + 1) << 8);
}

// ----------------------------------------------------------------------------
uint32_t mem_read_le32(const uint8_t* ptr)
{
    return (uint32_t)mem_read_le16(ptr + 0) | ((uint32_t)mem_read_le16(ptr + 2) << 16);
}

// ----------------------------------------------------------------------------
uint64_t mem_read_le64(const uint8_t* ptr)
{
    return (uint64_t)mem_read_le32(ptr + 0) | ((uint64_t)mem_read_le32(ptr + 4) << 32);
}
