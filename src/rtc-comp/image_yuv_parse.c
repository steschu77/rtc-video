/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */
#define _CRT_SECURE_NO_WARNINGS

#include <assert.h>

#include "cutil_buffer.h"
#include "cutil_path.h"
#include "cutil_refobj.h"
#include "cutil_file.h"

#include "rtc-comp/image_yuvfile.h"
#include "../rtc-api/image_sink_impl.h"
#include "../rtc-api/image_source_impl.h"

// ----------------------------------------------------------------------------
static void yuvparseConnect(void* ptr, image_sink_t* sink);
static void yuvparseChangeCaps(void* ptr, const image_caps_t* caps);

static void yuvparseParse(media_clock_t t, void* ptr);

static void gfxSkipYUVImages(utilsFile* file, const image_size_t* size, unsigned cImages);
static const image_t* gfxReadYUVImage(utilsFile* file, const image_size_t* size);
static int gfxReadYUVPlanes(utilsFile* file, const image_size_t* size, const fourcc_t fmt, const stride_t* stride,
    const buffer_t* buf_yuv[4]);
static const buffer_t* gfxReadYUVPlane(utilsFile* file, const size_t buf_size,
    const unsigned plane_cx, const unsigned plane_cy, size_t stride);

// ----------------------------------------------------------------------------
struct imgyuv_parser_s {

    ref_t ref;
    image_source_t imageSourceObj;

    image_sink_t* imageSink;
    task_queue_t* taskQueue;

    const utf8str_t* seqName;
    unsigned frameLimit;
    utilsFile* file;

    image_caps_t caps;
    image_size_t size;
    media_clock_t t0;

    mute_state_t mute;
    unsigned frameCount;
};

// ----------------------------------------------------------------------------
static void objAssign(void* ptr)
{
    const imgyuv_parser_t* obj = (const imgyuv_parser_t*)ptr;
    ref_inc(&obj->ref);
}

// ----------------------------------------------------------------------------
static void objRelease(void* ptr)
{
    const imgyuv_parser_t* obj = (const imgyuv_parser_t*)ptr;
    ref_dec(&obj->ref);
}

// ----------------------------------------------------------------------------
static void objDestroy(const void* ptr)
{
    imgyuv_parser_t* obj = (imgyuv_parser_t*)ptr;
    if (obj != nullptr) {
        assert(obj->imageSink == nullptr);
        taskqueueRelease(obj->taskQueue);
        utf8_free(&obj->seqName);
        freeFile(obj->file);
        free((void*)obj);
    }
}

// ----------------------------------------------------------------------------
imgyuv_parser_t* yuvparseCreate(task_queue_t* tasks, const utf8str_t* seqName, unsigned frameLimit)
{
    imgyuv_parser_t* obj = (imgyuv_parser_t*)malloc(sizeof(imgyuv_parser_t));
    if (obj == nullptr) {
        return nullptr;
    }

    ref_init(&obj->ref, obj, objDestroy);

    imgsrcInit(
        &obj->imageSourceObj,
        obj,
        objAssign,
        objRelease,
        yuvparseConnect,
        yuvparseChangeCaps);

    obj->imageSink = nullptr;
    obj->taskQueue = taskqueueAssign(tasks);

    obj->seqName = utf8_assign(seqName);
    obj->frameLimit = frameLimit;
    obj->file = nullptr;
    obj->caps.mute = muted;
    obj->t0 = 0;

    obj->frameCount = 0;

    return obj;
}

// ----------------------------------------------------------------------------
image_source_t* yuvparseQueryImageSource(imgyuv_parser_t* obj)
{
    return imgsrcCopy(&obj->imageSourceObj);
}

// ----------------------------------------------------------------------------
static int yuvparseInitFile(imgyuv_parser_t* obj, const image_caps_t* caps)
{
    const image_size_t* size = &caps->preferred_res;
    const utf8str_t* fileName = utf8_format("%s.%dx%d",
        utf8_cstr(obj->seqName), size->cx, size->cy);

    assert(obj->file == nullptr);
    obj->file = createFileRead(fileName);
    obj->size = *size;

    gfxSkipYUVImages(obj->file, size, obj->frameCount);

    utf8_free(&fileName);
    return 0;
}

// Image Source
// ----------------------------------------------------------------------------
static void yuvparseConnect(void* ptr, image_sink_t* sink)
{
    imgyuv_parser_t* obj = (imgyuv_parser_t*)ptr;
    imgsinkAssign(sink, &obj->imageSink);
}

// ----------------------------------------------------------------------------
static void scheduleNextPicture(imgyuv_parser_t* obj, media_clock_t t)
{
    task_t* task = task1pCreate(t, yuvparseParse, obj);
    taskqueueScheduleEvent(obj->taskQueue, task);
    taskRelease(task);
}

// ----------------------------------------------------------------------------
static void yuvparseChangeCaps(void* ptr, const image_caps_t* caps)
{
    imgyuv_parser_t* obj = (imgyuv_parser_t*)ptr;

    if (obj->caps.mute == muted && caps->mute != muted) {
        if (yuvparseInitFile(obj, caps)) {
            return;
        }
        obj->t0 = taskqueueTime(obj->taskQueue);
        scheduleNextPicture(obj, obj->t0);
    }

    if (obj->caps.mute != muted && caps->mute == muted) {
        assert(obj->file != nullptr);
        freeFile(obj->file);
        obj->file = nullptr;
    }

    obj->caps = *caps;
}

// ----------------------------------------------------------------------------
static void yuvparseParse(media_clock_t t, void* ptr)
{
    imgyuv_parser_t* obj = (imgyuv_parser_t*)ptr;

    if (obj->mute == muted) {
        return;
    }

    if (obj->frameLimit > 0 && obj->frameCount >= obj->frameLimit) {
        imgsinkDeliverPicture(obj->imageSink, t, nullptr);
        return;
    }

    const image_t* img = gfxReadYUVImage(obj->file, &obj->size);
    imgsinkDeliverPicture(obj->imageSink, t, img);

    if (img != nullptr) {

        gfxFreeImage(img);
        img = nullptr;

        obj->frameCount++;

        const media_clock_t dt = make_sec(1.0 / 30.0);
        task_t* task = task1pCreate(t + dt, yuvparseParse, obj);
        taskqueueScheduleEvent(obj->taskQueue, task);
        taskRelease(task);
    }
}

// ----------------------------------------------------------------------------
static void gfxSkipYUVImages(utilsFile* file, const image_size_t* size, unsigned cImages)
{
    const fourcc_t fmt = fccYUV12;
    const unsigned cx = size->cx;
    const unsigned cy = size->cy;

    const plane_scale_t shift = gfxPlaneShift(fmt);
    const plane_scale_t scale = gfxPlaneScale(fmt);

    size_t image_size = 0;

    for (int i = 0; i < 4; ++i) {

        const unsigned plane_cx = (scale.p[i] * cx) >> shift.p[i];
        const unsigned plane_cy = (scale.p[i] * cy) >> shift.p[i];

        const size_t plane_size = plane_cx * plane_cy;
        image_size += plane_size;
    }

    seekFile(file, image_size * cImages);
}

// ----------------------------------------------------------------------------
static const image_t* gfxReadYUVImage(utilsFile* file, const image_size_t* size)
{
    const image_t* img = nullptr;

    const fourcc_t fmt = fccYUV12;
    const stride_t stride = gfxCalcFormatStride(fmt, size->cx, 16u);
    const buffer_t* buf_yuv[4] = { 0 };

    if (gfxReadYUVPlanes(file, size, fmt, &stride, buf_yuv) == 0) {
        img = gfxCreateImage(&stride, size, fmt, buf_yuv);
    }

    for (int i = 0; i < 4; ++i) {
        freeBuffer(buf_yuv[i]);
    }

    return gfxAssignImage(img);
}

// ----------------------------------------------------------------------------
static int gfxReadYUVPlanes(utilsFile* file, const image_size_t* size,
    const fourcc_t fmt, const stride_t* stride, const buffer_t* buf_yuv[4])
{
    const unsigned cx = size->cx;
    const unsigned cy = size->cy;

    const plane_scale_t shift = gfxPlaneShift(fmt);
    const plane_scale_t scale = gfxPlaneScale(fmt);

    for (int i = 0; i < 4; ++i) {

        const unsigned plane_cx = (scale.p[i] * cx) >> shift.p[i];
        const unsigned plane_cy = (scale.p[i] * cy) >> shift.p[i];

        const size_t buf_size = plane_cy * stride->p[i];

        buf_yuv[i] = gfxReadYUVPlane(file, buf_size, plane_cx, plane_cy, stride->p[i]);
        if (buf_yuv[i] == nullptr) {
            return 1;
        }
    }

    return 0;
}

// ----------------------------------------------------------------------------
static const buffer_t* gfxReadYUVPlane(utilsFile* file, const size_t buf_size,
    const unsigned plane_cx, const unsigned plane_cy, size_t stride)
{
    void* data = nullptr;
    const buffer_t* buf_yuv = allocBuffer(buf_size, &data, 16u);

    uint8_t* line = (uint8_t*)data;
    for (unsigned y = 0; y < plane_cy; ++y) {
        if (readFile(file, line, plane_cx) != plane_cx) {
            freeBuffer(buf_yuv);
            buf_yuv = nullptr;
            break;
        }
        line += stride;
    }

    return buf_yuv;
}
