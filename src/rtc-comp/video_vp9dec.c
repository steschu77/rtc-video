/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */

#include <malloc.h>
#include <assert.h>

#include "cutil_nullptr.h"
#include "cutil_refobj.h"

#include "rtc-comp/codec_vp9.h"

#include "../rtc-api/video_sink_impl.h"
#include "../rtc-api/image_source_impl.h"

// ----------------------------------------------------------------------------
#include "vpx/vpx_codec.h"
#include "vpx/vpx_image.h"

// ----------------------------------------------------------------------------
extern vpx_codec_err_t vp9_codec_dec_init(
    vpx_codec_ctx_t* ctx,
    const vpx_codec_dec_cfg_t* cfg);

extern vpx_codec_err_t vp9_codec_dec_destroy(vpx_codec_ctx_t* ctx);
extern vpx_codec_err_t vp9_codec_decode(vpx_codec_ctx_t* ctx,
    const uint8_t* data, unsigned int data_sz,
    void* user_priv);
extern vpx_image_t* vp9_codec_get_frame(vpx_codec_ctx_t* ctx);

const image_t* vpx_image_unwrap(vpx_image_t* vpxImage);

// ----------------------------------------------------------------------------
struct vp9dec_s {
    ref_t ref;
    video_sink_t videoSinkObj;
    image_source_t imageSourceObj;

    image_sink_t* imageSink;
    video_source_t* videoSource;

    vpx_codec_ctx_t vp9ctx;

    video_caps_t caps;
};

// ----------------------------------------------------------------------------
void vp9decAssign(void* obj);
void vp9decRelease(void* obj);

// Image Source
// ----------------------------------------------------------------------------
void vp9decConnect(void* ptr, image_sink_t* sink)
{
    vp9dec_t* obj = (vp9dec_t*)ptr;
    imgsinkAssign(sink, &obj->imageSink);

    videosrcConnect(obj->videoSource, sink != nullptr ? &obj->videoSinkObj : nullptr);
}

// ----------------------------------------------------------------------------
void vp9decChangeCaps(void* ptr, const image_caps_t* caps)
{
    vp9dec_t* obj = (vp9dec_t*)ptr;

    obj->caps.imgcaps = *caps;
    videosrcChangeCaps(obj->videoSource, &obj->caps);
}

// Video Sink
// ----------------------------------------------------------------------------
void vp9decStartSeq(void* obj, media_clock_t ts, seq_id_t id, const video_format_t* fmt)
{
}

// ----------------------------------------------------------------------------
void vp9decFinishSeq(void* obj, media_clock_t ts)
{
}

// ----------------------------------------------------------------------------
void vp9decDeliverPic(void* obj, media_clock_t ts, const video_pic_t* pic)
{
    vp9dec_t* dec = (vp9dec_t*)obj;

    if (pic != nullptr) {
        const buffer_t* buf = getVideoPictureData(pic);

        vpx_codec_err_t res = vp9_codec_decode(&dec->vp9ctx,
            getBufferData(buf), (int)getBufferSize(buf), nullptr);
        if (res != 0) {
            return;
        }

        vpx_image_t* vpxImage = vp9_codec_get_frame(&dec->vp9ctx);
        if (vpxImage == nullptr) {
            return;
        }

        const image_t* img = vpx_image_unwrap(vpxImage);
        imgsinkDeliverPicture(dec->imageSink, ts, img);
        gfxFreeImage(img);
    } else {
        imgsinkDeliverPicture(dec->imageSink, ts, nullptr);
    }
}

// ----------------------------------------------------------------------------
void vp9decAssign(void* ptr)
{
    const vp9dec_t* obj = (const vp9dec_t*)ptr;
    ref_inc(&obj->ref);
}

// ----------------------------------------------------------------------------
void vp9decRelease(void* ptr)
{
    const vp9dec_t* obj = (const vp9dec_t*)ptr;
    ref_dec(&obj->ref);
}

// ----------------------------------------------------------------------------
void vp9decDestroy(const void* ptr)
{
    const vp9dec_t* obj = (const vp9dec_t*)ptr;
    if (obj != nullptr) {
        assert(obj->imageSink == nullptr);
        videosrcRelease(obj->videoSource);
        vp9_codec_dec_destroy((vpx_codec_ctx_t*)&obj->vp9ctx);
        free((void*)obj);
    }
}

// ----------------------------------------------------------------------------
vp9dec_t* vp9decCreate(video_source_t* videoSource, const video_caps_t* caps)
{
    vp9dec_t* obj = (vp9dec_t*)malloc(sizeof(vp9dec_t));
    if (obj == nullptr) {
        return nullptr;
    }

    vpx_codec_err_t res;

    vpx_codec_dec_cfg_t dx_cfg = { 0 };
    if ((res = vp9_codec_dec_init(&obj->vp9ctx, &dx_cfg)) != VPX_CODEC_OK) {
        free(obj);
        return nullptr;
    }

    ref_init(&obj->ref, obj, vp9decDestroy);
    videosinkInit(
        &obj->videoSinkObj,
        obj,
        vp9decAssign,
        vp9decRelease,
        vp9decStartSeq,
        vp9decFinishSeq,
        vp9decDeliverPic
    );

    imgsrcInit(
        &obj->imageSourceObj,
        obj,
        vp9decAssign,
        vp9decRelease,
        vp9decConnect,
        vp9decChangeCaps
    );

    obj->imageSink = nullptr;
    obj->videoSource = videosrcCopy(videoSource);

    obj->caps = *caps;

    return obj;
}

// ----------------------------------------------------------------------------
image_source_t* vp9decQueryImageSource(vp9dec_t* obj)
{
    return imgsrcCopy(&obj->imageSourceObj);
}
