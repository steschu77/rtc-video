/*
 * MIT License
 *
 * Copyright (c) 2020 Steffen Schulze
 */
#define _CRT_SECURE_NO_WARNINGS

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <assert.h>

#include "cutil_nullptr.h"
#include "cutil_math.h"
#include "cutil_bytewrite.h"
#include "cutil_refobj.h"
#include "cutil_sortedlist.h"

#include "rtc-comp/video_sender.h"

#include "rtc-proto/packet.h"
#include "rtc-proto/caps_frame.h"
#include "rtc-proto/video_frame.h"
#include "rtc-proto/endpoint.h"

#include "../rtc-api/packet_sink_impl.h"
#include "../rtc-api/packet_source_impl.h"
#include "../rtc-api/video_sink_impl.h"

// ----------------------------------------------------------------------------
struct video_sender_s {
    ref_t ref;
    video_sink_t videoSinkObj;
    packet_source_t packetSourceObj;
    packet_sink_t packetSendObj;

    video_source_t* videoSource;
    packet_sink_t* packetRecv;
    task_queue_t* taskQueue;

    uint64_t streamSeqNo;

    uint64_t packetSeqNo;
    size_t maxPacketSize;

    media_clock_t tNow;

    sortedlist_t* inflightPackets;
    uint64_t lastAckedTxSeqNo;
    endpoint_t* ep;

    const caps_frame_t* caps_req;
    network_qos_params_t qos;
    video_send_params_t params;
};

// ----------------------------------------------------------------------------
static void vidsendChangeCaps(video_sender_t* obj);

// ----------------------------------------------------------------------------
static void objAssign(void* ptr)
{
    const video_sender_t* obj = (const video_sender_t*)ptr;
    ref_inc(&obj->ref);
}

// ----------------------------------------------------------------------------
static void objRelease(void* ptr)
{
    const video_sender_t* obj = (const video_sender_t*)ptr;
    ref_dec(&obj->ref);
}

// ----------------------------------------------------------------------------
static void releasePacket(const void* ptr)
{
    const packet_t* obj = (const packet_t*)ptr;
    packetRelease(obj);
}

// ----------------------------------------------------------------------------
static void objDestroy(const void* ptr)
{
    const video_sender_t* obj = (const video_sender_t*)ptr;
    if (obj != nullptr) {
        assert(obj->packetRecv == nullptr);
        videosrcRelease(obj->videoSource);
        taskqueueRelease(obj->taskQueue);

        slistDestroy(obj->inflightPackets, releasePacket);
        epDestroy(obj->ep);

        free((void*)obj);
    }
}

// __ Packet Source ___________________________________________________________
// ----------------------------------------------------------------------------
static void objConnect(void* ptr, packet_sink_t* recv, packet_sink_t** send)
{
    video_sender_t* obj = (video_sender_t*)ptr;
    pcksinkAssign(recv, &obj->packetRecv);

    videosrcConnect(obj->videoSource, recv != nullptr ? &obj->videoSinkObj : nullptr);

    pcksinkAssign(recv != nullptr ? &obj->packetSendObj : nullptr, send);
}

// ----------------------------------------------------------------------------
void objQoSChanged(void* ptr, const network_qos_params_t* qos)
{
    video_sender_t* obj = (video_sender_t*)ptr;
    obj->qos = *qos;
    vidsendChangeCaps(obj);
}

// __ Video Sink ______________________________________________________________
// ----------------------------------------------------------------------------
static void objStartSeq(void* ptr, media_clock_t t, seq_id_t id, const video_format_t* fmt)
{
    video_sender_t* obj = (video_sender_t*)ptr;
    obj->tNow = t;
}

// ----------------------------------------------------------------------------
static void objFinishSeq(void* ptr, media_clock_t t)
{
    video_sender_t* obj = (video_sender_t*)ptr;
    obj->tNow = t;
}

// ----------------------------------------------------------------------------
static void sendVideoPacket(video_sender_t* obj, const video_frame_t* videoFrame)
{
    const uint64_t packetSeqNo = obj->packetSeqNo++;
    packet_t* packet = packetCreate(dir_send_recv, packetSeqNo);

    packetAddTime(packet, packet_send, obj->tNow);

    const ack_frame_t* ackFrame = epPreparePacket(obj->ep, packetSeqNo);
    packetAddAckFrame(packet, ackFrame);

    packetAddVideoFrame(packet, videoFrame);

    pcksinkDeliverPacket(obj->packetRecv, obj->tNow, packet);

    slistInsertItem(obj->inflightPackets, packetCompare, packetAssign(packet));

    packetRelease(packet);
}

// ----------------------------------------------------------------------------
static void sendEndFrame(video_sender_t* obj);

static void objResendEndframe(media_clock_t t, void* p0)
{
    video_sender_t* obj = (video_sender_t*)p0;
    obj->tNow = t;

    if (obj->packetRecv != nullptr) {
        sendEndFrame(obj);
    }
}

// ----------------------------------------------------------------------------
static void sendEndFrame(video_sender_t* obj)
{
    const uint64_t streamSeqNo = obj->streamSeqNo++;
    video_frame_t* videoFrame = videoframeCreate(streamSeqNo, 0, 3<<6, obj->tNow, nullptr, ref_endframe, 0);
    sendVideoPacket(obj, videoFrame);

    // make sure this last frame arrives at the receiver
    task_t* task = task1pCreate(obj->tNow + make_msec(200), objResendEndframe, obj);
    taskqueueScheduleEvent(obj->taskQueue, task);
    taskRelease(task);
}

// ----------------------------------------------------------------------------
static void objDeliverPic(void* ptr, media_clock_t t, const video_pic_t* pic)
{
    video_sender_t* obj = (video_sender_t*)ptr;
    obj->tNow = t;

    if (pic != nullptr) {

      const pic_id_t picid = getVideoPictureId(pic);
      const ref_type_t picType = getVideoPictureType(pic);
      const buffer_t* picBuf = getVideoPictureData(pic);
      const size_t picSize = getBufferSize(picBuf);
      const uint64_t psnr = getVideoPicturePSNR(pic);

      size_t consumedBytes = 0;
      while (consumedBytes < picSize) {

          const size_t payloadSize = size_t_min(picSize - consumedBytes, obj->maxPacketSize);

          const int picStart = consumedBytes == 0;
          const int picEnd = consumedBytes + payloadSize == picSize;
          const uint8_t picHeader = (picStart << 7) | (picEnd << 6);

          const buffer_t* frameData = allocSubBuffer(picBuf, consumedBytes, payloadSize);
          consumedBytes += payloadSize;

          const uint64_t streamSeqNo = obj->streamSeqNo++;
          const video_frame_t* videoFrame = videoframeCreate(streamSeqNo, picid, picHeader, t, frameData, picType, psnr);

          sendVideoPacket(obj, videoFrame);

          freeBuffer(frameData);
      }
    } else {
        sendEndFrame(obj);
    }
}

// ----------------------------------------------------------------------------
static int objTxVideoFrame(void* ptr, const packet_t* packet, const video_frame_t* videoFrame)
{
    video_sender_t* obj = (video_sender_t*)ptr;

    if (obj->params.rtx) {
        sendVideoPacket(obj, videoFrame);
    } else {
        media_id_t picid = videoframeGetPicId(videoFrame);
        videosrcNotifyPictureLost(obj->videoSource, picid);
    }
    return 0;
}

// ----------------------------------------------------------------------------
static frame_enum_t cbsResendPacket = {
    nullptr,
    nullptr,
    objTxVideoFrame
};

// ----------------------------------------------------------------------------
static void resendPacketData(video_sender_t* obj, const packet_t* packet)
{
    packetEnumFrames(packet, obj, &cbsResendPacket);
}

// ----------------------------------------------------------------------------
static void vidsendChangeCaps(video_sender_t* obj)
{
    if (obj->caps_req != nullptr) {
        video_caps_t caps = *capsframeGetCaps(obj->caps_req);
        caps.max_bitrate = uint64_max(caps.max_bitrate, obj->qos.bitrate);
        videosrcChangeCaps(obj->videoSource, &caps);
    }
}

// ----------------------------------------------------------------------------
// Called by endpoint.c when a previously sent packet is acknowledged by the receiver.
static void objAckPacket(void* ptr, uint64_t seqno)
{
    video_sender_t* obj = (video_sender_t*)ptr;

    for (uint64_t i = obj->lastAckedTxSeqNo; i < seqno; ++i) {
        const packet_t* packet = (const packet_t*)slistRemoveItem(obj->inflightPackets, packetPredicate, &i);
        if (packet != nullptr) {
            resendPacketData(obj, packet);
            packetRelease(packet);
        }
    }

    const packet_t* packet = (const packet_t*)slistRemoveItem(obj->inflightPackets, packetPredicate, &seqno);
    if (packet != nullptr) {
        packetRelease(packet);
    }

    obj->lastAckedTxSeqNo = uint64_max(obj->lastAckedTxSeqNo, seqno);
}

// __ Packet Frame Enum _______________________________________________________
// ----------------------------------------------------------------------------
static int objRxAckFrame(void* ptr, const packet_t* packet, const ack_frame_t* ackFrame)
{
    video_sender_t* obj = (video_sender_t*)ptr;
    const uint64_t seqno = packetGetSeqNo(packet);
    epPacketReceived(obj->ep, seqno, ackFrame);
    return 0;
}

// ----------------------------------------------------------------------------
static int objRxCapsFrame(void* ptr, const packet_t* packet, const caps_frame_t* capsFrame)
{
    video_sender_t* obj = (video_sender_t*)ptr;

    if (obj->caps_req == nullptr || capsframeGetSeqid(obj->caps_req) < capsframeGetSeqid(capsFrame)) {
        obj->caps_req = capsframeRelease(obj->caps_req);
        obj->caps_req = capsframeConstAssign(capsFrame);
        vidsendChangeCaps(obj);
    }
    return 0;
}

// ----------------------------------------------------------------------------
static frame_enum_t cbsDeliverPacket = {
    objRxAckFrame,
    objRxCapsFrame,
    nullptr
};

// ----------------------------------------------------------------------------
static void objDeliverPacket(void* ptr, media_clock_t t, const packet_t* packet)
{
    video_sender_t* obj = (video_sender_t*)ptr;
    obj->tNow = t;

    packetEnumFrames(packet, obj, &cbsDeliverPacket);
}

// ----------------------------------------------------------------------------
video_sender_t* videosendCreate(const video_send_params_t* params,
    task_queue_t* taskQueue, video_source_t* videoSource)
{
    video_sender_t* obj = (video_sender_t*)malloc(sizeof(video_sender_t));
    if (obj == nullptr) {
        return nullptr;
    }

    ref_init(&obj->ref, obj, objDestroy);

    videosinkInit(
        &obj->videoSinkObj,
        obj,
        objAssign,
        objRelease,
        objStartSeq,
        objFinishSeq,
        objDeliverPic);

    pcksinkInit(
        &obj->packetSendObj,
        obj,
        objAssign,
        objRelease,
        objDeliverPacket);

    pcksrcInit(
        &obj->packetSourceObj,
        obj,
        objAssign,
        objRelease,
        objConnect,
        objQoSChanged);

    obj->packetRecv = nullptr;
    obj->videoSource = videosrcCopy(videoSource);
    obj->taskQueue = taskqueueAssign(taskQueue);

    obj->streamSeqNo = 0;
    obj->packetSeqNo = 0;
    obj->maxPacketSize = 1000;
    obj->tNow = 0;

    obj->inflightPackets = slistCreate();
    obj->lastAckedTxSeqNo = 0;
    obj->ep = epCreate(objAckPacket, obj);

    obj->caps_req = nullptr;
    obj->params = *params;

    return obj;
}

// ----------------------------------------------------------------------------
packet_source_t* videosendQueryPacketSource(video_sender_t* obj)
{
    return pcksrcCopy(&obj->packetSourceObj);
}
