/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */
#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <math.h>

#include "cutil_nullptr.h"

#include "rtc-api/image_picture.h"

// ----------------------------------------------------------------------------
typedef struct lin_system_s {

    uint64_t values_sum;
    uint64_t values_mean;
    uint64_t x_mean;
    uint64_t y_mean;

    double xx;
    double xy;
    double yy;
    double x_values;
    double y_values;

} lin_system_t;

// ----------------------------------------------------------------------------
void calcMean(unsigned x, unsigned y, unsigned value, void* context)
{
    lin_system_t* lin_sys = (lin_system_t*)context;
    lin_sys->values_sum += value;
}

// ----------------------------------------------------------------------------
void calcLinSystem(unsigned x, unsigned y, unsigned value, void* context)
{
    lin_system_t* ls = (lin_system_t*)context;

    const int64_t dx = x - ls->x_mean;
    const int64_t dy = y - ls->y_mean;
    const int64_t d_value = (uint64_t)value - ls->values_mean;

    ls->xx += dx * dx;
    ls->xy += dx * dy;
    ls->yy += dy * dy;

    ls->x_values += dx * d_value;
    ls->y_values += dy * d_value;
}

// ----------------------------------------------------------------------------
int gfxVerifyGradientImage(const image_t* img)
{
    const uint64_t cx = gfxImageWidth(img);
    const uint64_t cy = gfxImageHeight(img);
    const uint64_t cImg = cx * cy;

    lin_system_t ls = { 0 };
    gfxImageForEachPixel(img, 0u, calcMean, &ls);

    ls.x_mean = cx / 2;
    ls.y_mean = cy / 2;
    ls.values_mean = (ls.values_sum + cImg / 2) / cImg;
    gfxImageForEachPixel(img, 0u, calcLinSystem, &ls);

    const double det = ls.xx * ls.yy - ls.xy * ls.xy;
    const double ha0 = ls.yy * ls.x_values - ls.xy * ls.y_values;
    const double ha1 = ls.xx * ls.y_values - ls.xy * ls.x_values;
    const double b0 = ha0 / det;
    const double b1 = ha1 / det;

    const double PI = 3.141f;
    const int idx = lround(atan2(b0, b1) * 256.0 / (2.0 * PI));

    //printf("verify: a0=%2.2f, a1=%2.2f, a2=%d, idx=%d\n", b0, b1, (int)ls.values_mean, (int)idx);

    return idx;
}
