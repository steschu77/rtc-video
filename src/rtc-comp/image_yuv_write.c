/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */
#define _CRT_SECURE_NO_WARNINGS

#include <assert.h>

#include "cutil_buffer.h"
#include "cutil_path.h"
#include "cutil_refobj.h"
#include "cutil_file.h"

#include "rtc-comp/image_yuvfile.h"
#include "../rtc-api/image_sink_impl.h"
#include "../rtc-api/image_source_impl.h"

// ----------------------------------------------------------------------------
struct imgyuv_writer_s {
    ref_t ref;
    image_sink_t imageSinkObj;
    image_source_t imageSourceObj;
    image_source_t* imageSource;
    image_sink_t* imageSink;

    const utf8str_t* seqName;
    utilsFile* file;

    image_size_t size;
    fourcc_t fmt;
};

// ----------------------------------------------------------------------------
static int yuvwriteInitFile(imgyuv_writer_t* obj, const image_size_t* size);

    // ----------------------------------------------------------------------------
static void objAssign(void* ptr)
{
    const imgyuv_writer_t* obj = (const imgyuv_writer_t*)ptr;
    ref_inc(&obj->ref);
}

// ----------------------------------------------------------------------------
static void objRelease(void* ptr)
{
    const imgyuv_writer_t* obj = (const imgyuv_writer_t*)ptr;
    ref_dec(&obj->ref);
}

// ----------------------------------------------------------------------------
static void objDestroy(const void* ptr)
{
    imgyuv_writer_t* obj = (imgyuv_writer_t*)ptr;
    if (obj != nullptr) {
        assert(obj->imageSink == nullptr);
        imgsrcRelease(obj->imageSource);
        utf8_free(&obj->seqName);
        freeFile(obj->file);
        free((void*)obj);
    }
}

// Image Sink
// ----------------------------------------------------------------------------
void yuvwriteDeliverPic(void* ptr, media_clock_t ts, const image_t* img)
{
    imgyuv_writer_t* obj = (imgyuv_writer_t*)ptr;

    if (img != nullptr) {

        const unsigned cx = gfxImageWidth(img);
        const unsigned cy = gfxImageHeight(img);
        const stride_t* stride = gfxImageStride(img);
        const fourcc_t fmt = gfxImageFormat(img);

        const plane_scale_t shift = gfxPlaneShift(fmt);
        const plane_scale_t scale = gfxPlaneScale(fmt);

        if (cx != obj->size.cx || cy != obj->size.cy) {
            yuvwriteInitFile(obj, gfxImageSize(img));
        }

        for (int i = 0; i < 4; ++i) {

            const buffer_t* buf = gfxImageData(img, i);
            const uint8_t* data = getBufferData(buf);

            const unsigned plane_cx = (scale.p[i] * cx) >> shift.p[i];
            const unsigned plane_cy = (scale.p[i] * cy) >> shift.p[i];

            for (unsigned y = 0; y < plane_cy; ++y) {
                writeFile(obj->file, data, plane_cx);
                data += stride->p[i];
            }
        }
    }

    imgsinkDeliverPicture(obj->imageSink, ts, img);
}

// Image Source
// ----------------------------------------------------------------------------
static void yuvwriteConnect(void* ptr, image_sink_t* sink)
{
    imgyuv_writer_t* obj = (imgyuv_writer_t*)ptr;
    imgsinkAssign(sink, &obj->imageSink);

    imgsrcConnect(obj->imageSource, sink != nullptr ? &obj->imageSinkObj : nullptr);
}

// ----------------------------------------------------------------------------
static void yuvwriteChangeCaps(void* ptr, const image_caps_t* caps)
{
    imgyuv_writer_t* obj = (imgyuv_writer_t*)ptr;
    imgsrcChangeCaps(obj->imageSource, caps);
}

// ----------------------------------------------------------------------------
imgyuv_writer_t* yuvwriteCreate(image_source_t* imageSource, const utf8str_t* seqName)
{
    imgyuv_writer_t* obj = (imgyuv_writer_t*)malloc(sizeof(imgyuv_writer_t));
    if (obj == nullptr) {
        return nullptr;
    }

    ref_init(&obj->ref, obj, objDestroy);
    imgsinkInit(&obj->imageSinkObj,
        obj,
        objAssign,
        objRelease,
        yuvwriteDeliverPic);

    imgsrcInit(
        &obj->imageSourceObj,
        obj,
        objAssign,
        objRelease,
        yuvwriteConnect,
        yuvwriteChangeCaps);

    obj->imageSource = imgsrcCopy(imageSource);
    obj->imageSink = nullptr;
    obj->seqName = utf8_assign(seqName);
    obj->file = nullptr;
    obj->size.cx = 0;
    obj->size.cy = 0;
    obj->fmt = 0;
    return obj;
}

// ----------------------------------------------------------------------------
image_source_t* yuvwriteQueryImageSource(imgyuv_writer_t* obj)
{
    return imgsrcCopy(&obj->imageSourceObj);
}

// ----------------------------------------------------------------------------
static int yuvwriteInitFile(imgyuv_writer_t* obj, const image_size_t* size)
{
    const utf8str_t* fileName = utf8_format("%s.%dx%d",
        utf8_cstr(obj->seqName), size->cx, size->cy);

    if (obj->file != nullptr) {
        freeFile(obj->file);
        obj->file = nullptr;
    }

    obj->file = createFileWrite(fileName, f_overwrite);
    obj->size = *size;

    utf8_free(&fileName);
    return 0;
}
