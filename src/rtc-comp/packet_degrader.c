/*
 * MIT License
 *
 * Copyright (c) 2021 Steffen Schulze
 */
#define _CRT_SECURE_NO_WARNINGS

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <assert.h>

#include "cutil_nullptr.h"
#include "cutil_math.h"
#include "cutil_refobj.h"
#include "cutil_random.h"

#include "cutil_file.h"
#include "cutil_bytewrite.h"
#include "cutil_vecu64.h"

#include "rtc-proto/frame.h"
#include "rtc-proto/ack_frame.h"
#include "rtc-proto/video_frame.h"
#include "rtc-proto/packet_log.h"

#include "rtc-comp/packet_degrader.h"

#include "../rtc-api/packet_sink_impl.h"
#include "../rtc-api/packet_source_impl.h"

// ----------------------------------------------------------------------------
struct packet_degrader_s {
    ref_t ref;
    packet_source_t packetsrcRecv;
    packet_sink_t packetsinkSend;
    packet_sink_t packetsinkRecv;

    packet_source_t* packetsrcSend;
    packet_sink_t* packetRecv;
    packet_sink_t* packetSend;
    task_queue_t* taskQueue;

    rnd_t* rnd[2];

    network_qos_params_t qos;
    media_clock_t t;
    size_t sendQLength;

    utilsFile* logFile;
};

// ----------------------------------------------------------------------------
static void updateSendQueue(packet_degrader_t* obj, media_clock_t t)
{
    const media_clock_t dt = t - obj->t;
    const size_t maxSentBits = dt * obj->qos.bitrate / clock_freq;

    size_t sentBits = size_t_min(maxSentBits, obj->sendQLength);
    obj->sendQLength -= sentBits;

    obj->t = t;
}

// Packet Source
// ----------------------------------------------------------------------------
static void objRecvConnect(void* ptr, packet_sink_t* recv, packet_sink_t** send)
{
    packet_degrader_t* obj = (packet_degrader_t*)ptr;
    pcksinkAssign(recv, &obj->packetRecv);

    pcksrcConnect(obj->packetsrcSend, recv != nullptr ? &obj->packetsinkSend : nullptr, &obj->packetSend);

    pcksinkAssign(recv != nullptr ? &obj->packetsinkRecv : nullptr, send);

    if (recv != nullptr) {
        pcksrcQoSChanged(obj->packetsrcSend, &obj->qos);
    }
}

// ----------------------------------------------------------------------------
void objRecvQoSChanged(void* ptr, const network_qos_params_t* qos)
{
    packet_degrader_t* obj = (packet_degrader_t*)ptr;
    obj->qos = *qos;

    pcksrcQoSChanged(obj->packetsrcSend, &obj->qos);
}

// ----------------------------------------------------------------------------
static void objTimerSendPacket(media_clock_t t, void* p0, void* p1)
{
    packet_sink_t* obj = (packet_sink_t*)p0;
    const packet_t* packet = (const packet_t*)p1;
    pcksinkDeliverPacket(obj, t, packet);
    packetRelease(packet);
}

// ----------------------------------------------------------------------------
static void objDeliverPacket(packet_degrader_t* obj, packet_sink_t* sink, media_clock_t t, media_clock_t delay, const packet_t* packet)
{
    const direction_t dir = packetGetDirection(packet);
    const uint64_t seqno = packetGetSeqNo(packet);

    const int loss = rndGenerate(obj->rnd[dir]);
    const int lost = loss < obj->qos.loss;

    packet_event_t* evtSend = packeteventCreate(dir, seqno, packet_send, t);
    packet_event_t* evtRecv = packeteventCreate(dir, seqno, packet_recv, t + delay);

    if (obj->logFile != nullptr) {
        packetlogWritePacketData(obj->logFile, packet);
        packetlogWritePacketEvent(obj->logFile, evtSend);
    }

    if (!lost) {
        const media_clock_t tRecv = t + delay;

        if (obj->logFile != nullptr) {
            packetlogWritePacketEvent(obj->logFile, evtRecv);
        }

        task_t* task = task2pCreate(tRecv, objTimerSendPacket, sink, (void*)packetAssign((packet_t*)packet));
        taskqueueScheduleEvent(obj->taskQueue, task);
        taskRelease(task);
    }

    packeteventRelease(evtSend);
    packeteventRelease(evtRecv);
}

// ----------------------------------------------------------------------------
static void objSendDeliverPacket(void* ptr, media_clock_t t, const packet_t* packet)
{
    packet_degrader_t* obj = (packet_degrader_t*)ptr;

    updateSendQueue(obj, t);
    
    const media_clock_t tQueue = obj->sendQLength * clock_freq / obj->qos.bitrate;
    const media_clock_t delay = obj->qos.delay + tQueue;

    objDeliverPacket(obj, obj->packetRecv, t, delay, packet);

    size_t len = packetGetLength(packet);
    obj->sendQLength += len * 8;
}

// Packet Sink
// ----------------------------------------------------------------------------
static void objRecvDeliverPacket(void* ptr, media_clock_t t, const packet_t* packet)
{
    packet_degrader_t* obj = (packet_degrader_t*)ptr;

    const media_clock_t tQueue = 0;
    const media_clock_t delay = obj->qos.delay + tQueue;

    objDeliverPacket(obj, obj->packetSend, t, delay, packet);
}

// ----------------------------------------------------------------------------
static void objAssign(void* ptr)
{
    const packet_degrader_t* obj = (const packet_degrader_t*)ptr;
    ref_inc(&obj->ref);
}

// ----------------------------------------------------------------------------
static void objRelease(void* ptr)
{
    const packet_degrader_t* obj = (const packet_degrader_t*)ptr;
    ref_dec(&obj->ref);
}

// ----------------------------------------------------------------------------
static void objDestroy(const void* ptr)
{
    const packet_degrader_t* obj = (const packet_degrader_t*)ptr;
    if (obj != nullptr) {
        assert(obj->packetRecv == nullptr);
        assert(obj->packetSend == nullptr);
        pcksrcRelease(obj->packetsrcSend);
        taskqueueRelease(obj->taskQueue);
        rndDestroy(obj->rnd[dir_send_recv]);
        rndDestroy(obj->rnd[dir_recv_send]);
        freeFile(obj->logFile);
        free((void*)obj);
    }
}

// ----------------------------------------------------------------------------
packet_degrader_t* pckdegraderCreate(task_queue_t* taskQueue, packet_source_t* packetSource, const network_qos_params_t* qos, utilsFile* logFile)
{
    packet_degrader_t* obj = (packet_degrader_t*)malloc(sizeof(packet_degrader_t));
    if (obj == nullptr) {
        return nullptr;
    }

    ref_init(&obj->ref, obj, objDestroy);

    pcksinkInit(
        &obj->packetsinkRecv,
        obj,
        objAssign,
        objRelease,
        objRecvDeliverPacket);

    pcksrcInit(
        &obj->packetsrcRecv,
        obj,
        objAssign,
        objRelease,
        objRecvConnect,
        objRecvQoSChanged);

    pcksinkInit(
        &obj->packetsinkSend,
        obj,
        objAssign,
        objRelease,
        objSendDeliverPacket);

    obj->packetsrcSend = pcksrcCopy(packetSource);

    obj->packetRecv = nullptr;
    obj->packetSend = nullptr;
    obj->taskQueue = taskqueueAssign(taskQueue);

    obj->rnd[dir_send_recv] = rndCreate(1789);
    obj->rnd[dir_recv_send] = rndCreate(1234);

    obj->qos = *qos;
    obj->t = 0;
    obj->sendQLength = 0;

    obj->logFile = assignFile(logFile);

    return obj;
}

// ----------------------------------------------------------------------------
packet_source_t* pckdegraderQueryPacketSource(packet_degrader_t* obj)
{
    return pcksrcCopy(&obj->packetsrcRecv);
}
