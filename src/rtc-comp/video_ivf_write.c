/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */
#define _CRT_SECURE_NO_WARNINGS

#include <assert.h>

#include "cutil_buffer.h"
#include "cutil_file.h"
#include "cutil_path.h"
#include "cutil_refobj.h"
#include "cutil_bytewrite.h"

#include "../rtc-api/video_sink_impl.h"
#include "../rtc-api/video_source_impl.h"
#include "rtc-comp/video_ivffile.h"

// ----------------------------------------------------------------------------
static void ivfwriteConnect(void* ptr, video_sink_t* sink);
static void ivfwriteChangeCaps(void* ptr, const video_caps_t* caps);
static void ivfwriteRequestSyncPoint(void* ptr);
static void ivfwriteNotifyPictureLost(void* ptr, const media_id_t id);
static void ivfwriteNotifyPictureProcessed(void* ptr, const media_id_t id);

// ----------------------------------------------------------------------------
static void ivfwriteStartSequence(void* ptr, media_clock_t ts, seq_id_t id, const video_format_t* fmt);
static void ivfwriteFinishSequence(void* ptr, media_clock_t ts);
static void ivfwriteDeliverPic(void* ptr, media_clock_t ts, const video_pic_t*);

// ----------------------------------------------------------------------------
static void writeIVFFileHeader(utilsFile* file, const frame_rate_t* rate,
    const image_size_t* size, const fourcc_t codec);
static void writeIVFPicture(utilsFile* file, const buffer_t* buf, uint64_t pts);

// ----------------------------------------------------------------------------
static void mem_write_le8(uint8_t* ptr, uint8_t val);
static void mem_write_le16(uint8_t* ptr, uint16_t val);
static void mem_write_le32(uint8_t* ptr, uint32_t val);
static void mem_write_le64(uint8_t* ptr, uint64_t val);

// ----------------------------------------------------------------------------
struct vidivf_writer_s {
    ref_t ref;
    video_sink_t videoSinkObj;
    video_source_t videoSourceObj;
    video_sink_t* videoSink;
    video_source_t* videoSource;

    utilsFile* file;

    image_size_t size;
    frame_rate_t rate;
    fourcc_t fmt;
};

// ----------------------------------------------------------------------------
static void objAssign(void* ptr)
{
    const vidivf_writer_t* obj = (const vidivf_writer_t*)ptr;
    ref_inc(&obj->ref);
}

// ----------------------------------------------------------------------------
static void objRelease(void* ptr)
{
    const vidivf_writer_t* obj = (const vidivf_writer_t*)ptr;
    ref_dec(&obj->ref);
}

// ----------------------------------------------------------------------------
static void objDestroy(const void* ptr)
{
    const vidivf_writer_t* obj = (const vidivf_writer_t*)ptr;
    if (obj != nullptr) {
        assert(obj->videoSink == nullptr);
        videosrcRelease(obj->videoSource);
        freeFile(obj->file);
        free((void*)obj);
    }
}

// ----------------------------------------------------------------------------
vidivf_writer_t* ivfwriteCreate(video_source_t* videoSource, utilsFile* file)
{
    vidivf_writer_t* obj = (vidivf_writer_t*)malloc(sizeof(vidivf_writer_t));
    if (obj == nullptr) {
        return nullptr;
    }

    ref_init(&obj->ref, obj, objDestroy);
    videosinkInit(&obj->videoSinkObj,
        obj,
        objAssign,
        objRelease,
        ivfwriteStartSequence,
        ivfwriteFinishSequence,
        ivfwriteDeliverPic);

    videosrcInit(
        &obj->videoSourceObj,
        obj,
        objAssign,
        objRelease,
        ivfwriteConnect,
        ivfwriteChangeCaps,
        ivfwriteRequestSyncPoint,
        ivfwriteNotifyPictureLost,
        ivfwriteNotifyPictureProcessed);

    obj->videoSink = nullptr;
    obj->videoSource = videosrcCopy(videoSource);
    obj->file = assignFile(file);

    return obj;
}

// ----------------------------------------------------------------------------
const vidivf_writer_t* ivfwriteAssign(const vidivf_writer_t* obj)
{
    if (obj != nullptr) {
        ref_inc(&obj->ref);
    }
    return obj;
}

// ----------------------------------------------------------------------------
void ivfwriteRelease(const vidivf_writer_t* obj)
{
    if (obj != nullptr) {
        ref_dec(&obj->ref);
    }
}

// ----------------------------------------------------------------------------
video_source_t* ivfwriteQueryVideoSource(vidivf_writer_t* obj)
{
    return videosrcCopy(&obj->videoSourceObj);
}

// Video Source interface
// ----------------------------------------------------------------------------
void ivfwriteConnect(void* ptr, video_sink_t* sink)
{
    vidivf_writer_t* obj = (vidivf_writer_t*)ptr;
    videosinkAssign(sink, &obj->videoSink);

    videosrcConnect(obj->videoSource, sink != nullptr ? &obj->videoSinkObj : nullptr);
    
}

// ----------------------------------------------------------------------------
void ivfwriteChangeCaps(void* ptr, const video_caps_t* caps)
{
    const vidivf_writer_t* obj = (const vidivf_writer_t*)ptr;
    videosrcChangeCaps(obj->videoSource, caps);
}

// ----------------------------------------------------------------------------
void ivfwriteRequestSyncPoint(void* ptr)
{
    vidivf_writer_t* obj = (vidivf_writer_t*)ptr;
    videosrcRequestSyncPoint(obj->videoSource);
}

// ----------------------------------------------------------------------------
void ivfwriteNotifyPictureLost(void* ptr, const media_id_t id)
{
    vidivf_writer_t* obj = (vidivf_writer_t*)ptr;
    videosrcNotifyPictureLost(obj->videoSource, id);
}

// ----------------------------------------------------------------------------
void ivfwriteNotifyPictureProcessed(void* ptr, const media_id_t id)
{
    vidivf_writer_t* obj = (vidivf_writer_t*)ptr;
    videosrcNotifyPictureProcessed(obj->videoSource, id);
}

// Video Sink interface
// ----------------------------------------------------------------------------
void ivfwriteStartSequence(void* ptr, media_clock_t ts, seq_id_t id, const video_format_t* fmt)
{
    vidivf_writer_t* obj = (vidivf_writer_t*)ptr;
    
    const frame_rate_t* rate = videofmtGetMaxFrameRate(fmt);
    const image_size_t* size = videofmtGetResolution(fmt);
    const fourcc_t codec = videofmtGetCodec(fmt);
    writeIVFFileHeader(obj->file, rate, size, codec);

    videosinkStartSeq(obj->videoSink, ts, id, fmt);
}

// ----------------------------------------------------------------------------
void ivfwriteFinishSequence(void* ptr, media_clock_t ts)
{
    const vidivf_writer_t* obj = (const vidivf_writer_t*)ptr;
    videosinkFinishSeq(obj->videoSink, ts);
}

// ----------------------------------------------------------------------------
void ivfwriteDeliverPic(void* ptr, media_clock_t ts, const video_pic_t* pic)
{
    const vidivf_writer_t* obj = (const vidivf_writer_t*)ptr;

    if (pic != nullptr) {
        media_clock_t pts = getVideoPresentationTime(pic);
        const buffer_t* data = getVideoPictureData(pic);
        writeIVFPicture(obj->file, data, pts);

        videosinkDeliverPic(obj->videoSink, ts, pic);
    } else {
        videosinkDeliverPic(obj->videoSink, ts, nullptr);
    }
}

// ----------------------------------------------------------------------------
void writeIVFFileHeader(utilsFile* file, const frame_rate_t* rate,
    const image_size_t* size, const fourcc_t codec)
{
    const fourcc_t dkif = make_fourcc('D', 'K', 'I', 'F');
    const uint16_t ivf_version = 0;
    const uint32_t nb_frames = 0;

    uint8_t header[32];
    mem_write_le32(header + 0, dkif);
    mem_write_le16(header + 4, ivf_version);
    mem_write_le16(header + 6, sizeof(header));
    mem_write_le32(header + 8, codec);
    mem_write_le16(header + 12, size->cx);
    mem_write_le16(header + 14, size->cy);
    mem_write_le32(header + 16, rate->den);
    mem_write_le32(header + 20, rate->num);
    mem_write_le32(header + 24, nb_frames);
    mem_write_le32(header + 28, 0);

    writeFile(file, header, sizeof(header));
}

// ----------------------------------------------------------------------------
void writeIVFPicture(utilsFile* file, const buffer_t* buf, uint64_t pts)
{
    const uint8_t* data = getBufferData(buf);
    const size_t size = getBufferSize(buf);

    uint8_t header[12];
    mem_write_le32(header, (uint32_t)size);
    mem_write_le64(header + 4, pts);

    writeFile(file, header, sizeof(header));
    writeFile(file, data, size);
}

// ----------------------------------------------------------------------------
void mem_write_le8(uint8_t* ptr, uint8_t val)
{
    ptr[0] = val;
}

// ----------------------------------------------------------------------------
void mem_write_le16(uint8_t* ptr, uint16_t val)
{
    mem_write_le8(ptr + 0, (uint8_t)val);
    mem_write_le8(ptr + 1, (uint8_t)(val >> 8));
}

// ----------------------------------------------------------------------------
void mem_write_le32(uint8_t* ptr, uint32_t val)
{
    mem_write_le16(ptr + 0, (uint16_t)val);
    mem_write_le16(ptr + 2, (uint16_t)(val >> 16));
}

// ----------------------------------------------------------------------------
void mem_write_le64(uint8_t* ptr, uint64_t val)
{
    mem_write_le32(ptr + 0, (uint32_t)val);
    mem_write_le32(ptr + 4, (uint32_t)(val >> 32));
}
