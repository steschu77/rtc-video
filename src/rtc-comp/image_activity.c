/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */
#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <assert.h>

#include "cutil_nullptr.h"
#include "cutil_refobj.h"
#include "cutil_math.h"

#include "rtc-api/image_picture.h"
#include "rtc-comp/image_activity.h"

#include "../rtc-api/image_sink_impl.h"
#include "../rtc-api/image_source_impl.h"

// ----------------------------------------------------------------------------
double gfxImagePSNR(const image_t* img0, const image_t* img1);
double gfxImageDynamics(const image_t* img0, const image_t* img1);

// ----------------------------------------------------------------------------
struct image_dynamics_s {
    ref_t ref;
    image_sink_t imageSinkObj;
    image_source_t imageSourceObj;
    image_source_t* imageSource;
    image_sink_t* imageSink;

    utilsFile* file;
    unsigned frameCount;
    const image_t* prevImg;
};

// ----------------------------------------------------------------------------
static void objAssign(void* ptr)
{
    const image_dynamics_t* obj = (const image_dynamics_t*)ptr;
    ref_inc(&obj->ref);
}

// ----------------------------------------------------------------------------
static void objRelease(void* ptr)
{
    const image_dynamics_t* obj = (const image_dynamics_t*)ptr;
    ref_dec(&obj->ref);
}

// ----------------------------------------------------------------------------
static void objDestroy(const void* ptr)
{
    const image_dynamics_t* obj = (const image_dynamics_t*)ptr;
    if (obj != nullptr) {
        assert(obj->imageSink == nullptr);
        gfxFreeImage(obj->prevImg);
        imgsrcRelease(obj->imageSource);
        free((void*)obj);
    }
}

// ----------------------------------------------------------------------------
static void printCSV(utilsFile* f, unsigned frameCount, double psnr, double activity)
{
    const utf8str_t* line = utf8_format("%3d,%2.2f,%2.2f\n",
        frameCount, psnr, activity);
    writeFile(f, utf8_cstr(line), utf8_length(line));
    utf8_free(&line);
}

// Image Sink
// ----------------------------------------------------------------------------
static void imgactivityDeliverPicture(void* ptr, media_clock_t t, const image_t* img)
{
    image_dynamics_t* obj = (image_dynamics_t*)ptr;
    
    const image_t* imgRef = obj->prevImg;

    if (imgRef == nullptr) {
        printCSV(obj->file, obj->frameCount, 0, 0);
    }
    if (imgRef != nullptr && img != nullptr) {

        const double psnr = gfxImagePSNR(img, imgRef);
        const double activity = gfxImageDynamics(img, imgRef);
        printCSV(obj->file, obj->frameCount, psnr, activity);

        gfxFreeImage(imgRef);
        imgRef = nullptr;
    }

    obj->frameCount++;
    obj->prevImg = gfxAssignImage(img);

    imgsinkDeliverPicture(obj->imageSink, t, img);
}

// Image Source
// ----------------------------------------------------------------------------
static void imgactivityConnect(void* ptr, image_sink_t* sink)
{
    image_dynamics_t* obj = (image_dynamics_t*)ptr;
    imgsinkAssign(sink, &obj->imageSink);
    imgsrcConnect(obj->imageSource, &obj->imageSinkObj);
}

// ----------------------------------------------------------------------------
static void imgactivityChangeCaps(void* ptr, const image_caps_t* caps)
{
    image_dynamics_t* obj = (image_dynamics_t*)ptr;
    imgsrcChangeCaps(obj->imageSource, caps);
}

// ----------------------------------------------------------------------------
image_dynamics_t* imgactivityCreate(image_source_t* imageSource, utilsFile* file)
{
    image_dynamics_t* obj = (image_dynamics_t*)malloc(sizeof(image_dynamics_t));
    if (obj == nullptr) {
        return nullptr;
    }

    ref_init(&obj->ref, obj, objDestroy);
    imgsinkInit(
        &obj->imageSinkObj,
        obj,
        objAssign,
        objRelease,
        imgactivityDeliverPicture);

    imgsrcInit(
        &obj->imageSourceObj,
        obj,
        objAssign,
        objRelease,
        imgactivityConnect,
        imgactivityChangeCaps);

    obj->imageSink = nullptr;
    obj->imageSource = imgsrcCopy(imageSource);

    obj->file = assignFile(file);
    obj->frameCount = 0;
    obj->prevImg = nullptr;

    return obj;
}

// ----------------------------------------------------------------------------
image_source_t* imgactivityQueryImageSource(image_dynamics_t* obj)
{
    return imgsrcCopy(&obj->imageSourceObj);
}
