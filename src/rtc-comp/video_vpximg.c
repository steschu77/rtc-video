/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */

#include <malloc.h>
#include <string.h>

#include "cutil_nullptr.h"
#include "cutil_refobj.h"

#include "rtc-api/image_picture.h"

// ----------------------------------------------------------------------------
#include "vpx/vpx_image.h"

// ----------------------------------------------------------------------------
vpx_image_t* vpx_img_wrap_2(const image_t* img)
{
    const unsigned w = gfxImageWidth(img);
    const unsigned h = gfxImageHeight(img);

    /* Allocate the new image */
    vpx_image_t* vpx_img = (vpx_image_t*)calloc(1, sizeof(vpx_image_t));
    if (vpx_img == nullptr) {
        return nullptr;
    }

    vpx_img->self_allocd = 1;

    vpx_img->fmt = VPX_IMG_FMT_I420;
    vpx_img->bit_depth = 8;
    vpx_img->x_chroma_shift = 1;
    vpx_img->y_chroma_shift = 1;
    vpx_img->bps = 12;
    vpx_img->w = w;
    vpx_img->h = h;

    const stride_t* stride = gfxImageStride(img);
    vpx_img->stride[VPX_PLANE_Y] = (int)stride->p[0];
    vpx_img->stride[VPX_PLANE_U] = (int)stride->p[1];
    vpx_img->stride[VPX_PLANE_V] = (int)stride->p[2];
    vpx_img->stride[VPX_PLANE_ALPHA] = (int)stride->p[0];

    /* Default viewport to entire image */
    vpx_img->d_w = w;
    vpx_img->d_h = h;
    vpx_img->planes[VPX_PLANE_Y] = (unsigned char*)getBufferData(gfxImageData(img, 0));
    vpx_img->planes[VPX_PLANE_U] = (unsigned char*)getBufferData(gfxImageData(img, 1));
    vpx_img->planes[VPX_PLANE_V] = (unsigned char*)getBufferData(gfxImageData(img, 2));
    return vpx_img;
}

// ----------------------------------------------------------------------------
const image_t* vpx_image_unwrap(vpx_image_t* vpxImage)
{
    const fourcc_t fmt = fccYUV12;
    const unsigned cx = vpxImage->d_w;
    const unsigned cy = vpxImage->d_h;

    const plane_scale_t shift = gfxPlaneShift(fmt);
    const plane_scale_t scale = gfxPlaneScale(fmt);
    const stride_t stride = gfxCalcFormatStride(fmt, cx, 16u);
    const plane_size_t psize = gfxCalcPlaneSize(fmt, &stride, cy);

    size_t buf_size = 0;
    size_t buf_ofs[4];
    for (int i = 0; i < 4; ++i) {
        buf_ofs[i] = buf_size;
        buf_size += psize.p[i];
    }

    void* data = nullptr;
    const buffer_t* buf = allocBuffer(buf_size, &data, 16u);

    const buffer_t* buf_yuv[4];
    for (int i = 0; i < 4; ++i) {
        buf_yuv[i] = allocSubBuffer(buf, buf_ofs[i], psize.p[i]);
    }

    freeBuffer(buf);

    for (int i = 0; i < 4; ++i) {

        const uint8_t* src = vpxImage->planes[i];
        uint8_t* dst = (uint8_t*)data + buf_ofs[i];

        const unsigned plane_cx = (scale.p[i] * cx) >> shift.p[i];
        const unsigned plane_cy = (scale.p[i] * cy) >> shift.p[i];

        for (unsigned y = 0; y < plane_cy; ++y) {
            memcpy(dst, src, plane_cx);
            dst += stride.p[i];
            src += vpxImage->stride[i];
        }
    }

    image_size_t img_size = { cx, cy };
    const image_t* img = gfxCreateImage(&stride, &img_size, fmt, buf_yuv);

    for (int i = 0; i < 4; ++i) {
        freeBuffer(buf_yuv[i]);
    }

    return gfxAssignImage(img);
}
