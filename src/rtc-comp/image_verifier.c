/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */
#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <assert.h>

#include "cutil_nullptr.h"
#include "cutil_refobj.h"

#include "rtc-api/image_picture.h"

#include "rtc-comp/image_gen.h"
#include "../rtc-api/image_sink_impl.h"
#include "../rtc-api/image_source_impl.h"

// ----------------------------------------------------------------------------
int gfxVerifyGradientImage(const image_t* img);
const image_t* gfxCreateGradientImage(unsigned idx, const image_size_t* size, fourcc_t fmt);

double gfxImagePSNR(const image_t* img0, const image_t* img1);

// ----------------------------------------------------------------------------
struct imgverify_s {
    ref_t ref;
    image_sink_t imageSinkObj;
    image_source_t imageSourceObj;
    image_source_t* imageSource;
    image_sink_t* imageSink;
};

// ----------------------------------------------------------------------------
static void objAssign(void* ptr)
{
    const imgverify_t* obj = (const imgverify_t*)ptr;
    ref_inc(&obj->ref);
}

// ----------------------------------------------------------------------------
static void objRelease(void* ptr)
{
    const imgverify_t* obj = (const imgverify_t*)ptr;
    ref_dec(&obj->ref);
}

// ----------------------------------------------------------------------------
static void objDestroy(const void* ptr)
{
    const imgverify_t* obj = (const imgverify_t*)ptr;
    if (obj != nullptr) {
        assert(obj->imageSink == nullptr);
        imgsrcRelease(obj->imageSource);
        free((void*)obj);
    }
}

// Image Sink
// ----------------------------------------------------------------------------
static void imgvfyDeliverPicture(void* ptr, media_clock_t t, const image_t* img)
{
    const imgverify_t* obj = (const imgverify_t*)ptr;
    
    int idx = gfxVerifyGradientImage(img);

    const image_size_t* size = gfxImageSize(img);
    const image_t* imgRef = gfxCreateGradientImage(idx, size, fccYUV12);

    const double psnr = gfxImagePSNR(img, imgRef);
    printf("verify[%d]: psnr=%2.2f\n", idx, psnr);

    gfxFreeImage(imgRef);
    imgRef = nullptr;

    imgsinkDeliverPicture(obj->imageSink, t, img);
}

// Image Source
// ----------------------------------------------------------------------------
static void imgvfyConnect(void* ptr, image_sink_t* sink)
{
    imgverify_t* obj = (imgverify_t*)ptr;
    imgsinkAssign(sink, &obj->imageSink);
    imgsrcConnect(obj->imageSource, &obj->imageSinkObj);
}

// ----------------------------------------------------------------------------
static void imgvfyChangeCaps(void* ptr, const image_caps_t* caps)
{
    imgverify_t* obj = (imgverify_t*)ptr;
    imgsrcChangeCaps(obj->imageSource, caps);
}

// ----------------------------------------------------------------------------
imgverify_t* imgvfyCreate(image_source_t* imageSource)
{
    imgverify_t* obj = (imgverify_t*)malloc(sizeof(imgverify_t));
    if (obj == nullptr) {
        return nullptr;
    }

    ref_init(&obj->ref, obj, objDestroy);
    imgsinkInit(
        &obj->imageSinkObj,
        obj,
        objAssign,
        objRelease,
        imgvfyDeliverPicture);

    imgsrcInit(
        &obj->imageSourceObj,
        obj,
        objAssign,
        objRelease,
        imgvfyConnect,
        imgvfyChangeCaps);

    obj->imageSink = nullptr;
    obj->imageSource = imgsrcCopy(imageSource);
    return obj;
}

// ----------------------------------------------------------------------------
image_source_t* imgvfyQueryImageSource(imgverify_t* obj)
{
    return imgsrcCopy(&obj->imageSourceObj);
}
