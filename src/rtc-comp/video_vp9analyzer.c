/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */

#include <malloc.h>
#include <assert.h>

#include "cutil_nullptr.h"
#include "cutil_refobj.h"
#include "cutil_file.h"
#include "cutil_bitreader.h"

#include "rtc-comp/codec_vp9.h"

#include "../rtc-api/video_sink_impl.h"
#include "../rtc-api/video_source_impl.h"

// ----------------------------------------------------------------------------
typedef enum vp9_profile_e {
    vp9_profile_0,
    vp9_profile_1,
    vp9_profile_2,
    vp9_profile_3
} vp9_profile_t;

// ----------------------------------------------------------------------------
typedef enum vp9_frametype_e {
    vp9_coded_key_frame,
    vp9_coded_ref_frame,
    vp9_existing_frame
} vp9_frametype_t;

// ----------------------------------------------------------------------------
typedef struct vp9_framehead_s {
    
    vp9_frametype_t type;
    int frameToShow;

    uint8_t showFrame;
    int refresh_frame_flags;
    int frame_refs[3];
} vp9_framehead_t;

// ----------------------------------------------------------------------------
struct vp9analyze_s {
    ref_t ref;
    video_sink_t videoSinkObj;
    video_source_t videoSourceObj;
    video_sink_t* videoSink;
    video_source_t* videoSource;

    utilsFile* file;

    int ref_frame_map[8];

    int frameCount;
};

// ----------------------------------------------------------------------------
static void vp9analyzeConnect(void* ptr, video_sink_t* sink);
static void vp9analyzeChangeCaps(void* ptr, const video_caps_t* caps);
static void vp9analyzeRequestSyncPoint(void* ptr);
static void vp9analyzeNotifyPictureLost(void* ptr, const media_id_t id);
static void vp9analyzeNotifyPictureProcessed(void* ptr, const media_id_t id);

// ----------------------------------------------------------------------------
static void vp9analyzeStartSequence(void* ptr, media_clock_t ts, seq_id_t id, const video_format_t* fmt);
static void vp9analyzeFinishSequence(void* ptr, media_clock_t ts);
static void vp9analyzeDeliverPic(void* ptr, media_clock_t ts, const video_pic_t*);

// ----------------------------------------------------------------------------
static int vp9ReadHeader(bitread_t* bs, vp9_framehead_t* head);
static unsigned vp9ReadProfile(bitread_t* bs);
static int vp9ReadSyncCode(bitread_t* bs);

// ----------------------------------------------------------------------------
static void printFrameInfo(utilsFile* file, media_clock_t t, media_clock_t ts, pic_id_t id, ref_type_t type, uint64_t psnr, vp9_framehead_t* head, size_t size);
static void printFileInfo(utilsFile* file, unsigned frameCount);

// ----------------------------------------------------------------------------
static void objAssign(void* ptr)
{
    const vp9analyze_t* obj = (const vp9analyze_t*)ptr;
    ref_inc(&obj->ref);
}

// ----------------------------------------------------------------------------
static void objRelease(void* ptr)
{
    const vp9analyze_t* obj = (const vp9analyze_t*)ptr;
    ref_dec(&obj->ref);
}

// ----------------------------------------------------------------------------
static void objDestroy(const void* ptr)
{
    const vp9analyze_t* obj = (const vp9analyze_t*)ptr;
    if (obj != nullptr) {
        assert(obj->videoSink == nullptr);
        printFileInfo(obj->file, obj->frameCount);
        videosrcRelease(obj->videoSource);
        freeFile(obj->file);
        free((void*)obj);
    }
}

// ----------------------------------------------------------------------------
vp9analyze_t* vp9analyzeCreate(video_source_t* videoSource, utilsFile* file)
{
    vp9analyze_t* obj = (vp9analyze_t*)malloc(sizeof(vp9analyze_t));
    if (obj == nullptr) {
        return nullptr;
    }

    ref_init(&obj->ref, obj, objDestroy);
    videosinkInit(&obj->videoSinkObj,
        obj,
        objAssign,
        objRelease,
        vp9analyzeStartSequence,
        vp9analyzeFinishSequence,
        vp9analyzeDeliverPic);

    videosrcInit(
        &obj->videoSourceObj,
        obj,
        objAssign,
        objRelease,
        vp9analyzeConnect,
        vp9analyzeChangeCaps,
        vp9analyzeRequestSyncPoint,
        vp9analyzeNotifyPictureLost,
        vp9analyzeNotifyPictureProcessed);

    obj->videoSink = nullptr;
    obj->videoSource = videosrcCopy(videoSource);
    obj->file = assignFile(file);

    obj->frameCount = 0;
    return obj;
}

// ----------------------------------------------------------------------------
video_source_t* vp9analyzeQueryVideoSource(vp9analyze_t* obj)
{
    return videosrcCopy(&obj->videoSourceObj);
}

// Video Source interface
// ----------------------------------------------------------------------------
void vp9analyzeConnect(void* ptr, video_sink_t* sink)
{
    vp9analyze_t* obj = (vp9analyze_t*)ptr;
    videosinkAssign(sink, &obj->videoSink);

    videosrcConnect(obj->videoSource, sink != nullptr ? &obj->videoSinkObj : nullptr);
}

// ----------------------------------------------------------------------------
void vp9analyzeChangeCaps(void* ptr, const video_caps_t* caps)
{
    const vp9analyze_t* obj = (const vp9analyze_t*)ptr;
    videosrcChangeCaps(obj->videoSource, caps);
}

// ----------------------------------------------------------------------------
void vp9analyzeRequestSyncPoint(void* ptr)
{
    const vp9analyze_t* obj = (const vp9analyze_t*)ptr;
    videosrcRequestSyncPoint(obj->videoSource);
}

// ----------------------------------------------------------------------------
void vp9analyzeNotifyPictureLost(void* ptr, const media_id_t id)
{
    const vp9analyze_t* obj = (const vp9analyze_t*)ptr;
    videosrcNotifyPictureLost(obj->videoSource, id);
}

// ----------------------------------------------------------------------------
void vp9analyzeNotifyPictureProcessed(void* ptr, const media_id_t id)
{
    const vp9analyze_t* obj = (const vp9analyze_t*)ptr;
    videosrcNotifyPictureProcessed(obj->videoSource, id);
}


// Video Sink interface
// ----------------------------------------------------------------------------
void vp9analyzeStartSequence(void* ptr, media_clock_t ts, seq_id_t id, const video_format_t* fmt)
{
    vp9analyze_t* obj = (vp9analyze_t*)ptr;
    videosinkStartSeq(obj->videoSink, ts, id, fmt);
}

// ----------------------------------------------------------------------------
void vp9analyzeFinishSequence(void* ptr, media_clock_t ts)
{
    const vp9analyze_t* obj = (const vp9analyze_t*)ptr;
    videosinkFinishSeq(obj->videoSink, ts);
}

// ----------------------------------------------------------------------------
static void printFrameInfo(utilsFile* file, media_clock_t t, media_clock_t ts, pic_id_t id, ref_type_t type, uint64_t psnr, vp9_framehead_t* head, size_t size)
{
    const char* typeName[] = { "I", "P", "H" };
    const char* showName[] = { "hide", "show" };
    const char* refName[] = { " key", "sync", " ref", "idle", " end" };
    const utf8str_t* line = utf8_format("%3d,%3.3f,%3.3f,%s,%s,%s,%2.2f,%6d,%02x,%2d,%2d,%2d\n",
        (int)id,
        1.0 / 90000.0 * t,
        1.0 / 90000.0 * ts,
        typeName[head->type],
        showName[head->showFrame],
        refName[type],
        1.0 / 256.0 * psnr,
        (int)size,
        head->refresh_frame_flags, head->frame_refs[0], head->frame_refs[1], head->frame_refs[2]);

    writeFile(file, utf8_cstr(line), utf8_length(line));

    utf8_free(&line);
}

// ----------------------------------------------------------------------------
static void printFileInfo(utilsFile* file, unsigned frameCount)
{
    const utf8str_t* line = utf8_format("%3d\n", frameCount);

    writeFile(file, utf8_cstr(line), utf8_length(line));

    utf8_free(&line);
}

// ----------------------------------------------------------------------------
void vp9analyzeDeliverPic(void* ptr, media_clock_t t, const video_pic_t* pic)
{
    vp9analyze_t* obj = (vp9analyze_t*)ptr;

    if (pic != nullptr) {
        const pic_id_t picid = getVideoPictureId(pic);
        const pic_id_t ts = getVideoPresentationTime(pic);
        const ref_type_t type = getVideoPictureType(pic);
        const uint64_t psnr = getVideoPicturePSNR(pic);
        const buffer_t* buf = getVideoPictureData(pic);
        const uint8_t* data = getBufferData(buf);
        const size_t size = getBufferSize(buf);
        bitread_t* bs = bitreadCreate(data, size);

        vp9_framehead_t head;
        if (vp9ReadHeader(bs, &head) == 0) {
            printFrameInfo(obj->file, t, ts, picid, type, psnr, &head, size);
        }

        bitreadDestroy(bs);

        obj->frameCount++;
    }

    videosinkDeliverPic(obj->videoSink, t, pic);
}

// ----------------------------------------------------------------------------
int vp9ReadHeader(bitread_t* bs, vp9_framehead_t* head)
{
    const uint16_t VP9_FRAME_MARKER = 2;
    if (bitreadGetBits2(bs) != VP9_FRAME_MARKER) {
        return -1;
    }

    if (vp9ReadProfile(bs) != vp9_profile_0) {
        return -1;
    }

    for (unsigned i = 0; i < 3; ++i) {
        head->frame_refs[i] = -1;
    }

    uint8_t showExistingFrame = bitreadGetBit(bs);
    if (showExistingFrame) {
        head->type = vp9_existing_frame;
        head->showFrame = 1;
        head->frameToShow = bitreadGetBits3(bs);
        head->refresh_frame_flags = 0;
        return 0;
    }

    head->type = (vp9_frametype_t)bitreadGetBit(bs);
    head->showFrame = bitreadGetBit(bs);
    const int errorResilientMode = bitreadGetBit(bs);

    if (head->type == vp9_coded_key_frame) {
        if (vp9ReadSyncCode(bs) != 0) {
            return -1;
        }

        head->refresh_frame_flags = (1 << 8) - 1;
        return 0;
    } else {
    
        const uint8_t intraOnly = head->showFrame ? 0 : bitreadGetBit(bs);
        
        if (!errorResilientMode) {
            bitreadFlushBits(bs, 2);
        }

        if (intraOnly) {
            if (vp9ReadSyncCode(bs) != 0) {
                return -1;
            }
            head->refresh_frame_flags = bitreadGetBits8(bs);
        } else {
            head->refresh_frame_flags = bitreadGetBits8(bs);
            for (unsigned i = 0; i < 3; ++i) {
                head->frame_refs[i] = bitreadGetBits3(bs);
                bitreadFlushBits(bs, 1); // ref_frame_sign_bias
            }        
        }
    }
    return 0;
}

// ----------------------------------------------------------------------------
unsigned vp9ReadProfile(bitread_t* bs)
{
    unsigned profile = bitreadGetBits2(bs);
    if (profile > 2)
        profile += bitreadGetBit(bs);
    return profile;
}

// ----------------------------------------------------------------------------
int vp9ReadSyncCode(bitread_t* bs)
{
    const uint16_t VP9_SYNC_CODE_0 = 0x49;
    const uint16_t VP9_SYNC_CODE_1 = 0x83;
    const uint16_t VP9_SYNC_CODE_2 = 0x42;

    if (bitreadGetBits8(bs) != VP9_SYNC_CODE_0 ||
        bitreadGetBits8(bs) != VP9_SYNC_CODE_1 ||
        bitreadGetBits8(bs) != VP9_SYNC_CODE_2) {
        return -1;
    }
    return 0;
}
