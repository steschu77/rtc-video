/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */
#define _CRT_SECURE_NO_WARNINGS

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "cutil_nullptr.h"
#include "cutil_refobj.h"

#include "rtc-api/image_picture.h"

// ----------------------------------------------------------------------------
const image_t* gfxCreateGradientImage(unsigned idx, const image_size_t* size, fourcc_t fmt)
{
    const unsigned cx = size->cx;
    const unsigned cy = size->cy;

    const plane_scale_t shift = gfxPlaneShift(fmt);
    const plane_scale_t scale = gfxPlaneScale(fmt);

    const stride_t stride = gfxCalcFormatStride(fmt, cx, 16u);

    const float PI = 3.141f;
    const unsigned a = (unsigned)(0x15000 * sinf(2 * PI / 256 * idx));
    const unsigned b = (unsigned)(0x15000 * cosf(2 * PI / 256 * idx));
    const unsigned c = (0x10000 * 0x80);

    //printf("gen:    a0=%2.2f, a1=%2.2f, a2=%d, idx=%d\n", (double)a / 0x10000, (double)b / 0x10000, (int)c / 0x10000, (int)idx);

    const buffer_t* buf_yuv[4] = { 0 };

    for (int i = 0; i < 4; ++i) {

        const unsigned plane_cx = (scale.p[i] * cx) >> shift.p[i];
        const unsigned plane_cy = (scale.p[i] * cy) >> shift.p[i];

        const unsigned mid_x = plane_cx / 2;
        const unsigned mid_y = plane_cy / 2;

        const size_t buf_size = plane_cy * stride.p[i];

        void* data = nullptr;
        buf_yuv[i] = allocBuffer(buf_size, &data, 16u);

        if (i == 0) {
            uint8_t* line = (uint8_t*)data;
            for (unsigned y = 0; y < plane_cy; ++y) {
                for (unsigned x = 0; x < plane_cx; ++x) {
                    line[x] = (a * (x - mid_x) + b * (y - mid_y) + c + 0x8000) >> 16;
                }
                line += stride.p[i];
            }
        } else {
            uint8_t* line = (uint8_t*)data;
            for (unsigned y = 0; y < plane_cy; ++y) {
                for (unsigned x = 0; x < plane_cx; ++x) {
                    line[x] = 32 + i * 32 + ((a * (x - mid_x) + b * (y - mid_y)) >> 16);
                }
                line += stride.p[i];
            }
        }
    }

    const image_t* img = gfxCreateImage(&stride, size, fmt, buf_yuv);
    for (int i = 0; i < 4; ++i) {
        freeBuffer(buf_yuv[i]);
    }

    return gfxAssignImage(img);
}
