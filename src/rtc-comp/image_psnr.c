/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */
#define _CRT_SECURE_NO_WARNINGS

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <assert.h>

#include "cutil_nullptr.h"
#include "cutil_buffer.h"

#include "rtc-api/image_picture.h"

// ----------------------------------------------------------------------------
static uint64_t line_ssd(const uint8_t* p0, const uint8_t* p1, const unsigned cx)
{
    uint64_t sumSquaredDiff = 0;

    for (unsigned i = 0; i < cx; i++) {
        const int diff = p0[i] - p1[i];
        const int squaredDiff = diff * diff;
        sumSquaredDiff += squaredDiff;
    }

    return sumSquaredDiff;
}

// ----------------------------------------------------------------------------
static uint64_t line_sad(const uint8_t* p0, const uint8_t* p1, const unsigned cx)
{
    uint64_t sumAbsDiff = 0;

    for (unsigned i = 0; i < cx; i++) {
        const int diff = p0[i] - p1[i];
        const int absDiff = abs(diff);
        sumAbsDiff += absDiff > 8 ? 1 : 0;
    }

    return sumAbsDiff;
}

// ----------------------------------------------------------------------------
double gfxImagePSNR(const image_t* img0, const image_t* img1)
{
    const stride_t* stride0 = gfxImageStride(img0);
    const stride_t* stride1 = gfxImageStride(img1);

    const unsigned cx = gfxImageWidth(img0);
    const unsigned cy = gfxImageHeight(img0);

    assert(cx == gfxImageWidth(img1));
    assert(cy == gfxImageHeight(img1));

    const uint8_t* py0 = getBufferData(gfxImageData(img0, 0));
    const uint8_t* py1 = getBufferData(gfxImageData(img1, 0));

    const uint64_t picSize = (uint64_t)cx * cy;
    uint64_t sumSquaredDiff = 0;
    for (unsigned i = 0; i < cy; i++) {
        sumSquaredDiff += line_ssd(py0, py1, cx);
        py0 += stride0->p[0];
        py1 += stride1->p[0];
    }

    if (sumSquaredDiff == 0) {
        return 100.0;
    }

    const double relDiff = (double)picSize * 255 * 255 / sumSquaredDiff;
    const double logDiff = 10.0 * log10(relDiff);
    return logDiff < 100.0 ? logDiff : 100.0;
}

// ----------------------------------------------------------------------------
double gfxImageDynamics(const image_t* img0, const image_t* img1)
{
    const stride_t* stride0 = gfxImageStride(img0);
    const stride_t* stride1 = gfxImageStride(img1);

    const unsigned cx = gfxImageWidth(img0);
    const unsigned cy = gfxImageHeight(img0);

    assert(cx == gfxImageWidth(img1));
    assert(cy == gfxImageHeight(img1));

    const uint8_t* py0 = getBufferData(gfxImageData(img0, 0));
    const uint8_t* py1 = getBufferData(gfxImageData(img1, 0));

    const uint64_t picSize = (uint64_t)cx * cy;
    uint64_t sumAbsDiff = 0;
    for (unsigned i = 0; i < cy; i++) {
        sumAbsDiff += line_sad(py0, py1, cx);
        py0 += stride0->p[0];
        py1 += stride1->p[0];
    }

    if (sumAbsDiff == 0) {
        return 100.0;
    }

    const double relDiff = (double)picSize / sumAbsDiff;
    const double logDiff = 10.0 * log10(relDiff);
    return logDiff < 100.0 ? logDiff : 100.0;
}
