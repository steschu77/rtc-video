/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */

#include <malloc.h>
#include <string.h>
#include <assert.h>

#include "cutil_nullptr.h"
#include "cutil_refobj.h"

#include "rtc-comp/codec_vp9.h"

#include "../rtc-api/image_sink_impl.h"
#include "../rtc-api/video_source_impl.h"

// ----------------------------------------------------------------------------
#include "vpx/vpx_codec.h"
#include "vpx/vpx_encoder.h"
#include "vpx/vp8cx.h"

#include "vp9/common/vp9_onyxc_int.h"
#include "vp9/encoder/vp9_encoder.h"
#include "vp9/vp9_iface_common.h"

// ----------------------------------------------------------------------------
struct vp9enc_s {
    ref_t ref;
    image_sink_t imageSinkObj;
    video_source_t videoSourceObj;

    video_sink_t* videoSink;
    image_source_t* imageSource;

    vp9enc_config_t cfg;
    image_size_t size;

    pic_id_t picid;
    unsigned need_syncframe[e_syncmode_count];
    unsigned packetCount;
    unsigned frameCount;

    VP9EncoderConfig oxcf;
    VP9_COMP* cpi;
    BufferPool* vpx_buffer_pool;
    size_t vpx_data_size;
    unsigned char* vpx_data;

    size_t rc_buffer_fill;
    bitrate_t rc_bitrate;
    unsigned rc_framerate;

    pic_id_t picid_kf;
};

// ----------------------------------------------------------------------------
void vp9encDeliverPic(void* ptr, media_clock_t t, const image_t* img);
void vp9encConnect(void* ptr, video_sink_t* sink);
void vp9encChangeCaps(void* ptr, const video_caps_t* caps);

// ----------------------------------------------------------------------------
static void vp9encSetTargetBitrate(vp9enc_t* obj, bitrate_t rate);

// ----------------------------------------------------------------------------
static void vp9encDeliverPacket(vp9enc_t* obj, media_clock_t ts, pic_id_t picid,
    const void* cx_data, size_t cx_sz, uint64_t psnr, ref_type_t refType)
{
    if (refType == ref_keyframe) {
        obj->need_syncframe[e_keyframe] = 0;
        obj->need_syncframe[e_syncframe] = 0;
    }

    if (refType == ref_syncframe) {
        obj->need_syncframe[e_syncframe] = 0;
    }

    void* data = nullptr;
    const buffer_t* buf = allocBuffer(cx_sz, &data, 16u);

    if (buf != nullptr) {
        memcpy(data, cx_data, cx_sz);

        const video_pic_t* pic = createVideoPicture(picid, ts, buf, refType, psnr);

        videosinkDeliverPic(obj->videoSink, ts, pic);
        videopicRelease(pic);

        freeBuffer(buf);
    }
}

// ----------------------------------------------------------------------------
void convertImgToYV12(const image_t* img, YV12_BUFFER_CONFIG* yv12)
{
    const unsigned w = gfxImageWidth(img);
    const unsigned h = gfxImageHeight(img);
    const stride_t* stride = gfxImageStride(img);

    yv12->y_buffer = (unsigned char*)getBufferData(gfxImageData(img, 0));
    yv12->u_buffer = (unsigned char*)getBufferData(gfxImageData(img, 1));
    yv12->v_buffer = (unsigned char*)getBufferData(gfxImageData(img, 2));

    yv12->y_crop_width = w;
    yv12->y_crop_height = h;
    yv12->render_width = 0;
    yv12->render_height = 0;
    yv12->y_width = w;
    yv12->y_height = h;

    yv12->uv_width = (1 + yv12->y_width) / 2;
    yv12->uv_height = (1 + yv12->y_height) / 2;
    yv12->uv_crop_width = yv12->uv_width;
    yv12->uv_crop_height = yv12->uv_height;

    yv12->y_stride = (int)stride->p[0];
    yv12->uv_stride = (int)stride->p[1];
    yv12->color_space = VPX_CS_UNKNOWN;
    yv12->color_range = VPX_CR_STUDIO_RANGE;

    yv12->border = (int)((stride->p[0] - w) / 2);
    yv12->subsampling_x = 1;
    yv12->subsampling_y = 1;
}

// ----------------------------------------------------------------------------
static int vp9encRcUpdateBufferPre(vp9enc_t* obj)
{
    const size_t bits_per_frame = obj->rc_bitrate / obj->rc_framerate;

    if (obj->rc_buffer_fill < bits_per_frame) {
        obj->rc_buffer_fill = 0;
    } else {
        obj->rc_buffer_fill -= bits_per_frame;
    }
    return 0;
}

// ----------------------------------------------------------------------------
static int vp9encRcUpdateBufferPost(vp9enc_t* obj, size_t size)
{
    obj->rc_buffer_fill += 8 * size;
    return 0;
}

// ----------------------------------------------------------------------------
static size_t vp9encRcTargetFrameSize(vp9enc_t* obj, int isIntra)
{
    const unsigned intra_target = 5u; // number of frames the intra frame can be bigger
    const unsigned delay_target = 8u; // number of frames it takes to reduce accumulated delay
    const size_t bits_per_frame = obj->rc_bitrate / obj->rc_framerate;
    const size_t target_delay = isIntra ? intra_target : 1u;
    const size_t target_size = target_delay * bits_per_frame;
    const size_t delay_reduce = obj->rc_buffer_fill / delay_target;
    if (target_size <= delay_reduce) {
        return 0;
    }

    return (target_size - delay_reduce + 7) / 8;
}

// ----------------------------------------------------------------------------
int vp9encInitialize(vp9enc_t* obj, const image_size_t* size)
{
    if (!valid_ref_frame_size(obj->size.cx, obj->size.cy, size->cx, size->cy) ||
      (obj->cpi->initial_width && (int)size->cx > obj->cpi->initial_width) ||
      (obj->cpi->initial_height && (int)size->cy > obj->cpi->initial_height))
        obj->need_syncframe[e_keyframe] = 1;

    obj->size.cx = size->cx;
    obj->size.cy = size->cy;

    obj->oxcf.width = size->cx;
    obj->oxcf.height = size->cy;

    if (obj->cpi != nullptr) {
        vp9_change_config(obj->cpi, &obj->oxcf);
    } else {
        obj->cpi = vp9_create_compressor(&obj->oxcf, obj->vpx_buffer_pool);
    }

    return 0;
}

// ----------------------------------------------------------------------------
static int vp9gopFlagsSyncFrame()
{
    return VP8_EFLAG_NO_QPOPT | VP8_EFLAG_NO_REF_LAST | VP8_EFLAG_NO_UPD_ARF;
}

// ----------------------------------------------------------------------------
static int vp9gopFlagsIP()
{
    return VP8_EFLAG_NO_UPD_GF | VP8_EFLAG_NO_UPD_ARF;
}

// ----------------------------------------------------------------------------
static int vp9gopFlagsIpP(unsigned frameCount)
{
    if (frameCount % 2 != 0) {
        return VP8_EFLAG_NO_UPD_LAST | VP8_EFLAG_NO_UPD_GF | VP8_EFLAG_NO_UPD_ARF;
    } else {
        return VP8_EFLAG_NO_QPOPT | VP8_EFLAG_NO_UPD_GF | VP8_EFLAG_NO_UPD_ARF;
    }
}

// ----------------------------------------------------------------------------
static int vp9gopFlagsIppP(unsigned frameCount)
{
    switch (frameCount % 3) {
    default:
    case 0:
        return VP8_EFLAG_NO_QPOPT | VP8_EFLAG_NO_REF_LAST | VP8_EFLAG_NO_UPD_ARF;
    case 1:
        return VP8_EFLAG_NO_UPD_GF | VP8_EFLAG_NO_UPD_ARF;
    case 2:
        return VP8_EFLAG_NO_UPD_LAST | VP8_EFLAG_NO_UPD_GF | VP8_EFLAG_NO_UPD_ARF;
    }
}
// ----------------------------------------------------------------------------
static int vp9gopFlags(vp9enc_gop_t gopType, unsigned frameCount)
{
    switch (gopType) {
    default:
        return vp9gopFlagsIP();
    case e_IpP:
        return vp9gopFlagsIpP(frameCount);
    case e_IppP:
        return vp9gopFlagsIppP(frameCount);
    }
}

// ----------------------------------------------------------------------------
static void vp9UpdateRefPics(vp9enc_t* obj, unsigned flags, pic_id_t picid, ref_type_t ref)
{
    if ((flags & VPX_EFLAG_FORCE_KF) != 0) {
        obj->picid_kf = picid;
    }
}

// ----------------------------------------------------------------------------
static void vp9encEncode(vp9enc_t* obj, media_clock_t ts, const image_t* img, int frameCount)
{
    const image_size_t* size = gfxImageSize(img);
    if (size->cx != obj->size.cx || size->cy != obj->size.cy) {
        vp9encInitialize(obj, size);
    }

    int flags = 0;
    if (obj->need_syncframe[e_keyframe]) {
        flags = VPX_EFLAG_FORCE_KF;
    } else if (obj->need_syncframe[e_syncframe]) {
        flags = vp9gopFlagsSyncFrame();
    } else {
        flags = vp9gopFlags(obj->cfg.gopType, frameCount);
    }

    vp9_apply_encoding_flags(obj->cpi, 0, 1, 2, flags);

    YV12_BUFFER_CONFIG sd = { 0 };
    convertImgToYV12(img, &sd);

    vp9encRcUpdateBufferPre(obj);
    const size_t targetSize = vp9encRcTargetFrameSize(obj, (flags & VPX_EFLAG_FORCE_KF) != 0);

    size_t cx_data_size = 0;
    unsigned int lib_flags = 0;
    unsigned char* cx_data = obj->vpx_data;
    PSNR_STATS stats;

    if (-1 != vp9_get_compressed_data(obj->cpi, &sd, flags, targetSize, &lib_flags, &cx_data_size, cx_data, &stats)) {
        static const ref_type_t refTypes[4] = { ref_refframe, ref_syncframe, ref_keyframe, ref_keyframe };
        const unsigned is_key = obj->need_syncframe[e_keyframe];
        const unsigned is_sync = obj->need_syncframe[e_syncframe];
        const ref_type_t ref = refTypes[is_key * 2 + is_sync];
        const pic_id_t picid = obj->picid++;
        const uint64_t psnr = (uint64_t)(256.0 * vpx_sse_to_psnr((double)stats.samples[0], 255.0, (double)stats.sse[0]));
        vp9encDeliverPacket(obj, ts, picid, cx_data, cx_data_size, psnr, ref);

        vp9UpdateRefPics(obj, flags, picid, ref);
    }

    vp9encRcUpdateBufferPost(obj, cx_data_size);
}

// Video Source interface
// ----------------------------------------------------------------------------
void vp9encConnect(void* ptr, video_sink_t* sink)
{
    vp9enc_t* obj = (vp9enc_t*)ptr;
    videosinkAssign(sink, &obj->videoSink);

    imgsrcConnect(obj->imageSource, sink != nullptr ? &obj->imageSinkObj : nullptr);

    if (obj->cpi != nullptr) {
        vp9_remove_compressor(obj->cpi);
        obj->cpi = nullptr; 
        obj->size.cx = 0;
        obj->size.cy = 0;
    }
}

// ----------------------------------------------------------------------------
void vp9encChangeCaps(void* ptr, const video_caps_t* caps)
{
    vp9enc_t* obj = (vp9enc_t*)ptr;

    // hack
    const unsigned cx = caps->imgcaps.preferred_res.cx;
    const unsigned cy = caps->imgcaps.preferred_res.cy;
    const procrate_t max_procrate = caps->imgcaps.max_procrate;
    const frame_rate_t max_framerate = caps->imgcaps.preferred_rate;

    const image_size_t resolution = { cx, cy };
    const fourcc_t codec = make_fourcc('V', 'P', '9', '0');
    const video_format_t* fmt = createVideoFormat(&max_framerate, &resolution, codec, max_procrate);
    videosinkStartSeq(obj->videoSink, 0, 0, fmt);
    freeVideoFormat(fmt);

    imgsrcChangeCaps(obj->imageSource, &caps->imgcaps);
    vp9encSetTargetBitrate(obj, caps->max_bitrate);
}

// ----------------------------------------------------------------------------
void vp9encRequestSyncPoint(void* ptr)
{
    vp9enc_t* obj = (vp9enc_t*)ptr;
    obj->need_syncframe[obj->cfg.syncmode] = 1;
}

// ----------------------------------------------------------------------------
void vp9encNotifyPictureLost(void* ptr, const media_id_t id)
{
    vp9enc_t* obj = (vp9enc_t*)ptr;
    if (id == obj->picid_kf) {
        // The key-frame was reported lost. Create a new one.
        obj->need_syncframe[e_keyframe] = 1;
    } else {
        // Create a sync-frame for lost non-key-frames.
        obj->need_syncframe[obj->cfg.syncmode] = 1;
    }
}

// ----------------------------------------------------------------------------
void vp9encNotifyPictureProcessed(void* ptr, const media_id_t id)
{
}

// ----------------------------------------------------------------------------
static void vp9encSetTargetBitrate(vp9enc_t* obj, bitrate_t rate)
{
    obj->rc_bitrate = rate;
}

// Video Sink interface
// ----------------------------------------------------------------------------
void vp9encDeliverPic(void* ptr, media_clock_t t, const image_t* img)
{
    vp9enc_t* obj = (vp9enc_t*)ptr;

    if (img != nullptr) {
        vp9encEncode(obj, t, img, obj->frameCount);
        obj->frameCount++;
    } else {
        videosinkDeliverPic(obj->videoSink, t, nullptr);
    }
}

// ----------------------------------------------------------------------------
void vp9encAssign(void* ptr)
{
    const vp9enc_t* obj = (const vp9enc_t*)ptr;
    ref_inc(&obj->ref);
}

// ----------------------------------------------------------------------------
void vp9encRelease(void* ptr)
{
    const vp9enc_t* obj = (const vp9enc_t*)ptr;
    ref_dec(&obj->ref);
}

// ----------------------------------------------------------------------------
void vp9encDestroy(const void* ptr)
{
    const vp9enc_t* obj = (const vp9enc_t*)ptr;
    if (obj != nullptr) {
        assert(obj->videoSink == nullptr);
        imgsrcRelease(obj->imageSource);
        free(obj->vpx_data);
        free(obj->vpx_buffer_pool);
        free((void*)obj);
    }
}

// ----------------------------------------------------------------------------
vp9enc_t* vp9encCreate(image_source_t* imageSource, const vp9enc_config_t* cfg)
{
    vp9enc_t* obj = (vp9enc_t*)malloc(sizeof(vp9enc_t));
    if (obj == nullptr) {
        return nullptr;
    }

    memset(obj, 0, sizeof(vp9enc_t));

    ref_init(&obj->ref, obj, vp9encDestroy);

    imgsinkInit(
        &obj->imageSinkObj,
        obj,
        vp9encAssign,
        vp9encRelease,
        vp9encDeliverPic);

    videosrcInit(
        &obj->videoSourceObj,
        obj,
        vp9encAssign,
        vp9encRelease,
        vp9encConnect,
        vp9encChangeCaps,
        vp9encRequestSyncPoint,
        vp9encNotifyPictureLost,
        vp9encNotifyPictureProcessed);

    obj->imageSource = imgsrcCopy(imageSource);    
    obj->videoSink = nullptr;

    obj->cfg = *cfg;

    obj->picid = 0;
    obj->need_syncframe[e_keyframe] = 1;
    obj->need_syncframe[e_syncframe] = 0;
    obj->packetCount = 0;
    obj->frameCount = 0;

    obj->rc_bitrate = 500000;
    obj->rc_framerate = 30;
    obj->rc_buffer_fill = 0;

    obj->vpx_buffer_pool = (BufferPool*)malloc(sizeof(BufferPool));
    if (obj->vpx_buffer_pool == nullptr) {
        vp9encDestroy(obj);
        return nullptr;
    }

    memset(obj->vpx_buffer_pool, 0, sizeof(BufferPool));

    obj->vpx_data_size = 1024 * 1024;
    obj->vpx_data = (unsigned char*)malloc(obj->vpx_data_size);
    if (obj->vpx_data == nullptr) {
        vp9encDestroy(obj);
        return nullptr;
    }

    vp9_initialize_enc();
    return obj;
}

// ----------------------------------------------------------------------------
video_source_t* vp9encQueryVideoSource(vp9enc_t* obj)
{
    return videosrcCopy(&obj->videoSourceObj);
}
