/*
 * MIT License
 *
 * Copyright (c) 2021 Steffen Schulze
 */

#pragma once

#include <stdint.h>

#include "cutil_refobj.h"

#include "rtc-proto/frame.h"

// ----------------------------------------------------------------------------
struct frame_s {
    ref_t ref;
    frame_type_t type;
};
