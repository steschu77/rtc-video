/*
 * MIT License
 *
 * Copyright (c) 2021 Steffen Schulze
 */

#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "cutil_nullptr.h"
#include "cutil_refobj.h"

#include "frame_impl.h"

// ----------------------------------------------------------------------------
const frame_t* frameAssign(const frame_t* obj)
{
    ref_inc(&obj->ref);
    return obj;
}

// ----------------------------------------------------------------------------
frame_t* frameRelease(const frame_t* obj)
{
    ref_dec(&obj->ref);
    return nullptr;
}

// ----------------------------------------------------------------------------
frame_type_t frameGetType(const frame_t* obj)
{
    return obj->type;
}
