/*
 * MIT License
 *
 * Copyright (c) 2021 Steffen Schulze
 */

#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "cutil_nullptr.h"
#include "cutil_refobj.h"
#include "cutil_vecptr.h"

#include "rtc-proto/packet_event.h"

// ----------------------------------------------------------------------------
struct packet_event_s {
    ref_t ref;

    packet_event_type_t type;
    media_clock_t t;
    uint64_t seqCount;

    direction_t packetDir;
    uint64_t packetSeqno;
};

// ----------------------------------------------------------------------------
int packeteventCompareSeqNo(const void* x0, const void* x1)
{
    const packet_event_t* e0 = (const packet_event_t*)x0;
    const packet_event_t* e1 = (const packet_event_t*)x1;

    return (int)(e0->packetSeqno - e1->packetSeqno);
}

// ----------------------------------------------------------------------------
int packeteventCompareTime(const void* x0, const void* x1)
{
    const packet_event_t* e0 = (const packet_event_t*)x0;
    const packet_event_t* e1 = (const packet_event_t*)x1;

    if (e0->t != e1->t) {
        return (int)(e0->t - e1->t);
    } else {
        return (int)(e0->seqCount - e1->seqCount);
    }
}

// ----------------------------------------------------------------------------
int packeteventPredicateSeqNo(const void* context, const void* item)
{
    const uint64_t* seqno = (const uint64_t*)context;
    const packet_event_t* event = (const packet_event_t*)item;
    return (int)(*seqno - event->packetSeqno);
}

// ----------------------------------------------------------------------------
static void objDestroy(const void* ptr)
{
    const packet_event_t* obj = (const packet_event_t*)ptr;
    if (obj != nullptr) {
        free((void*)obj);
    }
}

// ----------------------------------------------------------------------------
void packeteventRelease(const void* ptr)
{
    const packet_event_t* obj = (const packet_event_t*)ptr;
    ref_dec(&obj->ref);
}

// ----------------------------------------------------------------------------
packet_event_t* packeteventAssign(packet_event_t* obj)
{
    ref_inc(&obj->ref);
    return obj;
}

// ----------------------------------------------------------------------------
const packet_event_t* packeteventConstAssign(const packet_event_t* obj)
{
    ref_inc(&obj->ref);
    return obj;
}

// ----------------------------------------------------------------------------
packet_event_t* packeteventCreate(direction_t dir, uint64_t seqno, packet_event_type_t type, media_clock_t t)
{
    packet_event_t* obj = (packet_event_t*)malloc(sizeof(packet_event_t));
    if (obj == nullptr) {
        return nullptr;
    }

    ref_init(&obj->ref, obj, objDestroy);

    obj->type = type;
    obj->t = t;

    static uint64_t seqCounter = 0;
    obj->seqCount = seqCounter++; // since sorting by t is required and t is not unique

    obj->packetDir = dir;
    obj->packetSeqno = seqno;

    return packeteventAssign(obj);
}

// ----------------------------------------------------------------------------
packet_event_type_t packeteventGetType(const packet_event_t* obj)
{
    return obj->type;
}

// ----------------------------------------------------------------------------
media_clock_t packeteventGetTime(const packet_event_t* obj)
{
    return obj->t;
}

// ----------------------------------------------------------------------------
direction_t packeteventGetDirection(const packet_event_t* obj)
{
    return obj->packetDir;
}

// ----------------------------------------------------------------------------
uint64_t packeteventGetSeqNo(const packet_event_t* obj)
{
    return obj->packetSeqno;
}
