/*
 * MIT License
 *
 * Copyright (c) 2021 Steffen Schulze
 */

#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "cutil_nullptr.h"
#include "cutil_refobj.h"

#include "rtc-proto/caps_frame.h"
#include "frame_impl.h"

// ----------------------------------------------------------------------------
struct caps_frame_s {
    frame_t frame;
    uint64_t seqid;
    video_caps_t caps;
};

// ----------------------------------------------------------------------------
static void objDestroy(const void* ptr)
{
    const caps_frame_t* obj = (const caps_frame_t*)(ptr);
    free((void*)obj);
}

// ----------------------------------------------------------------------------
caps_frame_t* capsframeCreate(uint64_t seqid, const video_caps_t* caps)
{
    caps_frame_t* obj = (caps_frame_t*)malloc(sizeof(caps_frame_t));
    if (obj == nullptr) {
        return nullptr;
    }

    ref_init(&obj->frame.ref, obj, objDestroy);

    obj->frame.type = ft_caps_frame;
    obj->seqid = seqid;
    obj->caps = *caps;

    return obj;
}

// ----------------------------------------------------------------------------
const caps_frame_t* capsframeConstAssign(const caps_frame_t* obj)
{
    ref_inc(&obj->frame.ref);
    return obj;
}

// ----------------------------------------------------------------------------
caps_frame_t* capsframeAssign(caps_frame_t* obj)
{
    ref_inc(&obj->frame.ref);
    return obj;
}

// ----------------------------------------------------------------------------
caps_frame_t* capsframeRelease(const caps_frame_t* obj)
{
    if (obj != nullptr) {
        ref_dec(&obj->frame.ref);
    }
    return nullptr;
}

// ----------------------------------------------------------------------------
const frame_t* capsframeGetFrame(const caps_frame_t* obj)
{
    return &obj->frame;
}

// ----------------------------------------------------------------------------
size_t capsframeGetLength(const caps_frame_t* obj)
{
    return sizeof(video_caps_t);
}

// ----------------------------------------------------------------------------
uint64_t capsframeGetSeqid(const caps_frame_t* obj)
{
    return obj->seqid;
}

// ----------------------------------------------------------------------------
const video_caps_t* capsframeGetCaps(const caps_frame_t* obj)
{
    return &obj->caps;
}
