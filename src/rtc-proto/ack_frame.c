/*
 * MIT License
 *
 * Copyright (c) 2021 Steffen Schulze
 */

#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "cutil_nullptr.h"
#include "cutil_refobj.h"

#include "rtc-proto/ack_frame.h"
#include "frame_impl.h"

// ----------------------------------------------------------------------------
struct ack_frame_s {
    frame_t frame;
    const vecu64_t* acks;
};

// ----------------------------------------------------------------------------
static void objDestroy(const void* ptr)
{
    const ack_frame_t* obj = (const ack_frame_t*)(ptr);
    vecu64Destroy(obj->acks);
    free((void*)obj);
}

// ----------------------------------------------------------------------------
ack_frame_t* ackframeCreate(const vecu64_t* acks)
{
    ack_frame_t* obj = (ack_frame_t*)malloc(sizeof(ack_frame_t));
    if (obj == nullptr) {
        return nullptr;
    }

    ref_init(&obj->frame.ref, obj, objDestroy);

    obj->frame.type = ft_ack_frame;
    obj->acks = acks;

    return obj;
}

// ----------------------------------------------------------------------------
ack_frame_t* ackframeAssign(ack_frame_t* obj)
{
    ref_inc(&obj->frame.ref);
    return obj;
}

// ----------------------------------------------------------------------------
ack_frame_t* ackframeRelease(const ack_frame_t* obj)
{
    ref_dec(&obj->frame.ref);
    return nullptr;
}

// ----------------------------------------------------------------------------
const frame_t* ackframeGetFrame(const ack_frame_t* obj)
{
    return &obj->frame;
}

// ----------------------------------------------------------------------------
size_t ackframeGetLength(const ack_frame_t* obj)
{
    return vecu64ItemCount(obj->acks);
}

// ----------------------------------------------------------------------------
const vecu64_t* ackframeGetAcks(const ack_frame_t* obj)
{
    return obj->acks;
}