/*
 * MIT License
 *
 * Copyright (c) 2020 Steffen Schulze
 */

#include <malloc.h>
#include <string.h>
#include <stdio.h>

#include "cutil_nullptr.h"

#include "rtc-proto/packet.h"

#include "rtc-proto/ack_frame.h"
#include "rtc-proto/caps_frame.h"
#include "rtc-proto/video_frame.h"

#include "rtc-proto/packet_log.h"

// ----------------------------------------------------------------------------
enum chunk_type_e {
    chunk_packet_event, // carries a packet_event_t
    chunk_packet_data,  // carries a packet_t
};

typedef enum chunk_type_e chunk_type_t;

// ----------------------------------------------------------------------------
int ackframeParse(byteread_t* bs, const frame_t** frame, size_t* len)
{
    int rv = 0;

    uint64_t numAcks = 0;
    rv |= byteread_vint(bs, &numAcks);

    if (numAcks >= 0x100000000 || rv != 0) {
        return nullptr;
    }

    vecu64_t* acks = vecu64Create((unsigned)numAcks, 10);

    for (int i = 0; rv == 0 && i < numAcks; ++i) {
        uint64_t ack = 0;
        rv |= byteread_vint(bs, &ack);
        vecu64AddItem(acks, ack);
    }

    const ack_frame_t* ackframe = ackframeCreate(acks);
    *frame = ackframeGetFrame(ackframe);
    *len = ackframeGetLength(ackframe);

    return 0;
}

// ----------------------------------------------------------------------------
int capsframeParse(byteread_t* bs, const frame_t** frame, size_t* len)
{
    int rv = 0;

    uint64_t seqno;
    video_caps_t caps;
    uint8_t mute;
    rv |= byteread_vint(bs, &seqno);
    rv |= byteread_int8(bs, &mute);

    if (rv == 0 && mute != muted) {
        rv |= byteread_int32(bs, &caps.imgcaps.preferred_res.cx);
        rv |= byteread_int32(bs, &caps.imgcaps.preferred_res.cy);
        rv |= byteread_int32(bs, &caps.imgcaps.preferred_rate.num);
        rv |= byteread_int32(bs, &caps.imgcaps.preferred_rate.den);
        rv |= byteread_vint(bs, &caps.imgcaps.max_procrate);
        rv |= byteread_vint(bs, &caps.max_bitrate);
    }

    if (rv != 0) {
        return rv;
    }

    const caps_frame_t* capsframe = capsframeCreate(seqno, &caps);
    *frame = capsframeGetFrame(capsframe);
    *len = capsframeGetLength(capsframe);
    return 0;
}

// ----------------------------------------------------------------------------
int videoframeParse(byteread_t* bs, const frame_t** frame, size_t* len)
{
    int rv = 0;

    uint64_t length, seqno, picid, flags, type, ts, psnr;
    rv |= byteread_vint(bs, &seqno);
    rv |= byteread_vint(bs, &picid);
    rv |= byteread_vint(bs, &flags);
    rv |= byteread_vint(bs, &type);
    rv |= byteread_vint(bs, &ts);
    rv |= byteread_vint(bs, &length);
    rv |= byteread_vint(bs, &psnr);

    if (rv != 0) {
        return nullptr;
    }

    void* data = nullptr;
    const buffer_t* buf = allocBuffer(length, &data, 16);

    const video_frame_t* videoFrame = videoframeCreate(seqno, picid, flags, ts, buf, type, psnr);
    *frame = videoframeGetFrame(videoFrame);
    *len = videoframeGetLength(videoFrame);

    freeBuffer(buf);
    return 0;
}

// ----------------------------------------------------------------------------
int parsePacketFrame(byteread_t* bs, const frame_t** frame, size_t* len)
{
    int rv = 0;

    uint64_t frameType = 0;
    rv |= byteread_vint(bs, &frameType);

    if (rv != 0) {
        return rv;
    }

    switch (frameType) {
    case ft_ack_frame:
        return ackframeParse(bs, frame, len);

    case ft_caps_frame:
        return capsframeParse(bs, frame, len);

    case ft_video_frame:
        return videoframeParse(bs, frame, len);

    default:
        return -1;
    }
}

// ----------------------------------------------------------------------------
packet_t* packetParse(byteread_t* packetReader)
{
    int rv = 0;

    uint64_t dir, seqno;
    rv |= byteread_vint(packetReader, &dir);
    rv |= byteread_vint(packetReader, &seqno);

    if (rv != 0) {
        return nullptr;
    }

    packet_t* packet = packetCreate(dir, seqno);

    uint64_t len = 0;
    while (rv == 0 && byteread_vint(packetReader, &len) == 0) {

        byteread_t* frameReader = bytereadCreate(byteread_ptr(packetReader), len);
        byteread_skip(packetReader, len);

        const frame_t* frame;
        size_t frameLength;
        parsePacketFrame(frameReader, &frame, &frameLength);

        bytereadDestroy(frameReader);

        if (frame == nullptr) {
            break;
        }

        packetAddFrame(packet, frame, frameLength);
    }

    return packet;
}

// ----------------------------------------------------------------------------
const packet_event_t* packeteventParse(byteread_t* chunkReader)
{
    int rv = 0;

    uint64_t dir, type, seqno, t;
    rv |= byteread_vint(chunkReader, &dir);
    rv |= byteread_vint(chunkReader, &seqno);
    rv |= byteread_vint(chunkReader, &type);
    rv |= byteread_vint(chunkReader, &t);

    if (rv != 0) {
        return nullptr;
    }

    const packet_event_t* obj = packeteventCreate(dir, seqno, type, t);

    return obj;
}


// __ Packet Frame Enum _______________________________________________________
// ----------------------------------------------------------------------------
int ackframeWrite(void* ptr, const packet_t* packet, const ack_frame_t* frame)
{
    uint8_t frameData[4096];
    bytewrite_t* frameWriter = bytewriteCreate(frameData, 4096);

    bytewrite_vint(frameWriter, (uint64_t)ft_ack_frame);

    const vecu64_t* acks = ackframeGetAcks(frame);
    const unsigned numAcks = vecu64ItemCount(acks);

    bytewrite_vint(frameWriter, (uint64_t)numAcks);

    for (unsigned i = 0; i < numAcks; ++i) {
        bytewrite_vint(frameWriter, vecu64GetItem(acks, i));
    }

    const size_t frameLength = bytewrite_length(frameWriter);
    bytewriteDestroy(frameWriter);

    bytewrite_t* chunkWriter = (bytewrite_t*)ptr;
    bytewrite_vint(chunkWriter, (uint64_t)frameLength);
    bytewrite_buffer(chunkWriter, frameData, frameLength);
    return 0;
}

// ----------------------------------------------------------------------------
int capsframeWrite(void* ptr, const packet_t* packet, const caps_frame_t* frame)
{
    uint8_t frameData[4096];
    bytewrite_t* frameWriter = bytewriteCreate(frameData, 4096);

    bytewrite_vint(frameWriter, (uint64_t)ft_caps_frame);
    bytewrite_vint(frameWriter, (uint64_t)capsframeGetSeqid(frame));

    const video_caps_t* caps = capsframeGetCaps(frame);
    bytewrite_vint(frameWriter, caps->imgcaps.mute);
    if (caps->imgcaps.mute != muted) {
        bytewrite_int32(frameWriter, caps->imgcaps.preferred_res.cx);
        bytewrite_int32(frameWriter, caps->imgcaps.preferred_res.cy);
        bytewrite_int32(frameWriter, caps->imgcaps.preferred_rate.num);
        bytewrite_int32(frameWriter, caps->imgcaps.preferred_rate.den);
        bytewrite_vint(frameWriter, caps->imgcaps.max_procrate);
        bytewrite_vint(frameWriter, caps->max_bitrate);
    }

    const size_t frameLength = bytewrite_length(frameWriter);
    bytewriteDestroy(frameWriter);

    bytewrite_t* chunkWriter = (bytewrite_t*)ptr;
    bytewrite_vint(chunkWriter, (uint64_t)frameLength);
    bytewrite_buffer(chunkWriter, frameData, frameLength);
    return 0;
}

// ----------------------------------------------------------------------------
int videoframeWrite(void* ptr, const packet_t* packet, const video_frame_t* frame)
{
    uint8_t frameData[4096];
    bytewrite_t* frameWriter = bytewriteCreate(frameData, 4096);

    bytewrite_vint(frameWriter, (uint64_t)ft_video_frame);
    bytewrite_vint(frameWriter, (uint64_t)videoframeGetSeqNo(frame));
    bytewrite_vint(frameWriter, (uint64_t)videoframeGetPicId(frame));
    bytewrite_vint(frameWriter, (uint64_t)videoframeGetFlags(frame));
    bytewrite_vint(frameWriter, (uint64_t)videoframeGetRefType(frame));
    bytewrite_vint(frameWriter, (uint64_t)videoframeGetPresentationTime(frame));
    bytewrite_vint(frameWriter, (uint64_t)videoframeGetLength(frame));
    bytewrite_vint(frameWriter, (uint64_t)videoframeGetPSNR(frame));

    const size_t frameLength = bytewrite_length(frameWriter);
    bytewriteDestroy(frameWriter);

    bytewrite_t* chunkWriter = (bytewrite_t*)ptr;
    bytewrite_vint(chunkWriter, (uint64_t)frameLength);
    bytewrite_buffer(chunkWriter, frameData, frameLength);
    return 0;
}

// ----------------------------------------------------------------------------
static frame_enum_t cbsPacketWrite = {
    ackframeWrite,
    capsframeWrite,
    videoframeWrite
};

// ----------------------------------------------------------------------------
int packetWrite(const packet_t* obj, bytewrite_t* packetWriter)
{
    bytewrite_vint(packetWriter, (uint64_t)packetGetDirection(obj));
    bytewrite_vint(packetWriter, (uint64_t)packetGetSeqNo(obj));

    packetEnumFrames(obj, packetWriter, &cbsPacketWrite);
    return 0;
}

// ----------------------------------------------------------------------------
void packetlogWriteChunk(utilsFile* log, chunk_type_t type, const uint8_t* data, size_t length)
{
    uint8_t chunkData[4096];
    bytewrite_t* chunkWriter = bytewriteCreate(chunkData, 4096);
    bytewrite_vint(chunkWriter, (uint64_t)type);
    bytewrite_vint(chunkWriter, (uint64_t)length);
    bytewrite_buffer(chunkWriter, data, length);

    // Write header and chunk data
    const size_t chunkLength = bytewrite_length(chunkWriter);
    bytewriteDestroy(chunkWriter);

    writeFile(log, chunkData, chunkLength);
}

// ----------------------------------------------------------------------------
void packetlogWritePacketData(utilsFile* log, const packet_t* packet)
{
    uint8_t packetData[4096];
    bytewrite_t* packetWriter = bytewriteCreate(packetData, 4096);
    packetWrite(packet, packetWriter);

    const size_t packetLength = bytewrite_length(packetWriter);
    bytewriteDestroy(packetWriter);

    packetlogWriteChunk(log, chunk_packet_data, packetData, packetLength);
}

// ----------------------------------------------------------------------------
void packetlogWritePacketEvent(utilsFile* log, const packet_event_t* event)
{
    uint8_t eventData[4096];
    bytewrite_t* eventWriter = bytewriteCreate(eventData, 4096);
    bytewrite_vint(eventWriter, (uint64_t)packeteventGetDirection(event));
    bytewrite_vint(eventWriter, (uint64_t)packeteventGetSeqNo(event));
    bytewrite_vint(eventWriter, (uint64_t)packeteventGetType(event));
    bytewrite_vint(eventWriter, (uint64_t)packeteventGetTime(event));

    const size_t eventLength = bytewrite_length(eventWriter);
    bytewriteDestroy(eventWriter);

    packetlogWriteChunk(log, chunk_packet_event, eventData, eventLength);
}

// ----------------------------------------------------------------------------
int packetlogParseChunk(byteread_t* fileReader, void* ctx, packetlogFnPacket fnPacket, packetlogFnEvent fnEvent)
{
    int rv = 0;
    if (byteread_remain(fileReader) == 0) {
        return -1;
    }

    uint64_t chunkType, chunkLength;
    rv |= byteread_vint(fileReader, &chunkType);
    rv |= byteread_vint(fileReader, &chunkLength);

    if (rv != 0) {
        return -1;
    }

    byteread_t* chunkReader = bytereadCreate(byteread_ptr(fileReader), chunkLength);
    byteread_skip(fileReader, chunkLength);

    switch (chunkType) {
    case chunk_packet_event: {
        const packet_event_t* event = packeteventParse(chunkReader);
        fnEvent(ctx, event);
        packeteventRelease(event);
        break;
    }

    case chunk_packet_data: {
        const packet_t* packet = packetParse(chunkReader);
        fnPacket(ctx, packet);
        packetRelease(packet);
        break;
    }
    }

    bytereadDestroy(chunkReader);
    return 0;
}
