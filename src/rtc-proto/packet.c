/*
 * MIT License
 *
 * Copyright (c) 2021 Steffen Schulze
 */

#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "cutil_nullptr.h"
#include "cutil_refobj.h"
#include "cutil_vecptr.h"

#include "rtc-proto/packet.h"

#include "rtc-proto/video_frame.h"
#include "rtc-proto/ack_frame.h"
#include "rtc-proto/caps_frame.h"

// ----------------------------------------------------------------------------
struct packet_s {
    ref_t ref;

    direction_t dir;
    uint64_t seqno;
    
    vecptr_t* frames;

    media_clock_t times[2];
    uint64_t eventState;

    size_t length;
};

// ----------------------------------------------------------------------------
int packetCompare(const void* x0, const void* x1)
{
    const packet_t* p0 = (const packet_t*)x0;
    const packet_t* p1 = (const packet_t*)x1;

    return (int)(p0->seqno - p1->seqno);
}

// ----------------------------------------------------------------------------
int packetPredicate(const void* context, const void* item)
{
    const uint64_t* seqno = (const uint64_t*)context;
    const packet_t* packet = (const packet_t*)item;
    return (int)(*seqno - packet->seqno);
}

// ----------------------------------------------------------------------------
static void objDestroy(const void* ptr)
{
    const packet_t* obj = (const packet_t*)ptr;
    if (obj != nullptr) {
        vecptrDestroy(obj->frames);
        free((void*)obj);
    }
}

// ----------------------------------------------------------------------------
void objDestroyFrame(const void* ptr)
{
    const frame_t* obj = (const frame_t*)ptr;
    frameRelease(obj);
}

// ----------------------------------------------------------------------------
packet_t* packetRelease(const packet_t* obj)
{
    ref_dec(&obj->ref);
    return nullptr;
}

// ----------------------------------------------------------------------------
packet_t* packetAssign(packet_t* obj)
{
    ref_inc(&obj->ref);
    return obj;
}

// ----------------------------------------------------------------------------
const packet_t* packetConstAssign(const packet_t* obj)
{
    ref_inc(&obj->ref);
    return obj;
}

// ----------------------------------------------------------------------------
packet_t* packetCreate(direction_t dir, uint64_t seqno)
{
    packet_t* obj = (packet_t*)malloc(sizeof(packet_t));
    if (obj == nullptr) {
        return nullptr;
    }

    ref_init(&obj->ref, obj, objDestroy);

    obj->dir = dir;
    obj->seqno = seqno;
    obj->frames = vecptrCreate(16, 16, objDestroyFrame);

    obj->times[packet_send] = 0;
    obj->times[packet_recv] = 0;
    obj->eventState = 0;
    obj->length = 0;

    return packetAssign(obj);
}

// ----------------------------------------------------------------------------
void packetAddFrame(packet_t* obj, const frame_t* frame, size_t length)
{
    vecptrAddItem(obj->frames, (void*)frameAssign(frame));
    obj->length += length;
}

// ----------------------------------------------------------------------------
void packetAddAckFrame(packet_t* obj, const ack_frame_t* frame)
{
    packetAddFrame(obj, ackframeGetFrame(frame), ackframeGetLength(frame));
}

// ----------------------------------------------------------------------------
void packetAddCapsFrame(packet_t* obj, const caps_frame_t* frame)
{
    packetAddFrame(obj, capsframeGetFrame(frame), capsframeGetLength(frame));
}

// ----------------------------------------------------------------------------
void packetAddVideoFrame(packet_t* obj, const video_frame_t* frame)
{
    packetAddFrame(obj, videoframeGetFrame(frame), videoframeGetLength(frame));
}

// ----------------------------------------------------------------------------
void packetAddTime(packet_t* obj, packet_event_type_t type, media_clock_t time)
{
    obj->times[type] = time;
    obj->eventState |= 1ull << type;
}

// ----------------------------------------------------------------------------
typedef struct frame_enum_context_s {
    const packet_t* packet;
    void* context;
    const frame_enum_t* cbs;
} frame_enum_context_t;

// ----------------------------------------------------------------------------
int enumFramesCb(const void* item, void* context)
{
    const frame_t* frame = (const frame_t*)item;
    const frame_enum_context_t* ctx = (const frame_enum_context_t*)context;
    const frame_enum_t* cbs = ctx->cbs;

    switch (frameGetType(frame)) {
    case ft_ack_frame:
        return cbs->fnAckFrame != nullptr ?
            cbs->fnAckFrame(ctx->context, ctx->packet, (const ack_frame_t*)item) : 0;
    case ft_caps_frame:
        return cbs->fnCapsFrame != nullptr ?
            cbs->fnCapsFrame(ctx->context, ctx->packet, (const caps_frame_t*)item) : 0;
    case ft_video_frame:
        return cbs->fnVideoFrame != nullptr ?
            cbs->fnVideoFrame(ctx->context, ctx->packet, (const video_frame_t*)item) : 0;
    default:
        return 0;
    }
}

// ----------------------------------------------------------------------------
void packetEnumFrames(const packet_t* obj, void* context, const frame_enum_t* cbs)
{
    frame_enum_context_t ctx = { obj, context, cbs };
    vecptrIterateItems(obj->frames, enumFramesCb, &ctx);
}

// ----------------------------------------------------------------------------
direction_t packetGetDirection(const packet_t* obj)
{
    return obj->dir;
}

// ----------------------------------------------------------------------------
uint64_t packetGetSeqNo(const packet_t* obj)
{
    return obj->seqno;
}

// ----------------------------------------------------------------------------
media_clock_t packetGetTime(const packet_t* obj, packet_event_type_t type)
{
    return obj->times[type];
}

// ----------------------------------------------------------------------------
uint64_t packetGetState(const packet_t* obj)
{
    return obj->eventState;
}

// ----------------------------------------------------------------------------
size_t packetGetLength(const packet_t* obj)
{
    return obj->length;
}
