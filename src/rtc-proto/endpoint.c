/*
 * MIT License
 *
 * Copyright (c) 2020 Steffen Schulze
 */

#include <malloc.h>
#include <string.h>
#include <stdio.h>

#include "cutil_nullptr.h"
#include "cutil_refobj.h"
#include "cutil_sortedlist.h"
#include "cutil_vecu64.h"

#include "rtc-proto/endpoint.h"

// ----------------------------------------------------------------------------
typedef struct pair_s pair_t;

struct pair_s {
    uint64_t rxseqno; // seqno of packets received
    uint64_t txseqno; // seqno of packet sent acking the received packet, 0 if not acked yet
};

// ----------------------------------------------------------------------------
struct endpoint_s {

    FnAckPacket fnAckPacket;
    void* context;

    sortedlist_t* pendingRxSeqNos;
    sortedlist_t* inflightAcks;
};

// ----------------------------------------------------------------------------
int ackCompare(const void* x0, const void* x1)
{
    const pair_t* a0 = (const pair_t*)x0;
    const pair_t* a1 = (const pair_t*)x1;
    return (int)(a0->rxseqno - a1->rxseqno);
}

// ----------------------------------------------------------------------------
pair_t* ackCreate(uint64_t rxseqno, uint64_t txseqno)
{
    pair_t* obj = (pair_t*)malloc(sizeof(pair_t));
    if (obj == nullptr) {
        return nullptr;
    }

    obj->rxseqno = rxseqno;
    obj->txseqno = txseqno;

    return obj;
}

// ----------------------------------------------------------------------------
void ackDestroy(const void* ptr)
{
    free((void*)ptr);
}

// ----------------------------------------------------------------------------
endpoint_t* epCreate(FnAckPacket fnAckPacket, void* context)
{
    endpoint_t* obj = (endpoint_t*)malloc(sizeof(endpoint_t));
    if (obj == nullptr) {
        return nullptr;
    }

    obj->fnAckPacket = fnAckPacket;
    obj->context = context;
    obj->pendingRxSeqNos = slistCreate();
    obj->inflightAcks = slistCreate();

    return obj;
}

// ----------------------------------------------------------------------------
endpoint_t* epDestroy(const endpoint_t* obj)
{
    slistDestroy(obj->pendingRxSeqNos, ackDestroy);
    slistDestroy(obj->inflightAcks, ackDestroy);

    free((void*)obj);
    return nullptr;
}

// ----------------------------------------------------------------------------
int ackSetTxSeqNo(const void* item, void* context)
{
    pair_t* obj = (pair_t*)item;
    const uint64_t* txSeqNo = (const uint64_t*)context;
    obj->txseqno = *txSeqNo;
    return 0;
}

// ----------------------------------------------------------------------------
int ackAppend(const void* item, void* context)
{
    pair_t* obj = (pair_t*)item;
    vecu64_t* acks = (vecu64_t*)context;
    vecu64AddItem(acks, obj->rxseqno);
    return 0;
}

// ----------------------------------------------------------------------------
const ack_frame_t* epPreparePacket(endpoint_t* obj, uint64_t txseqno)
{
    slistIterate(obj->pendingRxSeqNos, ackSetTxSeqNo, &txseqno);
    slistMerge(obj->inflightAcks, obj->pendingRxSeqNos, ackCompare);

    const unsigned ackCount = slistGetLength(obj->inflightAcks);
    vecu64_t* acks = vecu64Create(ackCount, 10);
    slistIterate(obj->inflightAcks, ackAppend, acks);

    return ackframeCreate(acks);
}

// ----------------------------------------------------------------------------
static int filterAcked(const void* item, void* context)
{
    const pair_t* ack = (const pair_t*)item;
    const uint64_t* highestSeqNo = (const uint64_t*)context;
    return ack->txseqno <= *highestSeqNo;
}

// ----------------------------------------------------------------------------
static void epRemoveAckedAcks(sortedlist_t* inflightAcks, const vecu64_t* acks)
{
    if (vecu64IsEmpty(acks)) {
        return;
    }

    unsigned highestSeqNoIdx = vecu64ItemCount(acks) - 1;
    uint64_t highestSeqNo = vecu64GetItem(acks, highestSeqNoIdx);
    slistFilter(inflightAcks, filterAcked, ackDestroy, &highestSeqNo);
}

// ----------------------------------------------------------------------------
static void epRemoveAckedTxSeqNos(endpoint_t* obj, const vecu64_t* acks)
{
    if (obj->fnAckPacket == nullptr) {
        return;
    }

    unsigned ackCount = vecu64ItemCount(acks);
    for (unsigned i = 0; i < ackCount; ++i) {
        const uint64_t ackdSeqNo = vecu64GetItem(acks, i);
        obj->fnAckPacket(obj->context, ackdSeqNo);
    }
}

// ----------------------------------------------------------------------------
void epPacketReceived(endpoint_t* obj, uint64_t rxseqno, const ack_frame_t* ackFrame)
{
    const vecu64_t* acks = ackframeGetAcks(ackFrame);
    slistInsertItem(obj->pendingRxSeqNos, ackCompare, ackCreate(rxseqno, 0));

    epRemoveAckedAcks(obj->inflightAcks, acks);
    epRemoveAckedTxSeqNos(obj, acks);
}
