/*
 * MIT License
 *
 * Copyright (c) 2021 Steffen Schulze
 */

#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "cutil_nullptr.h"
#include "cutil_buffer.h"

#include "frame_impl.h"
#include "rtc-proto/video_frame.h"

// ----------------------------------------------------------------------------
struct video_frame_s {
    frame_t frame;

    uint64_t seqno;
    uint64_t picid;
    uint64_t flags;

    media_clock_t ts;
    const buffer_t* data;

    ref_type_t type;
    uint64_t psnr;
};

// ----------------------------------------------------------------------------
static void objDestroy(const void* ptr)
{
    const video_frame_t* obj = (const video_frame_t*)(ptr);
    freeBuffer(obj->data);
    free((void*)obj);
}

// ----------------------------------------------------------------------------
video_frame_t* videoframeCreate(uint64_t seqno, uint64_t picid, uint64_t flags,
    media_clock_t ts, const buffer_t* data, ref_type_t type, uint64_t psnr)
{
    video_frame_t* obj = (video_frame_t*)malloc(sizeof(video_frame_t));
    if (obj == nullptr) {
        return nullptr;
    }

    ref_init(&obj->frame.ref, obj, objDestroy);

    obj->frame.type = ft_video_frame;
    obj->seqno = seqno;
    obj->picid = picid;
    obj->flags = flags;
    obj->ts = ts;
    obj->data = assignBuffer(data);
    obj->type = type;
    obj->psnr = psnr;

    return obj;
}

// ----------------------------------------------------------------------------
video_frame_t* videoframeAssign(video_frame_t* obj)
{
    ref_inc(&obj->frame.ref);
    return obj;
}

// ----------------------------------------------------------------------------
video_frame_t* videoframeRelease(const video_frame_t* obj)
{
    ref_dec(&obj->frame.ref);
    return nullptr;
}

// ----------------------------------------------------------------------------
int videoFrameCompare(const void* x0, const void* x1)
{
    const video_frame_t* p0 = (const video_frame_t*)x0;
    const video_frame_t* p1 = (const video_frame_t*)x1;

    return (int)(p0->seqno - p1->seqno);
}

// ----------------------------------------------------------------------------
const frame_t* videoframeGetFrame(const video_frame_t* obj)
{
    return &obj->frame;
}

// ----------------------------------------------------------------------------
size_t videoframeGetLength(const video_frame_t* obj)
{
    return getBufferSize(obj->data);
}

// ----------------------------------------------------------------------------
uint64_t videoframeGetSeqNo(const video_frame_t* obj)
{
    return obj->seqno;
}

// ----------------------------------------------------------------------------
uint64_t videoframeGetPicId(const video_frame_t* obj)
{
    return obj->picid;
}

// ----------------------------------------------------------------------------
uint64_t videoframeGetFlags(const video_frame_t* obj)
{
    return obj->flags;
}

// ----------------------------------------------------------------------------
media_clock_t videoframeGetPresentationTime(const video_frame_t* obj)
{
    return obj->ts;
}

// ----------------------------------------------------------------------------
const buffer_t* videoframeGetData(const video_frame_t* obj)
{
    return obj->data;
}

// ----------------------------------------------------------------------------
ref_type_t videoframeGetRefType(const video_frame_t* obj)
{
    return obj->type;
}

// ----------------------------------------------------------------------------
uint64_t videoframeGetPSNR(const video_frame_t* obj)
{
    return obj->psnr;
}
