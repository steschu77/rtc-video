/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */

#include <stdlib.h>
#include <string.h>

#include "cutil_nullptr.h"

#include "video_sink_impl.h"

// ----------------------------------------------------------------------------
video_sink_t* videosinkInit(
    video_sink_t* obj,
    void* context,
    ptrFnAssign fnAssign,
    ptrFnRelease fnRelease,
    videosinkFnStartSeq fnStartSeq,
    videosinkFnFinishSeq fnFinishSeq,
    videosinkFnDeliverPic fnDeliverPic)
{
    obj->context = context;
    obj->fnAssign = fnAssign;
    obj->fnRelease = fnRelease;
    obj->fnStartSeq = fnStartSeq;
    obj->fnFinishSeq = fnFinishSeq;
    obj->fnDeliverPic = fnDeliverPic;

    return obj;
}

// ----------------------------------------------------------------------------
video_sink_t* videosinkCopy(video_sink_t* obj)
{
    obj->fnAssign(obj->context);
    return obj;
}

// ----------------------------------------------------------------------------
void videosinkRelease(const video_sink_t* obj)
{
    obj->fnRelease(obj->context);
}

// ----------------------------------------------------------------------------
void videosinkAssign(video_sink_t* obj, video_sink_t** pdst)
{
    video_sink_t* dst = *pdst;
    if (dst != nullptr) {
        dst->fnRelease(dst->context);
    }
    if (obj != nullptr) {
        obj->fnAssign(obj->context);
    }
    *pdst = obj;
}

// from video producer --> video consumer
// ----------------------------------------------------------------------------
void videosinkStartSeq(video_sink_t* sink, media_clock_t ts, seq_id_t id, const video_format_t* fmt)
{
    sink->fnStartSeq(sink->context, ts, id, fmt);
}

// ----------------------------------------------------------------------------
void videosinkFinishSeq(video_sink_t* sink, media_clock_t ts)
{
    sink->fnFinishSeq(sink->context, ts);
}

// ----------------------------------------------------------------------------
void videosinkDeliverPic(video_sink_t* sink, media_clock_t ts, const video_pic_t* pic)
{
    sink->fnDeliverPic(sink->context, ts, pic);
}
