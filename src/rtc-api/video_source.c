/*
 * MIT License
 *
 * Copyright (c) 2021 Steffen Schulze
 */

#include <stdlib.h>
#include <string.h>

#include "cutil_nullptr.h"

#include "video_source_impl.h"

// ----------------------------------------------------------------------------
video_source_t* videosrcInit(
    video_source_t* obj,
    void* context,
    ptrFnAssign fnAssign,
    ptrFnRelease fnRelease,
    videosrcFnConnect fnConnect,
    videosrcFnChangeCaps fnChangeCaps,
    videosrcFnRequestSyncPoint fnRequestSyncPoint,
    videosrcFnNotifyPictureLost fnNotifyPictureLost,
    videosrcFnNotifyPictureProcessed fnNotifyPictureProcessed)
{
    obj->context = context;
    obj->fnAssign = fnAssign;
    obj->fnRelease = fnRelease;
    obj->fnConnect = fnConnect;
    obj->fnChangeCaps = fnChangeCaps;
    obj->fnRequestSyncPoint = fnRequestSyncPoint;
    obj->fnNotifyPictureLost = fnNotifyPictureLost;
    obj->fnNotifyPictureProcessed = fnNotifyPictureProcessed;
    return obj;
}

// ----------------------------------------------------------------------------
video_source_t* videosrcCopy(video_source_t* obj)
{
    obj->fnAssign(obj->context);
    return obj;
}

// ----------------------------------------------------------------------------
void videosrcRelease(const video_source_t* obj)
{
    obj->fnRelease(obj->context);
}

// ----------------------------------------------------------------------------
void videosrcAssign(video_source_t* obj, video_source_t** pdst)
{
    video_source_t* dst = *pdst;
    if (dst != nullptr) {
        dst->fnRelease(dst->context);
    }
    if (obj != nullptr) {
        obj->fnAssign(obj->context);
    }
    *pdst = obj;
}

// ----------------------------------------------------------------------------
void videosrcConnect(video_source_t* obj, video_sink_t* sink)
{
    obj->fnConnect(obj->context, sink);
}

// ----------------------------------------------------------------------------
void videosrcChangeCaps(video_source_t* obj, const video_caps_t* caps)
{
    obj->fnChangeCaps(obj->context, caps);
}

// ----------------------------------------------------------------------------
void videosrcRequestSyncPoint(video_source_t* obj)
{
    obj->fnRequestSyncPoint(obj->context);
}

// ----------------------------------------------------------------------------
void videosrcNotifyPictureLost(video_source_t* obj, const media_id_t id)
{
    obj->fnNotifyPictureLost(obj->context, id);
}

// ----------------------------------------------------------------------------
void videosrcNotifyPictureProcessed(video_source_t* obj, const media_id_t id)
{
    obj->fnNotifyPictureProcessed(obj->context, id);
}
