/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */

#include <malloc.h>

#include "cutil_nullptr.h"
#include "cutil_refobj.h"

#include "rtc-api/video_format.h"

// ----------------------------------------------------------------------------
struct video_format_s {
    ref_t ref;

    frame_rate_t max_framerate;
    image_size_t resolution;

    fourcc_t codec;
    uint64_t max_procrate;
};

// ----------------------------------------------------------------------------
void destroyVideoFormat(const void* obj);

// ----------------------------------------------------------------------------
const video_format_t* createVideoFormat(
    const frame_rate_t* max_framerate, const image_size_t* resolution, fourcc_t codec, procrate_t max_procrate)
{
    video_format_t* fmt = (video_format_t*)malloc(sizeof(video_format_t));
    if (fmt == nullptr) {
        return nullptr;
    }

    ref_init(&fmt->ref, fmt, destroyVideoFormat);
    fmt->max_framerate = *max_framerate;
    fmt->resolution = *resolution;
    fmt->codec = codec;
    fmt->max_procrate = max_procrate;
    return assignVideoFormat(fmt);
}

// ----------------------------------------------------------------------------
void destroyVideoFormat(const void* obj)
{
    free((void*)obj);
}

// ----------------------------------------------------------------------------
void freeVideoFormat(const video_format_t* fmt)
{
    ref_dec(&fmt->ref);
}

// ----------------------------------------------------------------------------
const video_format_t* assignVideoFormat(const video_format_t* fmt)
{
    ref_inc(&fmt->ref);
    return fmt;
}

// ----------------------------------------------------------------------------
const frame_rate_t* videofmtGetMaxFrameRate(const video_format_t* fmt)
{
    return &fmt->max_framerate;
}

// ----------------------------------------------------------------------------
const image_size_t* videofmtGetResolution(const video_format_t* fmt)
{
    return &fmt->resolution;
}

// ----------------------------------------------------------------------------
fourcc_t videofmtGetCodec(const video_format_t* fmt)
{
    return fmt->codec;
}

// ----------------------------------------------------------------------------
uint64_t videofmtGetMaxProcRate(const video_format_t* fmt)
{
    return fmt->max_procrate;
}
