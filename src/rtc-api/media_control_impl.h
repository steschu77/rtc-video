/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */

#pragma once

#include "cutil_refobj.h"

#include "rtc-api/image_caps.h"
#include "rtc-api/media_control.h"

// ----------------------------------------------------------------------------
typedef void (*mediactrlFnRun)(void*, const image_caps_t*);

// ----------------------------------------------------------------------------
struct media_control_s {
    void* context;
    ptrFnAssign fnAssign;
    ptrFnRelease fnRelease;
    mediactrlFnRun fnRun;
};

// ----------------------------------------------------------------------------
media_control_t* mediactrlInit(
    media_control_t* obj,
    void* context,
    ptrFnAssign fnAssign,
    ptrFnRelease fnRelease,
    mediactrlFnRun fnRun);
