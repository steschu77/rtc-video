/*
 * MIT License
 *
 * Copyright (c) 2021 Steffen Schulze
 */

#pragma once

#include "cutil_refobj.h"

#include "rtc-api/video_source.h"

// ----------------------------------------------------------------------------
typedef void (*videosrcFnConnect)(void*, video_sink_t*);
typedef void (*videosrcFnChangeCaps)(void*, const video_caps_t*);
typedef void (*videosrcFnRequestSyncPoint)(void*);
typedef void (*videosrcFnNotifyPictureLost)(void*, const media_id_t);
typedef void (*videosrcFnNotifyPictureProcessed)(void*, const media_id_t);

// ----------------------------------------------------------------------------
struct video_source_s {
    void* context;
    ptrFnAssign fnAssign;
    ptrFnRelease fnRelease;
    videosrcFnConnect fnConnect;
    videosrcFnChangeCaps fnChangeCaps;
    videosrcFnRequestSyncPoint fnRequestSyncPoint;
    videosrcFnNotifyPictureLost fnNotifyPictureLost;
    videosrcFnNotifyPictureProcessed fnNotifyPictureProcessed;
};

// ----------------------------------------------------------------------------
video_source_t* videosrcInit(
    video_source_t* obj,
    void* context,
    ptrFnAssign fnAssign,
    ptrFnRelease fnRelease,
    videosrcFnConnect fnConnect,
    videosrcFnChangeCaps fnChangeCaps,
    videosrcFnRequestSyncPoint fnRequestSyncPoint,
    videosrcFnNotifyPictureLost fnNotifyPictureLost,
    videosrcFnNotifyPictureProcessed fnNotifyPictureProcessed);
