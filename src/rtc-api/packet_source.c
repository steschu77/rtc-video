/*
 * MIT License
 *
 * Copyright (c) 2021 Steffen Schulze
 */

#include <stdlib.h>
#include <string.h>

#include "cutil_nullptr.h"

#include "packet_source_impl.h"

// ----------------------------------------------------------------------------
packet_source_t* pcksrcInit(
    packet_source_t* obj,
    void* context,
    ptrFnAssign fnAssign,
    ptrFnRelease fnRelease,
    pcksrcFnConnect fnConnect,
    pcksrcFnQoSChanged fnQoSChanged)
{
    obj->context = context;
    obj->fnAssign = fnAssign;
    obj->fnRelease = fnRelease;
    obj->fnConnect = fnConnect;
    obj->fnQoSChanged = fnQoSChanged;

    return obj;
}

// ----------------------------------------------------------------------------
packet_source_t* pcksrcCopy(packet_source_t* obj)
{
    obj->fnAssign(obj->context);
    return obj;
}

// ----------------------------------------------------------------------------
void pcksrcRelease(const packet_source_t* obj)
{
    obj->fnRelease(obj->context);
}

// ----------------------------------------------------------------------------
void pcksrcAssign(packet_source_t* obj, packet_source_t** pdst)
{
    packet_source_t* dst = *pdst;
    if (dst != nullptr) {
        dst->fnRelease(dst->context);
    }
    if (obj != nullptr) {
        obj->fnAssign(obj->context);
    }
    *pdst = obj;
}

// ----------------------------------------------------------------------------
void pcksrcConnect(packet_source_t* obj, packet_sink_t* recv, packet_sink_t** send)
{
    obj->fnConnect(obj->context, recv, send);
}

// ----------------------------------------------------------------------------
void pcksrcQoSChanged(packet_source_t* obj, const network_qos_params_t* qos)
{
    obj->fnQoSChanged(obj->context, qos);
}
