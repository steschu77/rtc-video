/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */

#include <stdlib.h>
#include <string.h>

#include "cutil_nullptr.h"

#include "image_sink_impl.h"

// ----------------------------------------------------------------------------
image_sink_t* imgsinkInit(
    image_sink_t* obj,
    void* context,
    ptrFnAssign fnAssign,
    ptrFnRelease fnRelease,
    imgsinkFnDeliverImage fnDeliverImage)
{
    obj->context = context;
    obj->fnAssign = fnAssign;
    obj->fnRelease = fnRelease;
    obj->fnDeliverImage = fnDeliverImage;
    return obj;
}

// ----------------------------------------------------------------------------
image_sink_t* imgsinkCopy(image_sink_t* obj)
{
    obj->fnAssign(obj->context);
    return obj;
}

// ----------------------------------------------------------------------------
void imgsinkRelease(const image_sink_t* obj)
{
    obj->fnRelease(obj->context);
}

// ----------------------------------------------------------------------------
void imgsinkAssign(image_sink_t* obj, image_sink_t** pdst)
{
    image_sink_t* dst = *pdst;
    if (dst != nullptr) {
        dst->fnRelease(dst->context);
    }
    if (obj != nullptr) {
        obj->fnAssign(obj->context);
    }
    *pdst = obj;
}

// ----------------------------------------------------------------------------
void imgsinkDeliverPicture(image_sink_t* obj, media_clock_t t, const image_t* img)
{
    obj->fnDeliverImage(obj->context, t, img);
}
