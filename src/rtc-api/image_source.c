/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */

#include <stdlib.h>
#include <string.h>

#include "cutil_nullptr.h"

#include "image_source_impl.h"

// ----------------------------------------------------------------------------
image_source_t* imgsrcInit(
    image_source_t* obj,
    void* context,
    ptrFnAssign fnAssign,
    ptrFnRelease fnRelease,
    imgsrcFnConnect fnConnect,
    imgsrcFnChangeCaps fnChangeCaps)
{
    obj->context = context;
    obj->fnAssign = fnAssign;
    obj->fnRelease = fnRelease;
    obj->fnConnect = fnConnect;
    obj->fnChangeCaps = fnChangeCaps;
    return obj;
}

// ----------------------------------------------------------------------------
image_source_t* imgsrcCopy(image_source_t* obj)
{
    obj->fnAssign(obj->context);
    return obj;
}

// ----------------------------------------------------------------------------
void imgsrcRelease(const image_source_t* obj)
{
    obj->fnRelease(obj->context);
}

// ----------------------------------------------------------------------------
void imgsrcAssign(image_source_t* obj, image_source_t** pdst)
{
    image_source_t* dst = *pdst;
    if (dst != nullptr) {
        dst->fnRelease(dst->context);
    }
    if (obj != nullptr) {
        obj->fnAssign(obj->context);
    }
    *pdst = obj;
}

// ----------------------------------------------------------------------------
void imgsrcConnect(image_source_t* obj, image_sink_t* sink)
{
    obj->fnConnect(obj->context, sink);
}

// ----------------------------------------------------------------------------
void imgsrcChangeCaps(image_source_t* obj, const image_caps_t* caps)
{
    obj->fnChangeCaps(obj->context, caps);
}
