/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */

#include <malloc.h>
#include <string.h>

#include "media/media_id.h"

#include "cutil_math.h"
#include "cutil_nullptr.h"

// ----------------------------------------------------------------------------
struct media_ids_s {
    media_id_t* ids;
    unsigned num_ids;
    unsigned capacity;
    unsigned increase;
};

// ----------------------------------------------------------------------------
void copyIds(media_id_t* dst, const media_id_t* src, unsigned num_ids);

// ----------------------------------------------------------------------------
media_ids_t* createMediaIds(unsigned capacity, unsigned increase)
{
    media_ids_t* ids = (media_ids_t*)malloc(sizeof(media_ids_t));
    media_id_t* raw_ids = (media_id_t*)malloc(capacity * sizeof(media_id_t));

    if (ids == nullptr || raw_ids == nullptr) {
        free(ids);
        free(raw_ids);
        return 0;
    }

    ids->ids = raw_ids;
    ids->num_ids = 0;
    ids->capacity = capacity;
    ids->increase = max_unsigned(increase, 1u);
    return ids;
}

// ----------------------------------------------------------------------------
void destroyMediaIds(const media_ids_t* ids)
{
    if (ids != 0) {
        free((void*)(ids->ids));
        free((void*)ids);
    }
}

// ----------------------------------------------------------------------------
void reallocMediaIds(media_ids_t* ids, unsigned new_capacity)
{
    unsigned capacity = roundUpMultiple(new_capacity, ids->increase);
    if (capacity <= ids->capacity) {
        return;
    }

    media_id_t* raw_ids = (media_id_t*)malloc(capacity * sizeof(media_id_t));
    if (raw_ids == 0) {
        return;
    }

    copyIds(raw_ids, ids->ids, ids->num_ids);

    free(ids->ids);
    ids->ids = raw_ids;
    ids->capacity = capacity;
}

// ----------------------------------------------------------------------------
void addMediaId(media_ids_t* ids, media_id_t id)
{
    unsigned num_ids = ids->num_ids + 1;
    if (num_ids > ids->capacity) {
        reallocMediaIds(ids, num_ids);
    }

    if (num_ids <= ids->capacity) {
        ids->ids[ids->num_ids] = id;
        ids->num_ids = num_ids;
    }
}

// ----------------------------------------------------------------------------
void mergeMediaIds(media_ids_t* ids, const media_ids_t* src_ids)
{
    unsigned num_ids = ids->num_ids + src_ids->num_ids;
    if (num_ids > ids->capacity) {
        reallocMediaIds(ids, num_ids);
    }

    if (num_ids <= ids->capacity) {
        copyIds(ids->ids + ids->num_ids, src_ids->ids, src_ids->num_ids);
        ids->num_ids = num_ids;
    }
}

// ----------------------------------------------------------------------------
unsigned getNumIds(const media_ids_t* ids)
{
    return ids->num_ids;
}

// ----------------------------------------------------------------------------
media_id_t getId(const media_ids_t* ids, unsigned idx)
{
    return (idx < ids->num_ids) ? ids->ids[idx] : 0;
}

// ----------------------------------------------------------------------------
void copyIds(media_id_t* dst, const media_id_t* src, unsigned num_ids)
{
    memcpy(dst, src, num_ids * sizeof(media_id_t));
}
