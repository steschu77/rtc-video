/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */

#include <malloc.h>

#include "cutil_nullptr.h"
#include "cutil_refobj.h"

#include "rtc-api/video_picture.h"

// ----------------------------------------------------------------------------
struct video_pic_s {
    ref_t ref;

    pic_id_t id;
    media_clock_t ts;
    const buffer_t* data;
    ref_type_t type;
    uint64_t psnr;
};

// ----------------------------------------------------------------------------
void destroyVideoPicture(const void* ptr)
{
    video_pic_t* obj = (video_pic_t*)ptr;

    if (obj != nullptr) {
        freeBuffer(obj->data);
        free((void*)obj);
    }
}

// ----------------------------------------------------------------------------
const video_pic_t* videopicAssign(const video_pic_t* obj)
{
    if (obj != nullptr) {
        ref_inc(&obj->ref);
    }
    return obj;
}

// ----------------------------------------------------------------------------
const video_pic_t* videopicRelease(const video_pic_t* obj)
{
    if (obj != nullptr) {
        ref_dec(&obj->ref);
    }
    return nullptr;
}

// ----------------------------------------------------------------------------
const video_pic_t* createVideoPicture(pic_id_t id, media_clock_t ts,
    const buffer_t* data, ref_type_t type, uint64_t psnr)
{
    video_pic_t* obj = (video_pic_t*)malloc(sizeof(video_pic_t));
    if (obj == nullptr) {
        return nullptr;
    }

    ref_init(&obj->ref, obj, destroyVideoPicture);

    obj->id = id;
    obj->ts = ts;
    obj->data = assignBuffer(data);
    obj->type = type;
    obj->psnr = psnr;
    
    return videopicAssign(obj);
}

// ----------------------------------------------------------------------------
pic_id_t getVideoPictureId(const video_pic_t* pic)
{
    return pic->id;
}

// ----------------------------------------------------------------------------
media_clock_t getVideoPresentationTime(const video_pic_t* pic)
{
    return pic->ts;
}

// ----------------------------------------------------------------------------
const buffer_t* getVideoPictureData(const video_pic_t* pic)
{
    return pic->data;
}

// ----------------------------------------------------------------------------
ref_type_t getVideoPictureType(const video_pic_t* pic)
{
    return pic->type;
}

// ----------------------------------------------------------------------------
uint64_t getVideoPicturePSNR(const video_pic_t* pic)
{
    return pic->psnr;
}
