/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */

#pragma once

#include "cutil_refobj.h"

#include "rtc-api/video_sink.h"

// ----------------------------------------------------------------------------
typedef void (*videosinkFnStartSeq)(void*, media_clock_t, seq_id_t, const video_format_t*);
typedef void (*videosinkFnFinishSeq)(void*, media_clock_t);
typedef void (*videosinkFnDeliverPic)(void*, media_clock_t, const video_pic_t*);

// ----------------------------------------------------------------------------
struct video_sink_s {
    void* context;
    ptrFnAssign fnAssign;
    ptrFnRelease fnRelease;
    videosinkFnStartSeq fnStartSeq;
    videosinkFnFinishSeq fnFinishSeq;
    videosinkFnDeliverPic fnDeliverPic;
};

// ----------------------------------------------------------------------------
video_sink_t* videosinkInit(
    video_sink_t* sink,
    void* context,
    ptrFnAssign fnAssign,
    ptrFnRelease fnRelease,
    videosinkFnStartSeq fnStartSeq,
    videosinkFnFinishSeq fnFinishSeq,
    videosinkFnDeliverPic fnDeliverPic);
