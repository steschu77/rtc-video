/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */

#pragma once

#include "cutil_refobj.h"

#include "rtc-api/image_sink.h"

// ----------------------------------------------------------------------------
typedef void (*imgsinkFnDeliverImage)(void*, media_clock_t, const image_t*);

// ----------------------------------------------------------------------------
struct image_sink_s {
    void* context;
    ptrFnAssign fnAssign;
    ptrFnRelease fnRelease;
    imgsinkFnDeliverImage fnDeliverImage;
};

// ----------------------------------------------------------------------------
image_sink_t* imgsinkInit(
    image_sink_t* sink,
    void* context,
    ptrFnAssign fnAssign,
    ptrFnRelease fnRelease,
    imgsinkFnDeliverImage fnDeliverImage);
