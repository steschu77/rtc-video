/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */

#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "cutil_nullptr.h"
#include "cutil_refobj.h"

#include "rtc-api/image_colorformats.h"
#include "rtc-api/image_picture.h"

// ----------------------------------------------------------------------------
struct image_s {

    ref_t ref;

    fourcc_t fmt;
    image_size_t size;
    stride_t stride;

    const buffer_t* data[4];
};

// ----------------------------------------------------------------------------
void gfxDestroyImage(const void* obj)
{
    const image_t* img = (const image_t*)obj;
    if (img == nullptr) {
        return;
    }

    for (int i = 0; i < 4; ++i) {
        freeBuffer(img->data[i]);
    }

    free((void*)img);
}

// ----------------------------------------------------------------------------
const image_t* gfxCreateImage(const stride_t* stride, const image_size_t* size, fourcc_t fmt, const buffer_t* data[4])
{
    image_t* img = (image_t*)malloc(sizeof(image_t));
    if (img == nullptr) {
        return nullptr;
    }

    ref_init(&img->ref, img, gfxDestroyImage);

    img->fmt = fmt;
    img->size = *size;
    img->stride = *stride;

    for (int i = 0; i < 4; ++i) {
        img->data[i] = assignBuffer(data[i]);
    }

    return img;
}

// ----------------------------------------------------------------------------
const image_t* gfxCropImage(const image_t* img, unsigned x, unsigned y, const image_size_t* size)
{
    const fourcc_t fmt = img->fmt;

    image_t* cropped_img = (image_t*)malloc(sizeof(image_t));
    if (cropped_img == nullptr) {
        return nullptr;
    }

    ref_init(&cropped_img->ref, cropped_img, gfxDestroyImage);

    cropped_img->fmt = fmt;
    cropped_img->size = *size;
    cropped_img->stride = img->stride;

    const plane_scale_t shift = gfxPlaneShift(fmt);
    const plane_scale_t scale = gfxPlaneScale(fmt);

    for (int i = 0; i < 4; ++i) {

        const unsigned plane_x = (scale.p[i] * x) >> shift.p[i];
        const unsigned plane_y = (scale.p[i] * y) >> shift.p[i];

        size_t ofs = plane_x + plane_y * img->stride.p[i];
        size_t size = getBufferSize(img->data[i]) - ofs;
        cropped_img->data[i] = allocSubBuffer(img->data[i], ofs, size);
    }

    return cropped_img;
}

// ----------------------------------------------------------------------------
const image_t* gfxAssignImage(const image_t* obj)
{
    if (obj != nullptr) {
        ref_inc(&obj->ref);
    }
    return obj;
}

// ----------------------------------------------------------------------------
void gfxFreeImage(const image_t* obj)
{
    if (obj != nullptr) {
        ref_dec(&obj->ref);
    }
}

// ----------------------------------------------------------------------------
const stride_t* gfxImageStride(const image_t* img)
{
    return &img->stride;
}

// ----------------------------------------------------------------------------
const buffer_t* gfxImageData(const image_t* img, unsigned idx)
{
    return (idx < 4) ? img->data[idx] : nullptr;
}

// ----------------------------------------------------------------------------
const image_size_t* gfxImageSize(const image_t* img)
{
    return &img->size;
}

// ----------------------------------------------------------------------------
unsigned gfxImageWidth(const image_t* img)
{
    return img->size.cx;
}

// ----------------------------------------------------------------------------
unsigned gfxImageHeight(const image_t* img)
{
    return img->size.cy;
}

// ----------------------------------------------------------------------------
fourcc_t gfxImageFormat(const image_t* img)
{
    return img->fmt;
}

// ----------------------------------------------------------------------------
void gfxImageForEachPixel(const image_t* img, unsigned idx, gfxFnPixel fnPixel, void* context)
{
    const plane_scale_t shift = gfxPlaneShift(img->fmt);
    const plane_scale_t scale = gfxPlaneScale(img->fmt);

    const uint8_t* data = getBufferData(img->data[idx]);

    const unsigned plane_cx = (scale.p[idx] * img->size.cx) >> shift.p[idx];
    const unsigned plane_cy = (scale.p[idx] * img->size.cy) >> shift.p[idx];

    for (unsigned y = 0; y < plane_cy; ++y) {
        for (unsigned x = 0; x < plane_cx; ++x) {
            fnPixel(x, y, data[x], context);
        }

        data += img->stride.p[idx];
    }
}

// ----------------------------------------------------------------------------
plane_scale_t gfxPlaneShift(fourcc_t fmt)
{
    plane_scale_t shift;
    switch (fmt) {
    default:
        shift.p[0] = 0;
        shift.p[1] = 0;
        shift.p[2] = 0;
        shift.p[3] = 0;
        break;

    case fccYUV12:
        shift.p[0] = 0;
        shift.p[1] = 1;
        shift.p[2] = 1;
        shift.p[3] = 0;
        break;

    case fccYUV9:
        shift.p[0] = 0;
        shift.p[1] = 2;
        shift.p[2] = 2;
        shift.p[3] = 0;
        break;
    }

    return shift;
}

// ----------------------------------------------------------------------------
plane_scale_t gfxPlaneScale(fourcc_t fmt)
{
    plane_scale_t scale;
    switch (fmt) {
    default:
        scale.p[0] = 1;
        scale.p[1] = 1;
        scale.p[2] = 1;
        scale.p[3] = 0;
        break;

    case fccYUV12:
        scale.p[0] = 1;
        scale.p[1] = 1;
        scale.p[2] = 1;
        scale.p[3] = 0;
        break;

    case fccYUV9:
        scale.p[0] = 1;
        scale.p[1] = 1;
        scale.p[2] = 1;
        scale.p[3] = 0;
        break;
    }

    return scale;
}

// ----------------------------------------------------------------------------
stride_t gfxCalcFormatStride(fourcc_t fmt, unsigned cx, unsigned alignment)
{
    assert((alignment & (alignment - 1)) == 0); // Alignment is a power of 2?
    const unsigned mask = ~(alignment - 1);

    const plane_scale_t shift = gfxPlaneShift(fmt);
    const plane_scale_t scale = gfxPlaneScale(fmt);

    stride_t stride;
    for (int i = 0; i < 4; ++i) {
        const size_t plane_cx = (cx * scale.p[i]) >> shift.p[i];
        stride.p[i] = (plane_cx + alignment - 1) & mask;
    }

    return stride;
}

// ----------------------------------------------------------------------------
plane_size_t gfxCalcPlaneSize(fourcc_t fmt, const stride_t* stride, unsigned cy)
{
    const plane_scale_t shift = gfxPlaneShift(fmt);
    const plane_scale_t scale = gfxPlaneScale(fmt);

    plane_size_t psize;
    for (int i = 0; i < 4; ++i) {
        const unsigned plane_cy = (cy * scale.p[i]) >> shift.p[i];
        psize.p[i] = plane_cy * stride->p[i];
    }

    return psize;
}

// ----------------------------------------------------------------------------
struct color_s {
    uint8_t p[4];
};

typedef struct color_s color_t;

// ----------------------------------------------------------------------------
color_t gfxBlackColor(fourcc_t fmt)
{
    color_t color;
    switch (fmt) {
    default:
        color.p[0] = 0;
        color.p[1] = 0;
        color.p[2] = 0;
        color.p[3] = 0;
        break;

    case fccYUV9:
    case fccYUV12:
        color.p[0] = 0;
        color.p[1] = 0x80;
        color.p[2] = 0x80;
        color.p[3] = 0;
        break;
    }
    return color;
}

// ----------------------------------------------------------------------------
const image_t* gfxCreateBlackImage(const image_size_t* size, fourcc_t fmt)
{
    const unsigned cx = size->cx;
    const unsigned cy = size->cy;

    const stride_t stride = gfxCalcFormatStride(fmt, cx, 16u);
    const plane_size_t psize = gfxCalcPlaneSize(fmt, &stride, cy);
    const color_t black = gfxBlackColor(fmt);

    const buffer_t* buf_yuv[4];
    for (int i = 0; i < 4; ++i) {

        size_t buf_size = psize.p[i];

        void* data = nullptr;
        buf_yuv[i] = allocBuffer(buf_size, &data, 16u);

        memset(data, black.p[i], buf_size);
    }

    return gfxCreateImage(&stride, size, fmt, buf_yuv);
}
