/*
 * MIT License
 *
 * Copyright (c) 2021 Steffen Schulze
 */

#pragma once

#include "cutil_refobj.h"

#include "rtc-api/packet_source.h"

// ----------------------------------------------------------------------------
typedef void (*pcksrcFnConnect)(void*, packet_sink_t*, packet_sink_t**);
typedef void (*pcksrcFnQoSChanged)(void*, const network_qos_params_t*);

// ----------------------------------------------------------------------------
struct packet_source_s {
    void* context;
    ptrFnAssign fnAssign;
    ptrFnRelease fnRelease;
    pcksrcFnConnect fnConnect;
    pcksrcFnQoSChanged fnQoSChanged;
};

// ----------------------------------------------------------------------------
packet_source_t* pcksrcInit(
    packet_source_t* obj,
    void* context,
    ptrFnAssign fnAssign,
    ptrFnRelease fnRelease,
    pcksrcFnConnect fnConnect,
    pcksrcFnQoSChanged fnQoSChanged);
