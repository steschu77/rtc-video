/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */

#include <stdlib.h>
#include <string.h>

#include "cutil_nullptr.h"

#include "media_control_impl.h"

// ----------------------------------------------------------------------------
media_control_t* mediactrlInit(
    media_control_t* obj,
    void* context,
    ptrFnAssign fnAssign,
    ptrFnRelease fnRelease,
    mediactrlFnRun fnRun)
{
    obj->context = context;
    obj->fnAssign = fnAssign;
    obj->fnRelease = fnRelease;
    obj->fnRun = fnRun;
    return obj;
}

// ----------------------------------------------------------------------------
media_control_t* mediactrlCopy(media_control_t* obj)
{
    obj->fnAssign(obj->context);
    return obj;
}

// ----------------------------------------------------------------------------
void mediactrlRelease(const media_control_t* obj)
{
    obj->fnRelease(obj->context);
}

// ----------------------------------------------------------------------------
void mediactrlAssign(media_control_t* obj, media_control_t** pdst)
{
    media_control_t* dst = *pdst;
    if (dst != nullptr) {
        dst->fnRelease(dst->context);
    }
    if (obj != nullptr) {
        obj->fnAssign(obj->context);
    }
    *pdst = obj;
}

// ----------------------------------------------------------------------------
void mediactrlStart(media_control_t* obj, const image_caps_t* caps)
{
    obj->fnRun(obj->context, caps);
}
