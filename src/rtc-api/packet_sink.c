/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */

#include <stdlib.h>
#include <string.h>

#include "cutil_nullptr.h"

#include "packet_sink_impl.h"

// ----------------------------------------------------------------------------
packet_sink_t* pcksinkInit(
    packet_sink_t* obj,
    void* context,
    ptrFnAssign fnAssign,
    ptrFnRelease fnRelease,
    pcksinkFnDeliverPacket fnDeliverPacket)
{
    obj->context = context;
    obj->fnAssign = fnAssign;
    obj->fnRelease = fnRelease;
    obj->fnDeliverPacket = fnDeliverPacket;

    return obj;
}

// ----------------------------------------------------------------------------
packet_sink_t* pcksinkCopy(packet_sink_t* obj)
{
    obj->fnAssign(obj->context);
    return obj;
}

// ----------------------------------------------------------------------------
void pcksinkRelease(const packet_sink_t* obj)
{
    obj->fnRelease(obj->context);
}

// ----------------------------------------------------------------------------
void pcksinkAssign(packet_sink_t* obj, packet_sink_t** pdst)
{
    packet_sink_t* dst = *pdst;
    if (dst != nullptr) {
        dst->fnRelease(dst->context);
    }
    if (obj != nullptr) {
        obj->fnAssign(obj->context);
    }
    *pdst = obj;
}

// ----------------------------------------------------------------------------
void pcksinkDeliverPacket(packet_sink_t* sink, media_clock_t t, const packet_t* packet)
{
    sink->fnDeliverPacket(sink->context, t, packet);
}
