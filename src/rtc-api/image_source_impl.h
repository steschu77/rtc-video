/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */

#pragma once

#include "cutil_refobj.h"

#include "rtc-api/image_source.h"

// ----------------------------------------------------------------------------
typedef void (*imgsrcFnConnect)(void*, image_sink_t*);
typedef void (*imgsrcFnChangeCaps)(void*, const image_caps_t*);

// ----------------------------------------------------------------------------
struct image_source_s {
    void* context;
    ptrFnAssign fnAssign;
    ptrFnRelease fnRelease;
    imgsrcFnConnect fnConnect;
    imgsrcFnChangeCaps fnChangeCaps;
};

// ----------------------------------------------------------------------------
image_source_t* imgsrcInit(
    image_source_t* obj,
    void* context,
    ptrFnAssign fnAssign,
    ptrFnRelease fnRelease,
    imgsrcFnConnect fnConnect,
    imgsrcFnChangeCaps fnChangeCaps);
