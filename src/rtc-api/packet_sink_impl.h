/*
 * MIT License
 *
 * Copyright (c) 2020 Steffen Schulze
 */

#pragma once

#include "cutil_refobj.h"

#include "rtc-api/packet_sink.h"

// ----------------------------------------------------------------------------
typedef void (*pcksinkFnDeliverPacket)(void*, media_clock_t t, const packet_t* pck);

// ----------------------------------------------------------------------------
struct packet_sink_s {
    void* context;
    ptrFnAssign fnAssign;
    ptrFnRelease fnRelease;
    pcksinkFnDeliverPacket fnDeliverPacket;
};

// ----------------------------------------------------------------------------
packet_sink_t* pcksinkInit(
    packet_sink_t* obj,
    void* context,
    ptrFnAssign fnAssign,
    ptrFnRelease fnRelease,
    pcksinkFnDeliverPacket fnDeliverPacket);
