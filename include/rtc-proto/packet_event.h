/*
 * MIT License
 *
 * Copyright (c) 2021 Steffen Schulze
 */

#pragma once

#include <stdint.h>

#include "rtc-api/media_clock.h"
#include "rtc-proto/packet.h"

struct packet_event_s;
typedef struct packet_event_s packet_event_t;

// ----------------------------------------------------------------------------
packet_event_t* packeteventCreate(direction_t dir, uint64_t seqno, packet_event_type_t type, media_clock_t t);
void packeteventRelease(const void* ptr);
packet_event_t* packeteventAssign(packet_event_t*);
const packet_event_t* packeteventConstAssign(const packet_event_t*);

// ----------------------------------------------------------------------------
int packeteventCompareSeqNo(const void* x0, const void* x1);
int packeteventCompareTime(const void* x0, const void* x1);
int packeteventPredicateSeqNo(const void* context, const void* item);

// ----------------------------------------------------------------------------
packet_event_type_t packeteventGetType(const packet_event_t*);
media_clock_t packeteventGetTime(const packet_event_t*);
direction_t packeteventGetDirection(const packet_event_t*);
uint64_t packeteventGetSeqNo(const packet_event_t*);
