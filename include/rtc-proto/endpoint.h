/*
 * MIT License
 *
 * Copyright (c) 2021 Steffen Schulze
 */

#pragma once

#include <stdint.h>

#include "ack_frame.h"

// ----------------------------------------------------------------------------
typedef struct endpoint_s endpoint_t;

// ----------------------------------------------------------------------------
typedef void (*FnAckPacket)(void*, uint64_t);

// ----------------------------------------------------------------------------
endpoint_t* epCreate(FnAckPacket fnAckPacket, void* context);
endpoint_t* epDestroy(const endpoint_t* obj);

const ack_frame_t* epPreparePacket(endpoint_t* obj, uint64_t txseqno);
void epPacketReceived(endpoint_t*, uint64_t rxseqno, const ack_frame_t* acks);
