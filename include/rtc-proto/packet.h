/*
 * MIT License
 *
 * Copyright (c) 2020 Steffen Schulze
 */

#pragma once

#include <stdint.h>

#include "frame.h"
#include "rtc-api/media_clock.h"

// ----------------------------------------------------------------------------
enum direction_e {
    dir_send_recv,
    dir_recv_send,
};

typedef enum direction_e direction_t;

// ----------------------------------------------------------------------------
enum packet_state_e {
    packet_sent,
    packet_rcvd,
};

typedef enum packet_state_e packet_state_t;

// ----------------------------------------------------------------------------
enum packet_event_type_e {
    packet_send,
    packet_recv,
};

typedef enum packet_event_type_e packet_event_type_t;

// ----------------------------------------------------------------------------
struct packet_s;
typedef struct packet_s packet_t;

// ----------------------------------------------------------------------------
packet_t* packetCreate(direction_t dir, uint64_t seqno);
packet_t* packetRelease(const packet_t*);
packet_t* packetAssign(packet_t*);
const packet_t* packetConstAssign(const packet_t*);

int packetCompare(const void* x0, const void* x1);
int packetPredicate(const void* context, const void* item);

// ----------------------------------------------------------------------------
typedef int (*frmFnAckFrame)(void*, const packet_t*, const ack_frame_t*);
typedef int (*frmFnCapsFrame)(void*, const packet_t*, const caps_frame_t*);
typedef int (*frmFnVideoFrame)(void*, const packet_t*, const video_frame_t*);

typedef struct frame_enum_s {
    frmFnAckFrame fnAckFrame;
    frmFnCapsFrame fnCapsFrame;
    frmFnVideoFrame fnVideoFrame;
} frame_enum_t;

void packetEnumFrames(const packet_t*, void*, const frame_enum_t*);

// ----------------------------------------------------------------------------
void packetAddFrame(packet_t*, const frame_t* frame, size_t length);
void packetAddAckFrame(packet_t*, const ack_frame_t* frame);
void packetAddCapsFrame(packet_t* obj, const caps_frame_t* frame);
void packetAddVideoFrame(packet_t*, const video_frame_t* frame);

void packetAddTime(packet_t*, packet_event_type_t type, media_clock_t time);

direction_t packetGetDirection(const packet_t*);
uint64_t packetGetSeqNo(const packet_t*);
media_clock_t packetGetTime(const packet_t*, packet_event_type_t type);
uint64_t packetGetState(const packet_t*);
size_t packetGetLength(const packet_t*);
