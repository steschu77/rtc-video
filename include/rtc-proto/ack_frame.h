/*
 * MIT License
 *
 * Copyright (c) 2021 Steffen Schulze
 */

#pragma once

#include <stdint.h>

#include "cutil_vecu64.h"

// ----------------------------------------------------------------------------
typedef struct frame_s frame_t;
typedef struct ack_frame_s ack_frame_t;

// ----------------------------------------------------------------------------
ack_frame_t* ackframeCreate(const vecu64_t* acks);
ack_frame_t* ackframeAssign(ack_frame_t*);
ack_frame_t* ackframeRelease(const ack_frame_t*);

const frame_t* ackframeGetFrame(const ack_frame_t*);
size_t ackframeGetLength(const ack_frame_t*);
const vecu64_t* ackframeGetAcks(const ack_frame_t*);
