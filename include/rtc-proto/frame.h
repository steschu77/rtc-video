/*
 * MIT License
 *
 * Copyright (c) 2021 Steffen Schulze
 */

#pragma once

#include <stdint.h>

// ----------------------------------------------------------------------------
enum frame_type_e {
    ft_ack_frame,
    ft_caps_frame,
    ft_video_frame,
};

typedef enum frame_type_e frame_type_t;

// ----------------------------------------------------------------------------
typedef struct frame_s frame_t;
typedef struct ack_frame_s ack_frame_t;
typedef struct video_frame_s video_frame_t;
typedef struct caps_frame_s caps_frame_t;

const frame_t* frameAssign(const frame_t* obj);
frame_t* frameRelease(const frame_t* obj);

frame_type_t frameGetType(const frame_t*);
