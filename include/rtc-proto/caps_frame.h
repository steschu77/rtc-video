/*
 * MIT License
 *
 * Copyright (c) 2021 Steffen Schulze
 */

#pragma once

#include <stdint.h>

#include "rtc-api/video_caps.h"

// ----------------------------------------------------------------------------
typedef struct frame_s frame_t;
typedef struct caps_frame_s caps_frame_t;

// ----------------------------------------------------------------------------
caps_frame_t* capsframeCreate(uint64_t seqid, const video_caps_t* caps);
const caps_frame_t* capsframeConstAssign(const caps_frame_t*);
caps_frame_t* capsframeAssign(caps_frame_t*);
caps_frame_t* capsframeRelease(const caps_frame_t*);

const frame_t* capsframeGetFrame(const caps_frame_t*);
size_t capsframeGetLength(const caps_frame_t*);
uint64_t capsframeGetSeqid(const caps_frame_t*);
const video_caps_t* capsframeGetCaps(const caps_frame_t*);
