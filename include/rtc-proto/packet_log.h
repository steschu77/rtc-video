/*
 * MIT License
 *
 * Copyright (c) 2021 Steffen Schulze
 */

#pragma once

#include <stdint.h>

#include "cutil_byteread.h"
#include "cutil_bytewrite.h"
#include "cutil_file.h"

#include "rtc-proto/packet.h"
#include "rtc-proto/packet_event.h"

// ----------------------------------------------------------------------------
packet_t* packetParse(byteread_t* packetReader);
int packetWrite(const packet_t* obj, bytewrite_t* packetWriter);

// ----------------------------------------------------------------------------
typedef int (*packetlogFnPacket)(void*, const packet_t* packet);
typedef int (*packetlogFnEvent)(void*, const packet_event_t* event);

// ----------------------------------------------------------------------------
void packetlogWritePacketData(utilsFile* log, const packet_t* packet);
void packetlogWritePacketEvent(utilsFile* log, const packet_event_t* event);

int packetlogParseChunk(byteread_t* fileReader, void* ctx, packetlogFnPacket fnPacket, packetlogFnEvent fnEvent);
