/*
 * MIT License
 *
 * Copyright (c) 2021 Steffen Schulze
 */

#pragma once

#include <stdint.h>

#include "rtc-api/video_picture.h"

#include "cutil_buffer.h"

// ----------------------------------------------------------------------------
typedef struct frame_s frame_t;
typedef struct video_frame_s video_frame_t;

// ----------------------------------------------------------------------------
enum video_frame_flag_e {
    flag_pic_begin = 1,
    flag_pic_end = 2,
};

typedef enum video_frame_flag_e video_frame_flag_t;

// ----------------------------------------------------------------------------
video_frame_t* videoframeCreate(uint64_t seqno, uint64_t picid, uint64_t flags,
    media_clock_t ts, const buffer_t* data, ref_type_t type, uint64_t psnr);
video_frame_t* videoframeAssign(video_frame_t* obj);
video_frame_t* videoframeRelease(const video_frame_t* obj);

int videoFrameCompare(const void* x0, const void* x1);

const frame_t* videoframeGetFrame(const video_frame_t*);
size_t videoframeGetLength(const video_frame_t*);
uint64_t videoframeGetSeqNo(const video_frame_t*);
uint64_t videoframeGetPicId(const video_frame_t*);
uint64_t videoframeGetFlags(const video_frame_t*);
media_clock_t videoframeGetPresentationTime(const video_frame_t*);
const buffer_t* videoframeGetData(const video_frame_t*);
ref_type_t videoframeGetRefType(const video_frame_t*);
uint64_t videoframeGetPSNR(const video_frame_t*);
