/*
 * MIT License
 *
 * Copyright (c) 2020 Steffen Schulze
 */

#pragma once

#include "rtc-api/media_clock.h"
#include "task.h"

// ----------------------------------------------------------------------------
struct task_queue_s;
typedef struct task_queue_s task_queue_t;

// ----------------------------------------------------------------------------
task_queue_t* taskQueueCreate();
task_queue_t* taskqueueAssign(task_queue_t* obj);
task_queue_t* taskqueueRelease(const task_queue_t* obj);

int taskqueueProcessNextEvent(task_queue_t* obj);
void taskqueueScheduleEvent(task_queue_t* obj, task_t* task);
media_clock_t taskqueueTime(const task_queue_t* obj);
