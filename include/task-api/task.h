/*
 * MIT License
 *
 * Copyright (c) 2020 Steffen Schulze
 */

#pragma once

#include "rtc-api/media_clock.h"

// ----------------------------------------------------------------------------
struct task_s;
typedef struct task_s task_t;

// ----------------------------------------------------------------------------
typedef void (*taskFnRun)(void*, media_clock_t);

// ----------------------------------------------------------------------------
int taskCompareTime(const void*, const void*);

task_t* taskAssign(task_t* obj);
task_t* taskRelease(const task_t* obj);

void taskRun(const task_t* obj);
media_clock_t taskTime(const task_t* obj);

// ----------------------------------------------------------------------------
typedef void (*FnRunTask1p)(media_clock_t, void*);
typedef void (*FnRunTask2p)(media_clock_t, void*, void*);

// ----------------------------------------------------------------------------
task_t* task1pCreate(media_clock_t t, FnRunTask1p fnRun, void* p0);
task_t* task2pCreate(media_clock_t t, FnRunTask2p fnRun, void* p0, void* p1);
