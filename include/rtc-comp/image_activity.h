/*
 * MIT License
 *
 * Copyright (c) 2022 Steffen Schulze
 */

#pragma once

#include "cutil_file.h"

#include "rtc-api/image_sink.h"
#include "rtc-api/image_source.h"

// ----------------------------------------------------------------------------
// Image dynamics calculator

struct image_dynamics_s;
typedef struct image_dynamics_s image_dynamics_t;

image_dynamics_t* imgactivityCreate(image_source_t* imageSource, utilsFile* file);
image_source_t* imgactivityQueryImageSource(image_dynamics_t* obj);
