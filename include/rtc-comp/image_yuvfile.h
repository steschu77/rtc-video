/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */

#pragma once

#include "cutil_file.h"

#include "rtc-api/image_source.h"

#include "task-api/task_queue.h"

// ----------------------------------------------------------------------------
// YUV file parser

struct imgyuv_parser_s;
typedef struct imgyuv_parser_s imgyuv_parser_t;

imgyuv_parser_t* yuvparseCreate(task_queue_t* tasks, const utf8str_t* seqName, unsigned frameLimit);
image_source_t* yuvparseQueryImageSource(imgyuv_parser_t* obj);

// ----------------------------------------------------------------------------
// YUV file writer

struct imgyuv_writer_s;
typedef struct imgyuv_writer_s imgyuv_writer_t;

imgyuv_writer_t* yuvwriteCreate(image_source_t* imageSource, const utf8str_t* seqName);
image_source_t* yuvwriteQueryImageSource(imgyuv_writer_t* obj);
