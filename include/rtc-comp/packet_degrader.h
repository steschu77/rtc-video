/*
 * MIT License
 *
 * Copyright (c) 2021 Steffen Schulze
 */

#pragma once

#include "rtc-api/media_bitrate.h"
#include "rtc-api/packet_source.h"
#include "task-api/task_queue.h"

// ----------------------------------------------------------------------------
struct packet_degrader_s;
typedef struct packet_degrader_s packet_degrader_t;

packet_degrader_t* pckdegraderCreate(task_queue_t* taskQueue, packet_source_t* packetSource, const network_qos_params_t* qos, utilsFile* logFile);
packet_source_t* pckdegraderQueryPacketSource(packet_degrader_t* obj);
