/*
 * MIT License
 *
 * Copyright (c) 2020 Steffen Schulze
 */

#pragma once

#include "rtc-api/packet_sink.h"
#include "rtc-api/packet_source.h"
#include "rtc-api/video_sink.h"
#include "rtc-api/video_source.h"
#include "task-api/task_queue.h"

// ----------------------------------------------------------------------------
struct video_receiver_s;
typedef struct video_receiver_s video_receiver_t;

video_receiver_t* videorecvCreate(task_queue_t* taskQueue, packet_source_t* packetSource);
video_source_t* videorecvQueryVideoSource(video_receiver_t* obj);
