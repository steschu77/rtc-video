/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */

#pragma once

#include "cutil_file.h"

#include "rtc-api/image_sink.h"
#include "rtc-api/image_source.h"
#include "rtc-api/video_sink.h"
#include "rtc-api/video_source.h"

// ----------------------------------------------------------------------------
// VP9 Decoder

struct vp9dec_s;
typedef struct vp9dec_s vp9dec_t;

vp9dec_t* vp9decCreate(video_source_t* videoSource, const video_caps_t* caps);
image_source_t* vp9decQueryImageSource(vp9dec_t* obj);

// ----------------------------------------------------------------------------
// VP9 Encoder

struct vp9enc_s;
typedef struct vp9enc_s vp9enc_t;

// ----------------------------------------------------------------------------
// VP9 Encoder Configuration

typedef enum vp9enc_gop_e {
  e_IP,     // no temporal layer
  e_IpP,    // 1 temporal layer, frame-rate ratio 1:1
  e_IbP,    // 1 temporal layer, frame-rate ratio 1:1, bi-prediction (1 frame delay)
  e_IppP,   // 1 temporal layer, frame-rate ratio 1:2
  e_IbbP,   // 1 temporal layer, frame-rate ratio 1:2, bi-prediction (2 frames delay)
  e_IpPpP,  // 2 temporal layers, frame-rate ratio 1:1:1
  e_IbPbP,  // 2 temporal layers, frame-rate ratio 1:1:1, bi-prediction (1 frame delay)
  e_IbBbP,  // 2 temporal layers, frame-rate ratio 1:1:1, bi-prediction (3 frames delay)
} vp9enc_gop_t;

typedef enum vp9enc_syncmode_e {
    e_keyframe,
    e_syncframe,
    e_syncmode_count
} vp9enc_syncmode_t;

typedef struct vp9enc_config_s {
    vp9enc_gop_t gopType;
    vp9enc_syncmode_t syncmode;
} vp9enc_config_t;

vp9enc_t* vp9encCreate(image_source_t* imageSource, const vp9enc_config_t* cfg);
video_source_t* vp9encQueryVideoSource(vp9enc_t* enc);

// ----------------------------------------------------------------------------
// VP9 Analyzer

struct vp9analyze_s;
typedef struct vp9analyze_s vp9analyze_t;

vp9analyze_t* vp9analyzeCreate(video_source_t* videoSource, utilsFile* file);
video_source_t* vp9analyzeQueryVideoSource(vp9analyze_t* obj);
