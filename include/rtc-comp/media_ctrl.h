/*
 * MIT License
 *
 * Copyright (c) 2021 Steffen Schulze
 */

#pragma once

#include "rtc-api/media_control.h"

// ----------------------------------------------------------------------------
// Image Media Controller

struct img_mediactrl_s;
typedef struct img_mediactrl_s img_mediactrl_t;

img_mediactrl_t* imgctrlCreate(image_source_t* imageSource);
img_mediactrl_t* imgctrlAssign(img_mediactrl_t* obj);
img_mediactrl_t* imgctrlRelease(const img_mediactrl_t* obj);

media_control_t* imgctrlQueryMediaControl(img_mediactrl_t* obj);
