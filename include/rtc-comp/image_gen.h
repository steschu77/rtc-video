/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */

#pragma once

#include "rtc-api/image_sink.h"
#include "rtc-api/image_source.h"
#include "rtc-api/media_control.h"
#include "task-api/task_queue.h"

// ----------------------------------------------------------------------------
// Image generator

struct image_gen_s;
typedef struct image_gen_s image_gen_t;

image_gen_t* imggenCreate(task_queue_t* tasks, media_clock_t freq, unsigned maxPics);
image_source_t* imggenQueryImageSource(image_gen_t* obj);

// ----------------------------------------------------------------------------
// Image verifier

struct imgverify_s;
typedef struct imgverify_s imgverify_t;

imgverify_t* imgvfyCreate(image_source_t* imageSource);
image_source_t* imgvfyQueryImageSource(imgverify_t* obj);
