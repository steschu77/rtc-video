/*
 * MIT License
 *
 * Copyright (c) 2021 Steffen Schulze
 */

#pragma once

#include "cutil_file.h"

#include "rtc-api/video_source.h"

#include "task-api/task_queue.h"

// ----------------------------------------------------------------------------
struct vidivf_writer_s;
typedef struct vidivf_writer_s vidivf_writer_t;

vidivf_writer_t* ivfwriteCreate(video_source_t* videoSource, utilsFile* file);
video_source_t* ivfwriteQueryVideoSource(vidivf_writer_t* obj);

// ----------------------------------------------------------------------------
struct vidivf_parser_s;
typedef struct vidivf_parser_s vidivf_parser_t;

vidivf_parser_t* ivfparseCreate(task_queue_t* tasks, utilsFile* file);
video_source_t* ivfparseQueryVideoSource(vidivf_parser_t* obj);
