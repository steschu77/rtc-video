/*
 * MIT License
 *
 * Copyright (c) 2020 Steffen Schulze
 */

#pragma once

#include "rtc-api/video_sink.h"
#include "rtc-api/video_source.h"
#include "rtc-api/packet_sink.h"
#include "rtc-api/packet_source.h"
#include "task-api/task_queue.h"

// ----------------------------------------------------------------------------
typedef struct video_send_params_s {
    unsigned rtx;
} video_send_params_t;

// ----------------------------------------------------------------------------
struct video_sender_s;
typedef struct video_sender_s video_sender_t;

video_sender_t* videosendCreate(const video_send_params_t* params, task_queue_t* tasks, video_source_t* videoSource);
packet_source_t* videosendQueryPacketSource(video_sender_t* obj);
