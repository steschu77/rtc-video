/*
 * MIT License
 *
 * Copyright (c) 2021 Steffen Schulze
 */

#pragma once

// ----------------------------------------------------------------------------
typedef int lossrate_t;

#define make_percent(percent) (lossrate_t)((double)(percent)*0xffff / 100)
