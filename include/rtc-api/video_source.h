/*
 * MIT License
 *
 * Copyright (c) 2021 Steffen Schulze
 */

#pragma once

#include "media_bitrate.h"
#include "media_id.h"
#include "video_caps.h"
#include "video_sink.h"

// ----------------------------------------------------------------------------
struct video_source_s;
typedef struct video_source_s video_source_t;

// ----------------------------------------------------------------------------
video_source_t* videosrcCopy(video_source_t*);
void videosrcRelease(const video_source_t*);
void videosrcAssign(video_source_t*, video_source_t**);

// ----------------------------------------------------------------------------
void videosrcConnect(video_source_t*, video_sink_t*);
void videosrcChangeCaps(video_source_t*, const video_caps_t*);

void videosrcRequestSyncPoint(video_source_t*);
void videosrcNotifyPictureProcessed(video_source_t*, const media_id_t);
void videosrcNotifyPictureLost(video_source_t*, const media_id_t);
