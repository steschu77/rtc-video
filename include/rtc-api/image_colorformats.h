/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */

#pragma once

#include "fourcc.h"

// clang-format off
#define fccYUY2     make_fourcc('Y', 'U', 'Y', '2')
#define fccYUV9     make_fourcc('Y', 'U', 'V', '9')
#define fccYUV12    make_fourcc('Y', 'U', '1', '2')
// clang-format on
