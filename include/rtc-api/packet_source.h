/*
 * MIT License
 *
 * Copyright (c) 2021 Steffen Schulze
 */

#pragma once

#include "packet_sink.h"

#include "media_clock.h"
#include "media_bitrate.h"
#include "packet_lossrate.h"

// ----------------------------------------------------------------------------
struct network_qos_params_s {
    media_clock_t delay;
    bitrate_t bitrate;
    lossrate_t loss;
};
typedef struct network_qos_params_s network_qos_params_t;

// ----------------------------------------------------------------------------
struct packet_source_s;
typedef struct packet_source_s packet_source_t;

// ----------------------------------------------------------------------------
packet_source_t* pcksrcCopy(packet_source_t*);
void pcksrcRelease(const packet_source_t*);
void pcksrcAssign(packet_source_t*, packet_source_t**);

// ----------------------------------------------------------------------------
void pcksrcConnect(packet_source_t*, packet_sink_t*, packet_sink_t**);
void pcksrcQoSChanged(packet_source_t*, const network_qos_params_t*);
