/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */

#pragma once

#include "image_caps.h"
#include "image_sink.h"

// ----------------------------------------------------------------------------
struct image_source_s;
typedef struct image_source_s image_source_t;

// ----------------------------------------------------------------------------
image_source_t* imgsrcCopy(image_source_t*);
void imgsrcRelease(const image_source_t*);
void imgsrcAssign(image_source_t*, image_source_t**);

// ----------------------------------------------------------------------------
void imgsrcConnect(image_source_t*, image_sink_t*);
void imgsrcChangeCaps(image_source_t*, const image_caps_t*);
