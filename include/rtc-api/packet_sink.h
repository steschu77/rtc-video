/*
 * MIT License
 *
 * Copyright (c) 2020 Steffen Schulze
 */

#pragma once

#include "media_clock.h"
#include "rtc-proto/packet.h"

// ----------------------------------------------------------------------------
struct packet_sink_s;
typedef struct packet_sink_s packet_sink_t;

// ----------------------------------------------------------------------------
packet_sink_t* pcksinkCopy(packet_sink_t*);
void pcksinkRelease(const packet_sink_t*);
void pcksinkAssign(packet_sink_t*, packet_sink_t**);

// ----------------------------------------------------------------------------
void pcksinkDeliverPacket(packet_sink_t*, media_clock_t, const packet_t*);
