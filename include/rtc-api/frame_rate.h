/*
 * MIT License
 *
 * Copyright (c) 2021 Steffen Schulze
 */

#pragma once

// ----------------------------------------------------------------------------
struct frame_rate_s {
    unsigned den;
    unsigned num;
};

typedef struct frame_rate_s frame_rate_t;
