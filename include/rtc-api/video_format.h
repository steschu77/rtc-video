/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */

#pragma once

#include <stdint.h>

#include "frame_rate.h"
#include "image_size.h"
#include "video_procrate.h"
#include "fourcc.h"

struct video_format_s;
typedef struct video_format_s video_format_t;

const video_format_t* createVideoFormat(
    const frame_rate_t* max_framerate, const image_size_t* resolution, fourcc_t codec, procrate_t max_procrate);
const video_format_t* assignVideoFormat(const video_format_t*);
void freeVideoFormat(const video_format_t*);

const frame_rate_t* videofmtGetMaxFrameRate(const video_format_t*);
const image_size_t* videofmtGetResolution(const video_format_t*);
fourcc_t videofmtGetCodec(const video_format_t*);
uint64_t videofmtGetMaxProcRate(const video_format_t*);
