/*
 * MIT License
 *
 * Copyright (c) 2021 Steffen Schulze
 */

#pragma once

#include "image_caps.h"
#include "media_bitrate.h"

// ----------------------------------------------------------------------------
struct video_caps_s {
    image_caps_t imgcaps;
    bitrate_t max_bitrate;
};

typedef struct video_caps_s video_caps_t;
