/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */

#pragma once

#include "mute_state.h"
#include "image_size.h"
#include "frame_rate.h"
#include "video_procrate.h"

struct image_caps_s {
    mute_state_t mute;
    image_size_t preferred_res;
    frame_rate_t preferred_rate;
    procrate_t max_procrate;
};

typedef struct image_caps_s image_caps_t;

/*
// ----------------------------------------------------------------------------
struct image_caps_s;
typedef struct image_caps_s image_caps_t;

// ----------------------------------------------------------------------------
const image_size_t* getMaxResolution(const image_caps_t*);
unsigned getMaxFramerate(const image_caps_t*);
procrate_t getMaxProcRate(const image_caps_t*);

// ----------------------------------------------------------------------------
// move to impl
const image_caps_t* imgcapsCreate(const image_size_t* max_res, procrate_t max_procrate);
void imgcapsDestroy(const image_caps_t*);
*/