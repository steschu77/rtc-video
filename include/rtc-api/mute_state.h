/*
 * MIT License
 *
 * Copyright (c) 2021 Steffen Schulze
 */

#pragma once

// ----------------------------------------------------------------------------
enum mute_state_e {
    muted,
    unmuted
};

typedef enum mute_state_e mute_state_t;
