/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */

#pragma once

#include "media_clock.h"
#include "video_picture.h"
#include "video_format.h"

// ----------------------------------------------------------------------------
struct video_sink_s;
typedef struct video_sink_s video_sink_t;

typedef media_id_t seq_id_t;

// ----------------------------------------------------------------------------
video_sink_t* videosinkCopy(video_sink_t*);
void videosinkRelease(const video_sink_t*);
void videosinkAssign(video_sink_t*, video_sink_t**);

// ----------------------------------------------------------------------------
void videosinkStartSeq(video_sink_t*, media_clock_t, seq_id_t, const video_format_t*);
void videosinkFinishSeq(video_sink_t*, media_clock_t);
void videosinkDeliverPic(video_sink_t*, media_clock_t, const video_pic_t*);
