/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */

#pragma once

#include <stdint.h>

typedef uint64_t media_id_t;
typedef struct media_ids_s media_ids_t;

media_ids_t* createMediaIds(unsigned capacity, unsigned increase);
void destroyMediaIds(const media_ids_t*);

void addMediaId(media_ids_t*, media_id_t id);
void mergeMediaIds(media_ids_t*, const media_ids_t* src_ids);

unsigned getNumIds(const media_ids_t*);
media_id_t getId(const media_ids_t*, unsigned idx);
