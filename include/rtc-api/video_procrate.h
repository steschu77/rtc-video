/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */

#pragma once

#include <stdint.h>

typedef uint64_t procrate_t;

#define make_MBps(fps, cx, cy) ((fps) * ((((procrate_t)(cx) + 15) & (~15)) / 16) * ((((procrate_t)(cy) + 15) & (~15)) / 16))
