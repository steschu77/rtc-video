/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */

#pragma once

#include "media_clock.h"
#include "image_picture.h"

// ----------------------------------------------------------------------------
struct image_sink_s;
typedef struct image_sink_s image_sink_t;

// ----------------------------------------------------------------------------
image_sink_t* imgsinkCopy(image_sink_t*);
void imgsinkRelease(const image_sink_t*);
void imgsinkAssign(image_sink_t*, image_sink_t**);

// ----------------------------------------------------------------------------
void imgsinkDeliverPicture(image_sink_t*, media_clock_t, const image_t*);
