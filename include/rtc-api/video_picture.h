/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */

#pragma once

#include <stdint.h>

#include "cutil_buffer.h"

#include "media_id.h"
#include "media_clock.h"

// ----------------------------------------------------------------------------
struct video_pic_s;
typedef struct video_pic_s video_pic_t;

typedef media_id_t pic_id_t;
typedef media_ids_t pic_ids_t;

// ----------------------------------------------------------------------------
enum ref_type_e {
    ref_keyframe,
    ref_syncframe,
    ref_refframe,
    ref_idleframe,
    ref_endframe
};

typedef enum ref_type_e ref_type_t;

const video_pic_t* createVideoPicture(pic_id_t id, media_clock_t ts,
    const buffer_t* data, ref_type_t type, uint64_t psnr);
const video_pic_t* videopicAssign(const video_pic_t* obj);
const video_pic_t* videopicRelease(const video_pic_t* obj);

pic_id_t getVideoPictureId(const video_pic_t*);
media_clock_t getVideoPresentationTime(const video_pic_t*);
const buffer_t* getVideoPictureData(const video_pic_t*);
ref_type_t getVideoPictureType(const video_pic_t*);
uint64_t getVideoPicturePSNR(const video_pic_t* pic);
