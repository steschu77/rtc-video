/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */

#pragma once

// ----------------------------------------------------------------------------
struct image_size_s {
    unsigned cx;
    unsigned cy;
};

typedef struct image_size_s image_size_t;
