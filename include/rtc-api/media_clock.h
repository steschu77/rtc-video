/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */

#pragma once

#include <stdint.h>

typedef uint64_t media_clock_t;

#define clock_freq (90000u)
#define make_sec(sec) ((media_clock_t)((sec) * 90000u))
#define make_msec(msec) ((media_clock_t)((msec) * 90u))
