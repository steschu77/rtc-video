/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */

#pragma once

#include "media_clock.h"

// ----------------------------------------------------------------------------
struct media_control_s;
typedef struct media_control_s media_control_t;

// ----------------------------------------------------------------------------
media_control_t* mediactrlCopy(media_control_t*);
void mediactrlRelease(const media_control_t*);
void mediactrlAssign(media_control_t*, media_control_t**);

void mediactrlStart(media_control_t*, const image_caps_t*);
