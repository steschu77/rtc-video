/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */

#pragma once

#include "cutil_buffer.h"

#include "image_size.h"
#include "image_colorformats.h"

// ----------------------------------------------------------------------------
struct plane_size_s {
    size_t p[4];
};

// ----------------------------------------------------------------------------
struct plane_scale_s {
    unsigned p[4];
};

// ----------------------------------------------------------------------------
struct stride_s {
    size_t p[4];
};

// ----------------------------------------------------------------------------
struct image_s;

typedef struct image_size_s image_size_t;
typedef struct plane_size_s plane_size_t;
typedef struct plane_scale_s plane_scale_t;
typedef struct stride_s stride_t;
typedef struct image_s image_t;

// ----------------------------------------------------------------------------
typedef void (*gfxFnPixel)(unsigned x, unsigned y, unsigned value, void* context);

// ----------------------------------------------------------------------------
const image_t* gfxCreateImage(const stride_t* stride, const image_size_t* size, fourcc_t fmt, const buffer_t* data[4]);
const image_t* gfxAssignImage(const image_t* obj);
void gfxFreeImage(const image_t* obj);

// ----------------------------------------------------------------------------
const stride_t* gfxImageStride(const image_t* img);
const buffer_t* gfxImageData(const image_t* img, unsigned idx);
const image_size_t* gfxImageSize(const image_t* img);
unsigned gfxImageWidth(const image_t* img);
unsigned gfxImageHeight(const image_t* img);
fourcc_t gfxImageFormat(const image_t* img);

const image_t* gfxCropImage(const image_t* img, unsigned x, unsigned y, const image_size_t* size);
void gfxImageForEachPixel(const image_t* img, unsigned idx, gfxFnPixel fnPixel, void* context);

    // ----------------------------------------------------------------------------
plane_scale_t gfxPlaneShift(fourcc_t fmt);
plane_scale_t gfxPlaneScale(fourcc_t fmt);

stride_t gfxCalcFormatStride(fourcc_t fmt, unsigned cx, unsigned alignment);
plane_size_t gfxCalcPlaneSize(fourcc_t fmt, const stride_t* stride, unsigned cy);

// ----------------------------------------------------------------------------
const image_t* gfxCreateBlackImage(const image_size_t* size, fourcc_t fmt);
