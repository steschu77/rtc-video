/*
 * MIT License
 *
 * Copyright (c) 2021 Steffen Schulze
 */

#pragma once

#include <stdint.h>

typedef uint64_t bitrate_t;

#define make_bps(bps) ((bitrate_t)(bps))
#define make_kbps(kbps) ((bitrate_t)(kbps)*1000u)
#define make_Mbps(Mbps) ((bitrate_t)(Mbps)*1000000u)
