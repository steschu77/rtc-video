[![pipeline status](https://gitlab.com/steschu77/rtc-video/badges/master/pipeline.svg)](https://gitlab.com/steschu77/rtc-video/-/commits/master)

# Real-time Video Transmission

## About rtc-video

The goal of this project is to experiment with and measure the impact of various mechanisms to reduce the effect of network impairments on the video quality of video receivers.

The project features a simulation pipeline for

* reading and encoding video frames,
* sending and receiving video packets,
* simulating network impairments on video packets,
* decoding and writing video frames,
* and evaluating resulting video quality.

More detailed information can be found at [the documentation](doc/contents.md)

## How to Build

Prerequisites for building rtc-video are CMake, a c compiler (cgg, clang), and an assembler (yasm). A compatible Dockerfile can be found in the `docker/` sub-folder.

To build, create a build sub-folder and run cmake.

```
mkdir .build
cd .build
cmake ..
make
```
