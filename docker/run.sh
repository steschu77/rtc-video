set -e
source /etc/profile
source ~/.bashrc
cd /work
mkdir -p .docker ; cd .docker
cmake -GNinja -DDEBUG=1 -DASAN=1 -DCOVERAGE=1 ..
time -p ninja
cd ../scripts
python3 ./test_vp9enc.py
ccache -s
exit 1
