/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */

#define _CRT_SECURE_NO_WARNINGS

#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>

#include "cutil_nullptr.h"
#include "cutil_file.h"
#include "cutil_buffer.h"
#include "cutil_byteread.h"
#include "cutil_vecu64.h"

#include "rtc-proto/frame.h"
#include "rtc-proto/ack_frame.h"
#include "rtc-proto/data_frame.h"
#include "rtc-proto/video_frame.h"

// ----------------------------------------------------------------------------
enum direction_e {
    dir_send_recv,
    dir_recv_send,
};

typedef enum direction_e direction_t;

// ----------------------------------------------------------------------------
enum degrader_event_e {
    event_packet_sent,
    event_packet_lost,
};

typedef enum degrader_event_e degrader_event_t;

// ----------------------------------------------------------------------------
struct packet_s {
    uint64_t tSend;
    uint64_t tRecv;
    uint64_t dir;
    uint64_t length;
    uint64_t seqno;
    int lost;
};

typedef struct packet_s packet_t;

// ----------------------------------------------------------------------------
struct ack_frame_s {
    vecu64_t* acks;
};

typedef struct ack_frame_s ack_frame_t;

// ----------------------------------------------------------------------------
struct video_frame_s {
    uint64_t length;
    uint64_t seqno;
    uint64_t picid;
    uint64_t flags;
};

typedef struct video_frame_s video_frame_t;

// ----------------------------------------------------------------------------
typedef int (*FnPacketBegin)(void* obj, const packet_t* packet);
typedef int (*FnPacketEnd)(void* obj);
typedef int (*FnPacketFrame)(void* obj);

// ----------------------------------------------------------------------------
struct log_convert_s {
    FnPacketBegin onPacketBegin;
    FnPacketEnd onPacketEnd;
    FnPacketFrame onPacketFrame;
};

typedef struct log_convert_s log_convert_t;

// ----------------------------------------------------------------------------
struct svg_s {
    FILE* fSVG;
    unsigned numPackets;
};

typedef struct svg_s svg_t;

// ----------------------------------------------------------------------------
int svgOnPacketBegin(void* obj, const packet_t* packet)
{
    svg_t* svg = (svg_t*)obj;
    FILE* f = svg->fSVG;

    int y_pos = 32 + svg->numPackets * 32;

    const char* anchor[2] = { "start", "end" };
    const int idxRxTx = packet->dir == dir_send_recv;
    const int dirRxTx = packet->dir == dir_send_recv ? -1 : 1;

    const uint64_t tSend1 = packet->tSend / 90;
    const uint64_t tSend01 = (packet->tSend % 90) / 9;
    const uint64_t tRecv1 = packet->tRecv / 90;
    const uint64_t tRecv01 = (packet->tRecv % 90) / 9;

    if (packet->lost) {
        fprintf(f, "  <line x1=\"%d\" y1=\"%d\" x2=\"%d\" y2=\"%d\" stroke=\"#000\" stroke-width=\"1\" marker-end=\"url(#arrow)\" />\n",
            400 + dirRxTx * (400 - 100), y_pos, 400, y_pos);
    } else {
        fprintf(f, "  <line x1=\"%d\" y1=\"%d\" x2=\"%d\" y2=\"%d\" stroke=\"#000\" stroke-width=\"1\" marker-end=\"url(#arrow)\" />\n",
            400 + dirRxTx * (400 - 100), y_pos, 400 - dirRxTx * (400 - 100), y_pos);
    }

    fprintf(f, "  <text x=\"%d\" y=\"%d\" text-anchor=\"%s\" class=\"time\">%" PRIu64 ".%" PRIu64 " ms</text>\n", 400 + dirRxTx * (400 - 100 + 8), y_pos + 8, anchor[idxRxTx], tSend1, tSend01);
    fprintf(f, "  <text x=\"%d\" y=\"%d\" text-anchor=\"%s\" class=\"time\">%" PRIu64 ".%" PRIu64 " ms</text>\n", 400 - dirRxTx * (400 - 100 + 8), y_pos + 8, anchor[1 - idxRxTx], tRecv1, tRecv01);

    fprintf(f, "  <text x=\"%d\" y=\"%d\" text-anchor=\"%s\" class=\"seq\">%" PRIu64 "</text>\n", 400 + dirRxTx * (400 - 100 + 8), y_pos - 4, anchor[idxRxTx], packet->seqno);
    fprintf(f, "  <text x=\"%d\" y=\"%d\" text-anchor=\"%s\" class=\"arw\">%" PRIu64 " b</text>\n", 400 + dirRxTx * (400 - 100), y_pos - 2, anchor[1 - idxRxTx], packet->length);
    fprintf(f, "  <text x=\"%d\" y=\"%d\" text-anchor=\"%s\" class=\"frm\" xml:space=\"preserve\">", 400 + dirRxTx * (400 - 100), y_pos + 10, anchor[1 - idxRxTx]);

    svg->numPackets++;
    return 0;
}

// ----------------------------------------------------------------------------
int svgOnAckFrame(void* obj, const ack_frame_t* frame)
{
    svg_t* svg = (svg_t*)obj;
    FILE* f = svg->fSVG;
    fprintf(f, " ack(");

    const unsigned numAcks = vecu64ItemCount(frame->acks);
    for (unsigned i = 0; i < numAcks; ++i) {
        const uint64_t ack = vecu64GetItem(frame->acks, i);
        fprintf(f, "%s%" PRIu64, i == 0 ? "" : ",", ack);
    }

    fprintf(f, ")");
    return 0;
}

// ----------------------------------------------------------------------------
int svgOnDataFrame(void* obj)
{
    svg_t* svg = (svg_t*)obj;
    FILE* f = svg->fSVG;
    fprintf(f, " data()");
    return 0;
}

// ----------------------------------------------------------------------------
int svgOnVideoFrame(void* obj, const video_frame_t* frame)
{
    svg_t* svg = (svg_t*)obj;
    FILE* f = svg->fSVG;

    const char* refType[4] = {
        ", key", ", ref", ", idle", ""
    };

    fprintf(f, " video(%" PRIu64 " b, %" PRIu64 ", %s%" PRIu64 "%s%s) ",
        frame->length, frame->seqno, frame->flags & 0x80 ? "[" : "|", frame->picid, frame->flags & 0x40 ? "]" : "|", refType[frame->flags & 3]);
    return 0;
}

// ----------------------------------------------------------------------------
int svgOnPacketEnd(void* obj)
{
    svg_t* svg = (svg_t*)obj;
    FILE* f = svg->fSVG;
    fprintf(f, "</text>\n");
    return 0;
}

// ----------------------------------------------------------------------------
const buffer_t* fileReadContent(const utf8str_t* path)
{
    utilsFile* file = createFileRead(path);
    if (file == nullptr) {
        return nullptr;
    }

    size_t fileSize = (size_t)getFileSize(file);
    void* fileData = nullptr;
    const buffer_t* buffer = allocBuffer(fileSize, &fileData, 16);
    if (buffer == nullptr) {
        return nullptr;
    }

    readFile(file, fileData, fileSize);
    freeFile(file);

    return buffer;
}

// ----------------------------------------------------------------------------
int parseAckFrame(svg_t* svg, byteread_t* frameReader)
{
    int rv = 0;

    ack_frame_t frame;
    frame.acks = vecu64Create(10, 10);

    uint64_t numAcks = 0;
    rv |= byteread_vint(frameReader, &numAcks);

    for (int i = 0; rv == 0 && i < numAcks; ++i) {
        uint64_t ack = 0;
        rv |= byteread_vint(frameReader, &ack);
        vecu64AddItem(frame.acks, ack);
    }

    return svgOnAckFrame(svg, &frame);
}

// ----------------------------------------------------------------------------
int parseDataFrame(svg_t* svg, byteread_t* frameReader)
{
    return svgOnDataFrame(svg);
}

// ----------------------------------------------------------------------------
int parseVideoFrame(svg_t* svg, byteread_t* frameReader)
{
    int rv = 0;

    video_frame_t frame;
    rv |= byteread_vint(frameReader, &frame.length);
    rv |= byteread_vint(frameReader, &frame.seqno);
    rv |= byteread_vint(frameReader, &frame.picid);
    rv |= byteread_vint(frameReader, &frame.flags);

    if (rv != 0) {
        return rv;
    }

    return svgOnVideoFrame(svg, &frame);
}

// ----------------------------------------------------------------------------
int parsePacketSentEvent(svg_t* svg, byteread_t* packetReader, int lost)
{
    int rv = 0;

    packet_t packet;
    packet.lost = lost;

    rv |= byteread_vint(packetReader, &packet.tSend);
    rv |= byteread_vint(packetReader, &packet.tRecv);
    rv |= byteread_vint(packetReader, &packet.dir);
    rv |= byteread_vint(packetReader, &packet.length);
    rv |= byteread_vint(packetReader, &packet.seqno);

    if (rv != 0) {
        return rv;
    }

    svgOnPacketBegin(svg, &packet);

    uint64_t len = 0;
    while (rv == 0 && byteread_vint(packetReader, &len) == 0) {

        byteread_t* frameReader = bytereadCreate(byteread_ptr(packetReader), len);
        byteread_skip(packetReader, len);

        rv = parsePacketFrame(svg, frameReader);

        bytereadDestroy(frameReader);
    }

    svgOnPacketEnd(svg);

    return rv;
}

// ----------------------------------------------------------------------------
int parseChunk(svg_t* svg, byteread_t* chunkReader)
{
    int rv = 0;

    uint64_t eventType = 0;
    rv |= byteread_vint(chunkReader, &eventType);

    switch (eventType) {
    case event_packet_sent:
    case event_packet_lost:
        return parsePacketSentEvent(svg, chunkReader, eventType == event_packet_lost);
    }

    return -1;
}

// ----------------------------------------------------------------------------
int parseFile(svg_t* svg, byteread_t* fileReader)
{
    int ret = 0;

    uint64_t len = 0;
    while (ret == 0 && byteread_vint(fileReader, &len) == 0) {

        byteread_t* chunkReader = bytereadCreate(byteread_ptr(fileReader), len);
        byteread_skip(fileReader, len);

        ret = parseChunk(svg, chunkReader);

        bytereadDestroy(chunkReader);
    }

    return ret;
}

// ----------------------------------------------------------------------------
int main(int argc, char* argv[])
{
    const utf8str_t* plogPath = utf8_make("packets.plog", 12);
    const buffer_t* packetLogData = fileReadContent(plogPath);
    if (packetLogData == nullptr) {
        return -1;
    }

    byteread_t* fileReader = bytereadCreate(getBufferData(packetLogData), getBufferSize(packetLogData));
    if (fileReader == nullptr) {
        return -1;
    }

    FILE* fTemplate = fopen("template.svg", "r");

    svg_t svg;
    svg.fSVG = fopen("packets.svg", "w");
    svg.numPackets = 0;

    if (svg.fSVG == nullptr || fTemplate == nullptr) {
        return -1;
    }

    char line[256];
    while (fgets(line, sizeof(line), fTemplate) != nullptr) /* read a line */ {
        if (strcmp(line, "#\n") != 0) {
            fprintf(svg.fSVG, "%s", line); /* Copy the template to the SVG file */
        } else {
            parseFile(&svg, fileReader);
        }
    }

    bytereadDestroy(fileReader);
    freeBuffer(packetLogData);

    fclose(svg.fSVG);
    fclose(fTemplate);
}
