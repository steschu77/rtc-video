/*
 * MIT License
 *
 * Copyright (c) 2021 Steffen Schulze
 */

#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>

#include "cutil_nullptr.h"

#include "rtc-api/mute_state.h"

#include "rtc-comp/codec_vp9.h"
#include "rtc-comp/image_yuvfile.h"
#include "rtc-comp/media_ctrl.h"
#include "rtc-comp/video_ivffile.h"

#include "task-api/task.h"
#include "task-api/task_queue.h"

// ----------------------------------------------------------------------------
struct test_config_s {
    const image_size_t seqSize;
    vp9enc_gop_t gopType;
    unsigned bitrate;
    unsigned frameLimit;
    const utf8str_t* fileNameSeq;
    const utf8str_t* fileNameYUV;
    const utf8str_t* fileNameCSV;
    const utf8str_t* fileNameIVF;
    const utf8str_t* fileNameRef;
};

typedef struct test_config_s test_config_t;

// ----------------------------------------------------------------------------
static int runTest(const test_config_t* cfg);
static int compareIVF(const test_config_t* cfg);

// ----------------------------------------------------------------------------
int main(int argc, char* argv[])
{
    const image_size_t seqSize = { 320, 180 };
    int result = 0;

    if (argc != 4) {
        printf("Usage: vp9enc <gop> <seqid> <bitrate>\n");
        return -1;
    }

    const vp9enc_gop_t gopType = (vp9enc_gop_t)atoi(argv[1]);
    const unsigned seqid = atoi(argv[2]);
    const unsigned bitrate = atoi(argv[3]);
    const utf8str_t* name = utf8_format("gop%d_seq%d_%dk", (int)gopType, seqid, bitrate);

    test_config_t cfg = {
        seqSize,
        gopType,
        bitrate * 1000,
        0,
        utf8_format("seq%d", seqid),
        utf8_format("%s", utf8_cstr(name)),
        utf8_format("%s.csv", utf8_cstr(name)),
        utf8_format("%s.ivf", utf8_cstr(name)),
        utf8_format("%s.ivf.ref", utf8_cstr(name))
    };

    if (runTest(&cfg) != 0 || compareIVF(&cfg)) {
        printf("failed: %s\n", utf8_cstr(name));
        result = 1;
    }

    utf8_free(&name);
    utf8_free(&cfg.fileNameSeq);
    utf8_free(&cfg.fileNameYUV);
    utf8_free(&cfg.fileNameCSV);
    utf8_free(&cfg.fileNameIVF);
    utf8_free(&cfg.fileNameRef);

    return result;
}

// ----------------------------------------------------------------------------
static void testChangeCaps(media_clock_t t, void* ptr)
{
    image_source_t* obj = (image_source_t*)ptr;

    image_caps_t yuvcaps = { unmuted, { 160, 90 }, { 30, 1 }, 108000 };
    imgsrcChangeCaps(obj, &yuvcaps);
}

// ----------------------------------------------------------------------------
static int runTest(const test_config_t* cfg)
{
    utilsFile* csvFile = createFileWrite(cfg->fileNameCSV, f_overwrite);
    utilsFile* vp9File = createFileWrite(cfg->fileNameIVF, f_overwrite);

    task_queue_t* tasks = taskQueueCreate();

    imgyuv_parser_t* yuvparser = yuvparseCreate(tasks, cfg->fileNameSeq, cfg->frameLimit);
    image_source_t* imgsrcParser = yuvparseQueryImageSource(yuvparser);

    const vp9enc_config_t vp9cfg = {
        cfg->gopType,
    };
    vp9enc_t* vp9enc = vp9encCreate(imgsrcParser, &vp9cfg);
    video_source_t* videosrcEncoder = vp9encQueryVideoSource(vp9enc);

    vp9analyze_t* vp9analyzer = vp9analyzeCreate(videosrcEncoder, csvFile);
    video_source_t* videosrcAnalyzer = vp9analyzeQueryVideoSource(vp9analyzer);

    vidivf_writer_t* videowrite = ivfwriteCreate(videosrcAnalyzer, vp9File);
    video_source_t* videosrcWriter = ivfwriteQueryVideoSource(videowrite);

    const image_caps_t yuvcaps = { unmuted, cfg->seqSize, { 30, 1 }, 108000 };
    const video_caps_t vp9caps = {
        yuvcaps,
        cfg->bitrate
    };
    vp9dec_t* vp9dec = vp9decCreate(videosrcWriter, &vp9caps);
    image_source_t* imgsrcDecoder = vp9decQueryImageSource(vp9dec);

    imgyuv_writer_t* yuvwrite = yuvwriteCreate(imgsrcDecoder, cfg->fileNameYUV);
    image_source_t* imgsrcYuvWrite = yuvwriteQueryImageSource(yuvwrite);

    img_mediactrl_t* imgctrl = imgctrlCreate(imgsrcYuvWrite);
    media_control_t* mediactrl = imgctrlQueryMediaControl(imgctrl);

    mediactrlStart(mediactrl, &yuvcaps);

    media_clock_t t0 = taskqueueTime(tasks);
    task_t* task = task1pCreate(t0 + 60, testChangeCaps, imgsrcParser);
    taskqueueScheduleEvent(tasks, task);
    taskRelease(task);

    while (taskqueueProcessNextEvent(tasks)) {
        ;
    }

    imgsrcRelease(imgsrcParser);
    videosrcRelease(videosrcEncoder);
    videosrcRelease(videosrcAnalyzer);
    videosrcRelease(videosrcWriter);
    imgsrcRelease(imgsrcDecoder);
    imgsrcRelease(imgsrcYuvWrite);
    mediactrlRelease(mediactrl);

    freeFile(vp9File);
    freeFile(csvFile);

    return 0;
}

// ----------------------------------------------------------------------------
static int compareIVF(const test_config_t* cfg)
{
    utilsFile* vp9File = createFileRead(cfg->fileNameIVF);
    utilsFile* refFile = createFileRead(cfg->fileNameRef);

    int res = fileCompare(vp9File, refFile);

    freeFile(vp9File);
    freeFile(refFile);

    return res;
}
