/*
 * MIT License
 *
 * Copyright (c) 2021 Steffen Schulze
 */

#define _CRT_SECURE_NO_WARNINGS

#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>

#include "cutil_nullptr.h"
#include "cutil_file.h"
#include "cutil_buffer.h"
#include "cutil_byteread.h"

#include "rtc-proto/packet.h"
#include "rtc-proto/packet_event.h"
#include "rtc-proto/packet_log.h"

#include "sim_results.h"

// ----------------------------------------------------------------------------
struct sim_results_s {
    tree_t* packets[2];
    tree_t* events[2];
};

typedef struct sim_results_s sim_results_t;

// ----------------------------------------------------------------------------
const buffer_t* fileReadContent(const utf8str_t* path)
{
    utilsFile* file = createFileRead(path);
    if (file == nullptr) {
        return nullptr;
    }

    size_t fileSize = (size_t)getFileSize(file);
    void* fileData = nullptr;
    const buffer_t* buffer = allocBuffer(fileSize, &fileData, 16);
    if (buffer == nullptr) {
        return nullptr;
    }

    readFile(file, fileData, fileSize);
    freeFile(file);

    return buffer;
}

// ----------------------------------------------------------------------------
int onPacket(void* ctx, const packet_t* packet)
{
    sim_results_t* obj = (sim_results_t*)ctx;
    direction_t dir = packetGetDirection(packet);

    treeInsertItem(obj->packets[dir], packetCompare, packetConstAssign(packet));
    return 0;
}

// ----------------------------------------------------------------------------
int onEvent(void* ctx, const packet_event_t* event)
{
    sim_results_t* obj = (sim_results_t*)ctx;
    direction_t dir = packeteventGetDirection(event);
    treeInsertItem(obj->events[dir], packeteventCompareSeqNo, packeteventConstAssign(event));
    return 0;
}

// ----------------------------------------------------------------------------
static int fnEvent(const void* ptr, void* ctx)
{
    sim_results_t* obj = (sim_results_t*)ctx;
    const packet_event_t* e = (const packet_event_t*)ptr;

    direction_t dir = packeteventGetDirection(e);
    uint64_t seqno = packeteventGetSeqNo(e);

    packet_t* packet = simGetPacket(obj, dir, seqno);
    if (packet != nullptr) {
        packet_event_type_t type = packeteventGetType(e);
        media_clock_t time = packeteventGetTime(e);
        packetAddTime(packet, type, time);
    }

    return 0;
}

// ----------------------------------------------------------------------------
static void releasePacket(const void* ptr)
{
    const packet_t* obj = (const packet_t*)ptr;
    packetRelease(obj);
}

// ----------------------------------------------------------------------------
const sim_results_t* simReadResults(const utf8str_t* plogPath)
{
    const buffer_t* packetLogData = fileReadContent(plogPath);
    if (packetLogData == nullptr) {
        return nullptr;
    }

    byteread_t* fileReader = bytereadCreate(getBufferData(packetLogData), getBufferSize(packetLogData));
    if (fileReader == nullptr) {
        return nullptr;
    }

    sim_results_t* obj = (sim_results_t*)malloc(sizeof(sim_results_t));
    if (obj == nullptr) {
        return nullptr;
    }

    obj->packets[0] = treeCreate();
    obj->packets[1] = treeCreate();
    obj->events[0] = treeCreate();
    obj->events[1] = treeCreate();

    while (packetlogParseChunk(fileReader, obj, onPacket, onEvent) == 0) {
    }

    bytereadDestroy(fileReader);
    freeBuffer(packetLogData);

    treeIterate(simGetEvents(obj, dir_send_recv), fnEvent, obj);
    treeIterate(simGetEvents(obj, dir_recv_send), fnEvent, obj);

    return obj;
}

// ----------------------------------------------------------------------------
const sim_results_t* simDestroy(const sim_results_t* obj)
{
    treeDestroy(obj->packets[0], releasePacket);
    treeDestroy(obj->packets[1], releasePacket);
    treeDestroy(obj->events[0], packeteventRelease);
    treeDestroy(obj->events[1], packeteventRelease);

    free((void*)obj);
    return nullptr;
}

// ----------------------------------------------------------------------------
const tree_t* simGetPackets(const sim_results_t* obj, direction_t dir)
{
    return obj->packets[dir];
}

// ----------------------------------------------------------------------------
const tree_t* simGetEvents(const sim_results_t* obj, direction_t dir)
{
    return obj->events[dir];
}

// ----------------------------------------------------------------------------
packet_t* simGetPacket(const sim_results_t* obj, direction_t dir, uint64_t seqno)
{
    return (packet_t*)treeFindItem(obj->packets[dir], packetPredicate, &seqno);
}

// ----------------------------------------------------------------------------
const packet_event_t* simGetPacketEvent(const sim_results_t* obj, direction_t dir, uint64_t seqno)
{
    return (const packet_event_t*)treeFindItem(obj->events[dir], packeteventPredicateSeqNo, &seqno);
}

// ----------------------------------------------------------------------------
struct filter_s {
    tree_t* filtered;
    simFnFilterEvent fnFilter;
    cutilFnCompare fnCmp;
};

typedef struct filter_s filter_t;

// ----------------------------------------------------------------------------
static int fnEventFilter(const void* ptr, void* ctx)
{
    filter_t* obj = (filter_t*)ctx;
    const packet_event_t* e = (const packet_event_t*)ptr;

    if (obj->fnFilter(e)) {
        treeInsertItem(obj->filtered, obj->fnCmp, packeteventConstAssign(e));
    }

    return 0;
}

// ----------------------------------------------------------------------------
const tree_t* simFilterEvents(const sim_results_t* obj, simFnFilterEvent fnFilter, cutilFnCompare fnCmp)
{
    filter_t filter;
    filter.filtered = treeCreate();
    filter.fnFilter = fnFilter;
    filter.fnCmp = fnCmp;

    for (int i = 0; i < 2; i++) {
        treeIterate(obj->events[i], fnEventFilter, &filter);
    }

    return filter.filtered;
}
