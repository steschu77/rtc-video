/*
 * MIT License
 *
 * Copyright (c) 2021 Steffen Schulze
 */

#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>

#include "cutil_nullptr.h"

#include "rtc-comp/image_yuvfile.h"
#include "rtc-comp/image_activity.h"
#include "rtc-comp/media_ctrl.h"

#include "task-api/task.h"
#include "task-api/task_queue.h"

// ----------------------------------------------------------------------------
int main(int argc, char* argv[])
{
    utilsFile* csvFile = createFileWrite(STR("seq0.320x180.csv"), f_overwrite);

    task_queue_t* tasks = taskQueueCreate();

    imgyuv_parser_t* yuvparser = yuvparseCreate(tasks, STR("seq0"), 0);
    image_source_t* imgsrcParser = yuvparseQueryImageSource(yuvparser);

    image_dynamics_t* imgactivity = imgactivityCreate(imgsrcParser, csvFile);
    image_source_t* imgsrcDynamics = imgactivityQueryImageSource(imgactivity);

    img_mediactrl_t* imgctrl = imgctrlCreate(imgsrcDynamics);
    media_control_t* mediactrl = imgctrlQueryMediaControl(imgctrl);

    const image_caps_t yuvcaps = { unmuted, { 320, 180 }, { 30, 1 }, 108000 };
    mediactrlStart(mediactrl, &yuvcaps);

    while (taskqueueProcessNextEvent(tasks)) {
        ;
    }

    imgsrcRelease(imgsrcParser);
    imgsrcRelease(imgsrcDynamics);
    mediactrlRelease(mediactrl);

    freeFile(csvFile);
    return 0;
}
