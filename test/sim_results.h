/*
 * MIT License
 *
 * Copyright (c) 2021 Steffen Schulze
 */

#pragma once

#include "rtc-proto/packet.h"
#include "rtc-proto/packet_event.h"

#include "cutil_tree.h"
#include "cutil_utf8str.h"

// ----------------------------------------------------------------------------
struct sim_results_s;
typedef struct sim_results_s sim_results_t;

// ----------------------------------------------------------------------------
typedef int (*simFnFilterEvent)(const packet_event_t*);

// ----------------------------------------------------------------------------
const sim_results_t* simReadResults(const utf8str_t* plogPath);
const sim_results_t* simDestroy(const sim_results_t*);

const tree_t* simFilterEvents(const sim_results_t* obj, simFnFilterEvent, cutilFnCompare);

const tree_t* simGetPackets(const sim_results_t*, direction_t dir);
const tree_t* simGetEvents(const sim_results_t*, direction_t dir);
packet_t* simGetPacket(const sim_results_t* obj, direction_t dir, uint64_t seqno);
const packet_event_t* simGetPacketEvent(const sim_results_t* obj, direction_t dir, uint64_t seqno);
