/*
 * MIT License
 *
 * Copyright (c) 2021 Steffen Schulze
 */

#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>

#include "cutil_nullptr.h"

#include "rtc-comp/codec_vp9.h"
#include "rtc-comp/image_yuvfile.h"
#include "rtc-comp/media_ctrl.h"
#include "rtc-comp/video_ivffile.h"

#include "task-api/task.h"
#include "task-api/task_queue.h"

// ----------------------------------------------------------------------------
int main(int argc, char* argv[])
{
    utilsFile* vp9File = createFileRead(STR("text.ivf"));
    utilsFile* csvFile = createFileWrite(STR("test.csv"), f_overwrite);

    task_queue_t* tasks = taskQueueCreate();

    vidivf_parser_t* videoparser = ivfparseCreate(tasks, vp9File);
    video_source_t* videosrcParser = ivfparseQueryVideoSource(videoparser);

    vp9analyze_t* vp9analyzer = vp9analyzeCreate(videosrcParser, csvFile);
    video_source_t* videosrcAnalyzer = vp9analyzeQueryVideoSource(vp9analyzer);

    const image_size_t recvSize = { 128, 96 };
    const image_caps_t yuvcaps = { unmuted, recvSize, { 30, 1 }, 108000 };
    const video_caps_t vp9caps = { yuvcaps, 100 };
    vp9dec_t* vp9dec = vp9decCreate(videosrcAnalyzer, &vp9caps);
    image_source_t* imgsrcDecoder = vp9decQueryImageSource(vp9dec);

    imgyuv_writer_t* yuvwrite = yuvwriteCreate(imgsrcDecoder, STR("dec"));
    image_source_t* imgsrcYuvWrite = yuvwriteQueryImageSource(yuvwrite);

    img_mediactrl_t* imgctrl = imgctrlCreate(imgsrcYuvWrite);
    media_control_t* mediactrl = imgctrlQueryMediaControl(imgctrl);

    mediactrlStart(mediactrl, &yuvcaps);

    while (taskqueueProcessNextEvent(tasks)) {
        ;
    }

    videosrcRelease(videosrcParser);
    videosrcRelease(videosrcAnalyzer);
    imgsrcRelease(imgsrcDecoder);
    imgsrcRelease(imgsrcYuvWrite);
    mediactrlRelease(mediactrl);

    freeFile(vp9File);
    return 0;
}
