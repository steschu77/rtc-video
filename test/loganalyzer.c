/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */

#define _CRT_SECURE_NO_WARNINGS

#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>

#include "cutil_nullptr.h"
#include "cutil_tree.h"

#include "rtc-proto/packet.h"
#include "rtc-proto/packet_event.h"

#include "rtc-proto/ack_frame.h"
#include "rtc-proto/caps_frame.h"
#include "rtc-proto/video_frame.h"

#include "rtc-api/video_caps.h"

#include "sim_results.h"

// ----------------------------------------------------------------------------
struct analyzer_s {
    const sim_results_t* sim;
    FILE* fSVG;
    unsigned packetNumber;
};

typedef struct analyzer_s analyzer_t;

// ----------------------------------------------------------------------------
int svgOnAckFrame(void* context, const packet_t* packet, const ack_frame_t* frame)
{
    analyzer_t* obj = (analyzer_t*)context;
    FILE* f = obj->fSVG;

    const vecu64_t* acks = ackframeGetAcks(frame);

    fprintf(f, " ack(");

    const unsigned numAcks = vecu64ItemCount(acks);
    for (unsigned i = 0; i < numAcks; ++i) {
        const uint64_t ack = vecu64GetItem(acks, i);
        fprintf(f, "%s%" PRIu64, i == 0 ? "" : ",", ack);
    }

    fprintf(f, ")");
    return 0;
}

// ----------------------------------------------------------------------------
int svgOnCapsFrame(void* context, const packet_t* packet, const caps_frame_t* frame)
{
    analyzer_t* obj = (analyzer_t*)context;
    FILE* f = obj->fSVG;

    const video_caps_t* caps = capsframeGetCaps(frame);

    fprintf(f, " caps(id:%" PRIu64 ", %dx%d, %" PRIu64 " bps)",
        capsframeGetSeqid(frame),
        caps->imgcaps.preferred_res.cx,
        caps->imgcaps.preferred_res.cy,
        caps->max_bitrate);
    return 0;
}

// ----------------------------------------------------------------------------
int svgOnVideoFrame(void* context, const packet_t* packet, const video_frame_t* frame)
{
    analyzer_t* obj = (analyzer_t*)context;
    FILE* f = obj->fSVG;

    const char* refType[5] = {
        ", key", ", sync", ", ref", ", idle", ", end"
    };

    fprintf(f, " video(%" PRIu64 " b, %" PRIu64 ", %s%" PRIu64 "%s%s) ",
        videoframeGetLength(frame),
        videoframeGetSeqNo(frame),
        videoframeGetFlags(frame) & 0x80 ? "[" : "|",
        videoframeGetPicId(frame),
        videoframeGetFlags(frame) & 0x40 ? "]" : "|",
        refType[videoframeGetRefType(frame)]);
    return 0;
}

// ----------------------------------------------------------------------------
static frame_enum_t cbsOnPacket = {
    svgOnAckFrame,
    svgOnCapsFrame,
    svgOnVideoFrame
};

// ----------------------------------------------------------------------------
int svgOnPacket(const void* ptr, void* ctx)
{
    analyzer_t* obj = (analyzer_t*)ctx;
    const packet_event_t* e = (const packet_event_t*)ptr;
    const packet_t* p = simGetPacket(obj->sim, packeteventGetDirection(e), packeteventGetSeqNo(e));

    FILE* f = obj->fSVG;

    int y_pos = 32 + obj->packetNumber * 32;

    const char* anchor[2] = { "start", "end" };
    const int idxRxTx = packetGetDirection(p) == dir_send_recv;
    const int dirRxTx = packetGetDirection(p) == dir_send_recv ? -1 : 1;

    const uint64_t seqno = packetGetSeqNo(p);
    const size_t length = packetGetLength(p);
    const uint64_t tSend = packetGetTime(p, packet_send);
    const uint64_t tRecv = packetGetTime(p, packet_recv);
    const uint64_t state = packetGetState(p);

    const uint64_t tSend1 = tSend / 90;
    const uint64_t tSend01 = (tSend % 90) / 9;
    const uint64_t tRecv1 = tRecv / 90;
    const uint64_t tRecv01 = (tRecv % 90) / 9;

    const int packetLost = (state & (1ull << packet_recv)) == 0;

    if (packetLost) {
        fprintf(f, "  <line x1=\"%d\" y1=\"%d\" x2=\"%d\" y2=\"%d\" stroke=\"#000\" stroke-width=\"1\" marker-end=\"url(#arrow)\" />\n",
            400 + dirRxTx * (400 - 100), y_pos, 400, y_pos);
    } else {
        fprintf(f, "  <line x1=\"%d\" y1=\"%d\" x2=\"%d\" y2=\"%d\" stroke=\"#000\" stroke-width=\"1\" marker-end=\"url(#arrow)\" />\n",
            400 + dirRxTx * (400 - 100), y_pos, 400 - dirRxTx * (400 - 100), y_pos);
    }

    fprintf(f, "  <text x=\"%d\" y=\"%d\" text-anchor=\"%s\" class=\"time\">%" PRIu64 ".%" PRIu64 " ms</text>\n", 400 + dirRxTx * (400 - 100 + 8), y_pos + 8, anchor[idxRxTx], tSend1, tSend01);

    if (!packetLost) {
        fprintf(f, "  <text x=\"%d\" y=\"%d\" text-anchor=\"%s\" class=\"time\">%" PRIu64 ".%" PRIu64 " ms</text>\n", 400 - dirRxTx * (400 - 100 + 8), y_pos + 8, anchor[1 - idxRxTx], tRecv1, tRecv01);
    }

    fprintf(f, "  <text x=\"%d\" y=\"%d\" text-anchor=\"%s\" class=\"seq\">%" PRIu64 "</text>\n", 400 + dirRxTx * (400 - 100 + 8), y_pos - 4, anchor[idxRxTx], seqno);
    fprintf(f, "  <text x=\"%d\" y=\"%d\" text-anchor=\"%s\" class=\"arw\">%" PRIu64 " b</text>\n", 400 + dirRxTx * (400 - 100), y_pos - 2, anchor[1 - idxRxTx], length);
    fprintf(f, "  <text x=\"%d\" y=\"%d\" text-anchor=\"%s\" class=\"frm\" xml:space=\"preserve\">", 400 + dirRxTx * (400 - 100), y_pos + 10, anchor[1 - idxRxTx]);

    packetEnumFrames(p, obj, &cbsOnPacket);

    fprintf(f, "  </text>\n");

    obj->packetNumber++;
    return 0;
}

// ----------------------------------------------------------------------------
int renderSVG(const utf8str_t* svgPath, const sim_results_t* sim, simFnFilterEvent fnFilter, cutilFnCompare fnCmp)
{
    FILE* fTemplate = fopen("template.svg", "r");

    analyzer_t obj;
    obj.sim = sim;
    obj.fSVG = fopen(utf8_cstr(svgPath), "w");
    obj.packetNumber = 0;

    if (obj.fSVG == nullptr || fTemplate == nullptr) {
        return -1;
    }

    const tree_t* filteredEvents = simFilterEvents(obj.sim, fnFilter, fnCmp);

    char line[256];
    while (fgets(line, sizeof(line), fTemplate) != nullptr) /* read a line */ {
        if (strcmp(line, "#\n") != 0) {
            fprintf(obj.fSVG, "%s", line); /* Copy the template to the SVG file */
        } else {
            treeIterate(filteredEvents, svgOnPacket, &obj);
        }
    }

    treeDestroy(filteredEvents, packeteventRelease);

    fclose(obj.fSVG);
    fclose(fTemplate);
    return 0;
}
