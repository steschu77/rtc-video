/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */

#include <inttypes.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

#include "cutil_nullptr.h"
#include "cutil_getopt.h"
#include "cutil_tree.h"

#include "rtc-comp/codec_vp9.h"
#include "rtc-comp/image_gen.h"
#include "rtc-comp/media_ctrl.h"
#include "rtc-comp/image_yuvfile.h"
#include "rtc-comp/packet_degrader.h"
#include "rtc-comp/video_sender.h"
#include "rtc-comp/video_receiver.h"
#include "rtc-comp/video_ivffile.h"

#include "task-api/task_queue.h"
#include "task-api/task.h"

#include "sim_results.h"

// ----------------------------------------------------------------------------
struct parameter_s {
    int code;
    int val;
    const char* name;
};

typedef struct parameter_s parameter_t;

// ----------------------------------------------------------------------------
int compareParams(const void* x0, const void* x1)
{
    const parameter_t* p0 = (const parameter_t*)x0;
    const parameter_t* p1 = (const parameter_t*)x1;
    return p0->code - p1->code;
}

// The params array is sorted by the parameter key ("-x") for use by bsearch()
// ----------------------------------------------------------------------------
enum parameter_e {
    p_action = 0,
    p_bitrate,
    p_delay,
    p_framelimit,
    p_gopmode,
    p_syncmode,
    p_lossrate,
    p_rtx,
    p_seqid,
    p_seed,
    p_count
};

parameter_t params[p_count] = {
    { 'a', 0, "action (0:all, 1:test-only, 2:svg-only" },
    { 'b', 100, "bit-rate (kbps)" },
    { 'd', 10, "delay (ms)" },
    { 'f', 0, "frame-limit (0: unlimited)" },
    { 'g', 0, "GOP mode (0:IP, 1:IpP, 2:IbP, 3:IppP, 4:IbbP, 5:IpPpP, 6:IbPbP, 7:IbBbP" },
    { 'k', 1, "sync-mode (0:key-frame, 1:sync-frame)" },
    { 'l', 2, "loss-rate (%)" },
    { 'r', 1, "re-transmission" },
    { 's', 0, "test sequence" },
    { 'x', 20060, "rnd generator seed" }, // other useful seeds: 104 (first packet lost)
};

// ----------------------------------------------------------------------------
struct test_config_s {
    const image_size_t seqSize;
    vp9enc_gop_t gopMode;
    vp9enc_syncmode_t syncMode;
    unsigned rtx;
    unsigned lossrate;
    unsigned delay;
    bitrate_t bitrate;
    unsigned frameLimit;
    const utf8str_t* fileNameSeq;
    const utf8str_t* fileNameYUV;
    const utf8str_t* fileNameCSV;
    const utf8str_t* fileNameIVF;
    const utf8str_t* fileNamePLog;
    const utf8str_t* fileNameSVGSend;
    const utf8str_t* fileNameSVGRecv;
};

typedef struct test_config_s test_config_t;

// ----------------------------------------------------------------------------
static int runTest(const test_config_t* cfg);
static int generateSVGs(const test_config_t* cfg);

// ----------------------------------------------------------------------------
int main(int argc, char* argv[])
{
    // Read parameter values from command line
    parameter_t key = { 0, 0, nullptr };
    while ((key.code = getopt(argc, argv, "a:b:d:f:g:k:l:r:s:x:")) != -1) {
        parameter_t* param = (parameter_t*)bsearch(&key, params, p_count, sizeof(parameter_t), compareParams);
        if (param == nullptr || (param->val = atoi(optarg)) < 0) {
            printf("Invalid/incomplete argument %c\n", (char)optopt);
            return -1;
        }
    }

    // Create test configuration
    srand(params[p_seed].val);

    const image_size_t seqSize = { 320, 180 };
    const utf8str_t* name = utf8_format("gop%d_%c_rtx%d_seq%d_loss%d_%dms_%dk",
        params[p_gopmode].val, params[p_syncmode].val ? 's':'k',
        params[p_rtx].val,
        params[p_seqid].val,
        params[p_lossrate].val, params[p_delay].val, params[p_bitrate].val);

    test_config_t cfg = {
        seqSize,
        params[p_gopmode].val,
        params[p_syncmode].val,
        params[p_rtx].val,
        params[p_lossrate].val,
        params[p_delay].val,
        params[p_bitrate].val,
        params[p_framelimit].val,
        utf8_format("seq%d", params[p_seqid].val),
        utf8_format("%s", utf8_cstr(name)),
        utf8_format("%s.csv", utf8_cstr(name)),
        utf8_format("%s.ivf", utf8_cstr(name)),
        utf8_format("%s.plog", utf8_cstr(name)),
        utf8_format("%s_send.svg", utf8_cstr(name)),
        utf8_format("%s_recv.svg", utf8_cstr(name))
    };

    int result = 0;
    if (params[p_action].val == 0 || params[p_action].val == 1) {
        if (runTest(&cfg) != 0) {
            printf("failed: %s\n", utf8_cstr(name));
            result = 1;
        }
    }

    if (params[p_action].val == 0 || params[p_action].val == 2) {
        generateSVGs(&cfg);
    }

    utf8_free(&name);
    utf8_free(&cfg.fileNameSeq);
    utf8_free(&cfg.fileNameYUV);
    utf8_free(&cfg.fileNameCSV);
    utf8_free(&cfg.fileNameIVF);
    utf8_free(&cfg.fileNamePLog);

    return result;
}

// ----------------------------------------------------------------------------
static int runTest(const test_config_t* cfg)
{
    utilsFile* csvFile = createFileWrite(cfg->fileNameCSV, f_overwrite);
    utilsFile* vp9File = createFileWrite(cfg->fileNameIVF, f_overwrite);
    utilsFile* logFile = createFileWrite(cfg->fileNamePLog, f_overwrite);

    task_queue_t* tasks = taskQueueCreate();

    imgyuv_parser_t* yuvparser = yuvparseCreate(tasks, cfg->fileNameSeq, cfg->frameLimit);
    image_source_t* imgsrcParser = yuvparseQueryImageSource(yuvparser);

    const vp9enc_config_t vp9cfg = {
        cfg->gopMode,
        cfg->syncMode
    };
    vp9enc_t* vp9enc = vp9encCreate(imgsrcParser, &vp9cfg);
    video_source_t* videosrcEncoder = vp9encQueryVideoSource(vp9enc);

    vidivf_writer_t* videowrite = ivfwriteCreate(videosrcEncoder, vp9File);
    video_source_t* videosrcWriter = ivfwriteQueryVideoSource(videowrite);

    video_send_params_t sendparams = {
        cfg->rtx
    };
    video_sender_t* videosend = videosendCreate(&sendparams, tasks, videosrcWriter);
    packet_source_t* pcksrcSender = videosendQueryPacketSource(videosend);

    network_qos_params_t qos = {
        make_msec(cfg->delay),
        make_kbps(cfg->bitrate),
        make_percent(cfg->lossrate)
    };
    packet_degrader_t* degrader = pckdegraderCreate(tasks, pcksrcSender, &qos, logFile);
    packet_source_t* pcksrcDegrader = pckdegraderQueryPacketSource(degrader);

    video_receiver_t* videorecv = videorecvCreate(tasks, pcksrcDegrader);
    video_source_t* videosrcRecv = videorecvQueryVideoSource(videorecv);

    vp9analyze_t* vp9analyzer = vp9analyzeCreate(videosrcRecv, csvFile);
    video_source_t* videosrcAnalyzer = vp9analyzeQueryVideoSource(vp9analyzer);

    const image_caps_t yuvcaps = { unmuted, cfg->seqSize, { 30, 1 }, 108000 };
    const video_caps_t vp9caps = {
        yuvcaps,
        cfg->bitrate
    };
    vp9dec_t* vp9dec = vp9decCreate(videosrcAnalyzer, &vp9caps);
    image_source_t* imgsrcDecoder = vp9decQueryImageSource(vp9dec);

    imgyuv_writer_t* yuvwrite = yuvwriteCreate(imgsrcDecoder, cfg->fileNameYUV);
    image_source_t* imgsrcYuvWrite = yuvwriteQueryImageSource(yuvwrite);

    img_mediactrl_t* imgctrl = imgctrlCreate(imgsrcYuvWrite);
    media_control_t* mediactrl = imgctrlQueryMediaControl(imgctrl);

    mediactrlStart(mediactrl, &yuvcaps);

    while (taskqueueProcessNextEvent(tasks)) {
        ;
    }

    imgsrcRelease(imgsrcParser);
    videosrcRelease(videosrcEncoder);
    videosrcRelease(videosrcAnalyzer);
    videosrcRelease(videosrcWriter);
    pcksrcRelease(pcksrcSender);
    pcksrcRelease(pcksrcDegrader);
    videosrcRelease(videosrcRecv);
    imgsrcRelease(imgsrcDecoder);
    imgsrcRelease(imgsrcYuvWrite);
    mediactrlRelease(mediactrl);

    freeFile(csvFile);
    freeFile(vp9File);
    freeFile(logFile);
    return 0;
}

// ----------------------------------------------------------------------------
static int filterSenderEvent(const packet_event_t* e)
{
    return (packeteventGetDirection(e) == dir_send_recv && packeteventGetType(e) == packet_send) ||
        (packeteventGetDirection(e) == dir_recv_send && packeteventGetType(e) == packet_recv);
}

// ----------------------------------------------------------------------------
static int filterReceiverEvent(const packet_event_t* e)
{
    return (packeteventGetDirection(e) == dir_recv_send && packeteventGetType(e) == packet_send) ||
        (packeteventGetDirection(e) == dir_send_recv && packeteventGetType(e) == packet_recv);
}

// ----------------------------------------------------------------------------
int renderSVG(const utf8str_t* svgPath, const sim_results_t* sim, simFnFilterEvent fnFilter, cutilFnCompare fnCmp);

// ----------------------------------------------------------------------------
static int generateSVGs(const test_config_t* cfg)
{
    const sim_results_t* sim = simReadResults(cfg->fileNamePLog);

    renderSVG(cfg->fileNameSVGSend, sim, filterSenderEvent, packeteventCompareTime);
    renderSVG(cfg->fileNameSVGRecv, sim, filterReceiverEvent, packeteventCompareTime);

    simDestroy(sim);
    return 0;
}
