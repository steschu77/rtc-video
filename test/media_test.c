/*
 * MIT License
 *
 * Copyright (c) 2019 Steffen Schulze
 */

#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>

#include "cutil_nullptr.h"

#include "codec/codec_vp9.h"
#include "image/image_gen.h"
#include "image/image_yuvfile.h"

// ----------------------------------------------------------------------------
int main(int argc, char* argv[])
{
    const char* sz_path = "test.128x96";
    const utf8str_t* path = utf8_make(sz_path, strlen(sz_path));
    utilsFile* file = createFileWrite(path, f_overwrite);

    const image_size_t size = { 128, 96 };
    imgyuv_writer_t* yuvwrite = yuvwriteCreate(file, &size);
    image_sink_t* imgsinkYUVWrite = imgyuvWriterQueryImageSink(yuvwrite);

    vp9dec_t* vp9dec = vp9decCreate(imgsinkYUVWrite);
    video_sink_t* vidsinkVP9Dec = vp9decQueryVideoSink(vp9dec);

    vp9enc_t* vp9enc = vp9encCreate(vidsinkVP9Dec);
    image_sink_t* imgsinkVP9Enc = vp9encQueryImageSink(vp9enc);

    //imgverify_gradient_t* verify = imgvfyGradientCreate();
    //image_sink_t* vfy_imageSink = imgvfyGradientQueryImageSink(verify);

    //imggen_gradient_t* gen = imggenGradientCreate(imgsinkVP9Enc);
    //media_gen_t* mediaGen = imggenGradientQueryMediaGen(gen);

    //for (int i = 0; i < 100; i++) {
    //    mediagenGenerate(mediaGen, i);
    //}
    return 0;
}
