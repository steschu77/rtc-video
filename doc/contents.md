# Real-time Video Transmission

## Contents

- Real-time Video Transmission Concepts

  - [Real-time Requirements](stub.md)
    - Interactivity
    - One-way real-time event

  - [Peer-to-Peer vs. Multi-point Conferencing](stub.md)
    - [Multi-point Conferencing Topologies](stub.md)

  - [Video Encoder Load Control](stub.md)
    - Processing Delay

  - [Video Encoder Picture Management](EncoderPicManagement.md)
    - Sync-Frames
    - Penalty-Free Key-Frames

  - [Video Encoder Reference Control](stub.md)
    - Frame Reordering Delay
    - Increase duration of pictures held in the picture buffer for alternative referencing

  - [Congestion Control](stub.md)
    - Padding
    - Packet Reports / Advanced NAcks

  - [Paced Sender](stub.md)
    - [Video Encoder Rate Control](stub.md)
      - Virtual Buffer Delay

  - [Video Stream Control](stub.md)
    - Number of streams
    - Per stream
      - Codec
      - Resolution / Framerate / MB-rate
      - Simulcast / SVC
      - bit-rate limit

  - [Packet Loss Control](stub.md)
    - [Retransmissions](stub.md)
      - [SVC](stub.md)
    - [FEC](stub.md)
      - [SVC](stub.md)
    - [Video Encoder Recovery](stub.md)
      - [Instant Video Refresh](stub.md)
      - [Gradual Video Refresh](stub.md)
      - [Alternative Referencing](stub.md)
      - [SVC](stub.md)
    - [Sender Controlled Playout](stub.md)
    - [Receiver Controlled Playout](stub.md)
      - Stream contains meta-data about referencing schema

  - [Receiver Load Control](stub.md)
    - [Video Decoder Load Control](stub.md)
    - [Receiver Capabilities](stub.md)
    - [Receiver Requests](stub.md)
      - Pause / Resume (Permanent Pause)
      - On-Screen / Off-Screen (Temporary Pause, i.e. Lowest Temporal Layer)
      - Display Area
      - Display Rate

  - [Advanced Video Features](stub.md)
    - [Video Sender Pause with Freeze](stub.md)
      - Peer to Peer vs. Multi-point with late Joins
        - Last picture before Freeze should be sync-frame (which can be sent to late joiners)

- Implementation

  - [Network Simulator](stub.md)
    - [Network degradation parameters](stub.md)
      - Connection Bit-rate
      - Packet Queue Size
      - Random Loss
      - Latency / Delay / RTT
    - [Network Simulator Logging](stub.md)
      - Packet Drop Event
      - Network Parameter Change Event

- [Measurements](stub.md)

  - [Simulated Scenarios](stub.md)
    - Network impediments on 1:1 situations
    - Network impediments on 1:N situations
    - Late join impediments on 1:N situations
    - Bit-rate estimation mechanisms
    - Loss pattern estimation mechanisms

  - [Network Parameters](stub.md)
    - Bit-rates
    - Delays
    - Loss Patterns (occasional loss, burst loss, ...)

  - [Loss Recovery Mechanisms]

  - [Quality Estimation](QualityEstimation.md)
    - Determine dynamics of test video sequences
    - Determine per Picture quality by applying dynamics on picture basis
