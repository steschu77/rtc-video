# Real-time Video Transmission

# Quality Estimation

Different aspects of video playback affect the subjective experience of video quality.

To estimate the overall achieved quality of a video transmission the following aspects of the resulting video playback on receiver side are considered:

* video picture quality,
* video playback fluentness and
* end-to-end delay.

The first two aspects cannot be measured directly and are therefore derived by the following measurable metrics. Video picture quality describes a static quality matric and will be measured by

* PSNR and
* SSIM.

Dynamic effects that influence video playback fluentness are measured by

* Frame-Time,
* Jitter, and
* Frame-Loss.

Depending on the dynamics of the video static or dynamic metrics play different roles on subjective video quality. If a video does not have any movement, then dynamic effects like lost pictures or jitter don't affect the subjective quality. The effect of dynamic effect depends on how dynamic the video itself is.

Therefore the static and dynamic metrics are weighted by the amount of dynamics in the video. The dynamics of the video can be provided up-front to the simulator with a dynamics factor between 0 (picture didn't change at all compared to previous picture) and 1 (complete scene change).

## Penalties

### Image Quality Penalty

* PSNR
* SSIM
* VMAX

### Frame-Time Penalty

The _Frame-Time penalty_ is one of the penalties measuring the fluentness of the video playback.

The _Frame-Time penalty_ evaluates frame-rate drops below a limit that is required for a perceived fluent playback.

Video playback is considered fluent below frame-times of 40 ms (which corresponds to 25 frames per second).

### Jitter Penalty

The _Jitter penalty_ is the second penalty for measuring the fluentness of the video playback.

The _Jitter penalty_ expresses how frame-time changes between video pictures.

High jitter can lead to a perceived lower frame-rate as pictures with very low frame-times are treated similarly to pictures that were lost.

### Image Loss Penalty

The third metric to measure fluentness of video playback is the _Image Loss penalty_.

_Image Loss penalty_ estimates the video quality degradation of pictures that aren't presented to the user at all.

### Delay Penalty

For real-time video applications the time from image acquisition to image presentation shall be as low as possible. The _Delay penalty_ expresses how low the video transmission system was able to keep this latency.

## Automated Quality Estimation

In order to evaluate simulation results two methods can be employed:

* estimating a MOS (mean opinion score) per result and compare them with each other, or
* rank the individual results without calculating an intermediate value.

Since a MOS score is not relevant for evaluating simulation results the ranking method will be used.

This method also allows to retrieve subjective data more easily by presenting pairs of differently degraded video sequences to a person for comparison. Instead of assigning a MOS to each video sequence, the person only needs to decide which video sequence has a better subjective quality.

The resulting partially ordered data can be used to train a machine learning ranking algorithm which will then automatically produce rankings for simulation results of the approaches for handling a specific network impairment.

Since the training data is already pairwise comparisons, a machine learning approach for pairwise ranking is used.

### Ranking from Pairwise Comparisons

