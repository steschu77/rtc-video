# Video Encoder Picture Management

## Video Sequences

A video stream transmitted from a source to one or more receivers is divided into video sequences. A video sequence is therefore a part of the video stream, consisting of a starting key-frame and subsequent pictures.

References between video sequences are not allowed.

A video sequence should consist of similar content. That means with each scene change, a new video sequence should be created.

## Key-Frames

Key-frames are only allowed at the beginning of video sequences.

Due to their large size and key-frames should be avoided and alternative mechanisms should be used to solve use-cases where key-frames are usually used.

### Alternative Key-Frame Mechanisms

To avoid the need for key-frames, the following mechanisms can be used instead:

* Sync-Frames,
* Alternative Referencing.

These mechanisms are described in more detail below.

### Penalty-Free Key-Frames

A key-frame might be useful in case the current key-frame has been outdated and doesn't help much for predicting a sync-frame. Therefore the encoder might decide to start a new video sequence and create a key-frame. To minimize the adverse dynamic effects of the key-frame, like increased jitter, this could be done whenever the video is very static, i.e. the content doesn't change or only changes a little.

## Sync-Frames

If every component in the video transmission chain keeps either the coded or decoded representation of the last key-frame, then sync-frames can be used to only reference that key-frame. The idea is that the compression of such sync-frames is much better that that of a new key-frame, which is usually the case as the content within a video sequence shouldn't change too much.

## Alternative Referencing

If the key-frame is needed because the reference chain has partially been broken, then the video encoder can still use an alternative reference picture that is available to all decoders of the video stream.

This implies that either pictures that have been received are acknowledged (ACK) or that the particular lost picture is reported (NACK).