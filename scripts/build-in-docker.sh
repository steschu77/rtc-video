set -e
source /etc/profile
mkdir -p /build ; cd /build
cmake -GNinja -DASAN=1 /work
time -p ninja simulator
time -p ./simulator
ccache -s
