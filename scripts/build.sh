set -e

export PATH="/usr/lib/ccache/bin:$PATH"
export BUILD_ROOT="$PWD"
export CCACHE_DIR="$BUILD_ROOT/.ccache"

# A good value is an almost full cache after building with a clean cache.
ccache --set-config=max_size=50.0M
ccache -s

mkdir -p .build ; cd .build
cmake -GNinja ..
time -p ninja
cd ..
ccache -s
