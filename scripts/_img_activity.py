# -*- coding: utf-8 -*-
import csv


def _clamp(x, x0, x1):
    return min(max(x, x0), x1)

def _read_pic_props(fname):

    pics = []
    with open(fname) as csvfile:
        reader = csv.reader(csvfile)

        for row in reader:
            if len(row) == 3:
                picid = int(row[0])
                activity = float(row[2])

                pics.append({
                    'picid': picid,
                    'activity': activity
                })

    return pics


def _merge_pic_props(seq0, seq1):

    pics = []

    for p0, p1 in zip(seq0, seq1):

        picid = p0['picid']
        a0 = p0['activity']
        a1 = p1['activity']

        pics.append({
            'picid': picid,
            'activity': (a0 + a1) / 2
        })

    return pics


def _write_pic_props(pics, fname):

    with open(fname, 'w') as csvfile:

        for pic in pics:
            print(pic)
            picid = pic['picid']
            activity = pic['activity']
            normalized = 1 - _clamp((activity - 2), 0, 36) / 36

            csvfile.write("{},{:.2f}\n".format(picid, normalized))


for seqid in range(0, 3):

    seq_name = "../.msdev/seq{}".format(seqid)
    seq160x90 = _read_pic_props(seq_name + '.160x90.csv')
    seq320x180 = _read_pic_props(seq_name + '.320x180.csv')

    seq = _merge_pic_props(seq160x90, seq320x180)
    _write_pic_props(seq, seq_name + '.csv')
