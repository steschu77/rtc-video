# -*- coding: utf-8 -*-
import sys
import os
import subprocess
import multiprocessing
import threading
import queue

if os.name == 'nt':
    workdir = os.getcwd() + "/../.msdev"
    vp9enc = os.getcwd() + "/../.msdev/Release/vp9enc.exe"
else:
    workdir = os.getcwd() + "/../.docker"
    vp9enc = os.getcwd() + "/../.docker/vp9enc"

cCPUs = multiprocessing.cpu_count()
taskQueue = queue.Queue()
profiles = []

errors = 0

def defineTestCases(args):

    sls = [ 1, 2 ]
    tls = [ 1, 2 ]
    
    seqids = [ 0, 1, 2 ] if '--allseqs' in args else [ 0 ]
    
    if '--allrates' in args:
        bitrates = [ 20, 40, 80, 160, 320, 640, 1280 ] # for exhaustive quality testing
    else:
        bitrates = [ 40, 320 ]

    for sl in sls:
        for tl in tls:
            for seqid in seqids:
                for bitrate in bitrates:
                    profile = "sl{}_tl{}_seq{}_{}k.profraw".format(sl, tl, seqid, bitrate)
                    profiles.append(profile)
                    taskQueue.put({
                        'cmd': [
                            vp9enc,
                            "{}".format(sl),
                            "{}".format(tl),
                            "{}".format(seqid),
                            "{}".format(bitrate)],
                        'profile': profile})

def runCommand(cmd, e):
    try:
        print("> {}".format(' '.join(cmd)))
        p = subprocess.Popen(cmd, cwd=workdir, stdout=subprocess.PIPE, stderr=subprocess.PIPE, env=e)
        stdout, stderr = p.communicate()
        print(stdout.decode('ascii'))
    except:
        print("error executing {}".format(' '.join(cmd)))
        errors += 1

def runTestCase():
    while True:
        task = taskQueue.get()
        e = os.environ.copy()
        e["LLVM_PROFILE_FILE"] = task['profile'] # in case code coverage is active
        runCommand(task['cmd'], e)
        taskQueue.task_done()

def runTestCases():
    print("CPUs: {}".format(cCPUs))

    for i in range(cCPUs):
        t = threading.Thread(target=runTestCase)
        t.daemon = True
        t.start()

    taskQueue.join()

def createCoverageReport():
    if os.name != 'nt':
        runCommand(['llvm-profdata-14', 'merge'] + profiles + ['-o', 'vp9enc.profdata'], os.environ)
        runCommand(['llvm-cov-14', 'show', '-format=html', '-output-dir=cov', '-j={}'.format(cCPUs), './vp9enc', '-instr-profile=vp9enc.profdata'], os.environ)

defineTestCases(sys.argv)
runTestCases()
createCoverageReport()

print("Finished with {} errors".format(errors))
sys.exit(errors)
