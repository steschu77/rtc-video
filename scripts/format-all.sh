find include -type f -exec clang-format -i -style=file {} +
find src -type f -exec clang-format -i -style=file {} +
find deps/cunit -type f -exec clang-format -i -style=file {} +
